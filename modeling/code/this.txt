def find_kernel(data, numgrid = 1000, bw = 0.002):
   Xtrain = data[:,0:2]
   ytrain = data[2]
   # Set up the data grid for the contour plot
   xgrid = np.linspace(-74.05, -73.85, numgrid)
   ygrid = np.linspace(40.6, 40.75, numgrid)
   X, Y = np.meshgrid(xgrid, ygrid)    xy = np.vstack([Y.ravel(), X.ravel()]).T    # Plot map of with distributions of each species
   #fig = plt.figure()
   # construct a kernel density estimate of the distribution
   kde = KernelDensity(bandwidth=bw,
                   kernel='gaussian')
   kde.fit(Xtrain, y = ytrain) # evaluate only on the land: -9999 indicates ocean
   Z = np.exp(kde.score_samples(xy))
   Z = Z.reshape(X.shape)    # plot contours of the density
   #levels = np.linspace(0, Z.max(), 25)
   #plt.contourf(X, Y, Z, levels=levels, cmap=plt.cm.Reds)
   #plt.title('BK CRIME')#zip(*crime_location)
#plt.scatter(*zip(*crime_location))    #plt.show()
   return Z 