from bs4 import BeautifulSoup
import requests
import csv
import pandas as pd

def theurl(name):
 answer={"consolidated":"http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H155", "population": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H157", "medical": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H154", "risk": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H140", "employment": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H131", "jobs": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H158", "person_round_plan": "http://meps.ahrq.gov/mepsweb/data_stats/download_data_files_codebook.jsp?PUFId=H153"}
 return answer[name]

def make_soup(url):
 response=requests.get(url)
 soup=BeautifulSoup(response.content)
 return soup

def get_header(name,target):
 return get_data(make_soup(theurl(name)),target)

def get_data(soup,target):
 result=[]
 for row in soup.find_all("font"):
  result.append(row.contents)
 final=[(term[0].replace(u'\xa0',u'')).replace(',','') for term in result]
# get rid of junk
 final=prune_list(final,"::")
 final=prune_list(final,"MEPS HC-155")
 final=prune_list(final,"MEPS HC-157")
 final=prune_list(final,"MEPS H154 CODEBOOK")
 final=prune_list(final,"MEPS H140 CODEBOOK")
 final=prune_list(final,"MEPS H131 CODEBOOK")
 final=prune_list(final,"MEPS HC-150")
 final=prune_list(final,"MEPS HC-158")
 final=prune_list(final,"MEPS H153 CODEBOOK")
 final=prune_list(final,"2012 FULL YEAR CONSOLIDATED DATA CODEBOOK")
 final=prune_list(final,"2013 FULL YEAR POPULATION CHARACTERISTICS CODEBOOK")
 final=prune_list(final,"2012 MEPS MEDICAL CONDITIONS FILE")
 final=prune_list(final,"2002-2009 RISK ADJUSTMENT SCORES FILE")
 final=prune_list(final,"EMPLOYMENT VARIABLE IMPUTATION FILE")
 final=prune_list(final,"2012 JOBS FILE CODEBOOK")
 final=prune_list(final,"2013 JOBS FILE CODEBOOK")
 final=prune_list(final,"2012 PERSON ROUND PLAN FILE")
 final=prune_list(final,"DATE: August 21 2014")
 final=prune_list(final,"DATE: August 4 2014")
 final=prune_list(final,"DATE:     March 6 2015")
 final=prune_list(final,"DATE: December 15 2014")
 final=prune_list(final,"DATE:    April 10 2013")
 final=prune_list(final,"DATE:   August 12 2014")
 final=prune_list(final,"DATE: February 13 2015")
 done=final
 with open(target,"w") as f:
  f.write("start,end,variable\n")
  for skip in range(0,len(final)-1,3):
   f.write("%s,"%final[skip])
   f.write("%s,"%final[skip+1])
   f.write("%s\n"%((final[skip+2]).lstrip()).rstrip())
 print("Done")
 temp=pd.read_csv(target)
 temp=temp.sort("start").copy()
 temp.index=list(range(len(temp)))
 return temp


def prune_list(thelist, theterm):
 while True:
  try:
   thelist.pop(thelist.index(theterm))
  except ValueError:
   break
 return thelist

def pull_ascii_data(source):
 with open(source,'r') as f:
  result=f.read()
 return result.split('\n')


def make_frame(data, header):
 dictionary={header["variable"].ix[place]:[row[header["start"].ix[place]:header["end"].ix[place]] for row in data] for place in header.index}
 return pd.DataFrame(dictionary)

def all_together_now(datafile,headerfile):
 a=make_soup(theurl())
 header=get_data(a,headerfile)
 header.sort("start")
 data=pull_ascii_data(datafile)
 data=prune_list(data,'')
 return (data,header) 


def write_table(data,header,target,short="No"):
 if short!="No":
  data=data[:10]
 data=prune_list(data,'')
 header=header.sort("start").copy()
 header.index=list(range(len(header)))
 with open(target,"w") as f:
  for element in header["variable"]:
   f.write("%s, "%element)
  f.write("\n")
 for row in data:
  u=[row[(header["start"].iloc[place]-1):(header["end"].iloc[place])] for place in header.index]
  with open(target,"a") as f:
   writer=csv.writer(f)
   writer.writerow(u)
  print("Done with row %s"%data.index(row))


def swap_columns(theframe, here, there):
 temp=theframe[here].copy()
 theframe[here]=theframe[there].copy()
 theframe[there]=temp.copy()
 return theframe

def clean_columns(theframe):
 temp=theframe.columns.map(lambda x: (((str(x).lstrip()).rstrip()))).copy()
 theframe.columns=temp.copy()
 return theframe



