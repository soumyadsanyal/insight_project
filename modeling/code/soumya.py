# IPython log file

pwd
#[Out]# '/home/soumya/Downloads'
cd inpatient/
ls
%logstart?
# Fri, 11 Sep 2015 10:37:53
import pandas as pd
# Fri, 11 Sep 2015 10:37:55
ls
# Fri, 11 Sep 2015 10:38:06
data=pd.read_csv("./Medicare_Provider_Charge_Inpatient_DRG100_FY2013.csv")
# Fri, 11 Sep 2015 10:38:19
%logstate
# Fri, 11 Sep 2015 10:38:42
%logstop
pwd
#[Out]# '/home/soumya/Downloads'
cd inpatient/
ls
%logstart?
%logstart -ort soumya.py over
import pandas as pd
ls
data=pd.read_csv("./Medicare_Provider_Charge_Inpatient_DRG100_FY2013.csv")
%logstate
%logstop
%logstart?
# Fri, 11 Sep 2015 10:39:42
data
#[Out]#                                   DRG Definition  Provider Id  \
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10001   
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10005   
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10006   
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10011   
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10016   
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10023   
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10024   
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10029   
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10033   
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10039   
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10040   
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10046   
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10055   
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10056   
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10078   
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10085   
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10090   
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10092   
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10100   
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10103   
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10104   
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10113   
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10139   
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        20017   
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30002   
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30006   
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30007   
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30010   
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30011   
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30012   
#[Out]# ...                                          ...          ...   
#[Out]# 157717                                       NaN          NaN   
#[Out]# 157718                                       NaN          NaN   
#[Out]# 157719                                       NaN          NaN   
#[Out]# 157720                                       NaN          NaN   
#[Out]# 157721                                       NaN          NaN   
#[Out]# 157722                                       NaN          NaN   
#[Out]# 157723                                       NaN          NaN   
#[Out]# 157724                                       NaN          NaN   
#[Out]# 157725                                       NaN          NaN   
#[Out]# 157726                                       NaN          NaN   
#[Out]# 157727                                       NaN          NaN   
#[Out]# 157728                                       NaN          NaN   
#[Out]# 157729                                       NaN          NaN   
#[Out]# 157730                                       NaN          NaN   
#[Out]# 157731                                       NaN          NaN   
#[Out]# 157732                                       NaN          NaN   
#[Out]# 157733                                       NaN          NaN   
#[Out]# 157734                                       NaN          NaN   
#[Out]# 157735                                       NaN          NaN   
#[Out]# 157736                                       NaN          NaN   
#[Out]# 157737                                       NaN          NaN   
#[Out]# 157738                                       NaN          NaN   
#[Out]# 157739                                       NaN          NaN   
#[Out]# 157740                                       NaN          NaN   
#[Out]# 157741                                       NaN          NaN   
#[Out]# 157742                                       NaN          NaN   
#[Out]# 157743                                       NaN          NaN   
#[Out]# 157744                                       NaN          NaN   
#[Out]# 157745                                       NaN          NaN   
#[Out]# 157746                                       NaN          NaN   
#[Out]# 
#[Out]#                                  Provider Name  \
#[Out]# 0             SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 2               ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 3                            ST VINCENT'S EAST   
#[Out]# 4                SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 5                 BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 6                JACKSON HOSPITAL & CLINIC INC   
#[Out]# 7          EAST ALABAMA MEDICAL CENTER AND SNF   
#[Out]# 8               UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 9                          HUNTSVILLE HOSPITAL   
#[Out]# 10             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 11           RIVERVIEW REGIONAL MEDICAL CENTER   
#[Out]# 12                            FLOWERS HOSPITAL   
#[Out]# 13                     ST VINCENT'S BIRMINGHAM   
#[Out]# 14       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 15      DECATUR MORGAN HOSPITAL-DECATUR CAMPUS   
#[Out]# 16                         PROVIDENCE HOSPITAL   
#[Out]# 17               D C H REGIONAL MEDICAL CENTER   
#[Out]# 18                             THOMAS HOSPITAL   
#[Out]# 19            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 20                      TRINITY MEDICAL CENTER   
#[Out]# 21                            MOBILE INFIRMARY   
#[Out]# 22                    BROOKWOOD MEDICAL CENTER   
#[Out]# 23                    ALASKA REGIONAL HOSPITAL   
#[Out]# 24        BANNER GOOD SAMARITAN MEDICAL CENTER   
#[Out]# 25                       TUCSON MEDICAL CENTER   
#[Out]# 26                 VERDE VALLEY MEDICAL CENTER   
#[Out]# 27               CARONDELET ST  MARYS HOSPITAL   
#[Out]# 28             CARONDELET ST JOSEPH'S HOSPITAL   
#[Out]# 29             YAVAPAI REGIONAL MEDICAL CENTER   
#[Out]# ...                                        ...   
#[Out]# 157717                                     NaN   
#[Out]# 157718                                     NaN   
#[Out]# 157719                                     NaN   
#[Out]# 157720                                     NaN   
#[Out]# 157721                                     NaN   
#[Out]# 157722                                     NaN   
#[Out]# 157723                                     NaN   
#[Out]# 157724                                     NaN   
#[Out]# 157725                                     NaN   
#[Out]# 157726                                     NaN   
#[Out]# 157727                                     NaN   
#[Out]# 157728                                     NaN   
#[Out]# 157729                                     NaN   
#[Out]# 157730                                     NaN   
#[Out]# 157731                                     NaN   
#[Out]# 157732                                     NaN   
#[Out]# 157733                                     NaN   
#[Out]# 157734                                     NaN   
#[Out]# 157735                                     NaN   
#[Out]# 157736                                     NaN   
#[Out]# 157737                                     NaN   
#[Out]# 157738                                     NaN   
#[Out]# 157739                                     NaN   
#[Out]# 157740                                     NaN   
#[Out]# 157741                                     NaN   
#[Out]# 157742                                     NaN   
#[Out]# 157743                                     NaN   
#[Out]# 157744                                     NaN   
#[Out]# 157745                                     NaN   
#[Out]# 157746                                     NaN   
#[Out]# 
#[Out]#                     Provider Street Address Provider City Provider State  \
#[Out]# 0                    1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 1                2505 U S HIGHWAY 431 NORTH          BOAZ             AL   
#[Out]# 2                        205 MARENGO STREET      FLORENCE             AL   
#[Out]# 3                50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 4                   1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 5                 2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 6                          1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 7                    2000 PEPPERELL PARKWAY       OPELIKA             AL   
#[Out]# 8                     619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 9                             101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 10                     1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 11                   600 SOUTH THIRD STREET       GADSDEN             AL   
#[Out]# 12                    4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 13                   810 ST VINCENT'S DRIVE    BIRMINGHAM             AL   
#[Out]# 14                     400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 15                       1201 7TH STREET SE       DECATUR             AL   
#[Out]# 16                   6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 17            809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 18                        750 MORPHY AVENUE      FAIRHOPE             AL   
#[Out]# 19           701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 20                         800 MONTCLAIR RD    BIRMINGHAM             AL   
#[Out]# 21                5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 22      2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 23                         2801 DEBARR ROAD     ANCHORAGE             AK   
#[Out]# 24                  1111 EAST MCDOWELL ROAD       PHOENIX             AZ   
#[Out]# 25                     5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 26                     269 SOUTH CANDY LANE    COTTONWOOD             AZ   
#[Out]# 27                 1601 WEST ST MARY'S ROAD        TUCSON             AZ   
#[Out]# 28                    350 NORTH WILMOT ROAD        TUCSON             AZ   
#[Out]# 29                   1003 WILLOW CREEK ROAD      PRESCOTT             AZ   
#[Out]# ...                                     ...           ...            ...   
#[Out]# 157717                                  NaN           NaN            NaN   
#[Out]# 157718                                  NaN           NaN            NaN   
#[Out]# 157719                                  NaN           NaN            NaN   
#[Out]# 157720                                  NaN           NaN            NaN   
#[Out]# 157721                                  NaN           NaN            NaN   
#[Out]# 157722                                  NaN           NaN            NaN   
#[Out]# 157723                                  NaN           NaN            NaN   
#[Out]# 157724                                  NaN           NaN            NaN   
#[Out]# 157725                                  NaN           NaN            NaN   
#[Out]# 157726                                  NaN           NaN            NaN   
#[Out]# 157727                                  NaN           NaN            NaN   
#[Out]# 157728                                  NaN           NaN            NaN   
#[Out]# 157729                                  NaN           NaN            NaN   
#[Out]# 157730                                  NaN           NaN            NaN   
#[Out]# 157731                                  NaN           NaN            NaN   
#[Out]# 157732                                  NaN           NaN            NaN   
#[Out]# 157733                                  NaN           NaN            NaN   
#[Out]# 157734                                  NaN           NaN            NaN   
#[Out]# 157735                                  NaN           NaN            NaN   
#[Out]# 157736                                  NaN           NaN            NaN   
#[Out]# 157737                                  NaN           NaN            NaN   
#[Out]# 157738                                  NaN           NaN            NaN   
#[Out]# 157739                                  NaN           NaN            NaN   
#[Out]# 157740                                  NaN           NaN            NaN   
#[Out]# 157741                                  NaN           NaN            NaN   
#[Out]# 157742                                  NaN           NaN            NaN   
#[Out]# 157743                                  NaN           NaN            NaN   
#[Out]# 157744                                  NaN           NaN            NaN   
#[Out]# 157745                                  NaN           NaN            NaN   
#[Out]# 157746                                  NaN           NaN            NaN   
#[Out]# 
#[Out]#         Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                   36301                                AL - Dothan   
#[Out]# 1                   35957                            AL - Birmingham   
#[Out]# 2                   35631                            AL - Birmingham   
#[Out]# 3                   35235                            AL - Birmingham   
#[Out]# 4                   35007                            AL - Birmingham   
#[Out]# 5                   36116                            AL - Montgomery   
#[Out]# 6                   36106                            AL - Montgomery   
#[Out]# 7                   36801                            AL - Birmingham   
#[Out]# 8                   35233                            AL - Birmingham   
#[Out]# 9                   35801                            AL - Huntsville   
#[Out]# 10                  35903                            AL - Birmingham   
#[Out]# 11                  35901                            AL - Birmingham   
#[Out]# 12                  36305                                AL - Dothan   
#[Out]# 13                  35205                            AL - Birmingham   
#[Out]# 14                  36207                            AL - Birmingham   
#[Out]# 15                  35601                            AL - Huntsville   
#[Out]# 16                  36608                                AL - Mobile   
#[Out]# 17                  35401                            AL - Tuscaloosa   
#[Out]# 18                  36532                                AL - Mobile   
#[Out]# 19                  35211                            AL - Birmingham   
#[Out]# 20                  35213                            AL - Birmingham   
#[Out]# 21                  36652                                AL - Mobile   
#[Out]# 22                  35209                            AL - Birmingham   
#[Out]# 23                  99508                             AK - Anchorage   
#[Out]# 24                  85006                               AZ - Phoenix   
#[Out]# 25                  85712                                AZ - Tucson   
#[Out]# 26                  86326                               AZ - Phoenix   
#[Out]# 27                  85745                                AZ - Tucson   
#[Out]# 28                  85711                                AZ - Tucson   
#[Out]# 29                  86301                               AZ - Phoenix   
#[Out]# ...                   ...                                        ...   
#[Out]# 157717                NaN                                        NaN   
#[Out]# 157718                NaN                                        NaN   
#[Out]# 157719                NaN                                        NaN   
#[Out]# 157720                NaN                                        NaN   
#[Out]# 157721                NaN                                        NaN   
#[Out]# 157722                NaN                                        NaN   
#[Out]# 157723                NaN                                        NaN   
#[Out]# 157724                NaN                                        NaN   
#[Out]# 157725                NaN                                        NaN   
#[Out]# 157726                NaN                                        NaN   
#[Out]# 157727                NaN                                        NaN   
#[Out]# 157728                NaN                                        NaN   
#[Out]# 157729                NaN                                        NaN   
#[Out]# 157730                NaN                                        NaN   
#[Out]# 157731                NaN                                        NaN   
#[Out]# 157732                NaN                                        NaN   
#[Out]# 157733                NaN                                        NaN   
#[Out]# 157734                NaN                                        NaN   
#[Out]# 157735                NaN                                        NaN   
#[Out]# 157736                NaN                                        NaN   
#[Out]# 157737                NaN                                        NaN   
#[Out]# 157738                NaN                                        NaN   
#[Out]# 157739                NaN                                        NaN   
#[Out]# 157740                NaN                                        NaN   
#[Out]# 157741                NaN                                        NaN   
#[Out]# 157742                NaN                                        NaN   
#[Out]# 157743                NaN                                        NaN   
#[Out]# 157744                NaN                                        NaN   
#[Out]# 157745                NaN                                        NaN   
#[Out]# 157746                NaN                                        NaN   
#[Out]# 
#[Out]#         Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 0                     98              37988.97959             5872.193878   
#[Out]# 1                     34              15554.88235             6053.294118   
#[Out]# 2                     30              40380.10000             5319.833333   
#[Out]# 3                     17              22026.23529             5767.882353   
#[Out]# 4                     17              45663.05882             5785.117647   
#[Out]# 5                     44              16579.81818             6485.681818   
#[Out]# 6                     15              23243.73333             5553.866667   
#[Out]# 7                     46              11807.78261             6200.326087   
#[Out]# 8                     34              45288.23529             8977.323529   
#[Out]# 9                    111              33108.30631             5948.000000   
#[Out]# 10                    22              83400.09091             5942.272727   
#[Out]# 11                    14              81517.35714             5540.142857   
#[Out]# 12                    56              39789.98214             5498.857143   
#[Out]# 13                    42              21614.02381             5272.904762   
#[Out]# 14                    24              33509.83333             5423.458333   
#[Out]# 15                    26              15005.11538             5268.192308   
#[Out]# 16                    16              12574.06250             5975.812500   
#[Out]# 17                    40              22007.52500             6398.675000   
#[Out]# 18                    26              14847.00000             5103.153846   
#[Out]# 19                    22              48621.68182             6176.545455   
#[Out]# 20                    28              75521.96429             6282.285714   
#[Out]# 21                    53              16921.16981             5316.622642   
#[Out]# 22                    28             110405.75000             5877.321429   
#[Out]# 23                    38              41744.65789             8059.763158   
#[Out]# 24                    17              36434.82353             8223.235294   
#[Out]# 25                    24              27468.37500             7465.833333   
#[Out]# 26                    16              30707.87500             9331.437500   
#[Out]# 27                    14              35782.71429             6994.214286   
#[Out]# 28                    27              30702.70370             6577.148148   
#[Out]# 29                    29              29135.65517             6603.793103   
#[Out]# ...                  ...                      ...                     ...   
#[Out]# 157717               NaN                      NaN                     NaN   
#[Out]# 157718               NaN                      NaN                     NaN   
#[Out]# 157719               NaN                      NaN                     NaN   
#[Out]# 157720               NaN                      NaN                     NaN   
#[Out]# 157721               NaN                      NaN                     NaN   
#[Out]# 157722               NaN                      NaN                     NaN   
#[Out]# 157723               NaN                      NaN                     NaN   
#[Out]# 157724               NaN                      NaN                     NaN   
#[Out]# 157725               NaN                      NaN                     NaN   
#[Out]# 157726               NaN                      NaN                     NaN   
#[Out]# 157727               NaN                      NaN                     NaN   
#[Out]# 157728               NaN                      NaN                     NaN   
#[Out]# 157729               NaN                      NaN                     NaN   
#[Out]# 157730               NaN                      NaN                     NaN   
#[Out]# 157731               NaN                      NaN                     NaN   
#[Out]# 157732               NaN                      NaN                     NaN   
#[Out]# 157733               NaN                      NaN                     NaN   
#[Out]# 157734               NaN                      NaN                     NaN   
#[Out]# 157735               NaN                      NaN                     NaN   
#[Out]# 157736               NaN                      NaN                     NaN   
#[Out]# 157737               NaN                      NaN                     NaN   
#[Out]# 157738               NaN                      NaN                     NaN   
#[Out]# 157739               NaN                      NaN                     NaN   
#[Out]# 157740               NaN                      NaN                     NaN   
#[Out]# 157741               NaN                      NaN                     NaN   
#[Out]# 157742               NaN                      NaN                     NaN   
#[Out]# 157743               NaN                      NaN                     NaN   
#[Out]# 157744               NaN                      NaN                     NaN   
#[Out]# 157745               NaN                      NaN                     NaN   
#[Out]# 157746               NaN                      NaN                     NaN   
#[Out]# 
#[Out]#         Average Medicare Payments  
#[Out]# 0                     4838.316327  
#[Out]# 1                     5255.647059  
#[Out]# 2                     4150.866667  
#[Out]# 3                     4268.352941  
#[Out]# 4                     4886.294118  
#[Out]# 5                     5375.159091  
#[Out]# 6                     4376.600000  
#[Out]# 7                     4766.891304  
#[Out]# 8                     6238.852941  
#[Out]# 9                     4780.081081  
#[Out]# 10                    4872.272727  
#[Out]# 11                    4120.214286  
#[Out]# 12                    4434.946429  
#[Out]# 13                    4257.309524  
#[Out]# 14                    4469.916667  
#[Out]# 15                    4138.230769  
#[Out]# 16                    3675.062500  
#[Out]# 17                    5366.225000  
#[Out]# 18                    3815.961538  
#[Out]# 19                    5271.818182  
#[Out]# 20                    5187.357143  
#[Out]# 21                    4085.660377  
#[Out]# 22                    4380.892857  
#[Out]# 23                    6943.973684  
#[Out]# 24                    7115.470588  
#[Out]# 25                    6212.708333  
#[Out]# 26                    8105.125000  
#[Out]# 27                    6154.500000  
#[Out]# 28                    5528.851852  
#[Out]# 29                    5504.344828  
#[Out]# ...                           ...  
#[Out]# 157717                        NaN  
#[Out]# 157718                        NaN  
#[Out]# 157719                        NaN  
#[Out]# 157720                        NaN  
#[Out]# 157721                        NaN  
#[Out]# 157722                        NaN  
#[Out]# 157723                        NaN  
#[Out]# 157724                        NaN  
#[Out]# 157725                        NaN  
#[Out]# 157726                        NaN  
#[Out]# 157727                        NaN  
#[Out]# 157728                        NaN  
#[Out]# 157729                        NaN  
#[Out]# 157730                        NaN  
#[Out]# 157731                        NaN  
#[Out]# 157732                        NaN  
#[Out]# 157733                        NaN  
#[Out]# 157734                        NaN  
#[Out]# 157735                        NaN  
#[Out]# 157736                        NaN  
#[Out]# 157737                        NaN  
#[Out]# 157738                        NaN  
#[Out]# 157739                        NaN  
#[Out]# 157740                        NaN  
#[Out]# 157741                        NaN  
#[Out]# 157742                        NaN  
#[Out]# 157743                        NaN  
#[Out]# 157744                        NaN  
#[Out]# 157745                        NaN  
#[Out]# 157746                        NaN  
#[Out]# 
#[Out]# [157747 rows x 12 columns]
# Fri, 11 Sep 2015 10:39:44
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:39:56
data["DRG Definition"].describe()
#[Out]# count                                     153600
#[Out]# unique                                       100
#[Out]# top       194 - SIMPLE PNEUMONIA & PLEURISY W CC
#[Out]# freq                                        2953
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:41:00
data["DRG Definition"].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:41:41
data["HYP"]=data["DRG Definition"].map(lambda x: x[:3]=="305")
# Fri, 11 Sep 2015 10:42:05
data["DRG Definition"]
#[Out]# 0         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 5         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 6         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 7         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 8         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 9         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 10        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 11        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 12        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 13        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 14        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 15        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 16        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 17        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 18        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 19        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 20        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 21        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 22        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 23        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 24        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 25        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 26        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 27        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 28        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 29        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]#                             ...                   
#[Out]# 157717                                         NaN
#[Out]# 157718                                         NaN
#[Out]# 157719                                         NaN
#[Out]# 157720                                         NaN
#[Out]# 157721                                         NaN
#[Out]# 157722                                         NaN
#[Out]# 157723                                         NaN
#[Out]# 157724                                         NaN
#[Out]# 157725                                         NaN
#[Out]# 157726                                         NaN
#[Out]# 157727                                         NaN
#[Out]# 157728                                         NaN
#[Out]# 157729                                         NaN
#[Out]# 157730                                         NaN
#[Out]# 157731                                         NaN
#[Out]# 157732                                         NaN
#[Out]# 157733                                         NaN
#[Out]# 157734                                         NaN
#[Out]# 157735                                         NaN
#[Out]# 157736                                         NaN
#[Out]# 157737                                         NaN
#[Out]# 157738                                         NaN
#[Out]# 157739                                         NaN
#[Out]# 157740                                         NaN
#[Out]# 157741                                         NaN
#[Out]# 157742                                         NaN
#[Out]# 157743                                         NaN
#[Out]# 157744                                         NaN
#[Out]# 157745                                         NaN
#[Out]# 157746                                         NaN
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:42:08
data["DRG Definition"].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:42:22
data["DRG Definition"]["3].head()
# Fri, 11 Sep 2015 10:42:26
data["DRG Definition"][:3].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:42:39
data["DRG Definition"].to_str()
# Fri, 11 Sep 2015 10:42:49
data["DRG Definition"].astype(str)
#[Out]# 0         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 5         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 6         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 7         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 8         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 9         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 10        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 11        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 12        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 13        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 14        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 15        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 16        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 17        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 18        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 19        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 20        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 21        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 22        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 23        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 24        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 25        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 26        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 27        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 28        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 29        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]#                             ...                   
#[Out]# 157717                                         nan
#[Out]# 157718                                         nan
#[Out]# 157719                                         nan
#[Out]# 157720                                         nan
#[Out]# 157721                                         nan
#[Out]# 157722                                         nan
#[Out]# 157723                                         nan
#[Out]# 157724                                         nan
#[Out]# 157725                                         nan
#[Out]# 157726                                         nan
#[Out]# 157727                                         nan
#[Out]# 157728                                         nan
#[Out]# 157729                                         nan
#[Out]# 157730                                         nan
#[Out]# 157731                                         nan
#[Out]# 157732                                         nan
#[Out]# 157733                                         nan
#[Out]# 157734                                         nan
#[Out]# 157735                                         nan
#[Out]# 157736                                         nan
#[Out]# 157737                                         nan
#[Out]# 157738                                         nan
#[Out]# 157739                                         nan
#[Out]# 157740                                         nan
#[Out]# 157741                                         nan
#[Out]# 157742                                         nan
#[Out]# 157743                                         nan
#[Out]# 157744                                         nan
#[Out]# 157745                                         nan
#[Out]# 157746                                         nan
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:42:53
data["DRG Definition"].astype(str).head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
# Fri, 11 Sep 2015 10:43:10
data["PROC"]=data["DRG Definition"].astype(str).copy()
# Fri, 11 Sep 2015 10:43:13
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'PROC'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:43:18
data["PROC"]
#[Out]# 0         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 5         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 6         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 7         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 8         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 9         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 10        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 11        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 12        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 13        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 14        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 15        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 16        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 17        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 18        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 19        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 20        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 21        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 22        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 23        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 24        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 25        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 26        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 27        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 28        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 29        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]#                             ...                   
#[Out]# 157717                                         nan
#[Out]# 157718                                         nan
#[Out]# 157719                                         nan
#[Out]# 157720                                         nan
#[Out]# 157721                                         nan
#[Out]# 157722                                         nan
#[Out]# 157723                                         nan
#[Out]# 157724                                         nan
#[Out]# 157725                                         nan
#[Out]# 157726                                         nan
#[Out]# 157727                                         nan
#[Out]# 157728                                         nan
#[Out]# 157729                                         nan
#[Out]# 157730                                         nan
#[Out]# 157731                                         nan
#[Out]# 157732                                         nan
#[Out]# 157733                                         nan
#[Out]# 157734                                         nan
#[Out]# 157735                                         nan
#[Out]# 157736                                         nan
#[Out]# 157737                                         nan
#[Out]# 157738                                         nan
#[Out]# 157739                                         nan
#[Out]# 157740                                         nan
#[Out]# 157741                                         nan
#[Out]# 157742                                         nan
#[Out]# 157743                                         nan
#[Out]# 157744                                         nan
#[Out]# 157745                                         nan
#[Out]# 157746                                         nan
#[Out]# Name: PROC, dtype: object
# Fri, 11 Sep 2015 10:43:25
data["PROC"][:100]
#[Out]# 0     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 5     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 6     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 7     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 8     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 9     039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 10    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 11    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 12    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 13    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 14    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 15    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 16    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 17    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 18    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 19    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 20    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 21    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 22    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 23    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 24    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 25    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 26    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 27    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 28    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 29    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]#                         ...                   
#[Out]# 70    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 71    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 72    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 73    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 74    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 75    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 76    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 77    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 78    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 79    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 80    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 81    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 82    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 83    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 84    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 85    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 86    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 87    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 88    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 89    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 90    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 91    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 92    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 93    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 94    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 95    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 96    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 97    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 98    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 99    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: PROC, dtype: object
# Fri, 11 Sep 2015 10:44:04
data["PROC CODE"]=data["PROC"].map(lambda x: x[:3])
# Fri, 11 Sep 2015 10:44:11
data["PROC CODE"]
#[Out]# 0         039
#[Out]# 1         039
#[Out]# 2         039
#[Out]# 3         039
#[Out]# 4         039
#[Out]# 5         039
#[Out]# 6         039
#[Out]# 7         039
#[Out]# 8         039
#[Out]# 9         039
#[Out]# 10        039
#[Out]# 11        039
#[Out]# 12        039
#[Out]# 13        039
#[Out]# 14        039
#[Out]# 15        039
#[Out]# 16        039
#[Out]# 17        039
#[Out]# 18        039
#[Out]# 19        039
#[Out]# 20        039
#[Out]# 21        039
#[Out]# 22        039
#[Out]# 23        039
#[Out]# 24        039
#[Out]# 25        039
#[Out]# 26        039
#[Out]# 27        039
#[Out]# 28        039
#[Out]# 29        039
#[Out]#          ... 
#[Out]# 157717    nan
#[Out]# 157718    nan
#[Out]# 157719    nan
#[Out]# 157720    nan
#[Out]# 157721    nan
#[Out]# 157722    nan
#[Out]# 157723    nan
#[Out]# 157724    nan
#[Out]# 157725    nan
#[Out]# 157726    nan
#[Out]# 157727    nan
#[Out]# 157728    nan
#[Out]# 157729    nan
#[Out]# 157730    nan
#[Out]# 157731    nan
#[Out]# 157732    nan
#[Out]# 157733    nan
#[Out]# 157734    nan
#[Out]# 157735    nan
#[Out]# 157736    nan
#[Out]# 157737    nan
#[Out]# 157738    nan
#[Out]# 157739    nan
#[Out]# 157740    nan
#[Out]# 157741    nan
#[Out]# 157742    nan
#[Out]# 157743    nan
#[Out]# 157744    nan
#[Out]# 157745    nan
#[Out]# 157746    nan
#[Out]# Name: PROC CODE, dtype: object
# Fri, 11 Sep 2015 10:44:14
data["PROC CODE"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: PROC CODE, dtype: object
# Fri, 11 Sep 2015 10:44:33
data[data["PROC CODE"]=="305"]
#[Out]#                    DRG Definition  Provider Id  \
#[Out]# 65060  305 - HYPERTENSION W/O MCC        10001   
#[Out]# 65061  305 - HYPERTENSION W/O MCC        10006   
#[Out]# 65062  305 - HYPERTENSION W/O MCC        10011   
#[Out]# 65063  305 - HYPERTENSION W/O MCC        10016   
#[Out]# 65064  305 - HYPERTENSION W/O MCC        10019   
#[Out]# 65065  305 - HYPERTENSION W/O MCC        10023   
#[Out]# 65066  305 - HYPERTENSION W/O MCC        10024   
#[Out]# 65067  305 - HYPERTENSION W/O MCC        10033   
#[Out]# 65068  305 - HYPERTENSION W/O MCC        10039   
#[Out]# 65069  305 - HYPERTENSION W/O MCC        10040   
#[Out]# 65070  305 - HYPERTENSION W/O MCC        10055   
#[Out]# 65071  305 - HYPERTENSION W/O MCC        10078   
#[Out]# 65072  305 - HYPERTENSION W/O MCC        10090   
#[Out]# 65073  305 - HYPERTENSION W/O MCC        10092   
#[Out]# 65074  305 - HYPERTENSION W/O MCC        10103   
#[Out]# 65075  305 - HYPERTENSION W/O MCC        10112   
#[Out]# 65076  305 - HYPERTENSION W/O MCC        10113   
#[Out]# 65077  305 - HYPERTENSION W/O MCC        10139   
#[Out]# 65078  305 - HYPERTENSION W/O MCC        10144   
#[Out]# 65079  305 - HYPERTENSION W/O MCC        30006   
#[Out]# 65080  305 - HYPERTENSION W/O MCC        30014   
#[Out]# 65081  305 - HYPERTENSION W/O MCC        30023   
#[Out]# 65082  305 - HYPERTENSION W/O MCC        30036   
#[Out]# 65083  305 - HYPERTENSION W/O MCC        30043   
#[Out]# 65084  305 - HYPERTENSION W/O MCC        30055   
#[Out]# 65085  305 - HYPERTENSION W/O MCC        30061   
#[Out]# 65086  305 - HYPERTENSION W/O MCC        30065   
#[Out]# 65087  305 - HYPERTENSION W/O MCC        30088   
#[Out]# 65088  305 - HYPERTENSION W/O MCC        30089   
#[Out]# 65089  305 - HYPERTENSION W/O MCC        30092   
#[Out]# ...                           ...          ...   
#[Out]# 65999  305 - HYPERTENSION W/O MCC       500108   
#[Out]# 66000  305 - HYPERTENSION W/O MCC       500119   
#[Out]# 66001  305 - HYPERTENSION W/O MCC       500124   
#[Out]# 66002  305 - HYPERTENSION W/O MCC       500129   
#[Out]# 66003  305 - HYPERTENSION W/O MCC       500150   
#[Out]# 66004  305 - HYPERTENSION W/O MCC       500151   
#[Out]# 66005  305 - HYPERTENSION W/O MCC       510001   
#[Out]# 66006  305 - HYPERTENSION W/O MCC       510006   
#[Out]# 66007  305 - HYPERTENSION W/O MCC       510007   
#[Out]# 66008  305 - HYPERTENSION W/O MCC       510022   
#[Out]# 66009  305 - HYPERTENSION W/O MCC       510030   
#[Out]# 66010  305 - HYPERTENSION W/O MCC       510058   
#[Out]# 66011  305 - HYPERTENSION W/O MCC       510062   
#[Out]# 66012  305 - HYPERTENSION W/O MCC       510070   
#[Out]# 66013  305 - HYPERTENSION W/O MCC       520008   
#[Out]# 66014  305 - HYPERTENSION W/O MCC       520021   
#[Out]# 66015  305 - HYPERTENSION W/O MCC       520051   
#[Out]# 66016  305 - HYPERTENSION W/O MCC       520083   
#[Out]# 66017  305 - HYPERTENSION W/O MCC       520136   
#[Out]# 66018  305 - HYPERTENSION W/O MCC       520138   
#[Out]# 66019  305 - HYPERTENSION W/O MCC       520139   
#[Out]# 66020  305 - HYPERTENSION W/O MCC       520177   
#[Out]# 66021  305 - HYPERTENSION W/O MCC       530012   
#[Out]# 66022  305 - HYPERTENSION W/O MCC       530014   
#[Out]# 66023  305 - HYPERTENSION W/O MCC       670023   
#[Out]# 66024  305 - HYPERTENSION W/O MCC       670024   
#[Out]# 66025  305 - HYPERTENSION W/O MCC       670041   
#[Out]# 66026  305 - HYPERTENSION W/O MCC       670055   
#[Out]# 66027  305 - HYPERTENSION W/O MCC       670056   
#[Out]# 66028  305 - HYPERTENSION W/O MCC       670060   
#[Out]# 
#[Out]#                                     Provider Name  \
#[Out]# 65060            SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 65061              ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 65062                           ST VINCENT'S EAST   
#[Out]# 65063               SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 65064              HELEN KELLER MEMORIAL HOSPITAL   
#[Out]# 65065                BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 65066               JACKSON HOSPITAL & CLINIC INC   
#[Out]# 65067              UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 65068                         HUNTSVILLE HOSPITAL   
#[Out]# 65069             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 65070                            FLOWERS HOSPITAL   
#[Out]# 65071       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 65072                         PROVIDENCE HOSPITAL   
#[Out]# 65073               D C H REGIONAL MEDICAL CENTER   
#[Out]# 65074            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 65075              BRYAN W WHITFIELD MEM HOSP INC   
#[Out]# 65076                            MOBILE INFIRMARY   
#[Out]# 65077                    BROOKWOOD MEDICAL CENTER   
#[Out]# 65078                   SPRINGHILL MEDICAL CENTER   
#[Out]# 65079                       TUCSON MEDICAL CENTER   
#[Out]# 65080      JOHN C LINCOLN NORTH MOUNTAIN HOSPITAL   
#[Out]# 65081                    FLAGSTAFF MEDICAL CENTER   
#[Out]# 65082            CHANDLER REGIONAL MEDICAL CENTER   
#[Out]# 65083         SIERRA VISTA REGIONAL HEALTH CENTER   
#[Out]# 65084             KINGMAN REGIONAL MEDICAL CENTER   
#[Out]# 65085               BANNER BOSWELL MEDICAL CENTER   
#[Out]# 65086                BANNER DESERT MEDICAL CENTER   
#[Out]# 65087               BANNER BAYWOOD MEDICAL CENTER   
#[Out]# 65088           BANNER THUNDERBIRD MEDICAL CENTER   
#[Out]# 65089         JOHN C LINCOLN DEER VALLEY HOSPITAL   
#[Out]# ...                                           ...   
#[Out]# 65999                    ST JOSEPH MEDICAL CENTER   
#[Out]# 66000                             VALLEY HOSPITAL   
#[Out]# 66001           EVERGREEN HOSPITAL MEDICAL CENTER   
#[Out]# 66002           TACOMA GENERAL ALLENMORE HOSPITAL   
#[Out]# 66003          LEGACY SALMON CREEK MEDICAL CENTER   
#[Out]# 66004                         ST ANTHONY HOSPITAL   
#[Out]# 66005          WEST VIRGINIA UNIVERSITY HOSPITALS   
#[Out]# 66006                      UNITED HOSPITAL CENTER   
#[Out]# 66007                    ST MARY'S MEDICAL CENTER   
#[Out]# 66008              CHARLESTON AREA MEDICAL CENTER   
#[Out]# 66009                     DAVIS MEMORIAL HOSPITAL   
#[Out]# 66010                 CAMDEN CLARK MEDICAL CENTER   
#[Out]# 66011                        BECKLEY ARH HOSPITAL   
#[Out]# 66012                    RALEIGH GENERAL HOSPITAL   
#[Out]# 66013                  WAUKESHA MEMORIAL HOSPITAL   
#[Out]# 66014                      UNITED HOSPITAL SYSTEM   
#[Out]# 66015        COLUMBIA ST MARYS HOSPITAL MILWAUKEE   
#[Out]# 66016                           ST MARYS HOSPITAL   
#[Out]# 66017                WHEATON FRANCISCAN ST JOSEPH   
#[Out]# 66018              AURORA ST LUKES MEDICAL CENTER   
#[Out]# 66019            AURORA WEST ALLIS MEDICAL CENTER   
#[Out]# 66020                FROEDTERT MEM LUTHERAN HSPTL   
#[Out]# 66021                      WYOMING MEDICAL CENTER   
#[Out]# 66022            CHEYENNE REGIONAL MEDICAL CENTER   
#[Out]# 66023          METHODIST MANSFIELD MEDICAL CENTER   
#[Out]# 66024                NORTH CYPRESS MEDICAL CENTER   
#[Out]# 66025             SETON MEDICAL CENTER WILLIAMSON   
#[Out]# 66026                METHODIST STONE OAK HOSPITAL   
#[Out]# 66027                   SETON MEDICAL CENTER HAYS   
#[Out]# 66028  TEXAS REGIONAL MEDICAL CENTER AT SUNNYVALE   
#[Out]# 
#[Out]#                    Provider Street Address Provider City Provider State  \
#[Out]# 65060               1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 65061                   205 MARENGO STREET      FLORENCE             AL   
#[Out]# 65062           50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 65063              1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 65064         1300 SOUTH MONTGOMERY AVENUE     SHEFFIELD             AL   
#[Out]# 65065            2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 65066                     1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 65067                619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 65068                        101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 65069                 1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 65070                4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 65071                 400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 65072               6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 65073        809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 65074       701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 65075                  105 HIGHWAY 80 EAST     DEMOPOLIS             AL   
#[Out]# 65076            5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 65077  2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 65078                  3719 DAUPHIN STREET        MOBILE             AL   
#[Out]# 65079                 5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 65080               250 EAST DUNLAP AVENUE       PHOENIX             AZ   
#[Out]# 65081             1200 NORTH BEAVER STREET     FLAGSTAFF             AZ   
#[Out]# 65082                  1955 WEST FRYE ROAD      CHANDLER             AZ   
#[Out]# 65083                   300 EL CAMINO REAL  SIERRA VISTA             AZ   
#[Out]# 65084              3269 STOCKTON HILL ROAD       KINGMAN             AZ   
#[Out]# 65085     10401 WEST THUNDERBIRD BOULEVARD      SUN CITY             AZ   
#[Out]# 65086              1400 SOUTH  DOBSON ROAD          MESA             AZ   
#[Out]# 65087             6644 EAST BAYWOOD AVENUE          MESA             AZ   
#[Out]# 65088           5555 WEST THUNDERBIRD ROAD      GLENDALE             AZ   
#[Out]# 65089              19829 NORTH 27TH AVENUE       PHOENIX             AZ   
#[Out]# ...                                    ...           ...            ...   
#[Out]# 65999                  1717 SOUTH J STREET        TACOMA             WA   
#[Out]# 66000            12606 EAST MISSION AVENUE       SPOKANE             WA   
#[Out]# 66001                12040 NE 128TH STREET      KIRKLAND             WA   
#[Out]# 66002                     315 S MLK JR WAY        TACOMA             WA   
#[Out]# 66003                 2211 NE 139TH STREET     VANCOUVER             WA   
#[Out]# 66004        11567 CANTERWOOD BOULEVARD NW    GIG HARBOR             WA   
#[Out]# 66005                 MEDICAL CENTER DRIVE    MORGANTOWN             WV   
#[Out]# 66006               327 MEDICAL PARK DRIVE    BRIDGEPORT             WV   
#[Out]# 66007                      2900 1ST AVENUE    HUNTINGTON             WV   
#[Out]# 66008                    501 MORRIS STREET    CHARLESTON             WV   
#[Out]# 66009                          PO BOX 1484        ELKINS             WV   
#[Out]# 66010                     800 GARFIELD AVE   PARKERSBURG             WV   
#[Out]# 66011                   306 STANAFORD ROAD       BECKLEY             WV   
#[Out]# 66012                     1710 HARPER ROAD       BECKLEY             WV   
#[Out]# 66013                     725 AMERICAN AVE      WAUKESHA             WI   
#[Out]# 66014                      6308 EIGHTH AVE       KENOSHA             WI   
#[Out]# 66015                       2323 N LAKE DR     MILWAUKEE             WI   
#[Out]# 66016                    700 SOUTH PARK ST       MADISON             WI   
#[Out]# 66017                   5000 W CHAMBERS ST     MILWAUKEE             WI   
#[Out]# 66018                  2900 W OKLAHOMA AVE     MILWAUKEE             WI   
#[Out]# 66019                   8901 W LINCOLN AVE    WEST ALLIS             WI   
#[Out]# 66020                 9200 W WISCONSIN AVE     MILWAUKEE             WI   
#[Out]# 66021                     1233 EAST 2ND ST        CASPER             WY   
#[Out]# 66022                 214 EAST 23RD STREET      CHEYENNE             WY   
#[Out]# 66023                  2700 E BROAD STREET     MANSFIELD             TX   
#[Out]# 66024              21214 NORTHWEST FREEWAY       CYPRESS             TX   
#[Out]# 66025                    201 SETON PARKWAY    ROUND ROCK             TX   
#[Out]# 66026                 1139 E SONTERRA BLVD   SAN ANTONIO             TX   
#[Out]# 66027                       6001 KYLE PKWY          KYLE             TX   
#[Out]# 66028               231 SOUTH COLLINS ROAD     SUNNYVALE             TX   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 65060              36301                                AL - Dothan   
#[Out]# 65061              35631                            AL - Birmingham   
#[Out]# 65062              35235                            AL - Birmingham   
#[Out]# 65063              35007                            AL - Birmingham   
#[Out]# 65064              35660                            AL - Birmingham   
#[Out]# 65065              36116                            AL - Montgomery   
#[Out]# 65066              36106                            AL - Montgomery   
#[Out]# 65067              35233                            AL - Birmingham   
#[Out]# 65068              35801                            AL - Huntsville   
#[Out]# 65069              35903                            AL - Birmingham   
#[Out]# 65070              36305                                AL - Dothan   
#[Out]# 65071              36207                            AL - Birmingham   
#[Out]# 65072              36608                                AL - Mobile   
#[Out]# 65073              35401                            AL - Tuscaloosa   
#[Out]# 65074              35211                            AL - Birmingham   
#[Out]# 65075              36732                            AL - Birmingham   
#[Out]# 65076              36652                                AL - Mobile   
#[Out]# 65077              35209                            AL - Birmingham   
#[Out]# 65078              36608                                AL - Mobile   
#[Out]# 65079              85712                                AZ - Tucson   
#[Out]# 65080              85020                               AZ - Phoenix   
#[Out]# 65081              86001                               AZ - Phoenix   
#[Out]# 65082              85224                                  AZ - Mesa   
#[Out]# 65083              85635                                AZ - Tucson   
#[Out]# 65084              86409                               AZ - Phoenix   
#[Out]# 65085              85351                              AZ - Sun City   
#[Out]# 65086              85202                                  AZ - Mesa   
#[Out]# 65087              85206                                  AZ - Mesa   
#[Out]# 65088              85306                               AZ - Phoenix   
#[Out]# 65089              85027                               AZ - Phoenix   
#[Out]# ...                  ...                                        ...   
#[Out]# 65999              98405                                WA - Tacoma   
#[Out]# 66000              99216                               WA - Spokane   
#[Out]# 66001              98034                               WA - Seattle   
#[Out]# 66002              98415                                WA - Tacoma   
#[Out]# 66003              98686                              OR - Portland   
#[Out]# 66004              98332                                WA - Tacoma   
#[Out]# 66005              26506                            WV - Morgantown   
#[Out]# 66006              26330                            WV - Morgantown   
#[Out]# 66007              25701                            WV - Huntington   
#[Out]# 66008              25301                            WV - Charleston   
#[Out]# 66009              26241                            WV - Morgantown   
#[Out]# 66010              26101                            WV - Charleston   
#[Out]# 66011              25801                            WV - Charleston   
#[Out]# 66012              25801                            WV - Charleston   
#[Out]# 66013              53188                             WI - Milwaukee   
#[Out]# 66014              53143                             WI - Milwaukee   
#[Out]# 66015              53211                             WI - Milwaukee   
#[Out]# 66016              53715                               WI - Madison   
#[Out]# 66017              53210                             WI - Milwaukee   
#[Out]# 66018              53215                             WI - Milwaukee   
#[Out]# 66019              53227                             WI - Milwaukee   
#[Out]# 66020              53226                             WI - Milwaukee   
#[Out]# 66021              82601                                WY - Casper   
#[Out]# 66022              82001                          CO - Fort Collins   
#[Out]# 66023              76063                            TX - Fort Worth   
#[Out]# 66024              77429                               TX - Houston   
#[Out]# 66025              78664                                TX - Austin   
#[Out]# 66026              78258                           TX - San Antonio   
#[Out]# 66027              78640                                TX - Austin   
#[Out]# 66028              75182                                TX - Dallas   
#[Out]# 
#[Out]#        Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 65060                21             17187.666670             3532.476190   
#[Out]# 65061                13             16818.384620             3557.230769   
#[Out]# 65062                11             21731.545450             3530.272727   
#[Out]# 65063                19             17048.263160             3494.736842   
#[Out]# 65064                11             17953.818180             3861.909091   
#[Out]# 65065                11             10451.090910             3962.545455   
#[Out]# 65066                11             13748.363640             3421.636364   
#[Out]# 65067                38             15960.000000             4642.578947   
#[Out]# 65068                25             23610.600000             3634.280000   
#[Out]# 65069                19             41036.263160             3571.789474   
#[Out]# 65070                11             21801.454550             3285.272727   
#[Out]# 65071                17              9504.411765             3329.176471   
#[Out]# 65072                11             11451.636360             4659.181818   
#[Out]# 65073                82             15648.707320             3957.939024   
#[Out]# 65074                11             19641.181820             3761.636364   
#[Out]# 65075                17              5397.117647             3276.411765   
#[Out]# 65076                37             14267.216220             3397.459459   
#[Out]# 65077                32             50541.531250             3484.906250   
#[Out]# 65078                11             12068.181820             3002.545455   
#[Out]# 65079                11             16036.363640             4523.000000   
#[Out]# 65080                31             21633.612900             4088.870968   
#[Out]# 65081                21             20513.142860             6458.619048   
#[Out]# 65082                16             30256.437500             4104.562500   
#[Out]# 65083                15             15548.066670             5431.000000   
#[Out]# 65084                16             22512.375000             4672.062500   
#[Out]# 65085                33             24754.060610             3708.424242   
#[Out]# 65086                15             17849.800000             5014.133333   
#[Out]# 65087                27             18233.444440             3777.222222   
#[Out]# 65088                15             21456.466670             4409.866667   
#[Out]# 65089                12             21351.500000             4057.083333   
#[Out]# ...                 ...                      ...                     ...   
#[Out]# 65999                18             28179.222220             4818.055556   
#[Out]# 66000                18             19485.055560             3946.277778   
#[Out]# 66001                13             23903.615380             4174.076923   
#[Out]# 66002                12             25560.666670             9616.000000   
#[Out]# 66003                13             20224.538460             5035.769231   
#[Out]# 66004                11             19109.000000             4008.909091   
#[Out]# 66005                44             12360.181820             5590.363636   
#[Out]# 66006                11             10338.545450             3934.272727   
#[Out]# 66007                31             13822.032260             4333.387097   
#[Out]# 66008                22             18664.045450             4311.727273   
#[Out]# 66009                13              7266.692308             3556.461538   
#[Out]# 66010                14              9910.285714             3532.428571   
#[Out]# 66011                13              9601.846154             3624.615385   
#[Out]# 66012                54              8335.407407             3736.648148   
#[Out]# 66013                14             14119.285710             4432.714286   
#[Out]# 66014                19             15567.526320             3918.789474   
#[Out]# 66015                12             14715.083330             4714.583333   
#[Out]# 66016                12             17924.500000             4575.833333   
#[Out]# 66017                16             13459.625000             5628.562500   
#[Out]# 66018                37             16401.891890             4636.567568   
#[Out]# 66019                13             16192.076920             3822.230769   
#[Out]# 66020                13             13989.230770             5578.615385   
#[Out]# 66021                19             13095.578950             4156.052632   
#[Out]# 66022                21             12831.571430             5433.142857   
#[Out]# 66023                13             22199.076920             3484.538462   
#[Out]# 66024                14             47589.000000             3542.142857   
#[Out]# 66025                11             26933.727270             3820.454545   
#[Out]# 66026                16             23698.375000             3691.312500   
#[Out]# 66027                12             38615.666670             3646.500000   
#[Out]# 66028                12             25933.833330             3564.833333   
#[Out]# 
#[Out]#        Average Medicare Payments                        PROC PROC CODE  
#[Out]# 65060                2579.333333  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65061                2006.615385  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65062                2079.181818  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65063                2315.157895  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65064                2108.727273  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65065                2901.454545  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65066                2668.181818  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65067                3462.605263  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65068                2501.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65069                2640.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65070                2216.545455  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65071                2450.352941  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65072                2060.636364  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65073                2838.012195  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65074                2687.818182  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65075                2236.647059  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65076                2374.216216  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65077                2529.906250  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65078                1933.818182  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65079                3214.454545  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65080                3135.064516  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65081                5447.761905  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65082                2999.812500  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65083                3610.333333  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65084                3464.500000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65085                2687.909091  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65086                3161.533333  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65087                2772.777778  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65088                3207.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 65089                3174.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# ...                          ...                         ...       ...  
#[Out]# 65999                3297.833333  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66000                2766.944444  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66001                3146.615385  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66002                3331.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66003                3360.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66004                3160.545455  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66005                3840.386364  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66006                2572.545455  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66007                2535.612903  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66008                2869.636364  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66009                2543.000000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66010                2177.714286  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66011                2767.153846  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66012                2442.555556  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66013                2271.285714  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66014                2932.052632  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66015                3591.416667  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66016                3146.833333  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66017                3308.187500  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66018                2996.864865  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66019                3015.461538  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66020                4063.692308  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66021                2807.315789  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66022                4372.571429  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66023                2563.615385  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66024                2529.285714  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66025                2953.909091  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66026                1931.250000  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66027                2559.166667  305 - HYPERTENSION W/O MCC       305  
#[Out]# 66028                2686.166667  305 - HYPERTENSION W/O MCC       305  
#[Out]# 
#[Out]# [969 rows x 14 columns]
# Fri, 11 Sep 2015 10:44:49
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'PROC', 'PROC CODE'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:45:00
data["proc"]=data["PROC"]
# Fri, 11 Sep 2015 10:45:06
data["proc code"]=data["PROC CODE"]
# Fri, 11 Sep 2015 10:45:09
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'PROC', 'PROC CODE', 'proc', 'proc code'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:45:16
data.drop("PROC")
# Fri, 11 Sep 2015 10:45:19
data.drop("PROC",axis=1)
#[Out]#                                   DRG Definition  Provider Id  \
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10001   
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10005   
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10006   
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10011   
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10016   
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10023   
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10024   
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10029   
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10033   
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10039   
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10040   
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10046   
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10055   
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10056   
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10078   
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10085   
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10090   
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10092   
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10100   
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10103   
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10104   
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10113   
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10139   
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        20017   
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30002   
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30006   
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30007   
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30010   
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30011   
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30012   
#[Out]# ...                                          ...          ...   
#[Out]# 157717                                       NaN          NaN   
#[Out]# 157718                                       NaN          NaN   
#[Out]# 157719                                       NaN          NaN   
#[Out]# 157720                                       NaN          NaN   
#[Out]# 157721                                       NaN          NaN   
#[Out]# 157722                                       NaN          NaN   
#[Out]# 157723                                       NaN          NaN   
#[Out]# 157724                                       NaN          NaN   
#[Out]# 157725                                       NaN          NaN   
#[Out]# 157726                                       NaN          NaN   
#[Out]# 157727                                       NaN          NaN   
#[Out]# 157728                                       NaN          NaN   
#[Out]# 157729                                       NaN          NaN   
#[Out]# 157730                                       NaN          NaN   
#[Out]# 157731                                       NaN          NaN   
#[Out]# 157732                                       NaN          NaN   
#[Out]# 157733                                       NaN          NaN   
#[Out]# 157734                                       NaN          NaN   
#[Out]# 157735                                       NaN          NaN   
#[Out]# 157736                                       NaN          NaN   
#[Out]# 157737                                       NaN          NaN   
#[Out]# 157738                                       NaN          NaN   
#[Out]# 157739                                       NaN          NaN   
#[Out]# 157740                                       NaN          NaN   
#[Out]# 157741                                       NaN          NaN   
#[Out]# 157742                                       NaN          NaN   
#[Out]# 157743                                       NaN          NaN   
#[Out]# 157744                                       NaN          NaN   
#[Out]# 157745                                       NaN          NaN   
#[Out]# 157746                                       NaN          NaN   
#[Out]# 
#[Out]#                                  Provider Name  \
#[Out]# 0             SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 2               ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 3                            ST VINCENT'S EAST   
#[Out]# 4                SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 5                 BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 6                JACKSON HOSPITAL & CLINIC INC   
#[Out]# 7          EAST ALABAMA MEDICAL CENTER AND SNF   
#[Out]# 8               UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 9                          HUNTSVILLE HOSPITAL   
#[Out]# 10             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 11           RIVERVIEW REGIONAL MEDICAL CENTER   
#[Out]# 12                            FLOWERS HOSPITAL   
#[Out]# 13                     ST VINCENT'S BIRMINGHAM   
#[Out]# 14       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 15      DECATUR MORGAN HOSPITAL-DECATUR CAMPUS   
#[Out]# 16                         PROVIDENCE HOSPITAL   
#[Out]# 17               D C H REGIONAL MEDICAL CENTER   
#[Out]# 18                             THOMAS HOSPITAL   
#[Out]# 19            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 20                      TRINITY MEDICAL CENTER   
#[Out]# 21                            MOBILE INFIRMARY   
#[Out]# 22                    BROOKWOOD MEDICAL CENTER   
#[Out]# 23                    ALASKA REGIONAL HOSPITAL   
#[Out]# 24        BANNER GOOD SAMARITAN MEDICAL CENTER   
#[Out]# 25                       TUCSON MEDICAL CENTER   
#[Out]# 26                 VERDE VALLEY MEDICAL CENTER   
#[Out]# 27               CARONDELET ST  MARYS HOSPITAL   
#[Out]# 28             CARONDELET ST JOSEPH'S HOSPITAL   
#[Out]# 29             YAVAPAI REGIONAL MEDICAL CENTER   
#[Out]# ...                                        ...   
#[Out]# 157717                                     NaN   
#[Out]# 157718                                     NaN   
#[Out]# 157719                                     NaN   
#[Out]# 157720                                     NaN   
#[Out]# 157721                                     NaN   
#[Out]# 157722                                     NaN   
#[Out]# 157723                                     NaN   
#[Out]# 157724                                     NaN   
#[Out]# 157725                                     NaN   
#[Out]# 157726                                     NaN   
#[Out]# 157727                                     NaN   
#[Out]# 157728                                     NaN   
#[Out]# 157729                                     NaN   
#[Out]# 157730                                     NaN   
#[Out]# 157731                                     NaN   
#[Out]# 157732                                     NaN   
#[Out]# 157733                                     NaN   
#[Out]# 157734                                     NaN   
#[Out]# 157735                                     NaN   
#[Out]# 157736                                     NaN   
#[Out]# 157737                                     NaN   
#[Out]# 157738                                     NaN   
#[Out]# 157739                                     NaN   
#[Out]# 157740                                     NaN   
#[Out]# 157741                                     NaN   
#[Out]# 157742                                     NaN   
#[Out]# 157743                                     NaN   
#[Out]# 157744                                     NaN   
#[Out]# 157745                                     NaN   
#[Out]# 157746                                     NaN   
#[Out]# 
#[Out]#                     Provider Street Address Provider City Provider State  \
#[Out]# 0                    1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 1                2505 U S HIGHWAY 431 NORTH          BOAZ             AL   
#[Out]# 2                        205 MARENGO STREET      FLORENCE             AL   
#[Out]# 3                50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 4                   1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 5                 2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 6                          1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 7                    2000 PEPPERELL PARKWAY       OPELIKA             AL   
#[Out]# 8                     619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 9                             101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 10                     1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 11                   600 SOUTH THIRD STREET       GADSDEN             AL   
#[Out]# 12                    4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 13                   810 ST VINCENT'S DRIVE    BIRMINGHAM             AL   
#[Out]# 14                     400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 15                       1201 7TH STREET SE       DECATUR             AL   
#[Out]# 16                   6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 17            809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 18                        750 MORPHY AVENUE      FAIRHOPE             AL   
#[Out]# 19           701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 20                         800 MONTCLAIR RD    BIRMINGHAM             AL   
#[Out]# 21                5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 22      2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 23                         2801 DEBARR ROAD     ANCHORAGE             AK   
#[Out]# 24                  1111 EAST MCDOWELL ROAD       PHOENIX             AZ   
#[Out]# 25                     5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 26                     269 SOUTH CANDY LANE    COTTONWOOD             AZ   
#[Out]# 27                 1601 WEST ST MARY'S ROAD        TUCSON             AZ   
#[Out]# 28                    350 NORTH WILMOT ROAD        TUCSON             AZ   
#[Out]# 29                   1003 WILLOW CREEK ROAD      PRESCOTT             AZ   
#[Out]# ...                                     ...           ...            ...   
#[Out]# 157717                                  NaN           NaN            NaN   
#[Out]# 157718                                  NaN           NaN            NaN   
#[Out]# 157719                                  NaN           NaN            NaN   
#[Out]# 157720                                  NaN           NaN            NaN   
#[Out]# 157721                                  NaN           NaN            NaN   
#[Out]# 157722                                  NaN           NaN            NaN   
#[Out]# 157723                                  NaN           NaN            NaN   
#[Out]# 157724                                  NaN           NaN            NaN   
#[Out]# 157725                                  NaN           NaN            NaN   
#[Out]# 157726                                  NaN           NaN            NaN   
#[Out]# 157727                                  NaN           NaN            NaN   
#[Out]# 157728                                  NaN           NaN            NaN   
#[Out]# 157729                                  NaN           NaN            NaN   
#[Out]# 157730                                  NaN           NaN            NaN   
#[Out]# 157731                                  NaN           NaN            NaN   
#[Out]# 157732                                  NaN           NaN            NaN   
#[Out]# 157733                                  NaN           NaN            NaN   
#[Out]# 157734                                  NaN           NaN            NaN   
#[Out]# 157735                                  NaN           NaN            NaN   
#[Out]# 157736                                  NaN           NaN            NaN   
#[Out]# 157737                                  NaN           NaN            NaN   
#[Out]# 157738                                  NaN           NaN            NaN   
#[Out]# 157739                                  NaN           NaN            NaN   
#[Out]# 157740                                  NaN           NaN            NaN   
#[Out]# 157741                                  NaN           NaN            NaN   
#[Out]# 157742                                  NaN           NaN            NaN   
#[Out]# 157743                                  NaN           NaN            NaN   
#[Out]# 157744                                  NaN           NaN            NaN   
#[Out]# 157745                                  NaN           NaN            NaN   
#[Out]# 157746                                  NaN           NaN            NaN   
#[Out]# 
#[Out]#         Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                   36301                                AL - Dothan   
#[Out]# 1                   35957                            AL - Birmingham   
#[Out]# 2                   35631                            AL - Birmingham   
#[Out]# 3                   35235                            AL - Birmingham   
#[Out]# 4                   35007                            AL - Birmingham   
#[Out]# 5                   36116                            AL - Montgomery   
#[Out]# 6                   36106                            AL - Montgomery   
#[Out]# 7                   36801                            AL - Birmingham   
#[Out]# 8                   35233                            AL - Birmingham   
#[Out]# 9                   35801                            AL - Huntsville   
#[Out]# 10                  35903                            AL - Birmingham   
#[Out]# 11                  35901                            AL - Birmingham   
#[Out]# 12                  36305                                AL - Dothan   
#[Out]# 13                  35205                            AL - Birmingham   
#[Out]# 14                  36207                            AL - Birmingham   
#[Out]# 15                  35601                            AL - Huntsville   
#[Out]# 16                  36608                                AL - Mobile   
#[Out]# 17                  35401                            AL - Tuscaloosa   
#[Out]# 18                  36532                                AL - Mobile   
#[Out]# 19                  35211                            AL - Birmingham   
#[Out]# 20                  35213                            AL - Birmingham   
#[Out]# 21                  36652                                AL - Mobile   
#[Out]# 22                  35209                            AL - Birmingham   
#[Out]# 23                  99508                             AK - Anchorage   
#[Out]# 24                  85006                               AZ - Phoenix   
#[Out]# 25                  85712                                AZ - Tucson   
#[Out]# 26                  86326                               AZ - Phoenix   
#[Out]# 27                  85745                                AZ - Tucson   
#[Out]# 28                  85711                                AZ - Tucson   
#[Out]# 29                  86301                               AZ - Phoenix   
#[Out]# ...                   ...                                        ...   
#[Out]# 157717                NaN                                        NaN   
#[Out]# 157718                NaN                                        NaN   
#[Out]# 157719                NaN                                        NaN   
#[Out]# 157720                NaN                                        NaN   
#[Out]# 157721                NaN                                        NaN   
#[Out]# 157722                NaN                                        NaN   
#[Out]# 157723                NaN                                        NaN   
#[Out]# 157724                NaN                                        NaN   
#[Out]# 157725                NaN                                        NaN   
#[Out]# 157726                NaN                                        NaN   
#[Out]# 157727                NaN                                        NaN   
#[Out]# 157728                NaN                                        NaN   
#[Out]# 157729                NaN                                        NaN   
#[Out]# 157730                NaN                                        NaN   
#[Out]# 157731                NaN                                        NaN   
#[Out]# 157732                NaN                                        NaN   
#[Out]# 157733                NaN                                        NaN   
#[Out]# 157734                NaN                                        NaN   
#[Out]# 157735                NaN                                        NaN   
#[Out]# 157736                NaN                                        NaN   
#[Out]# 157737                NaN                                        NaN   
#[Out]# 157738                NaN                                        NaN   
#[Out]# 157739                NaN                                        NaN   
#[Out]# 157740                NaN                                        NaN   
#[Out]# 157741                NaN                                        NaN   
#[Out]# 157742                NaN                                        NaN   
#[Out]# 157743                NaN                                        NaN   
#[Out]# 157744                NaN                                        NaN   
#[Out]# 157745                NaN                                        NaN   
#[Out]# 157746                NaN                                        NaN   
#[Out]# 
#[Out]#         Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 0                     98              37988.97959             5872.193878   
#[Out]# 1                     34              15554.88235             6053.294118   
#[Out]# 2                     30              40380.10000             5319.833333   
#[Out]# 3                     17              22026.23529             5767.882353   
#[Out]# 4                     17              45663.05882             5785.117647   
#[Out]# 5                     44              16579.81818             6485.681818   
#[Out]# 6                     15              23243.73333             5553.866667   
#[Out]# 7                     46              11807.78261             6200.326087   
#[Out]# 8                     34              45288.23529             8977.323529   
#[Out]# 9                    111              33108.30631             5948.000000   
#[Out]# 10                    22              83400.09091             5942.272727   
#[Out]# 11                    14              81517.35714             5540.142857   
#[Out]# 12                    56              39789.98214             5498.857143   
#[Out]# 13                    42              21614.02381             5272.904762   
#[Out]# 14                    24              33509.83333             5423.458333   
#[Out]# 15                    26              15005.11538             5268.192308   
#[Out]# 16                    16              12574.06250             5975.812500   
#[Out]# 17                    40              22007.52500             6398.675000   
#[Out]# 18                    26              14847.00000             5103.153846   
#[Out]# 19                    22              48621.68182             6176.545455   
#[Out]# 20                    28              75521.96429             6282.285714   
#[Out]# 21                    53              16921.16981             5316.622642   
#[Out]# 22                    28             110405.75000             5877.321429   
#[Out]# 23                    38              41744.65789             8059.763158   
#[Out]# 24                    17              36434.82353             8223.235294   
#[Out]# 25                    24              27468.37500             7465.833333   
#[Out]# 26                    16              30707.87500             9331.437500   
#[Out]# 27                    14              35782.71429             6994.214286   
#[Out]# 28                    27              30702.70370             6577.148148   
#[Out]# 29                    29              29135.65517             6603.793103   
#[Out]# ...                  ...                      ...                     ...   
#[Out]# 157717               NaN                      NaN                     NaN   
#[Out]# 157718               NaN                      NaN                     NaN   
#[Out]# 157719               NaN                      NaN                     NaN   
#[Out]# 157720               NaN                      NaN                     NaN   
#[Out]# 157721               NaN                      NaN                     NaN   
#[Out]# 157722               NaN                      NaN                     NaN   
#[Out]# 157723               NaN                      NaN                     NaN   
#[Out]# 157724               NaN                      NaN                     NaN   
#[Out]# 157725               NaN                      NaN                     NaN   
#[Out]# 157726               NaN                      NaN                     NaN   
#[Out]# 157727               NaN                      NaN                     NaN   
#[Out]# 157728               NaN                      NaN                     NaN   
#[Out]# 157729               NaN                      NaN                     NaN   
#[Out]# 157730               NaN                      NaN                     NaN   
#[Out]# 157731               NaN                      NaN                     NaN   
#[Out]# 157732               NaN                      NaN                     NaN   
#[Out]# 157733               NaN                      NaN                     NaN   
#[Out]# 157734               NaN                      NaN                     NaN   
#[Out]# 157735               NaN                      NaN                     NaN   
#[Out]# 157736               NaN                      NaN                     NaN   
#[Out]# 157737               NaN                      NaN                     NaN   
#[Out]# 157738               NaN                      NaN                     NaN   
#[Out]# 157739               NaN                      NaN                     NaN   
#[Out]# 157740               NaN                      NaN                     NaN   
#[Out]# 157741               NaN                      NaN                     NaN   
#[Out]# 157742               NaN                      NaN                     NaN   
#[Out]# 157743               NaN                      NaN                     NaN   
#[Out]# 157744               NaN                      NaN                     NaN   
#[Out]# 157745               NaN                      NaN                     NaN   
#[Out]# 157746               NaN                      NaN                     NaN   
#[Out]# 
#[Out]#         Average Medicare Payments PROC CODE  \
#[Out]# 0                     4838.316327       039   
#[Out]# 1                     5255.647059       039   
#[Out]# 2                     4150.866667       039   
#[Out]# 3                     4268.352941       039   
#[Out]# 4                     4886.294118       039   
#[Out]# 5                     5375.159091       039   
#[Out]# 6                     4376.600000       039   
#[Out]# 7                     4766.891304       039   
#[Out]# 8                     6238.852941       039   
#[Out]# 9                     4780.081081       039   
#[Out]# 10                    4872.272727       039   
#[Out]# 11                    4120.214286       039   
#[Out]# 12                    4434.946429       039   
#[Out]# 13                    4257.309524       039   
#[Out]# 14                    4469.916667       039   
#[Out]# 15                    4138.230769       039   
#[Out]# 16                    3675.062500       039   
#[Out]# 17                    5366.225000       039   
#[Out]# 18                    3815.961538       039   
#[Out]# 19                    5271.818182       039   
#[Out]# 20                    5187.357143       039   
#[Out]# 21                    4085.660377       039   
#[Out]# 22                    4380.892857       039   
#[Out]# 23                    6943.973684       039   
#[Out]# 24                    7115.470588       039   
#[Out]# 25                    6212.708333       039   
#[Out]# 26                    8105.125000       039   
#[Out]# 27                    6154.500000       039   
#[Out]# 28                    5528.851852       039   
#[Out]# 29                    5504.344828       039   
#[Out]# ...                           ...       ...   
#[Out]# 157717                        NaN       nan   
#[Out]# 157718                        NaN       nan   
#[Out]# 157719                        NaN       nan   
#[Out]# 157720                        NaN       nan   
#[Out]# 157721                        NaN       nan   
#[Out]# 157722                        NaN       nan   
#[Out]# 157723                        NaN       nan   
#[Out]# 157724                        NaN       nan   
#[Out]# 157725                        NaN       nan   
#[Out]# 157726                        NaN       nan   
#[Out]# 157727                        NaN       nan   
#[Out]# 157728                        NaN       nan   
#[Out]# 157729                        NaN       nan   
#[Out]# 157730                        NaN       nan   
#[Out]# 157731                        NaN       nan   
#[Out]# 157732                        NaN       nan   
#[Out]# 157733                        NaN       nan   
#[Out]# 157734                        NaN       nan   
#[Out]# 157735                        NaN       nan   
#[Out]# 157736                        NaN       nan   
#[Out]# 157737                        NaN       nan   
#[Out]# 157738                        NaN       nan   
#[Out]# 157739                        NaN       nan   
#[Out]# 157740                        NaN       nan   
#[Out]# 157741                        NaN       nan   
#[Out]# 157742                        NaN       nan   
#[Out]# 157743                        NaN       nan   
#[Out]# 157744                        NaN       nan   
#[Out]# 157745                        NaN       nan   
#[Out]# 157746                        NaN       nan   
#[Out]# 
#[Out]#                                             proc proc code  
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# ...                                          ...       ...  
#[Out]# 157717                                       nan       nan  
#[Out]# 157718                                       nan       nan  
#[Out]# 157719                                       nan       nan  
#[Out]# 157720                                       nan       nan  
#[Out]# 157721                                       nan       nan  
#[Out]# 157722                                       nan       nan  
#[Out]# 157723                                       nan       nan  
#[Out]# 157724                                       nan       nan  
#[Out]# 157725                                       nan       nan  
#[Out]# 157726                                       nan       nan  
#[Out]# 157727                                       nan       nan  
#[Out]# 157728                                       nan       nan  
#[Out]# 157729                                       nan       nan  
#[Out]# 157730                                       nan       nan  
#[Out]# 157731                                       nan       nan  
#[Out]# 157732                                       nan       nan  
#[Out]# 157733                                       nan       nan  
#[Out]# 157734                                       nan       nan  
#[Out]# 157735                                       nan       nan  
#[Out]# 157736                                       nan       nan  
#[Out]# 157737                                       nan       nan  
#[Out]# 157738                                       nan       nan  
#[Out]# 157739                                       nan       nan  
#[Out]# 157740                                       nan       nan  
#[Out]# 157741                                       nan       nan  
#[Out]# 157742                                       nan       nan  
#[Out]# 157743                                       nan       nan  
#[Out]# 157744                                       nan       nan  
#[Out]# 157745                                       nan       nan  
#[Out]# 157746                                       nan       nan  
#[Out]# 
#[Out]# [157747 rows x 15 columns]
# Fri, 11 Sep 2015 10:45:24
data.drop("PROC CODE",axis=1)
#[Out]#                                   DRG Definition  Provider Id  \
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10001   
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10005   
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10006   
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10011   
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10016   
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10023   
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10024   
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10029   
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10033   
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10039   
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10040   
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10046   
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10055   
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10056   
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10078   
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10085   
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10090   
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10092   
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10100   
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10103   
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10104   
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10113   
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10139   
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        20017   
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30002   
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30006   
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30007   
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30010   
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30011   
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30012   
#[Out]# ...                                          ...          ...   
#[Out]# 157717                                       NaN          NaN   
#[Out]# 157718                                       NaN          NaN   
#[Out]# 157719                                       NaN          NaN   
#[Out]# 157720                                       NaN          NaN   
#[Out]# 157721                                       NaN          NaN   
#[Out]# 157722                                       NaN          NaN   
#[Out]# 157723                                       NaN          NaN   
#[Out]# 157724                                       NaN          NaN   
#[Out]# 157725                                       NaN          NaN   
#[Out]# 157726                                       NaN          NaN   
#[Out]# 157727                                       NaN          NaN   
#[Out]# 157728                                       NaN          NaN   
#[Out]# 157729                                       NaN          NaN   
#[Out]# 157730                                       NaN          NaN   
#[Out]# 157731                                       NaN          NaN   
#[Out]# 157732                                       NaN          NaN   
#[Out]# 157733                                       NaN          NaN   
#[Out]# 157734                                       NaN          NaN   
#[Out]# 157735                                       NaN          NaN   
#[Out]# 157736                                       NaN          NaN   
#[Out]# 157737                                       NaN          NaN   
#[Out]# 157738                                       NaN          NaN   
#[Out]# 157739                                       NaN          NaN   
#[Out]# 157740                                       NaN          NaN   
#[Out]# 157741                                       NaN          NaN   
#[Out]# 157742                                       NaN          NaN   
#[Out]# 157743                                       NaN          NaN   
#[Out]# 157744                                       NaN          NaN   
#[Out]# 157745                                       NaN          NaN   
#[Out]# 157746                                       NaN          NaN   
#[Out]# 
#[Out]#                                  Provider Name  \
#[Out]# 0             SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 2               ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 3                            ST VINCENT'S EAST   
#[Out]# 4                SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 5                 BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 6                JACKSON HOSPITAL & CLINIC INC   
#[Out]# 7          EAST ALABAMA MEDICAL CENTER AND SNF   
#[Out]# 8               UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 9                          HUNTSVILLE HOSPITAL   
#[Out]# 10             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 11           RIVERVIEW REGIONAL MEDICAL CENTER   
#[Out]# 12                            FLOWERS HOSPITAL   
#[Out]# 13                     ST VINCENT'S BIRMINGHAM   
#[Out]# 14       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 15      DECATUR MORGAN HOSPITAL-DECATUR CAMPUS   
#[Out]# 16                         PROVIDENCE HOSPITAL   
#[Out]# 17               D C H REGIONAL MEDICAL CENTER   
#[Out]# 18                             THOMAS HOSPITAL   
#[Out]# 19            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 20                      TRINITY MEDICAL CENTER   
#[Out]# 21                            MOBILE INFIRMARY   
#[Out]# 22                    BROOKWOOD MEDICAL CENTER   
#[Out]# 23                    ALASKA REGIONAL HOSPITAL   
#[Out]# 24        BANNER GOOD SAMARITAN MEDICAL CENTER   
#[Out]# 25                       TUCSON MEDICAL CENTER   
#[Out]# 26                 VERDE VALLEY MEDICAL CENTER   
#[Out]# 27               CARONDELET ST  MARYS HOSPITAL   
#[Out]# 28             CARONDELET ST JOSEPH'S HOSPITAL   
#[Out]# 29             YAVAPAI REGIONAL MEDICAL CENTER   
#[Out]# ...                                        ...   
#[Out]# 157717                                     NaN   
#[Out]# 157718                                     NaN   
#[Out]# 157719                                     NaN   
#[Out]# 157720                                     NaN   
#[Out]# 157721                                     NaN   
#[Out]# 157722                                     NaN   
#[Out]# 157723                                     NaN   
#[Out]# 157724                                     NaN   
#[Out]# 157725                                     NaN   
#[Out]# 157726                                     NaN   
#[Out]# 157727                                     NaN   
#[Out]# 157728                                     NaN   
#[Out]# 157729                                     NaN   
#[Out]# 157730                                     NaN   
#[Out]# 157731                                     NaN   
#[Out]# 157732                                     NaN   
#[Out]# 157733                                     NaN   
#[Out]# 157734                                     NaN   
#[Out]# 157735                                     NaN   
#[Out]# 157736                                     NaN   
#[Out]# 157737                                     NaN   
#[Out]# 157738                                     NaN   
#[Out]# 157739                                     NaN   
#[Out]# 157740                                     NaN   
#[Out]# 157741                                     NaN   
#[Out]# 157742                                     NaN   
#[Out]# 157743                                     NaN   
#[Out]# 157744                                     NaN   
#[Out]# 157745                                     NaN   
#[Out]# 157746                                     NaN   
#[Out]# 
#[Out]#                     Provider Street Address Provider City Provider State  \
#[Out]# 0                    1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 1                2505 U S HIGHWAY 431 NORTH          BOAZ             AL   
#[Out]# 2                        205 MARENGO STREET      FLORENCE             AL   
#[Out]# 3                50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 4                   1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 5                 2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 6                          1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 7                    2000 PEPPERELL PARKWAY       OPELIKA             AL   
#[Out]# 8                     619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 9                             101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 10                     1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 11                   600 SOUTH THIRD STREET       GADSDEN             AL   
#[Out]# 12                    4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 13                   810 ST VINCENT'S DRIVE    BIRMINGHAM             AL   
#[Out]# 14                     400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 15                       1201 7TH STREET SE       DECATUR             AL   
#[Out]# 16                   6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 17            809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 18                        750 MORPHY AVENUE      FAIRHOPE             AL   
#[Out]# 19           701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 20                         800 MONTCLAIR RD    BIRMINGHAM             AL   
#[Out]# 21                5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 22      2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 23                         2801 DEBARR ROAD     ANCHORAGE             AK   
#[Out]# 24                  1111 EAST MCDOWELL ROAD       PHOENIX             AZ   
#[Out]# 25                     5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 26                     269 SOUTH CANDY LANE    COTTONWOOD             AZ   
#[Out]# 27                 1601 WEST ST MARY'S ROAD        TUCSON             AZ   
#[Out]# 28                    350 NORTH WILMOT ROAD        TUCSON             AZ   
#[Out]# 29                   1003 WILLOW CREEK ROAD      PRESCOTT             AZ   
#[Out]# ...                                     ...           ...            ...   
#[Out]# 157717                                  NaN           NaN            NaN   
#[Out]# 157718                                  NaN           NaN            NaN   
#[Out]# 157719                                  NaN           NaN            NaN   
#[Out]# 157720                                  NaN           NaN            NaN   
#[Out]# 157721                                  NaN           NaN            NaN   
#[Out]# 157722                                  NaN           NaN            NaN   
#[Out]# 157723                                  NaN           NaN            NaN   
#[Out]# 157724                                  NaN           NaN            NaN   
#[Out]# 157725                                  NaN           NaN            NaN   
#[Out]# 157726                                  NaN           NaN            NaN   
#[Out]# 157727                                  NaN           NaN            NaN   
#[Out]# 157728                                  NaN           NaN            NaN   
#[Out]# 157729                                  NaN           NaN            NaN   
#[Out]# 157730                                  NaN           NaN            NaN   
#[Out]# 157731                                  NaN           NaN            NaN   
#[Out]# 157732                                  NaN           NaN            NaN   
#[Out]# 157733                                  NaN           NaN            NaN   
#[Out]# 157734                                  NaN           NaN            NaN   
#[Out]# 157735                                  NaN           NaN            NaN   
#[Out]# 157736                                  NaN           NaN            NaN   
#[Out]# 157737                                  NaN           NaN            NaN   
#[Out]# 157738                                  NaN           NaN            NaN   
#[Out]# 157739                                  NaN           NaN            NaN   
#[Out]# 157740                                  NaN           NaN            NaN   
#[Out]# 157741                                  NaN           NaN            NaN   
#[Out]# 157742                                  NaN           NaN            NaN   
#[Out]# 157743                                  NaN           NaN            NaN   
#[Out]# 157744                                  NaN           NaN            NaN   
#[Out]# 157745                                  NaN           NaN            NaN   
#[Out]# 157746                                  NaN           NaN            NaN   
#[Out]# 
#[Out]#         Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                   36301                                AL - Dothan   
#[Out]# 1                   35957                            AL - Birmingham   
#[Out]# 2                   35631                            AL - Birmingham   
#[Out]# 3                   35235                            AL - Birmingham   
#[Out]# 4                   35007                            AL - Birmingham   
#[Out]# 5                   36116                            AL - Montgomery   
#[Out]# 6                   36106                            AL - Montgomery   
#[Out]# 7                   36801                            AL - Birmingham   
#[Out]# 8                   35233                            AL - Birmingham   
#[Out]# 9                   35801                            AL - Huntsville   
#[Out]# 10                  35903                            AL - Birmingham   
#[Out]# 11                  35901                            AL - Birmingham   
#[Out]# 12                  36305                                AL - Dothan   
#[Out]# 13                  35205                            AL - Birmingham   
#[Out]# 14                  36207                            AL - Birmingham   
#[Out]# 15                  35601                            AL - Huntsville   
#[Out]# 16                  36608                                AL - Mobile   
#[Out]# 17                  35401                            AL - Tuscaloosa   
#[Out]# 18                  36532                                AL - Mobile   
#[Out]# 19                  35211                            AL - Birmingham   
#[Out]# 20                  35213                            AL - Birmingham   
#[Out]# 21                  36652                                AL - Mobile   
#[Out]# 22                  35209                            AL - Birmingham   
#[Out]# 23                  99508                             AK - Anchorage   
#[Out]# 24                  85006                               AZ - Phoenix   
#[Out]# 25                  85712                                AZ - Tucson   
#[Out]# 26                  86326                               AZ - Phoenix   
#[Out]# 27                  85745                                AZ - Tucson   
#[Out]# 28                  85711                                AZ - Tucson   
#[Out]# 29                  86301                               AZ - Phoenix   
#[Out]# ...                   ...                                        ...   
#[Out]# 157717                NaN                                        NaN   
#[Out]# 157718                NaN                                        NaN   
#[Out]# 157719                NaN                                        NaN   
#[Out]# 157720                NaN                                        NaN   
#[Out]# 157721                NaN                                        NaN   
#[Out]# 157722                NaN                                        NaN   
#[Out]# 157723                NaN                                        NaN   
#[Out]# 157724                NaN                                        NaN   
#[Out]# 157725                NaN                                        NaN   
#[Out]# 157726                NaN                                        NaN   
#[Out]# 157727                NaN                                        NaN   
#[Out]# 157728                NaN                                        NaN   
#[Out]# 157729                NaN                                        NaN   
#[Out]# 157730                NaN                                        NaN   
#[Out]# 157731                NaN                                        NaN   
#[Out]# 157732                NaN                                        NaN   
#[Out]# 157733                NaN                                        NaN   
#[Out]# 157734                NaN                                        NaN   
#[Out]# 157735                NaN                                        NaN   
#[Out]# 157736                NaN                                        NaN   
#[Out]# 157737                NaN                                        NaN   
#[Out]# 157738                NaN                                        NaN   
#[Out]# 157739                NaN                                        NaN   
#[Out]# 157740                NaN                                        NaN   
#[Out]# 157741                NaN                                        NaN   
#[Out]# 157742                NaN                                        NaN   
#[Out]# 157743                NaN                                        NaN   
#[Out]# 157744                NaN                                        NaN   
#[Out]# 157745                NaN                                        NaN   
#[Out]# 157746                NaN                                        NaN   
#[Out]# 
#[Out]#         Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 0                     98              37988.97959             5872.193878   
#[Out]# 1                     34              15554.88235             6053.294118   
#[Out]# 2                     30              40380.10000             5319.833333   
#[Out]# 3                     17              22026.23529             5767.882353   
#[Out]# 4                     17              45663.05882             5785.117647   
#[Out]# 5                     44              16579.81818             6485.681818   
#[Out]# 6                     15              23243.73333             5553.866667   
#[Out]# 7                     46              11807.78261             6200.326087   
#[Out]# 8                     34              45288.23529             8977.323529   
#[Out]# 9                    111              33108.30631             5948.000000   
#[Out]# 10                    22              83400.09091             5942.272727   
#[Out]# 11                    14              81517.35714             5540.142857   
#[Out]# 12                    56              39789.98214             5498.857143   
#[Out]# 13                    42              21614.02381             5272.904762   
#[Out]# 14                    24              33509.83333             5423.458333   
#[Out]# 15                    26              15005.11538             5268.192308   
#[Out]# 16                    16              12574.06250             5975.812500   
#[Out]# 17                    40              22007.52500             6398.675000   
#[Out]# 18                    26              14847.00000             5103.153846   
#[Out]# 19                    22              48621.68182             6176.545455   
#[Out]# 20                    28              75521.96429             6282.285714   
#[Out]# 21                    53              16921.16981             5316.622642   
#[Out]# 22                    28             110405.75000             5877.321429   
#[Out]# 23                    38              41744.65789             8059.763158   
#[Out]# 24                    17              36434.82353             8223.235294   
#[Out]# 25                    24              27468.37500             7465.833333   
#[Out]# 26                    16              30707.87500             9331.437500   
#[Out]# 27                    14              35782.71429             6994.214286   
#[Out]# 28                    27              30702.70370             6577.148148   
#[Out]# 29                    29              29135.65517             6603.793103   
#[Out]# ...                  ...                      ...                     ...   
#[Out]# 157717               NaN                      NaN                     NaN   
#[Out]# 157718               NaN                      NaN                     NaN   
#[Out]# 157719               NaN                      NaN                     NaN   
#[Out]# 157720               NaN                      NaN                     NaN   
#[Out]# 157721               NaN                      NaN                     NaN   
#[Out]# 157722               NaN                      NaN                     NaN   
#[Out]# 157723               NaN                      NaN                     NaN   
#[Out]# 157724               NaN                      NaN                     NaN   
#[Out]# 157725               NaN                      NaN                     NaN   
#[Out]# 157726               NaN                      NaN                     NaN   
#[Out]# 157727               NaN                      NaN                     NaN   
#[Out]# 157728               NaN                      NaN                     NaN   
#[Out]# 157729               NaN                      NaN                     NaN   
#[Out]# 157730               NaN                      NaN                     NaN   
#[Out]# 157731               NaN                      NaN                     NaN   
#[Out]# 157732               NaN                      NaN                     NaN   
#[Out]# 157733               NaN                      NaN                     NaN   
#[Out]# 157734               NaN                      NaN                     NaN   
#[Out]# 157735               NaN                      NaN                     NaN   
#[Out]# 157736               NaN                      NaN                     NaN   
#[Out]# 157737               NaN                      NaN                     NaN   
#[Out]# 157738               NaN                      NaN                     NaN   
#[Out]# 157739               NaN                      NaN                     NaN   
#[Out]# 157740               NaN                      NaN                     NaN   
#[Out]# 157741               NaN                      NaN                     NaN   
#[Out]# 157742               NaN                      NaN                     NaN   
#[Out]# 157743               NaN                      NaN                     NaN   
#[Out]# 157744               NaN                      NaN                     NaN   
#[Out]# 157745               NaN                      NaN                     NaN   
#[Out]# 157746               NaN                      NaN                     NaN   
#[Out]# 
#[Out]#         Average Medicare Payments                                      PROC  \
#[Out]# 0                     4838.316327  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 1                     5255.647059  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 2                     4150.866667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 3                     4268.352941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 4                     4886.294118  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 5                     5375.159091  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 6                     4376.600000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 7                     4766.891304  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 8                     6238.852941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 9                     4780.081081  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 10                    4872.272727  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 11                    4120.214286  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 12                    4434.946429  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 13                    4257.309524  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 14                    4469.916667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 15                    4138.230769  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 16                    3675.062500  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 17                    5366.225000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 18                    3815.961538  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 19                    5271.818182  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 20                    5187.357143  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 21                    4085.660377  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 22                    4380.892857  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 23                    6943.973684  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 24                    7115.470588  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 25                    6212.708333  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 26                    8105.125000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 27                    6154.500000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 28                    5528.851852  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 29                    5504.344828  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# ...                           ...                                       ...   
#[Out]# 157717                        NaN                                       nan   
#[Out]# 157718                        NaN                                       nan   
#[Out]# 157719                        NaN                                       nan   
#[Out]# 157720                        NaN                                       nan   
#[Out]# 157721                        NaN                                       nan   
#[Out]# 157722                        NaN                                       nan   
#[Out]# 157723                        NaN                                       nan   
#[Out]# 157724                        NaN                                       nan   
#[Out]# 157725                        NaN                                       nan   
#[Out]# 157726                        NaN                                       nan   
#[Out]# 157727                        NaN                                       nan   
#[Out]# 157728                        NaN                                       nan   
#[Out]# 157729                        NaN                                       nan   
#[Out]# 157730                        NaN                                       nan   
#[Out]# 157731                        NaN                                       nan   
#[Out]# 157732                        NaN                                       nan   
#[Out]# 157733                        NaN                                       nan   
#[Out]# 157734                        NaN                                       nan   
#[Out]# 157735                        NaN                                       nan   
#[Out]# 157736                        NaN                                       nan   
#[Out]# 157737                        NaN                                       nan   
#[Out]# 157738                        NaN                                       nan   
#[Out]# 157739                        NaN                                       nan   
#[Out]# 157740                        NaN                                       nan   
#[Out]# 157741                        NaN                                       nan   
#[Out]# 157742                        NaN                                       nan   
#[Out]# 157743                        NaN                                       nan   
#[Out]# 157744                        NaN                                       nan   
#[Out]# 157745                        NaN                                       nan   
#[Out]# 157746                        NaN                                       nan   
#[Out]# 
#[Out]#                                             proc proc code  
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC       039  
#[Out]# ...                                          ...       ...  
#[Out]# 157717                                       nan       nan  
#[Out]# 157718                                       nan       nan  
#[Out]# 157719                                       nan       nan  
#[Out]# 157720                                       nan       nan  
#[Out]# 157721                                       nan       nan  
#[Out]# 157722                                       nan       nan  
#[Out]# 157723                                       nan       nan  
#[Out]# 157724                                       nan       nan  
#[Out]# 157725                                       nan       nan  
#[Out]# 157726                                       nan       nan  
#[Out]# 157727                                       nan       nan  
#[Out]# 157728                                       nan       nan  
#[Out]# 157729                                       nan       nan  
#[Out]# 157730                                       nan       nan  
#[Out]# 157731                                       nan       nan  
#[Out]# 157732                                       nan       nan  
#[Out]# 157733                                       nan       nan  
#[Out]# 157734                                       nan       nan  
#[Out]# 157735                                       nan       nan  
#[Out]# 157736                                       nan       nan  
#[Out]# 157737                                       nan       nan  
#[Out]# 157738                                       nan       nan  
#[Out]# 157739                                       nan       nan  
#[Out]# 157740                                       nan       nan  
#[Out]# 157741                                       nan       nan  
#[Out]# 157742                                       nan       nan  
#[Out]# 157743                                       nan       nan  
#[Out]# 157744                                       nan       nan  
#[Out]# 157745                                       nan       nan  
#[Out]# 157746                                       nan       nan  
#[Out]# 
#[Out]# [157747 rows x 15 columns]
# Fri, 11 Sep 2015 10:45:26
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'PROC', 'PROC CODE', 'proc', 'proc code'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:45:32
data=data.drop("PROC CODE",axis=1)
# Fri, 11 Sep 2015 10:45:37
data=data.drop("PROC",axis=1)
# Fri, 11 Sep 2015 10:45:39
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:45:41
data
#[Out]#                                   DRG Definition  Provider Id  \
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10001   
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10005   
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10006   
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10011   
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10016   
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10023   
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10024   
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10029   
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10033   
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10039   
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10040   
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10046   
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10055   
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10056   
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10078   
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10085   
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10090   
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10092   
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10100   
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10103   
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10104   
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10113   
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10139   
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        20017   
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30002   
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30006   
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30007   
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30010   
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30011   
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30012   
#[Out]# ...                                          ...          ...   
#[Out]# 157717                                       NaN          NaN   
#[Out]# 157718                                       NaN          NaN   
#[Out]# 157719                                       NaN          NaN   
#[Out]# 157720                                       NaN          NaN   
#[Out]# 157721                                       NaN          NaN   
#[Out]# 157722                                       NaN          NaN   
#[Out]# 157723                                       NaN          NaN   
#[Out]# 157724                                       NaN          NaN   
#[Out]# 157725                                       NaN          NaN   
#[Out]# 157726                                       NaN          NaN   
#[Out]# 157727                                       NaN          NaN   
#[Out]# 157728                                       NaN          NaN   
#[Out]# 157729                                       NaN          NaN   
#[Out]# 157730                                       NaN          NaN   
#[Out]# 157731                                       NaN          NaN   
#[Out]# 157732                                       NaN          NaN   
#[Out]# 157733                                       NaN          NaN   
#[Out]# 157734                                       NaN          NaN   
#[Out]# 157735                                       NaN          NaN   
#[Out]# 157736                                       NaN          NaN   
#[Out]# 157737                                       NaN          NaN   
#[Out]# 157738                                       NaN          NaN   
#[Out]# 157739                                       NaN          NaN   
#[Out]# 157740                                       NaN          NaN   
#[Out]# 157741                                       NaN          NaN   
#[Out]# 157742                                       NaN          NaN   
#[Out]# 157743                                       NaN          NaN   
#[Out]# 157744                                       NaN          NaN   
#[Out]# 157745                                       NaN          NaN   
#[Out]# 157746                                       NaN          NaN   
#[Out]# 
#[Out]#                                  Provider Name  \
#[Out]# 0             SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 2               ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 3                            ST VINCENT'S EAST   
#[Out]# 4                SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 5                 BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 6                JACKSON HOSPITAL & CLINIC INC   
#[Out]# 7          EAST ALABAMA MEDICAL CENTER AND SNF   
#[Out]# 8               UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 9                          HUNTSVILLE HOSPITAL   
#[Out]# 10             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 11           RIVERVIEW REGIONAL MEDICAL CENTER   
#[Out]# 12                            FLOWERS HOSPITAL   
#[Out]# 13                     ST VINCENT'S BIRMINGHAM   
#[Out]# 14       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 15      DECATUR MORGAN HOSPITAL-DECATUR CAMPUS   
#[Out]# 16                         PROVIDENCE HOSPITAL   
#[Out]# 17               D C H REGIONAL MEDICAL CENTER   
#[Out]# 18                             THOMAS HOSPITAL   
#[Out]# 19            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 20                      TRINITY MEDICAL CENTER   
#[Out]# 21                            MOBILE INFIRMARY   
#[Out]# 22                    BROOKWOOD MEDICAL CENTER   
#[Out]# 23                    ALASKA REGIONAL HOSPITAL   
#[Out]# 24        BANNER GOOD SAMARITAN MEDICAL CENTER   
#[Out]# 25                       TUCSON MEDICAL CENTER   
#[Out]# 26                 VERDE VALLEY MEDICAL CENTER   
#[Out]# 27               CARONDELET ST  MARYS HOSPITAL   
#[Out]# 28             CARONDELET ST JOSEPH'S HOSPITAL   
#[Out]# 29             YAVAPAI REGIONAL MEDICAL CENTER   
#[Out]# ...                                        ...   
#[Out]# 157717                                     NaN   
#[Out]# 157718                                     NaN   
#[Out]# 157719                                     NaN   
#[Out]# 157720                                     NaN   
#[Out]# 157721                                     NaN   
#[Out]# 157722                                     NaN   
#[Out]# 157723                                     NaN   
#[Out]# 157724                                     NaN   
#[Out]# 157725                                     NaN   
#[Out]# 157726                                     NaN   
#[Out]# 157727                                     NaN   
#[Out]# 157728                                     NaN   
#[Out]# 157729                                     NaN   
#[Out]# 157730                                     NaN   
#[Out]# 157731                                     NaN   
#[Out]# 157732                                     NaN   
#[Out]# 157733                                     NaN   
#[Out]# 157734                                     NaN   
#[Out]# 157735                                     NaN   
#[Out]# 157736                                     NaN   
#[Out]# 157737                                     NaN   
#[Out]# 157738                                     NaN   
#[Out]# 157739                                     NaN   
#[Out]# 157740                                     NaN   
#[Out]# 157741                                     NaN   
#[Out]# 157742                                     NaN   
#[Out]# 157743                                     NaN   
#[Out]# 157744                                     NaN   
#[Out]# 157745                                     NaN   
#[Out]# 157746                                     NaN   
#[Out]# 
#[Out]#                     Provider Street Address Provider City Provider State  \
#[Out]# 0                    1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 1                2505 U S HIGHWAY 431 NORTH          BOAZ             AL   
#[Out]# 2                        205 MARENGO STREET      FLORENCE             AL   
#[Out]# 3                50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 4                   1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 5                 2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 6                          1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 7                    2000 PEPPERELL PARKWAY       OPELIKA             AL   
#[Out]# 8                     619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 9                             101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 10                     1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 11                   600 SOUTH THIRD STREET       GADSDEN             AL   
#[Out]# 12                    4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 13                   810 ST VINCENT'S DRIVE    BIRMINGHAM             AL   
#[Out]# 14                     400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 15                       1201 7TH STREET SE       DECATUR             AL   
#[Out]# 16                   6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 17            809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 18                        750 MORPHY AVENUE      FAIRHOPE             AL   
#[Out]# 19           701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 20                         800 MONTCLAIR RD    BIRMINGHAM             AL   
#[Out]# 21                5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 22      2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 23                         2801 DEBARR ROAD     ANCHORAGE             AK   
#[Out]# 24                  1111 EAST MCDOWELL ROAD       PHOENIX             AZ   
#[Out]# 25                     5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 26                     269 SOUTH CANDY LANE    COTTONWOOD             AZ   
#[Out]# 27                 1601 WEST ST MARY'S ROAD        TUCSON             AZ   
#[Out]# 28                    350 NORTH WILMOT ROAD        TUCSON             AZ   
#[Out]# 29                   1003 WILLOW CREEK ROAD      PRESCOTT             AZ   
#[Out]# ...                                     ...           ...            ...   
#[Out]# 157717                                  NaN           NaN            NaN   
#[Out]# 157718                                  NaN           NaN            NaN   
#[Out]# 157719                                  NaN           NaN            NaN   
#[Out]# 157720                                  NaN           NaN            NaN   
#[Out]# 157721                                  NaN           NaN            NaN   
#[Out]# 157722                                  NaN           NaN            NaN   
#[Out]# 157723                                  NaN           NaN            NaN   
#[Out]# 157724                                  NaN           NaN            NaN   
#[Out]# 157725                                  NaN           NaN            NaN   
#[Out]# 157726                                  NaN           NaN            NaN   
#[Out]# 157727                                  NaN           NaN            NaN   
#[Out]# 157728                                  NaN           NaN            NaN   
#[Out]# 157729                                  NaN           NaN            NaN   
#[Out]# 157730                                  NaN           NaN            NaN   
#[Out]# 157731                                  NaN           NaN            NaN   
#[Out]# 157732                                  NaN           NaN            NaN   
#[Out]# 157733                                  NaN           NaN            NaN   
#[Out]# 157734                                  NaN           NaN            NaN   
#[Out]# 157735                                  NaN           NaN            NaN   
#[Out]# 157736                                  NaN           NaN            NaN   
#[Out]# 157737                                  NaN           NaN            NaN   
#[Out]# 157738                                  NaN           NaN            NaN   
#[Out]# 157739                                  NaN           NaN            NaN   
#[Out]# 157740                                  NaN           NaN            NaN   
#[Out]# 157741                                  NaN           NaN            NaN   
#[Out]# 157742                                  NaN           NaN            NaN   
#[Out]# 157743                                  NaN           NaN            NaN   
#[Out]# 157744                                  NaN           NaN            NaN   
#[Out]# 157745                                  NaN           NaN            NaN   
#[Out]# 157746                                  NaN           NaN            NaN   
#[Out]# 
#[Out]#         Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                   36301                                AL - Dothan   
#[Out]# 1                   35957                            AL - Birmingham   
#[Out]# 2                   35631                            AL - Birmingham   
#[Out]# 3                   35235                            AL - Birmingham   
#[Out]# 4                   35007                            AL - Birmingham   
#[Out]# 5                   36116                            AL - Montgomery   
#[Out]# 6                   36106                            AL - Montgomery   
#[Out]# 7                   36801                            AL - Birmingham   
#[Out]# 8                   35233                            AL - Birmingham   
#[Out]# 9                   35801                            AL - Huntsville   
#[Out]# 10                  35903                            AL - Birmingham   
#[Out]# 11                  35901                            AL - Birmingham   
#[Out]# 12                  36305                                AL - Dothan   
#[Out]# 13                  35205                            AL - Birmingham   
#[Out]# 14                  36207                            AL - Birmingham   
#[Out]# 15                  35601                            AL - Huntsville   
#[Out]# 16                  36608                                AL - Mobile   
#[Out]# 17                  35401                            AL - Tuscaloosa   
#[Out]# 18                  36532                                AL - Mobile   
#[Out]# 19                  35211                            AL - Birmingham   
#[Out]# 20                  35213                            AL - Birmingham   
#[Out]# 21                  36652                                AL - Mobile   
#[Out]# 22                  35209                            AL - Birmingham   
#[Out]# 23                  99508                             AK - Anchorage   
#[Out]# 24                  85006                               AZ - Phoenix   
#[Out]# 25                  85712                                AZ - Tucson   
#[Out]# 26                  86326                               AZ - Phoenix   
#[Out]# 27                  85745                                AZ - Tucson   
#[Out]# 28                  85711                                AZ - Tucson   
#[Out]# 29                  86301                               AZ - Phoenix   
#[Out]# ...                   ...                                        ...   
#[Out]# 157717                NaN                                        NaN   
#[Out]# 157718                NaN                                        NaN   
#[Out]# 157719                NaN                                        NaN   
#[Out]# 157720                NaN                                        NaN   
#[Out]# 157721                NaN                                        NaN   
#[Out]# 157722                NaN                                        NaN   
#[Out]# 157723                NaN                                        NaN   
#[Out]# 157724                NaN                                        NaN   
#[Out]# 157725                NaN                                        NaN   
#[Out]# 157726                NaN                                        NaN   
#[Out]# 157727                NaN                                        NaN   
#[Out]# 157728                NaN                                        NaN   
#[Out]# 157729                NaN                                        NaN   
#[Out]# 157730                NaN                                        NaN   
#[Out]# 157731                NaN                                        NaN   
#[Out]# 157732                NaN                                        NaN   
#[Out]# 157733                NaN                                        NaN   
#[Out]# 157734                NaN                                        NaN   
#[Out]# 157735                NaN                                        NaN   
#[Out]# 157736                NaN                                        NaN   
#[Out]# 157737                NaN                                        NaN   
#[Out]# 157738                NaN                                        NaN   
#[Out]# 157739                NaN                                        NaN   
#[Out]# 157740                NaN                                        NaN   
#[Out]# 157741                NaN                                        NaN   
#[Out]# 157742                NaN                                        NaN   
#[Out]# 157743                NaN                                        NaN   
#[Out]# 157744                NaN                                        NaN   
#[Out]# 157745                NaN                                        NaN   
#[Out]# 157746                NaN                                        NaN   
#[Out]# 
#[Out]#         Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 0                     98              37988.97959             5872.193878   
#[Out]# 1                     34              15554.88235             6053.294118   
#[Out]# 2                     30              40380.10000             5319.833333   
#[Out]# 3                     17              22026.23529             5767.882353   
#[Out]# 4                     17              45663.05882             5785.117647   
#[Out]# 5                     44              16579.81818             6485.681818   
#[Out]# 6                     15              23243.73333             5553.866667   
#[Out]# 7                     46              11807.78261             6200.326087   
#[Out]# 8                     34              45288.23529             8977.323529   
#[Out]# 9                    111              33108.30631             5948.000000   
#[Out]# 10                    22              83400.09091             5942.272727   
#[Out]# 11                    14              81517.35714             5540.142857   
#[Out]# 12                    56              39789.98214             5498.857143   
#[Out]# 13                    42              21614.02381             5272.904762   
#[Out]# 14                    24              33509.83333             5423.458333   
#[Out]# 15                    26              15005.11538             5268.192308   
#[Out]# 16                    16              12574.06250             5975.812500   
#[Out]# 17                    40              22007.52500             6398.675000   
#[Out]# 18                    26              14847.00000             5103.153846   
#[Out]# 19                    22              48621.68182             6176.545455   
#[Out]# 20                    28              75521.96429             6282.285714   
#[Out]# 21                    53              16921.16981             5316.622642   
#[Out]# 22                    28             110405.75000             5877.321429   
#[Out]# 23                    38              41744.65789             8059.763158   
#[Out]# 24                    17              36434.82353             8223.235294   
#[Out]# 25                    24              27468.37500             7465.833333   
#[Out]# 26                    16              30707.87500             9331.437500   
#[Out]# 27                    14              35782.71429             6994.214286   
#[Out]# 28                    27              30702.70370             6577.148148   
#[Out]# 29                    29              29135.65517             6603.793103   
#[Out]# ...                  ...                      ...                     ...   
#[Out]# 157717               NaN                      NaN                     NaN   
#[Out]# 157718               NaN                      NaN                     NaN   
#[Out]# 157719               NaN                      NaN                     NaN   
#[Out]# 157720               NaN                      NaN                     NaN   
#[Out]# 157721               NaN                      NaN                     NaN   
#[Out]# 157722               NaN                      NaN                     NaN   
#[Out]# 157723               NaN                      NaN                     NaN   
#[Out]# 157724               NaN                      NaN                     NaN   
#[Out]# 157725               NaN                      NaN                     NaN   
#[Out]# 157726               NaN                      NaN                     NaN   
#[Out]# 157727               NaN                      NaN                     NaN   
#[Out]# 157728               NaN                      NaN                     NaN   
#[Out]# 157729               NaN                      NaN                     NaN   
#[Out]# 157730               NaN                      NaN                     NaN   
#[Out]# 157731               NaN                      NaN                     NaN   
#[Out]# 157732               NaN                      NaN                     NaN   
#[Out]# 157733               NaN                      NaN                     NaN   
#[Out]# 157734               NaN                      NaN                     NaN   
#[Out]# 157735               NaN                      NaN                     NaN   
#[Out]# 157736               NaN                      NaN                     NaN   
#[Out]# 157737               NaN                      NaN                     NaN   
#[Out]# 157738               NaN                      NaN                     NaN   
#[Out]# 157739               NaN                      NaN                     NaN   
#[Out]# 157740               NaN                      NaN                     NaN   
#[Out]# 157741               NaN                      NaN                     NaN   
#[Out]# 157742               NaN                      NaN                     NaN   
#[Out]# 157743               NaN                      NaN                     NaN   
#[Out]# 157744               NaN                      NaN                     NaN   
#[Out]# 157745               NaN                      NaN                     NaN   
#[Out]# 157746               NaN                      NaN                     NaN   
#[Out]# 
#[Out]#         Average Medicare Payments                                      proc  \
#[Out]# 0                     4838.316327  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 1                     5255.647059  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 2                     4150.866667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 3                     4268.352941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 4                     4886.294118  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 5                     5375.159091  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 6                     4376.600000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 7                     4766.891304  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 8                     6238.852941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 9                     4780.081081  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 10                    4872.272727  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 11                    4120.214286  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 12                    4434.946429  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 13                    4257.309524  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 14                    4469.916667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 15                    4138.230769  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 16                    3675.062500  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 17                    5366.225000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 18                    3815.961538  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 19                    5271.818182  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 20                    5187.357143  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 21                    4085.660377  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 22                    4380.892857  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 23                    6943.973684  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 24                    7115.470588  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 25                    6212.708333  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 26                    8105.125000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 27                    6154.500000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 28                    5528.851852  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 29                    5504.344828  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# ...                           ...                                       ...   
#[Out]# 157717                        NaN                                       nan   
#[Out]# 157718                        NaN                                       nan   
#[Out]# 157719                        NaN                                       nan   
#[Out]# 157720                        NaN                                       nan   
#[Out]# 157721                        NaN                                       nan   
#[Out]# 157722                        NaN                                       nan   
#[Out]# 157723                        NaN                                       nan   
#[Out]# 157724                        NaN                                       nan   
#[Out]# 157725                        NaN                                       nan   
#[Out]# 157726                        NaN                                       nan   
#[Out]# 157727                        NaN                                       nan   
#[Out]# 157728                        NaN                                       nan   
#[Out]# 157729                        NaN                                       nan   
#[Out]# 157730                        NaN                                       nan   
#[Out]# 157731                        NaN                                       nan   
#[Out]# 157732                        NaN                                       nan   
#[Out]# 157733                        NaN                                       nan   
#[Out]# 157734                        NaN                                       nan   
#[Out]# 157735                        NaN                                       nan   
#[Out]# 157736                        NaN                                       nan   
#[Out]# 157737                        NaN                                       nan   
#[Out]# 157738                        NaN                                       nan   
#[Out]# 157739                        NaN                                       nan   
#[Out]# 157740                        NaN                                       nan   
#[Out]# 157741                        NaN                                       nan   
#[Out]# 157742                        NaN                                       nan   
#[Out]# 157743                        NaN                                       nan   
#[Out]# 157744                        NaN                                       nan   
#[Out]# 157745                        NaN                                       nan   
#[Out]# 157746                        NaN                                       nan   
#[Out]# 
#[Out]#        proc code  
#[Out]# 0            039  
#[Out]# 1            039  
#[Out]# 2            039  
#[Out]# 3            039  
#[Out]# 4            039  
#[Out]# 5            039  
#[Out]# 6            039  
#[Out]# 7            039  
#[Out]# 8            039  
#[Out]# 9            039  
#[Out]# 10           039  
#[Out]# 11           039  
#[Out]# 12           039  
#[Out]# 13           039  
#[Out]# 14           039  
#[Out]# 15           039  
#[Out]# 16           039  
#[Out]# 17           039  
#[Out]# 18           039  
#[Out]# 19           039  
#[Out]# 20           039  
#[Out]# 21           039  
#[Out]# 22           039  
#[Out]# 23           039  
#[Out]# 24           039  
#[Out]# 25           039  
#[Out]# 26           039  
#[Out]# 27           039  
#[Out]# 28           039  
#[Out]# 29           039  
#[Out]# ...          ...  
#[Out]# 157717       nan  
#[Out]# 157718       nan  
#[Out]# 157719       nan  
#[Out]# 157720       nan  
#[Out]# 157721       nan  
#[Out]# 157722       nan  
#[Out]# 157723       nan  
#[Out]# 157724       nan  
#[Out]# 157725       nan  
#[Out]# 157726       nan  
#[Out]# 157727       nan  
#[Out]# 157728       nan  
#[Out]# 157729       nan  
#[Out]# 157730       nan  
#[Out]# 157731       nan  
#[Out]# 157732       nan  
#[Out]# 157733       nan  
#[Out]# 157734       nan  
#[Out]# 157735       nan  
#[Out]# 157736       nan  
#[Out]# 157737       nan  
#[Out]# 157738       nan  
#[Out]# 157739       nan  
#[Out]# 157740       nan  
#[Out]# 157741       nan  
#[Out]# 157742       nan  
#[Out]# 157743       nan  
#[Out]# 157744       nan  
#[Out]# 157745       nan  
#[Out]# 157746       nan  
#[Out]# 
#[Out]# [157747 rows x 14 columns]
# Fri, 11 Sep 2015 10:45:43
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:46:42
data["markup"]=data["Average Covered Charges"]/data["Average Medicare Payments"]
# Fri, 11 Sep 2015 10:46:44
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:46:51
data["markup"]
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 157717          NaN
#[Out]# 157718          NaN
#[Out]# 157719          NaN
#[Out]# 157720          NaN
#[Out]# 157721          NaN
#[Out]# 157722          NaN
#[Out]# 157723          NaN
#[Out]# 157724          NaN
#[Out]# 157725          NaN
#[Out]# 157726          NaN
#[Out]# 157727          NaN
#[Out]# 157728          NaN
#[Out]# 157729          NaN
#[Out]# 157730          NaN
#[Out]# 157731          NaN
#[Out]# 157732          NaN
#[Out]# 157733          NaN
#[Out]# 157734          NaN
#[Out]# 157735          NaN
#[Out]# 157736          NaN
#[Out]# 157737          NaN
#[Out]# 157738          NaN
#[Out]# 157739          NaN
#[Out]# 157740          NaN
#[Out]# 157741          NaN
#[Out]# 157742          NaN
#[Out]# 157743          NaN
#[Out]# 157744          NaN
#[Out]# 157745          NaN
#[Out]# 157746          NaN
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:47:09
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:47:21
data["markup"][data["proc code"]=="305"]
#[Out]# 65060     6.663608
#[Out]# 65061     8.381469
#[Out]# 65062    10.451970
#[Out]# 65063     7.363758
#[Out]# 65064     8.514054
#[Out]# 65065     3.602018
#[Out]# 65066     5.152709
#[Out]# 65067     4.609246
#[Out]# 65068     9.440464
#[Out]# 65069    15.544039
#[Out]# 65070     9.835780
#[Out]# 65071     3.878793
#[Out]# 65072     5.557330
#[Out]# 65073     5.513968
#[Out]# 65074     7.307482
#[Out]# 65075     2.413039
#[Out]# 65076     6.009232
#[Out]# 65077    19.977630
#[Out]# 65078     6.240598
#[Out]# 65079     4.988829
#[Out]# 65080     6.900532
#[Out]# 65081     3.765426
#[Out]# 65082    10.086110
#[Out]# 65083     4.306546
#[Out]# 65084     6.498016
#[Out]# 65085     9.209411
#[Out]# 65086     5.645931
#[Out]# 65087     6.575877
#[Out]# 65088     6.690510
#[Out]# 65089     6.727001
#[Out]#            ...    
#[Out]# 65999     8.544768
#[Out]# 66000     7.042084
#[Out]# 66001     7.596612
#[Out]# 66002     7.673572
#[Out]# 66003     6.019208
#[Out]# 66004     6.046108
#[Out]# 66005     3.218474
#[Out]# 66006     4.018800
#[Out]# 66007     5.451160
#[Out]# 66008     6.503976
#[Out]# 66009     2.857527
#[Out]# 66010     4.550774
#[Out]# 66011     3.469936
#[Out]# 66012     3.412576
#[Out]# 66013     6.216429
#[Out]# 66014     5.309429
#[Out]# 66015     4.097292
#[Out]# 66016     5.696044
#[Out]# 66017     4.068580
#[Out]# 66018     5.473017
#[Out]# 66019     5.369684
#[Out]# 66020     3.442493
#[Out]# 66021     4.664804
#[Out]# 66022     2.934560
#[Out]# 66023     8.659285
#[Out]# 66024    18.815193
#[Out]# 66025     9.117995
#[Out]# 66026    12.271003
#[Out]# 66027    15.089157
#[Out]# 66028     9.654588
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:47:30
a=data["markup"][data["proc code"]=="305"]
# Fri, 11 Sep 2015 10:47:35
plt.hist(a,bins=1000)
# Fri, 11 Sep 2015 10:47:41
a
#[Out]# 65060     6.663608
#[Out]# 65061     8.381469
#[Out]# 65062    10.451970
#[Out]# 65063     7.363758
#[Out]# 65064     8.514054
#[Out]# 65065     3.602018
#[Out]# 65066     5.152709
#[Out]# 65067     4.609246
#[Out]# 65068     9.440464
#[Out]# 65069    15.544039
#[Out]# 65070     9.835780
#[Out]# 65071     3.878793
#[Out]# 65072     5.557330
#[Out]# 65073     5.513968
#[Out]# 65074     7.307482
#[Out]# 65075     2.413039
#[Out]# 65076     6.009232
#[Out]# 65077    19.977630
#[Out]# 65078     6.240598
#[Out]# 65079     4.988829
#[Out]# 65080     6.900532
#[Out]# 65081     3.765426
#[Out]# 65082    10.086110
#[Out]# 65083     4.306546
#[Out]# 65084     6.498016
#[Out]# 65085     9.209411
#[Out]# 65086     5.645931
#[Out]# 65087     6.575877
#[Out]# 65088     6.690510
#[Out]# 65089     6.727001
#[Out]#            ...    
#[Out]# 65999     8.544768
#[Out]# 66000     7.042084
#[Out]# 66001     7.596612
#[Out]# 66002     7.673572
#[Out]# 66003     6.019208
#[Out]# 66004     6.046108
#[Out]# 66005     3.218474
#[Out]# 66006     4.018800
#[Out]# 66007     5.451160
#[Out]# 66008     6.503976
#[Out]# 66009     2.857527
#[Out]# 66010     4.550774
#[Out]# 66011     3.469936
#[Out]# 66012     3.412576
#[Out]# 66013     6.216429
#[Out]# 66014     5.309429
#[Out]# 66015     4.097292
#[Out]# 66016     5.696044
#[Out]# 66017     4.068580
#[Out]# 66018     5.473017
#[Out]# 66019     5.369684
#[Out]# 66020     3.442493
#[Out]# 66021     4.664804
#[Out]# 66022     2.934560
#[Out]# 66023     8.659285
#[Out]# 66024    18.815193
#[Out]# 66025     9.117995
#[Out]# 66026    12.271003
#[Out]# 66027    15.089157
#[Out]# 66028     9.654588
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:47:47
len(a)
#[Out]# 969
# Fri, 11 Sep 2015 10:47:52
plt.hist(a,bins=100)
# Fri, 11 Sep 2015 10:48:07
okt
# Fri, 11 Sep 2015 10:48:09
plt
#[Out]# <module 'matplotlib.pyplot' from '/usr/lib/python3.4/site-packages/matplotlib/pyplot.py'>
# Fri, 11 Sep 2015 10:48:11
plt.hist
#[Out]# <function matplotlib.pyplot.hist>
# Fri, 11 Sep 2015 10:48:15
plt.hist?
# Fri, 11 Sep 2015 10:48:26
a
#[Out]# 65060     6.663608
#[Out]# 65061     8.381469
#[Out]# 65062    10.451970
#[Out]# 65063     7.363758
#[Out]# 65064     8.514054
#[Out]# 65065     3.602018
#[Out]# 65066     5.152709
#[Out]# 65067     4.609246
#[Out]# 65068     9.440464
#[Out]# 65069    15.544039
#[Out]# 65070     9.835780
#[Out]# 65071     3.878793
#[Out]# 65072     5.557330
#[Out]# 65073     5.513968
#[Out]# 65074     7.307482
#[Out]# 65075     2.413039
#[Out]# 65076     6.009232
#[Out]# 65077    19.977630
#[Out]# 65078     6.240598
#[Out]# 65079     4.988829
#[Out]# 65080     6.900532
#[Out]# 65081     3.765426
#[Out]# 65082    10.086110
#[Out]# 65083     4.306546
#[Out]# 65084     6.498016
#[Out]# 65085     9.209411
#[Out]# 65086     5.645931
#[Out]# 65087     6.575877
#[Out]# 65088     6.690510
#[Out]# 65089     6.727001
#[Out]#            ...    
#[Out]# 65999     8.544768
#[Out]# 66000     7.042084
#[Out]# 66001     7.596612
#[Out]# 66002     7.673572
#[Out]# 66003     6.019208
#[Out]# 66004     6.046108
#[Out]# 66005     3.218474
#[Out]# 66006     4.018800
#[Out]# 66007     5.451160
#[Out]# 66008     6.503976
#[Out]# 66009     2.857527
#[Out]# 66010     4.550774
#[Out]# 66011     3.469936
#[Out]# 66012     3.412576
#[Out]# 66013     6.216429
#[Out]# 66014     5.309429
#[Out]# 66015     4.097292
#[Out]# 66016     5.696044
#[Out]# 66017     4.068580
#[Out]# 66018     5.473017
#[Out]# 66019     5.369684
#[Out]# 66020     3.442493
#[Out]# 66021     4.664804
#[Out]# 66022     2.934560
#[Out]# 66023     8.659285
#[Out]# 66024    18.815193
#[Out]# 66025     9.117995
#[Out]# 66026    12.271003
#[Out]# 66027    15.089157
#[Out]# 66028     9.654588
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:48:28
a
#[Out]# 65060     6.663608
#[Out]# 65061     8.381469
#[Out]# 65062    10.451970
#[Out]# 65063     7.363758
#[Out]# 65064     8.514054
#[Out]# 65065     3.602018
#[Out]# 65066     5.152709
#[Out]# 65067     4.609246
#[Out]# 65068     9.440464
#[Out]# 65069    15.544039
#[Out]# 65070     9.835780
#[Out]# 65071     3.878793
#[Out]# 65072     5.557330
#[Out]# 65073     5.513968
#[Out]# 65074     7.307482
#[Out]# 65075     2.413039
#[Out]# 65076     6.009232
#[Out]# 65077    19.977630
#[Out]# 65078     6.240598
#[Out]# 65079     4.988829
#[Out]# 65080     6.900532
#[Out]# 65081     3.765426
#[Out]# 65082    10.086110
#[Out]# 65083     4.306546
#[Out]# 65084     6.498016
#[Out]# 65085     9.209411
#[Out]# 65086     5.645931
#[Out]# 65087     6.575877
#[Out]# 65088     6.690510
#[Out]# 65089     6.727001
#[Out]#            ...    
#[Out]# 65999     8.544768
#[Out]# 66000     7.042084
#[Out]# 66001     7.596612
#[Out]# 66002     7.673572
#[Out]# 66003     6.019208
#[Out]# 66004     6.046108
#[Out]# 66005     3.218474
#[Out]# 66006     4.018800
#[Out]# 66007     5.451160
#[Out]# 66008     6.503976
#[Out]# 66009     2.857527
#[Out]# 66010     4.550774
#[Out]# 66011     3.469936
#[Out]# 66012     3.412576
#[Out]# 66013     6.216429
#[Out]# 66014     5.309429
#[Out]# 66015     4.097292
#[Out]# 66016     5.696044
#[Out]# 66017     4.068580
#[Out]# 66018     5.473017
#[Out]# 66019     5.369684
#[Out]# 66020     3.442493
#[Out]# 66021     4.664804
#[Out]# 66022     2.934560
#[Out]# 66023     8.659285
#[Out]# 66024    18.815193
#[Out]# 66025     9.117995
#[Out]# 66026    12.271003
#[Out]# 66027    15.089157
#[Out]# 66028     9.654588
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:48:34
data
#[Out]#                                   DRG Definition  Provider Id  \
#[Out]# 0       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10001   
#[Out]# 1       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10005   
#[Out]# 2       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10006   
#[Out]# 3       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10011   
#[Out]# 4       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10016   
#[Out]# 5       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10023   
#[Out]# 6       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10024   
#[Out]# 7       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10029   
#[Out]# 8       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10033   
#[Out]# 9       039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10039   
#[Out]# 10      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10040   
#[Out]# 11      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10046   
#[Out]# 12      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10055   
#[Out]# 13      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10056   
#[Out]# 14      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10078   
#[Out]# 15      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10085   
#[Out]# 16      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10090   
#[Out]# 17      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10092   
#[Out]# 18      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10100   
#[Out]# 19      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10103   
#[Out]# 20      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10104   
#[Out]# 21      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10113   
#[Out]# 22      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        10139   
#[Out]# 23      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        20017   
#[Out]# 24      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30002   
#[Out]# 25      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30006   
#[Out]# 26      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30007   
#[Out]# 27      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30010   
#[Out]# 28      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30011   
#[Out]# 29      039 - EXTRACRANIAL PROCEDURES W/O CC/MCC        30012   
#[Out]# ...                                          ...          ...   
#[Out]# 157717                                       NaN          NaN   
#[Out]# 157718                                       NaN          NaN   
#[Out]# 157719                                       NaN          NaN   
#[Out]# 157720                                       NaN          NaN   
#[Out]# 157721                                       NaN          NaN   
#[Out]# 157722                                       NaN          NaN   
#[Out]# 157723                                       NaN          NaN   
#[Out]# 157724                                       NaN          NaN   
#[Out]# 157725                                       NaN          NaN   
#[Out]# 157726                                       NaN          NaN   
#[Out]# 157727                                       NaN          NaN   
#[Out]# 157728                                       NaN          NaN   
#[Out]# 157729                                       NaN          NaN   
#[Out]# 157730                                       NaN          NaN   
#[Out]# 157731                                       NaN          NaN   
#[Out]# 157732                                       NaN          NaN   
#[Out]# 157733                                       NaN          NaN   
#[Out]# 157734                                       NaN          NaN   
#[Out]# 157735                                       NaN          NaN   
#[Out]# 157736                                       NaN          NaN   
#[Out]# 157737                                       NaN          NaN   
#[Out]# 157738                                       NaN          NaN   
#[Out]# 157739                                       NaN          NaN   
#[Out]# 157740                                       NaN          NaN   
#[Out]# 157741                                       NaN          NaN   
#[Out]# 157742                                       NaN          NaN   
#[Out]# 157743                                       NaN          NaN   
#[Out]# 157744                                       NaN          NaN   
#[Out]# 157745                                       NaN          NaN   
#[Out]# 157746                                       NaN          NaN   
#[Out]# 
#[Out]#                                  Provider Name  \
#[Out]# 0             SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 2               ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 3                            ST VINCENT'S EAST   
#[Out]# 4                SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 5                 BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 6                JACKSON HOSPITAL & CLINIC INC   
#[Out]# 7          EAST ALABAMA MEDICAL CENTER AND SNF   
#[Out]# 8               UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 9                          HUNTSVILLE HOSPITAL   
#[Out]# 10             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 11           RIVERVIEW REGIONAL MEDICAL CENTER   
#[Out]# 12                            FLOWERS HOSPITAL   
#[Out]# 13                     ST VINCENT'S BIRMINGHAM   
#[Out]# 14       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 15      DECATUR MORGAN HOSPITAL-DECATUR CAMPUS   
#[Out]# 16                         PROVIDENCE HOSPITAL   
#[Out]# 17               D C H REGIONAL MEDICAL CENTER   
#[Out]# 18                             THOMAS HOSPITAL   
#[Out]# 19            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 20                      TRINITY MEDICAL CENTER   
#[Out]# 21                            MOBILE INFIRMARY   
#[Out]# 22                    BROOKWOOD MEDICAL CENTER   
#[Out]# 23                    ALASKA REGIONAL HOSPITAL   
#[Out]# 24        BANNER GOOD SAMARITAN MEDICAL CENTER   
#[Out]# 25                       TUCSON MEDICAL CENTER   
#[Out]# 26                 VERDE VALLEY MEDICAL CENTER   
#[Out]# 27               CARONDELET ST  MARYS HOSPITAL   
#[Out]# 28             CARONDELET ST JOSEPH'S HOSPITAL   
#[Out]# 29             YAVAPAI REGIONAL MEDICAL CENTER   
#[Out]# ...                                        ...   
#[Out]# 157717                                     NaN   
#[Out]# 157718                                     NaN   
#[Out]# 157719                                     NaN   
#[Out]# 157720                                     NaN   
#[Out]# 157721                                     NaN   
#[Out]# 157722                                     NaN   
#[Out]# 157723                                     NaN   
#[Out]# 157724                                     NaN   
#[Out]# 157725                                     NaN   
#[Out]# 157726                                     NaN   
#[Out]# 157727                                     NaN   
#[Out]# 157728                                     NaN   
#[Out]# 157729                                     NaN   
#[Out]# 157730                                     NaN   
#[Out]# 157731                                     NaN   
#[Out]# 157732                                     NaN   
#[Out]# 157733                                     NaN   
#[Out]# 157734                                     NaN   
#[Out]# 157735                                     NaN   
#[Out]# 157736                                     NaN   
#[Out]# 157737                                     NaN   
#[Out]# 157738                                     NaN   
#[Out]# 157739                                     NaN   
#[Out]# 157740                                     NaN   
#[Out]# 157741                                     NaN   
#[Out]# 157742                                     NaN   
#[Out]# 157743                                     NaN   
#[Out]# 157744                                     NaN   
#[Out]# 157745                                     NaN   
#[Out]# 157746                                     NaN   
#[Out]# 
#[Out]#                     Provider Street Address Provider City Provider State  \
#[Out]# 0                    1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 1                2505 U S HIGHWAY 431 NORTH          BOAZ             AL   
#[Out]# 2                        205 MARENGO STREET      FLORENCE             AL   
#[Out]# 3                50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 4                   1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 5                 2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 6                          1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 7                    2000 PEPPERELL PARKWAY       OPELIKA             AL   
#[Out]# 8                     619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 9                             101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 10                     1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 11                   600 SOUTH THIRD STREET       GADSDEN             AL   
#[Out]# 12                    4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 13                   810 ST VINCENT'S DRIVE    BIRMINGHAM             AL   
#[Out]# 14                     400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 15                       1201 7TH STREET SE       DECATUR             AL   
#[Out]# 16                   6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 17            809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 18                        750 MORPHY AVENUE      FAIRHOPE             AL   
#[Out]# 19           701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 20                         800 MONTCLAIR RD    BIRMINGHAM             AL   
#[Out]# 21                5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 22      2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 23                         2801 DEBARR ROAD     ANCHORAGE             AK   
#[Out]# 24                  1111 EAST MCDOWELL ROAD       PHOENIX             AZ   
#[Out]# 25                     5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 26                     269 SOUTH CANDY LANE    COTTONWOOD             AZ   
#[Out]# 27                 1601 WEST ST MARY'S ROAD        TUCSON             AZ   
#[Out]# 28                    350 NORTH WILMOT ROAD        TUCSON             AZ   
#[Out]# 29                   1003 WILLOW CREEK ROAD      PRESCOTT             AZ   
#[Out]# ...                                     ...           ...            ...   
#[Out]# 157717                                  NaN           NaN            NaN   
#[Out]# 157718                                  NaN           NaN            NaN   
#[Out]# 157719                                  NaN           NaN            NaN   
#[Out]# 157720                                  NaN           NaN            NaN   
#[Out]# 157721                                  NaN           NaN            NaN   
#[Out]# 157722                                  NaN           NaN            NaN   
#[Out]# 157723                                  NaN           NaN            NaN   
#[Out]# 157724                                  NaN           NaN            NaN   
#[Out]# 157725                                  NaN           NaN            NaN   
#[Out]# 157726                                  NaN           NaN            NaN   
#[Out]# 157727                                  NaN           NaN            NaN   
#[Out]# 157728                                  NaN           NaN            NaN   
#[Out]# 157729                                  NaN           NaN            NaN   
#[Out]# 157730                                  NaN           NaN            NaN   
#[Out]# 157731                                  NaN           NaN            NaN   
#[Out]# 157732                                  NaN           NaN            NaN   
#[Out]# 157733                                  NaN           NaN            NaN   
#[Out]# 157734                                  NaN           NaN            NaN   
#[Out]# 157735                                  NaN           NaN            NaN   
#[Out]# 157736                                  NaN           NaN            NaN   
#[Out]# 157737                                  NaN           NaN            NaN   
#[Out]# 157738                                  NaN           NaN            NaN   
#[Out]# 157739                                  NaN           NaN            NaN   
#[Out]# 157740                                  NaN           NaN            NaN   
#[Out]# 157741                                  NaN           NaN            NaN   
#[Out]# 157742                                  NaN           NaN            NaN   
#[Out]# 157743                                  NaN           NaN            NaN   
#[Out]# 157744                                  NaN           NaN            NaN   
#[Out]# 157745                                  NaN           NaN            NaN   
#[Out]# 157746                                  NaN           NaN            NaN   
#[Out]# 
#[Out]#         Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                   36301                                AL - Dothan   
#[Out]# 1                   35957                            AL - Birmingham   
#[Out]# 2                   35631                            AL - Birmingham   
#[Out]# 3                   35235                            AL - Birmingham   
#[Out]# 4                   35007                            AL - Birmingham   
#[Out]# 5                   36116                            AL - Montgomery   
#[Out]# 6                   36106                            AL - Montgomery   
#[Out]# 7                   36801                            AL - Birmingham   
#[Out]# 8                   35233                            AL - Birmingham   
#[Out]# 9                   35801                            AL - Huntsville   
#[Out]# 10                  35903                            AL - Birmingham   
#[Out]# 11                  35901                            AL - Birmingham   
#[Out]# 12                  36305                                AL - Dothan   
#[Out]# 13                  35205                            AL - Birmingham   
#[Out]# 14                  36207                            AL - Birmingham   
#[Out]# 15                  35601                            AL - Huntsville   
#[Out]# 16                  36608                                AL - Mobile   
#[Out]# 17                  35401                            AL - Tuscaloosa   
#[Out]# 18                  36532                                AL - Mobile   
#[Out]# 19                  35211                            AL - Birmingham   
#[Out]# 20                  35213                            AL - Birmingham   
#[Out]# 21                  36652                                AL - Mobile   
#[Out]# 22                  35209                            AL - Birmingham   
#[Out]# 23                  99508                             AK - Anchorage   
#[Out]# 24                  85006                               AZ - Phoenix   
#[Out]# 25                  85712                                AZ - Tucson   
#[Out]# 26                  86326                               AZ - Phoenix   
#[Out]# 27                  85745                                AZ - Tucson   
#[Out]# 28                  85711                                AZ - Tucson   
#[Out]# 29                  86301                               AZ - Phoenix   
#[Out]# ...                   ...                                        ...   
#[Out]# 157717                NaN                                        NaN   
#[Out]# 157718                NaN                                        NaN   
#[Out]# 157719                NaN                                        NaN   
#[Out]# 157720                NaN                                        NaN   
#[Out]# 157721                NaN                                        NaN   
#[Out]# 157722                NaN                                        NaN   
#[Out]# 157723                NaN                                        NaN   
#[Out]# 157724                NaN                                        NaN   
#[Out]# 157725                NaN                                        NaN   
#[Out]# 157726                NaN                                        NaN   
#[Out]# 157727                NaN                                        NaN   
#[Out]# 157728                NaN                                        NaN   
#[Out]# 157729                NaN                                        NaN   
#[Out]# 157730                NaN                                        NaN   
#[Out]# 157731                NaN                                        NaN   
#[Out]# 157732                NaN                                        NaN   
#[Out]# 157733                NaN                                        NaN   
#[Out]# 157734                NaN                                        NaN   
#[Out]# 157735                NaN                                        NaN   
#[Out]# 157736                NaN                                        NaN   
#[Out]# 157737                NaN                                        NaN   
#[Out]# 157738                NaN                                        NaN   
#[Out]# 157739                NaN                                        NaN   
#[Out]# 157740                NaN                                        NaN   
#[Out]# 157741                NaN                                        NaN   
#[Out]# 157742                NaN                                        NaN   
#[Out]# 157743                NaN                                        NaN   
#[Out]# 157744                NaN                                        NaN   
#[Out]# 157745                NaN                                        NaN   
#[Out]# 157746                NaN                                        NaN   
#[Out]# 
#[Out]#         Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 0                     98              37988.97959             5872.193878   
#[Out]# 1                     34              15554.88235             6053.294118   
#[Out]# 2                     30              40380.10000             5319.833333   
#[Out]# 3                     17              22026.23529             5767.882353   
#[Out]# 4                     17              45663.05882             5785.117647   
#[Out]# 5                     44              16579.81818             6485.681818   
#[Out]# 6                     15              23243.73333             5553.866667   
#[Out]# 7                     46              11807.78261             6200.326087   
#[Out]# 8                     34              45288.23529             8977.323529   
#[Out]# 9                    111              33108.30631             5948.000000   
#[Out]# 10                    22              83400.09091             5942.272727   
#[Out]# 11                    14              81517.35714             5540.142857   
#[Out]# 12                    56              39789.98214             5498.857143   
#[Out]# 13                    42              21614.02381             5272.904762   
#[Out]# 14                    24              33509.83333             5423.458333   
#[Out]# 15                    26              15005.11538             5268.192308   
#[Out]# 16                    16              12574.06250             5975.812500   
#[Out]# 17                    40              22007.52500             6398.675000   
#[Out]# 18                    26              14847.00000             5103.153846   
#[Out]# 19                    22              48621.68182             6176.545455   
#[Out]# 20                    28              75521.96429             6282.285714   
#[Out]# 21                    53              16921.16981             5316.622642   
#[Out]# 22                    28             110405.75000             5877.321429   
#[Out]# 23                    38              41744.65789             8059.763158   
#[Out]# 24                    17              36434.82353             8223.235294   
#[Out]# 25                    24              27468.37500             7465.833333   
#[Out]# 26                    16              30707.87500             9331.437500   
#[Out]# 27                    14              35782.71429             6994.214286   
#[Out]# 28                    27              30702.70370             6577.148148   
#[Out]# 29                    29              29135.65517             6603.793103   
#[Out]# ...                  ...                      ...                     ...   
#[Out]# 157717               NaN                      NaN                     NaN   
#[Out]# 157718               NaN                      NaN                     NaN   
#[Out]# 157719               NaN                      NaN                     NaN   
#[Out]# 157720               NaN                      NaN                     NaN   
#[Out]# 157721               NaN                      NaN                     NaN   
#[Out]# 157722               NaN                      NaN                     NaN   
#[Out]# 157723               NaN                      NaN                     NaN   
#[Out]# 157724               NaN                      NaN                     NaN   
#[Out]# 157725               NaN                      NaN                     NaN   
#[Out]# 157726               NaN                      NaN                     NaN   
#[Out]# 157727               NaN                      NaN                     NaN   
#[Out]# 157728               NaN                      NaN                     NaN   
#[Out]# 157729               NaN                      NaN                     NaN   
#[Out]# 157730               NaN                      NaN                     NaN   
#[Out]# 157731               NaN                      NaN                     NaN   
#[Out]# 157732               NaN                      NaN                     NaN   
#[Out]# 157733               NaN                      NaN                     NaN   
#[Out]# 157734               NaN                      NaN                     NaN   
#[Out]# 157735               NaN                      NaN                     NaN   
#[Out]# 157736               NaN                      NaN                     NaN   
#[Out]# 157737               NaN                      NaN                     NaN   
#[Out]# 157738               NaN                      NaN                     NaN   
#[Out]# 157739               NaN                      NaN                     NaN   
#[Out]# 157740               NaN                      NaN                     NaN   
#[Out]# 157741               NaN                      NaN                     NaN   
#[Out]# 157742               NaN                      NaN                     NaN   
#[Out]# 157743               NaN                      NaN                     NaN   
#[Out]# 157744               NaN                      NaN                     NaN   
#[Out]# 157745               NaN                      NaN                     NaN   
#[Out]# 157746               NaN                      NaN                     NaN   
#[Out]# 
#[Out]#         Average Medicare Payments                                      proc  \
#[Out]# 0                     4838.316327  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 1                     5255.647059  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 2                     4150.866667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 3                     4268.352941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 4                     4886.294118  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 5                     5375.159091  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 6                     4376.600000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 7                     4766.891304  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 8                     6238.852941  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 9                     4780.081081  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 10                    4872.272727  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 11                    4120.214286  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 12                    4434.946429  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 13                    4257.309524  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 14                    4469.916667  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 15                    4138.230769  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 16                    3675.062500  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 17                    5366.225000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 18                    3815.961538  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 19                    5271.818182  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 20                    5187.357143  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 21                    4085.660377  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 22                    4380.892857  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 23                    6943.973684  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 24                    7115.470588  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 25                    6212.708333  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 26                    8105.125000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 27                    6154.500000  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 28                    5528.851852  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# 29                    5504.344828  039 - EXTRACRANIAL PROCEDURES W/O CC/MCC   
#[Out]# ...                           ...                                       ...   
#[Out]# 157717                        NaN                                       nan   
#[Out]# 157718                        NaN                                       nan   
#[Out]# 157719                        NaN                                       nan   
#[Out]# 157720                        NaN                                       nan   
#[Out]# 157721                        NaN                                       nan   
#[Out]# 157722                        NaN                                       nan   
#[Out]# 157723                        NaN                                       nan   
#[Out]# 157724                        NaN                                       nan   
#[Out]# 157725                        NaN                                       nan   
#[Out]# 157726                        NaN                                       nan   
#[Out]# 157727                        NaN                                       nan   
#[Out]# 157728                        NaN                                       nan   
#[Out]# 157729                        NaN                                       nan   
#[Out]# 157730                        NaN                                       nan   
#[Out]# 157731                        NaN                                       nan   
#[Out]# 157732                        NaN                                       nan   
#[Out]# 157733                        NaN                                       nan   
#[Out]# 157734                        NaN                                       nan   
#[Out]# 157735                        NaN                                       nan   
#[Out]# 157736                        NaN                                       nan   
#[Out]# 157737                        NaN                                       nan   
#[Out]# 157738                        NaN                                       nan   
#[Out]# 157739                        NaN                                       nan   
#[Out]# 157740                        NaN                                       nan   
#[Out]# 157741                        NaN                                       nan   
#[Out]# 157742                        NaN                                       nan   
#[Out]# 157743                        NaN                                       nan   
#[Out]# 157744                        NaN                                       nan   
#[Out]# 157745                        NaN                                       nan   
#[Out]# 157746                        NaN                                       nan   
#[Out]# 
#[Out]#        proc code     markup  
#[Out]# 0            039   7.851694  
#[Out]# 1            039   2.959651  
#[Out]# 2            039   9.728113  
#[Out]# 3            039   5.160359  
#[Out]# 4            039   9.345131  
#[Out]# 5            039   3.084526  
#[Out]# 6            039   5.310911  
#[Out]# 7            039   2.477040  
#[Out]# 8            039   7.259064  
#[Out]# 9            039   6.926306  
#[Out]# 10           039  17.117287  
#[Out]# 11           039  19.784737  
#[Out]# 12           039   8.971919  
#[Out]# 13           039   5.076921  
#[Out]# 14           039   7.496747  
#[Out]# 15           039   3.625974  
#[Out]# 16           039   3.421455  
#[Out]# 17           039   4.101119  
#[Out]# 18           039   3.890762  
#[Out]# 19           039   9.222944  
#[Out]# 20           039  14.558852  
#[Out]# 21           039   4.141600  
#[Out]# 22           039  25.201655  
#[Out]# 23           039   6.011638  
#[Out]# 24           039   5.120508  
#[Out]# 25           039   4.421321  
#[Out]# 26           039   3.788699  
#[Out]# 27           039   5.814073  
#[Out]# 28           039   5.553179  
#[Out]# 29           039   5.293210  
#[Out]# ...          ...        ...  
#[Out]# 157717       nan        NaN  
#[Out]# 157718       nan        NaN  
#[Out]# 157719       nan        NaN  
#[Out]# 157720       nan        NaN  
#[Out]# 157721       nan        NaN  
#[Out]# 157722       nan        NaN  
#[Out]# 157723       nan        NaN  
#[Out]# 157724       nan        NaN  
#[Out]# 157725       nan        NaN  
#[Out]# 157726       nan        NaN  
#[Out]# 157727       nan        NaN  
#[Out]# 157728       nan        NaN  
#[Out]# 157729       nan        NaN  
#[Out]# 157730       nan        NaN  
#[Out]# 157731       nan        NaN  
#[Out]# 157732       nan        NaN  
#[Out]# 157733       nan        NaN  
#[Out]# 157734       nan        NaN  
#[Out]# 157735       nan        NaN  
#[Out]# 157736       nan        NaN  
#[Out]# 157737       nan        NaN  
#[Out]# 157738       nan        NaN  
#[Out]# 157739       nan        NaN  
#[Out]# 157740       nan        NaN  
#[Out]# 157741       nan        NaN  
#[Out]# 157742       nan        NaN  
#[Out]# 157743       nan        NaN  
#[Out]# 157744       nan        NaN  
#[Out]# 157745       nan        NaN  
#[Out]# 157746       nan        NaN  
#[Out]# 
#[Out]# [157747 rows x 15 columns]
# Fri, 11 Sep 2015 10:48:37
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 10:48:41
data["markup"]
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 157717          NaN
#[Out]# 157718          NaN
#[Out]# 157719          NaN
#[Out]# 157720          NaN
#[Out]# 157721          NaN
#[Out]# 157722          NaN
#[Out]# 157723          NaN
#[Out]# 157724          NaN
#[Out]# 157725          NaN
#[Out]# 157726          NaN
#[Out]# 157727          NaN
#[Out]# 157728          NaN
#[Out]# 157729          NaN
#[Out]# 157730          NaN
#[Out]# 157731          NaN
#[Out]# 157732          NaN
#[Out]# 157733          NaN
#[Out]# 157734          NaN
#[Out]# 157735          NaN
#[Out]# 157736          NaN
#[Out]# 157737          NaN
#[Out]# 157738          NaN
#[Out]# 157739          NaN
#[Out]# 157740          NaN
#[Out]# 157741          NaN
#[Out]# 157742          NaN
#[Out]# 157743          NaN
#[Out]# 157744          NaN
#[Out]# 157745          NaN
#[Out]# 157746          NaN
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:49:05
plt.hist(data["markup"][data["proc code"]==305],bins=100)
# Fri, 11 Sep 2015 10:49:19
data["proc code"]
#[Out]# 0         039
#[Out]# 1         039
#[Out]# 2         039
#[Out]# 3         039
#[Out]# 4         039
#[Out]# 5         039
#[Out]# 6         039
#[Out]# 7         039
#[Out]# 8         039
#[Out]# 9         039
#[Out]# 10        039
#[Out]# 11        039
#[Out]# 12        039
#[Out]# 13        039
#[Out]# 14        039
#[Out]# 15        039
#[Out]# 16        039
#[Out]# 17        039
#[Out]# 18        039
#[Out]# 19        039
#[Out]# 20        039
#[Out]# 21        039
#[Out]# 22        039
#[Out]# 23        039
#[Out]# 24        039
#[Out]# 25        039
#[Out]# 26        039
#[Out]# 27        039
#[Out]# 28        039
#[Out]# 29        039
#[Out]#          ... 
#[Out]# 157717    nan
#[Out]# 157718    nan
#[Out]# 157719    nan
#[Out]# 157720    nan
#[Out]# 157721    nan
#[Out]# 157722    nan
#[Out]# 157723    nan
#[Out]# 157724    nan
#[Out]# 157725    nan
#[Out]# 157726    nan
#[Out]# 157727    nan
#[Out]# 157728    nan
#[Out]# 157729    nan
#[Out]# 157730    nan
#[Out]# 157731    nan
#[Out]# 157732    nan
#[Out]# 157733    nan
#[Out]# 157734    nan
#[Out]# 157735    nan
#[Out]# 157736    nan
#[Out]# 157737    nan
#[Out]# 157738    nan
#[Out]# 157739    nan
#[Out]# 157740    nan
#[Out]# 157741    nan
#[Out]# 157742    nan
#[Out]# 157743    nan
#[Out]# 157744    nan
#[Out]# 157745    nan
#[Out]# 157746    nan
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:49:25
data["proc code"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:49:44
data["proc code"]=data["proc code"].astype(str)
# Fri, 11 Sep 2015 10:49:47
data["proc code"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:50:02
a=data["proc code"].astype(str)
# Fri, 11 Sep 2015 10:50:03
a
#[Out]# 0         039
#[Out]# 1         039
#[Out]# 2         039
#[Out]# 3         039
#[Out]# 4         039
#[Out]# 5         039
#[Out]# 6         039
#[Out]# 7         039
#[Out]# 8         039
#[Out]# 9         039
#[Out]# 10        039
#[Out]# 11        039
#[Out]# 12        039
#[Out]# 13        039
#[Out]# 14        039
#[Out]# 15        039
#[Out]# 16        039
#[Out]# 17        039
#[Out]# 18        039
#[Out]# 19        039
#[Out]# 20        039
#[Out]# 21        039
#[Out]# 22        039
#[Out]# 23        039
#[Out]# 24        039
#[Out]# 25        039
#[Out]# 26        039
#[Out]# 27        039
#[Out]# 28        039
#[Out]# 29        039
#[Out]#          ... 
#[Out]# 157717    nan
#[Out]# 157718    nan
#[Out]# 157719    nan
#[Out]# 157720    nan
#[Out]# 157721    nan
#[Out]# 157722    nan
#[Out]# 157723    nan
#[Out]# 157724    nan
#[Out]# 157725    nan
#[Out]# 157726    nan
#[Out]# 157727    nan
#[Out]# 157728    nan
#[Out]# 157729    nan
#[Out]# 157730    nan
#[Out]# 157731    nan
#[Out]# 157732    nan
#[Out]# 157733    nan
#[Out]# 157734    nan
#[Out]# 157735    nan
#[Out]# 157736    nan
#[Out]# 157737    nan
#[Out]# 157738    nan
#[Out]# 157739    nan
#[Out]# 157740    nan
#[Out]# 157741    nan
#[Out]# 157742    nan
#[Out]# 157743    nan
#[Out]# 157744    nan
#[Out]# 157745    nan
#[Out]# 157746    nan
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:50:07
data["proc code"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:50:14
data["proc"].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: proc, dtype: object
# Fri, 11 Sep 2015 10:50:23
data["proc"].iloc[0]
#[Out]# '039 - EXTRACRANIAL PROCEDURES W/O CC/MCC'
# Fri, 11 Sep 2015 10:50:26
data["proc"].iloc[0][:3]
#[Out]# '039'
# Fri, 11 Sep 2015 10:50:43
data["proc code"]
#[Out]# 0         039
#[Out]# 1         039
#[Out]# 2         039
#[Out]# 3         039
#[Out]# 4         039
#[Out]# 5         039
#[Out]# 6         039
#[Out]# 7         039
#[Out]# 8         039
#[Out]# 9         039
#[Out]# 10        039
#[Out]# 11        039
#[Out]# 12        039
#[Out]# 13        039
#[Out]# 14        039
#[Out]# 15        039
#[Out]# 16        039
#[Out]# 17        039
#[Out]# 18        039
#[Out]# 19        039
#[Out]# 20        039
#[Out]# 21        039
#[Out]# 22        039
#[Out]# 23        039
#[Out]# 24        039
#[Out]# 25        039
#[Out]# 26        039
#[Out]# 27        039
#[Out]# 28        039
#[Out]# 29        039
#[Out]#          ... 
#[Out]# 157717    nan
#[Out]# 157718    nan
#[Out]# 157719    nan
#[Out]# 157720    nan
#[Out]# 157721    nan
#[Out]# 157722    nan
#[Out]# 157723    nan
#[Out]# 157724    nan
#[Out]# 157725    nan
#[Out]# 157726    nan
#[Out]# 157727    nan
#[Out]# 157728    nan
#[Out]# 157729    nan
#[Out]# 157730    nan
#[Out]# 157731    nan
#[Out]# 157732    nan
#[Out]# 157733    nan
#[Out]# 157734    nan
#[Out]# 157735    nan
#[Out]# 157736    nan
#[Out]# 157737    nan
#[Out]# 157738    nan
#[Out]# 157739    nan
#[Out]# 157740    nan
#[Out]# 157741    nan
#[Out]# 157742    nan
#[Out]# 157743    nan
#[Out]# 157744    nan
#[Out]# 157745    nan
#[Out]# 157746    nan
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:50:46
data["proc code"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:50:50
data["proc code"].head()
#[Out]# 0    039
#[Out]# 1    039
#[Out]# 2    039
#[Out]# 3    039
#[Out]# 4    039
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:51:12
a=data["proc code"][data["proc code"]=="305"]
# Fri, 11 Sep 2015 10:51:15
a=data["proc code"][data["proc code"]=="305"].copy()
# Fri, 11 Sep 2015 10:51:16
a
#[Out]# 65060    305
#[Out]# 65061    305
#[Out]# 65062    305
#[Out]# 65063    305
#[Out]# 65064    305
#[Out]# 65065    305
#[Out]# 65066    305
#[Out]# 65067    305
#[Out]# 65068    305
#[Out]# 65069    305
#[Out]# 65070    305
#[Out]# 65071    305
#[Out]# 65072    305
#[Out]# 65073    305
#[Out]# 65074    305
#[Out]# 65075    305
#[Out]# 65076    305
#[Out]# 65077    305
#[Out]# 65078    305
#[Out]# 65079    305
#[Out]# 65080    305
#[Out]# 65081    305
#[Out]# 65082    305
#[Out]# 65083    305
#[Out]# 65084    305
#[Out]# 65085    305
#[Out]# 65086    305
#[Out]# 65087    305
#[Out]# 65088    305
#[Out]# 65089    305
#[Out]#         ... 
#[Out]# 65999    305
#[Out]# 66000    305
#[Out]# 66001    305
#[Out]# 66002    305
#[Out]# 66003    305
#[Out]# 66004    305
#[Out]# 66005    305
#[Out]# 66006    305
#[Out]# 66007    305
#[Out]# 66008    305
#[Out]# 66009    305
#[Out]# 66010    305
#[Out]# 66011    305
#[Out]# 66012    305
#[Out]# 66013    305
#[Out]# 66014    305
#[Out]# 66015    305
#[Out]# 66016    305
#[Out]# 66017    305
#[Out]# 66018    305
#[Out]# 66019    305
#[Out]# 66020    305
#[Out]# 66021    305
#[Out]# 66022    305
#[Out]# 66023    305
#[Out]# 66024    305
#[Out]# 66025    305
#[Out]# 66026    305
#[Out]# 66027    305
#[Out]# 66028    305
#[Out]# Name: proc code, dtype: object
# Fri, 11 Sep 2015 10:51:24
a=data["markup"][data["proc code"]=="305"].copy()
# Fri, 11 Sep 2015 10:51:25
a
#[Out]# 65060     6.663608
#[Out]# 65061     8.381469
#[Out]# 65062    10.451970
#[Out]# 65063     7.363758
#[Out]# 65064     8.514054
#[Out]# 65065     3.602018
#[Out]# 65066     5.152709
#[Out]# 65067     4.609246
#[Out]# 65068     9.440464
#[Out]# 65069    15.544039
#[Out]# 65070     9.835780
#[Out]# 65071     3.878793
#[Out]# 65072     5.557330
#[Out]# 65073     5.513968
#[Out]# 65074     7.307482
#[Out]# 65075     2.413039
#[Out]# 65076     6.009232
#[Out]# 65077    19.977630
#[Out]# 65078     6.240598
#[Out]# 65079     4.988829
#[Out]# 65080     6.900532
#[Out]# 65081     3.765426
#[Out]# 65082    10.086110
#[Out]# 65083     4.306546
#[Out]# 65084     6.498016
#[Out]# 65085     9.209411
#[Out]# 65086     5.645931
#[Out]# 65087     6.575877
#[Out]# 65088     6.690510
#[Out]# 65089     6.727001
#[Out]#            ...    
#[Out]# 65999     8.544768
#[Out]# 66000     7.042084
#[Out]# 66001     7.596612
#[Out]# 66002     7.673572
#[Out]# 66003     6.019208
#[Out]# 66004     6.046108
#[Out]# 66005     3.218474
#[Out]# 66006     4.018800
#[Out]# 66007     5.451160
#[Out]# 66008     6.503976
#[Out]# 66009     2.857527
#[Out]# 66010     4.550774
#[Out]# 66011     3.469936
#[Out]# 66012     3.412576
#[Out]# 66013     6.216429
#[Out]# 66014     5.309429
#[Out]# 66015     4.097292
#[Out]# 66016     5.696044
#[Out]# 66017     4.068580
#[Out]# 66018     5.473017
#[Out]# 66019     5.369684
#[Out]# 66020     3.442493
#[Out]# 66021     4.664804
#[Out]# 66022     2.934560
#[Out]# 66023     8.659285
#[Out]# 66024    18.815193
#[Out]# 66025     9.117995
#[Out]# 66026    12.271003
#[Out]# 66027    15.089157
#[Out]# 66028     9.654588
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:51:29
len(a)
#[Out]# 969
# Fri, 11 Sep 2015 10:51:36
plt.hist(a,bins=100)
# Fri, 11 Sep 2015 10:52:03
plt.hist(np.asarray(a),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 10:52:19
a.describe()
#[Out]# count    969.000000
#[Out]# mean       6.577188
#[Out]# std        3.248230
#[Out]# min        1.015370
#[Out]# 25%        4.331689
#[Out]# 50%        6.019208
#[Out]# 75%        8.337765
#[Out]# max       19.977630
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 10:56:06
plt.ylabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb40dfb70>
# Fri, 11 Sep 2015 10:56:21
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb416d588>
# Fri, 11 Sep 2015 10:56:29
plt.ylabel("")
#[Out]# <matplotlib.text.Text at 0x7fefb40dfb70>
# Fri, 11 Sep 2015 10:58:52
plt.title("Histogram of markups for inpatient hypertension codes")
#[Out]# <matplotlib.text.Text at 0x7fefb40f8ef0>
# Fri, 11 Sep 2015 10:59:13
plt.savefig("hypertension histogram")
# Fri, 11 Sep 2015 11:00:38
diabetesmcc=data[data["proc code"]=="637"]
# Fri, 11 Sep 2015 11:00:44
diabetescc=data[data["proc code"]=="638"]
# Fri, 11 Sep 2015 11:00:50
diabetesmcc
#[Out]#               DRG Definition  Provider Id  \
#[Out]# 118210  637 - DIABETES W MCC        10001   
#[Out]# 118211  637 - DIABETES W MCC        10006   
#[Out]# 118212  637 - DIABETES W MCC        10011   
#[Out]# 118213  637 - DIABETES W MCC        10019   
#[Out]# 118214  637 - DIABETES W MCC        10024   
#[Out]# 118215  637 - DIABETES W MCC        10033   
#[Out]# 118216  637 - DIABETES W MCC        10035   
#[Out]# 118217  637 - DIABETES W MCC        10039   
#[Out]# 118218  637 - DIABETES W MCC        10056   
#[Out]# 118219  637 - DIABETES W MCC        10092   
#[Out]# 118220  637 - DIABETES W MCC        10103   
#[Out]# 118221  637 - DIABETES W MCC        10113   
#[Out]# 118222  637 - DIABETES W MCC        10118   
#[Out]# 118223  637 - DIABETES W MCC        10144   
#[Out]# 118224  637 - DIABETES W MCC        20001   
#[Out]# 118225  637 - DIABETES W MCC        30010   
#[Out]# 118226  637 - DIABETES W MCC        30055   
#[Out]# 118227  637 - DIABETES W MCC        30064   
#[Out]# 118228  637 - DIABETES W MCC        30088   
#[Out]# 118229  637 - DIABETES W MCC        30093   
#[Out]# 118230  637 - DIABETES W MCC        30115   
#[Out]# 118231  637 - DIABETES W MCC        30122   
#[Out]# 118232  637 - DIABETES W MCC        40007   
#[Out]# 118233  637 - DIABETES W MCC        40014   
#[Out]# 118234  637 - DIABETES W MCC        40016   
#[Out]# 118235  637 - DIABETES W MCC        40029   
#[Out]# 118236  637 - DIABETES W MCC        40036   
#[Out]# 118237  637 - DIABETES W MCC        40055   
#[Out]# 118238  637 - DIABETES W MCC        40071   
#[Out]# 118239  637 - DIABETES W MCC        40114   
#[Out]# ...                      ...          ...   
#[Out]# 118949  637 - DIABETES W MCC       490093   
#[Out]# 118950  637 - DIABETES W MCC       490112   
#[Out]# 118951  637 - DIABETES W MCC       490118   
#[Out]# 118952  637 - DIABETES W MCC       490120   
#[Out]# 118953  637 - DIABETES W MCC       500014   
#[Out]# 118954  637 - DIABETES W MCC       500024   
#[Out]# 118955  637 - DIABETES W MCC       500027   
#[Out]# 118956  637 - DIABETES W MCC       500030   
#[Out]# 118957  637 - DIABETES W MCC       500036   
#[Out]# 118958  637 - DIABETES W MCC       500039   
#[Out]# 118959  637 - DIABETES W MCC       500050   
#[Out]# 118960  637 - DIABETES W MCC       500079   
#[Out]# 118961  637 - DIABETES W MCC       500088   
#[Out]# 118962  637 - DIABETES W MCC       500108   
#[Out]# 118963  637 - DIABETES W MCC       500129   
#[Out]# 118964  637 - DIABETES W MCC       500150   
#[Out]# 118965  637 - DIABETES W MCC       510001   
#[Out]# 118966  637 - DIABETES W MCC       510007   
#[Out]# 118967  637 - DIABETES W MCC       510022   
#[Out]# 118968  637 - DIABETES W MCC       510058   
#[Out]# 118969  637 - DIABETES W MCC       510070   
#[Out]# 118970  637 - DIABETES W MCC       520008   
#[Out]# 118971  637 - DIABETES W MCC       520013   
#[Out]# 118972  637 - DIABETES W MCC       520066   
#[Out]# 118973  637 - DIABETES W MCC       520070   
#[Out]# 118974  637 - DIABETES W MCC       520083   
#[Out]# 118975  637 - DIABETES W MCC       520087   
#[Out]# 118976  637 - DIABETES W MCC       520096   
#[Out]# 118977  637 - DIABETES W MCC       520138   
#[Out]# 118978  637 - DIABETES W MCC       520177   
#[Out]# 
#[Out]#                                             Provider Name  \
#[Out]# 118210                   SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 118211                     ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 118212                                  ST VINCENT'S EAST   
#[Out]# 118213                     HELEN KELLER MEMORIAL HOSPITAL   
#[Out]# 118214                      JACKSON HOSPITAL & CLINIC INC   
#[Out]# 118215                     UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 118216                    CULLMAN REGIONAL MEDICAL CENTER   
#[Out]# 118217                                HUNTSVILLE HOSPITAL   
#[Out]# 118218                            ST VINCENT'S BIRMINGHAM   
#[Out]# 118219                      D C H REGIONAL MEDICAL CENTER   
#[Out]# 118220                   BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 118221                                   MOBILE INFIRMARY   
#[Out]# 118222     VAUGHAN REGIONAL MEDICAL CENTER PARKWAY CAMPUS   
#[Out]# 118223                          SPRINGHILL MEDICAL CENTER   
#[Out]# 118224                   PROVIDENCE ALASKA MEDICAL CENTER   
#[Out]# 118225                      CARONDELET ST  MARYS HOSPITAL   
#[Out]# 118226                    KINGMAN REGIONAL MEDICAL CENTER   
#[Out]# 118227  UNIVERSITY OF ARIZONA MEDICAL CTR-UNIVERSITY, THE   
#[Out]# 118228                      BANNER BAYWOOD MEDICAL CENTER   
#[Out]# 118229                   BANNER DEL E WEBB MEDICAL CENTER   
#[Out]# 118230                     BANNER ESTRELLA MEDICAL CENTER   
#[Out]# 118231                      BANNER GATEWAY MEDICAL CENTER   
#[Out]# 118232                ST VINCENT INFIRMARY MEDICAL CENTER   
#[Out]# 118233                        WHITE COUNTY MEDICAL CENTER   
#[Out]# 118234                                UAMS MEDICAL CENTER   
#[Out]# 118235                     CONWAY REGIONAL MEDICAL CENTER   
#[Out]# 118236    BAPTIST HEALTH MEDICAL CENTER NORTH LITTLE ROCK   
#[Out]# 118237                     SPARKS REGIONAL MEDICAL CENTER   
#[Out]# 118238                  JEFFERSON REGIONAL MEDICAL CENTER   
#[Out]# 118239          BAPTIST HEALTH MEDICAL CENTER-LITTLE ROCK   
#[Out]# ...                                                   ...   
#[Out]# 118949                          SENTARA CAREPLEX HOSPITAL   
#[Out]# 118950                                 CJW MEDICAL CENTER   
#[Out]# 118951                          HENRICO DOCTORS' HOSPITAL   
#[Out]# 118952                        CHESAPEAKE GENERAL HOSPITAL   
#[Out]# 118953         PROVIDENCE REGIONAL MEDICAL CENTER EVERETT   
#[Out]# 118954                       PROVIDENCE ST PETER HOSPITAL   
#[Out]# 118955                             SWEDISH MEDICAL CENTER   
#[Out]# 118956               PEACEHEALTH ST JOSEPH MEDICAL CENTER   
#[Out]# 118957                    YAKIMA VALLEY MEMORIAL HOSPITAL   
#[Out]# 118958                           HARRISON MEMORIAL CENTER   
#[Out]# 118959               PEACEHEALTH SOUTHWEST MEDICAL CENTER   
#[Out]# 118960                  MULTICARE GOOD SAMARITAN HOSPITAL   
#[Out]# 118961                              VALLEY MEDICAL CENTER   
#[Out]# 118962                           ST JOSEPH MEDICAL CENTER   
#[Out]# 118963                  TACOMA GENERAL ALLENMORE HOSPITAL   
#[Out]# 118964                 LEGACY SALMON CREEK MEDICAL CENTER   
#[Out]# 118965                 WEST VIRGINIA UNIVERSITY HOSPITALS   
#[Out]# 118966                           ST MARY'S MEDICAL CENTER   
#[Out]# 118967                     CHARLESTON AREA MEDICAL CENTER   
#[Out]# 118968                        CAMDEN CLARK MEDICAL CENTER   
#[Out]# 118969                           RALEIGH GENERAL HOSPITAL   
#[Out]# 118970                         WAUKESHA MEMORIAL HOSPITAL   
#[Out]# 118971                              SACRED HEART HOSPITAL   
#[Out]# 118972                           MERCY HEALTH SYSTEM CORP   
#[Out]# 118973      MAYO CLINIC HEALTH SYSTEM EAU CLAIRE HOSPITAL   
#[Out]# 118974                                  ST MARYS HOSPITAL   
#[Out]# 118975                     GUNDERSEN LUTHERAN MEDICAL CTR   
#[Out]# 118976          WHEATON FRANCISCAN HEALTHCARE  ALL SAINTS   
#[Out]# 118977                     AURORA ST LUKES MEDICAL CENTER   
#[Out]# 118978                       FROEDTERT MEM LUTHERAN HSPTL   
#[Out]# 
#[Out]#                        Provider Street Address    Provider City  \
#[Out]# 118210                  1108 ROSS CLARK CIRCLE           DOTHAN   
#[Out]# 118211                      205 MARENGO STREET         FLORENCE   
#[Out]# 118212              50 MEDICAL PARK EAST DRIVE       BIRMINGHAM   
#[Out]# 118213            1300 SOUTH MONTGOMERY AVENUE        SHEFFIELD   
#[Out]# 118214                        1725 PINE STREET       MONTGOMERY   
#[Out]# 118215                   619 SOUTH 19TH STREET       BIRMINGHAM   
#[Out]# 118216                1912 ALABAMA HIGHWAY 157          CULLMAN   
#[Out]# 118217                           101 SIVLEY RD       HUNTSVILLE   
#[Out]# 118218                  810 ST VINCENT'S DRIVE       BIRMINGHAM   
#[Out]# 118219           809 UNIVERSITY BOULEVARD EAST       TUSCALOOSA   
#[Out]# 118220          701 PRINCETON AVENUE SOUTHWEST       BIRMINGHAM   
#[Out]# 118221               5 MOBILE INFIRMARY CIRCLE           MOBILE   
#[Out]# 118222             1015 MEDICAL CENTER PARKWAY            SELMA   
#[Out]# 118223                     3719 DAUPHIN STREET           MOBILE   
#[Out]# 118224                              BOX 196604        ANCHORAGE   
#[Out]# 118225                1601 WEST ST MARY'S ROAD           TUCSON   
#[Out]# 118226                 3269 STOCKTON HILL ROAD          KINGMAN   
#[Out]# 118227              1501 NORTH CAMPBELL AVENUE           TUCSON   
#[Out]# 118228                6644 EAST BAYWOOD AVENUE             MESA   
#[Out]# 118229             14502 WEST MEEKER BOULEVARD    SUN CITY WEST   
#[Out]# 118230                   9201 WEST THOMAS ROAD          PHOENIX   
#[Out]# 118231                  1900 NORTH HIGLEY ROAD          GILBERT   
#[Out]# 118232                   TWO ST VINCENT CIRCLE      LITTLE ROCK   
#[Out]# 118233                   3214 EAST RACE AVENUE           SEARCY   
#[Out]# 118234  4301 WEST MARKHAM STREET MAIL SLOT 612      LITTLE ROCK   
#[Out]# 118235                     2302 COLLEGE AVENUE           CONWAY   
#[Out]# 118236                   3333 SPRINGHILL DRIVE  NORTH LITTLE RO   
#[Out]# 118237                      1001 TOWSON AVENUE       FORT SMITH   
#[Out]# 118238                   1600 WEST 40TH AVENUE       PINE BLUFF   
#[Out]# 118239             9601 INTERSTATE 630, EXIT 7      LITTLE ROCK   
#[Out]# ...                                        ...              ...   
#[Out]# 118949                     3000 COLISEUM DRIVE          HAMPTON   
#[Out]# 118950                        7101 JAHNKE ROAD         RICHMOND   
#[Out]# 118951                      1602 SKIPWITH ROAD         RICHMOND   
#[Out]# 118952             736 BATTLEFIELD BLVD, NORTH       CHESAPEAKE   
#[Out]# 118953                       1321 COLBY AVENUE          EVERETT   
#[Out]# 118954                       413 LILLY ROAD NE          OLYMPIA   
#[Out]# 118955                            747 BROADWAY          SEATTLE   
#[Out]# 118956                  2901 SQUALICUM PARKWAY       BELLINGHAM   
#[Out]# 118957                       2811 TIETON DRIVE           YAKIMA   
#[Out]# 118958                      2520 CHERRY AVENUE        BREMERTON   
#[Out]# 118959              400 NE MOTHER JOSEPH PLACE        VANCOUVER   
#[Out]# 118960                      401 15TH AVENUE SE         PUYALLUP   
#[Out]# 118961                           400 S 43RD ST           RENTON   
#[Out]# 118962                     1717 SOUTH J STREET           TACOMA   
#[Out]# 118963                        315 S MLK JR WAY           TACOMA   
#[Out]# 118964                    2211 NE 139TH STREET        VANCOUVER   
#[Out]# 118965                    MEDICAL CENTER DRIVE       MORGANTOWN   
#[Out]# 118966                         2900 1ST AVENUE       HUNTINGTON   
#[Out]# 118967                       501 MORRIS STREET       CHARLESTON   
#[Out]# 118968                        800 GARFIELD AVE      PARKERSBURG   
#[Out]# 118969                        1710 HARPER ROAD          BECKLEY   
#[Out]# 118970                        725 AMERICAN AVE         WAUKESHA   
#[Out]# 118971                    900 W CLAIREMONT AVE       EAU CLAIRE   
#[Out]# 118972                  1000 MINERAL POINT AVE       JANESVILLE   
#[Out]# 118973                         1221 WHIPPLE ST       EAU CLAIRE   
#[Out]# 118974                       700 SOUTH PARK ST          MADISON   
#[Out]# 118975                          1910 SOUTH AVE        LA CROSSE   
#[Out]# 118976                          3801 SPRING ST           RACINE   
#[Out]# 118977                     2900 W OKLAHOMA AVE        MILWAUKEE   
#[Out]# 118978                    9200 W WISCONSIN AVE        MILWAUKEE   
#[Out]# 
#[Out]#        Provider State  Provider Zip Code  \
#[Out]# 118210             AL              36301   
#[Out]# 118211             AL              35631   
#[Out]# 118212             AL              35235   
#[Out]# 118213             AL              35660   
#[Out]# 118214             AL              36106   
#[Out]# 118215             AL              35233   
#[Out]# 118216             AL              35058   
#[Out]# 118217             AL              35801   
#[Out]# 118218             AL              35205   
#[Out]# 118219             AL              35401   
#[Out]# 118220             AL              35211   
#[Out]# 118221             AL              36652   
#[Out]# 118222             AL              36701   
#[Out]# 118223             AL              36608   
#[Out]# 118224             AK              99519   
#[Out]# 118225             AZ              85745   
#[Out]# 118226             AZ              86409   
#[Out]# 118227             AZ              85724   
#[Out]# 118228             AZ              85206   
#[Out]# 118229             AZ              85375   
#[Out]# 118230             AZ              85037   
#[Out]# 118231             AZ              85234   
#[Out]# 118232             AR              72205   
#[Out]# 118233             AR              72143   
#[Out]# 118234             AR              72205   
#[Out]# 118235             AR              72034   
#[Out]# 118236             AR              72117   
#[Out]# 118237             AR              72901   
#[Out]# 118238             AR              71603   
#[Out]# 118239             AR              72205   
#[Out]# ...               ...                ...   
#[Out]# 118949             VA              23666   
#[Out]# 118950             VA              23235   
#[Out]# 118951             VA              23229   
#[Out]# 118952             VA              23320   
#[Out]# 118953             WA              98201   
#[Out]# 118954             WA              98506   
#[Out]# 118955             WA              98122   
#[Out]# 118956             WA              98225   
#[Out]# 118957             WA              98902   
#[Out]# 118958             WA              98310   
#[Out]# 118959             WA              98668   
#[Out]# 118960             WA              98372   
#[Out]# 118961             WA              98055   
#[Out]# 118962             WA              98405   
#[Out]# 118963             WA              98415   
#[Out]# 118964             WA              98686   
#[Out]# 118965             WV              26506   
#[Out]# 118966             WV              25701   
#[Out]# 118967             WV              25301   
#[Out]# 118968             WV              26101   
#[Out]# 118969             WV              25801   
#[Out]# 118970             WI              53188   
#[Out]# 118971             WI              54701   
#[Out]# 118972             WI              53548   
#[Out]# 118973             WI              54703   
#[Out]# 118974             WI              53715   
#[Out]# 118975             WI              54601   
#[Out]# 118976             WI              53405   
#[Out]# 118977             WI              53215   
#[Out]# 118978             WI              53226   
#[Out]# 
#[Out]#        Hospital Referral Region (HRR) Description  Total Discharges  \
#[Out]# 118210                                AL - Dothan                31   
#[Out]# 118211                            AL - Birmingham                14   
#[Out]# 118212                            AL - Birmingham                15   
#[Out]# 118213                            AL - Birmingham                11   
#[Out]# 118214                            AL - Montgomery                15   
#[Out]# 118215                            AL - Birmingham                24   
#[Out]# 118216                            AL - Birmingham                15   
#[Out]# 118217                            AL - Huntsville                58   
#[Out]# 118218                            AL - Birmingham                17   
#[Out]# 118219                            AL - Tuscaloosa                39   
#[Out]# 118220                            AL - Birmingham                11   
#[Out]# 118221                                AL - Mobile                24   
#[Out]# 118222                            AL - Birmingham                11   
#[Out]# 118223                                AL - Mobile                17   
#[Out]# 118224                             AK - Anchorage                11   
#[Out]# 118225                                AZ - Tucson                14   
#[Out]# 118226                               AZ - Phoenix                11   
#[Out]# 118227                                AZ - Tucson                15   
#[Out]# 118228                                  AZ - Mesa                16   
#[Out]# 118229                              AZ - Sun City                13   
#[Out]# 118230                               AZ - Phoenix                14   
#[Out]# 118231                                  AZ - Mesa                11   
#[Out]# 118232                           AR - Little Rock                17   
#[Out]# 118233                           AR - Little Rock                11   
#[Out]# 118234                           AR - Little Rock                11   
#[Out]# 118235                           AR - Little Rock                13   
#[Out]# 118236                           AR - Little Rock                21   
#[Out]# 118237                            AR - Fort Smith                20   
#[Out]# 118238                           AR - Little Rock                21   
#[Out]# 118239                           AR - Little Rock                28   
#[Out]# ...                                           ...               ...   
#[Out]# 118949                          VA - Newport News                17   
#[Out]# 118950                              VA - Richmond                26   
#[Out]# 118951                              VA - Richmond                19   
#[Out]# 118952                               VA - Norfolk                27   
#[Out]# 118953                               WA - Everett                18   
#[Out]# 118954                               WA - Olympia                23   
#[Out]# 118955                               WA - Seattle                12   
#[Out]# 118956                               WA - Seattle                12   
#[Out]# 118957                                WA - Yakima                14   
#[Out]# 118958                               WA - Seattle                25   
#[Out]# 118959                              OR - Portland                22   
#[Out]# 118960                                WA - Tacoma                20   
#[Out]# 118961                               WA - Seattle                15   
#[Out]# 118962                                WA - Tacoma                11   
#[Out]# 118963                                WA - Tacoma                23   
#[Out]# 118964                              OR - Portland                11   
#[Out]# 118965                            WV - Morgantown                31   
#[Out]# 118966                            WV - Huntington                12   
#[Out]# 118967                            WV - Charleston                43   
#[Out]# 118968                            WV - Charleston                16   
#[Out]# 118969                            WV - Charleston                16   
#[Out]# 118970                             WI - Milwaukee                12   
#[Out]# 118971                           MN - Minneapolis                13   
#[Out]# 118972                               WI - Madison                12   
#[Out]# 118973                           MN - Minneapolis                11   
#[Out]# 118974                               WI - Madison                16   
#[Out]# 118975                             WI - La Crosse                11   
#[Out]# 118976                             WI - Milwaukee                24   
#[Out]# 118977                             WI - Milwaukee                34   
#[Out]# 118978                             WI - Milwaukee                17   
#[Out]# 
#[Out]#         Average Covered Charges  Average Total Payments  \
#[Out]# 118210              29462.12903             7737.774194   
#[Out]# 118211              26931.85714             6974.928571   
#[Out]# 118212              40459.46667             8283.000000   
#[Out]# 118213              27278.18182             8405.272727   
#[Out]# 118214              33632.80000             8212.133333   
#[Out]# 118215              59158.75000            15207.708330   
#[Out]# 118216              17483.06667             8982.733333   
#[Out]# 118217              43692.67241             9056.517241   
#[Out]# 118218              37751.17647             8102.764706   
#[Out]# 118219              32560.23077             8854.410256   
#[Out]# 118220              68807.90909             8916.727273   
#[Out]# 118221              35092.04167            10654.875000   
#[Out]# 118222              29857.09091             9278.818182   
#[Out]# 118223              40478.17647            11323.411760   
#[Out]# 118224             103164.45450            25415.909090   
#[Out]# 118225              30065.35714             9456.785714   
#[Out]# 118226              28193.63636            10332.818180   
#[Out]# 118227              60919.20000            17331.133330   
#[Out]# 118228              32055.81250             8436.437500   
#[Out]# 118229              42202.15385             8430.153846   
#[Out]# 118230              46562.85714             9991.928571   
#[Out]# 118231              37176.18182             9381.090909   
#[Out]# 118232              32791.76471             8398.117647   
#[Out]# 118233              25286.27273             7854.909091   
#[Out]# 118234              27459.63636            11371.272730   
#[Out]# 118235              32181.00000            11356.846150   
#[Out]# 118236              26999.71429             7920.476190   
#[Out]# 118237              28739.80000             8168.300000   
#[Out]# 118238              34552.57143             9227.571429   
#[Out]# 118239              29684.50000             8216.857143   
#[Out]# ...                         ...                     ...   
#[Out]# 118949              30166.41176             8224.529412   
#[Out]# 118950              99748.00000            10440.807690   
#[Out]# 118951              65802.94737             9636.578947   
#[Out]# 118952              18402.44444             8094.925926   
#[Out]# 118953              32093.22222             9602.166667   
#[Out]# 118954              41304.43478             9883.956522   
#[Out]# 118955              30224.91667            11219.833330   
#[Out]# 118956              18992.33333            10465.166670   
#[Out]# 118957              19035.85714             9888.928571   
#[Out]# 118958              41658.20000            10068.280000   
#[Out]# 118959              36155.27273            11435.954550   
#[Out]# 118960              46733.15000            10265.550000   
#[Out]# 118961              44851.40000            13392.666670   
#[Out]# 118962              54797.72727            10335.454550   
#[Out]# 118963              38493.04348            11501.695650   
#[Out]# 118964              27526.18182            10437.090910   
#[Out]# 118965              50819.29032            19578.096770   
#[Out]# 118966              19948.66667             9083.666667   
#[Out]# 118967              35486.37209            10156.023260   
#[Out]# 118968              18825.81250             7372.875000   
#[Out]# 118969              17441.31250             8128.062500   
#[Out]# 118970              27791.58333             9063.250000   
#[Out]# 118971              27296.23077             9779.153846   
#[Out]# 118972              35253.66667             9837.916667   
#[Out]# 118973              27840.36364             8547.818182   
#[Out]# 118974              26362.68750             9584.125000   
#[Out]# 118975              40713.90909            12092.363640   
#[Out]# 118976              22957.54167             8958.541667   
#[Out]# 118977              29834.79412            10192.676470   
#[Out]# 118978              34435.41176            12461.823530   
#[Out]# 
#[Out]#         Average Medicare Payments                  proc proc code     markup  
#[Out]# 118210                6928.870968  637 - DIABETES W MCC       637   4.252082  
#[Out]# 118211                6189.857143  637 - DIABETES W MCC       637   4.350966  
#[Out]# 118212                7813.066667  637 - DIABETES W MCC       637   5.178436  
#[Out]# 118213                6498.818182  637 - DIABETES W MCC       637   4.197407  
#[Out]# 118214                7000.266667  637 - DIABETES W MCC       637   4.804503  
#[Out]# 118215                9743.500000  637 - DIABETES W MCC       637   6.071612  
#[Out]# 118216                8195.266667  637 - DIABETES W MCC       637   2.133313  
#[Out]# 118217                7568.793103  637 - DIABETES W MCC       637   5.772740  
#[Out]# 118218                7341.470588  637 - DIABETES W MCC       637   5.142182  
#[Out]# 118219                7928.051282  637 - DIABETES W MCC       637   4.106965  
#[Out]# 118220                8197.818182  637 - DIABETES W MCC       637   8.393442  
#[Out]# 118221                7833.791667  637 - DIABETES W MCC       637   4.479573  
#[Out]# 118222                8178.090909  637 - DIABETES W MCC       637   3.650863  
#[Out]# 118223                9643.411765  637 - DIABETES W MCC       637   4.197495  
#[Out]# 118224               19971.454550  637 - DIABETES W MCC       637   5.165595  
#[Out]# 118225                8699.642857  637 - DIABETES W MCC       637   3.455930  
#[Out]# 118226                9588.090909  637 - DIABETES W MCC       637   2.940485  
#[Out]# 118227               13496.066670  637 - DIABETES W MCC       637   4.513848  
#[Out]# 118228                7772.187500  637 - DIABETES W MCC       637   4.124426  
#[Out]# 118229                7515.615385  637 - DIABETES W MCC       637   5.615263  
#[Out]# 118230                9467.357143  637 - DIABETES W MCC       637   4.918253  
#[Out]# 118231                8955.636364  637 - DIABETES W MCC       637   4.151149  
#[Out]# 118232                7582.352941  637 - DIABETES W MCC       637   4.324748  
#[Out]# 118233                7109.090909  637 - DIABETES W MCC       637   3.556893  
#[Out]# 118234               10338.545450  637 - DIABETES W MCC       637   2.656044  
#[Out]# 118235               10139.153850  637 - DIABETES W MCC       637   3.173933  
#[Out]# 118236                6743.190476  637 - DIABETES W MCC       637   4.003997  
#[Out]# 118237                7410.900000  637 - DIABETES W MCC       637   3.878045  
#[Out]# 118238                8132.047619  637 - DIABETES W MCC       637   4.248939  
#[Out]# 118239                6396.714286  637 - DIABETES W MCC       637   4.640586  
#[Out]# ...                           ...                   ...       ...        ...  
#[Out]# 118949                7410.117647  637 - DIABETES W MCC       637   4.070976  
#[Out]# 118950                9577.346154  637 - DIABETES W MCC       637  10.414994  
#[Out]# 118951                8488.947368  637 - DIABETES W MCC       637   7.751603  
#[Out]# 118952                6769.555556  637 - DIABETES W MCC       637   2.718412  
#[Out]# 118953                9210.611111  637 - DIABETES W MCC       637   3.484375  
#[Out]# 118954                8918.608696  637 - DIABETES W MCC       637   4.631264  
#[Out]# 118955               10336.500000  637 - DIABETES W MCC       637   2.924096  
#[Out]# 118956                9389.666667  637 - DIABETES W MCC       637   2.022685  
#[Out]# 118957                9064.785714  637 - DIABETES W MCC       637   2.099979  
#[Out]# 118958                8832.240000  637 - DIABETES W MCC       637   4.716606  
#[Out]# 118959               10631.000000  637 - DIABETES W MCC       637   3.400929  
#[Out]# 118960                9785.550000  637 - DIABETES W MCC       637   4.775731  
#[Out]# 118961               11551.266670  637 - DIABETES W MCC       637   3.882812  
#[Out]# 118962                9328.181818  637 - DIABETES W MCC       637   5.874427  
#[Out]# 118963               10536.260870  637 - DIABETES W MCC       637   3.653387  
#[Out]# 118964                9581.090909  637 - DIABETES W MCC       637   2.872969  
#[Out]# 118965               10480.516130  637 - DIABETES W MCC       637   4.848930  
#[Out]# 118966                7485.750000  637 - DIABETES W MCC       637   2.664886  
#[Out]# 118967                8533.813953  637 - DIABETES W MCC       637   4.158325  
#[Out]# 118968                6829.000000  637 - DIABETES W MCC       637   2.756745  
#[Out]# 118969                6900.937500  637 - DIABETES W MCC       637   2.527383  
#[Out]# 118970                8341.500000  637 - DIABETES W MCC       637   3.331725  
#[Out]# 118971                8897.076923  637 - DIABETES W MCC       637   3.068000  
#[Out]# 118972                9039.333333  637 - DIABETES W MCC       637   3.900030  
#[Out]# 118973                7671.090909  637 - DIABETES W MCC       637   3.629257  
#[Out]# 118974                8601.125000  637 - DIABETES W MCC       637   3.065028  
#[Out]# 118975               11199.545450  637 - DIABETES W MCC       637   3.635318  
#[Out]# 118976                8114.500000  637 - DIABETES W MCC       637   2.829200  
#[Out]# 118977                8836.176471  637 - DIABETES W MCC       637   3.376437  
#[Out]# 118978               10241.647060  637 - DIABETES W MCC       637   3.362292  
#[Out]# 
#[Out]# [769 rows x 15 columns]
# Fri, 11 Sep 2015 11:00:55
diabetesmcc["markup"]
#[Out]# 118210     4.252082
#[Out]# 118211     4.350966
#[Out]# 118212     5.178436
#[Out]# 118213     4.197407
#[Out]# 118214     4.804503
#[Out]# 118215     6.071612
#[Out]# 118216     2.133313
#[Out]# 118217     5.772740
#[Out]# 118218     5.142182
#[Out]# 118219     4.106965
#[Out]# 118220     8.393442
#[Out]# 118221     4.479573
#[Out]# 118222     3.650863
#[Out]# 118223     4.197495
#[Out]# 118224     5.165595
#[Out]# 118225     3.455930
#[Out]# 118226     2.940485
#[Out]# 118227     4.513848
#[Out]# 118228     4.124426
#[Out]# 118229     5.615263
#[Out]# 118230     4.918253
#[Out]# 118231     4.151149
#[Out]# 118232     4.324748
#[Out]# 118233     3.556893
#[Out]# 118234     2.656044
#[Out]# 118235     3.173933
#[Out]# 118236     4.003997
#[Out]# 118237     3.878045
#[Out]# 118238     4.248939
#[Out]# 118239     4.640586
#[Out]#             ...    
#[Out]# 118949     4.070976
#[Out]# 118950    10.414994
#[Out]# 118951     7.751603
#[Out]# 118952     2.718412
#[Out]# 118953     3.484375
#[Out]# 118954     4.631264
#[Out]# 118955     2.924096
#[Out]# 118956     2.022685
#[Out]# 118957     2.099979
#[Out]# 118958     4.716606
#[Out]# 118959     3.400929
#[Out]# 118960     4.775731
#[Out]# 118961     3.882812
#[Out]# 118962     5.874427
#[Out]# 118963     3.653387
#[Out]# 118964     2.872969
#[Out]# 118965     4.848930
#[Out]# 118966     2.664886
#[Out]# 118967     4.158325
#[Out]# 118968     2.756745
#[Out]# 118969     2.527383
#[Out]# 118970     3.331725
#[Out]# 118971     3.068000
#[Out]# 118972     3.900030
#[Out]# 118973     3.629257
#[Out]# 118974     3.065028
#[Out]# 118975     3.635318
#[Out]# 118976     2.829200
#[Out]# 118977     3.376437
#[Out]# 118978     3.362292
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:01:00
plt.clf()
# Fri, 11 Sep 2015 11:01:14
plt.hist(diabetesmcc["markup"],bins=100)
# Fri, 11 Sep 2015 11:01:37
plt.hist(np.asarray(diabetesmcc["markup"]),bins=100)
#[Out]# (array([  2.,   1.,  23.,   7.,   5.,   7.,   4.,  13.,  16.,  18.,  24.,
#[Out]#          35.,  29.,  19.,  29.,  35.,  33.,  37.,  32.,  39.,  34.,  28.,
#[Out]#          25.,  20.,  20.,  20.,  19.,  11.,  14.,  13.,  13.,  16.,  12.,
#[Out]#          12.,   6.,   3.,   6.,   5.,   6.,  11.,   4.,   2.,   0.,   3.,
#[Out]#           6.,   4.,   4.,   5.,   5.,   1.,   1.,   0.,   4.,   3.,   2.,
#[Out]#           3.,   4.,   1.,   0.,   1.,   0.,   2.,   2.,   2.,   0.,   1.,
#[Out]#           0.,   0.,   1.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   1.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  0.73852453,   0.90979033,   1.08105613,   1.25232194,
#[Out]#           1.42358774,   1.59485354,   1.76611934,   1.93738514,
#[Out]#           2.10865094,   2.27991674,   2.45118255,   2.62244835,
#[Out]#           2.79371415,   2.96497995,   3.13624575,   3.30751155,
#[Out]#           3.47877735,   3.65004316,   3.82130896,   3.99257476,
#[Out]#           4.16384056,   4.33510636,   4.50637216,   4.67763796,
#[Out]#           4.84890377,   5.02016957,   5.19143537,   5.36270117,
#[Out]#           5.53396697,   5.70523277,   5.87649857,   6.04776438,
#[Out]#           6.21903018,   6.39029598,   6.56156178,   6.73282758,
#[Out]#           6.90409338,   7.07535918,   7.24662499,   7.41789079,
#[Out]#           7.58915659,   7.76042239,   7.93168819,   8.10295399,
#[Out]#           8.27421979,   8.4454856 ,   8.6167514 ,   8.7880172 ,
#[Out]#           8.959283  ,   9.1305488 ,   9.3018146 ,   9.4730804 ,
#[Out]#           9.64434621,   9.81561201,   9.98687781,  10.15814361,
#[Out]#          10.32940941,  10.50067521,  10.67194102,  10.84320682,
#[Out]#          11.01447262,  11.18573842,  11.35700422,  11.52827002,
#[Out]#          11.69953582,  11.87080163,  12.04206743,  12.21333323,
#[Out]#          12.38459903,  12.55586483,  12.72713063,  12.89839643,
#[Out]#          13.06966224,  13.24092804,  13.41219384,  13.58345964,
#[Out]#          13.75472544,  13.92599124,  14.09725704,  14.26852285,
#[Out]#          14.43978865,  14.61105445,  14.78232025,  14.95358605,
#[Out]#          15.12485185,  15.29611765,  15.46738346,  15.63864926,
#[Out]#          15.80991506,  15.98118086,  16.15244666,  16.32371246,
#[Out]#          16.49497826,  16.66624407,  16.83750987,  17.00877567,
#[Out]#          17.18004147,  17.35130727,  17.52257307,  17.69383887,  17.86510468]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:02:10
plt.hist(np.asarray(diabetescc["markup"]),bins=100)
#[Out]# (array([  13.,   44.,   18.,   28.,   36.,   52.,   59.,   74.,   81.,
#[Out]#          103.,   91.,   90.,   95.,  103.,   82.,   75.,   67.,   54.,
#[Out]#           51.,   43.,   39.,   57.,   31.,   39.,   33.,   29.,   20.,
#[Out]#           20.,   16.,   20.,   12.,   20.,    7.,   13.,   16.,    9.,
#[Out]#            6.,    9.,    5.,    9.,    4.,   12.,   10.,    1.,    2.,
#[Out]#            2.,    2.,    3.,    2.,    0.,    5.,    2.,    2.,    1.,
#[Out]#            0.,    1.,    1.,    1.,    1.,    0.,    1.,    1.,    0.,
#[Out]#            1.,    0.,    0.,    1.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            1.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    1.]),
#[Out]#  array([  0.90106072,   1.1650915 ,   1.42912227,   1.69315305,
#[Out]#           1.95718383,   2.22121461,   2.48524539,   2.74927616,
#[Out]#           3.01330694,   3.27733772,   3.5413685 ,   3.80539928,
#[Out]#           4.06943005,   4.33346083,   4.59749161,   4.86152239,
#[Out]#           5.12555317,   5.38958394,   5.65361472,   5.9176455 ,
#[Out]#           6.18167628,   6.44570706,   6.70973783,   6.97376861,
#[Out]#           7.23779939,   7.50183017,   7.76586095,   8.02989173,
#[Out]#           8.2939225 ,   8.55795328,   8.82198406,   9.08601484,
#[Out]#           9.35004562,   9.61407639,   9.87810717,  10.14213795,
#[Out]#          10.40616873,  10.67019951,  10.93423028,  11.19826106,
#[Out]#          11.46229184,  11.72632262,  11.9903534 ,  12.25438417,
#[Out]#          12.51841495,  12.78244573,  13.04647651,  13.31050729,
#[Out]#          13.57453806,  13.83856884,  14.10259962,  14.3666304 ,
#[Out]#          14.63066118,  14.89469195,  15.15872273,  15.42275351,
#[Out]#          15.68678429,  15.95081507,  16.21484585,  16.47887662,
#[Out]#          16.7429074 ,  17.00693818,  17.27096896,  17.53499974,
#[Out]#          17.79903051,  18.06306129,  18.32709207,  18.59112285,
#[Out]#          18.85515363,  19.1191844 ,  19.38321518,  19.64724596,
#[Out]#          19.91127674,  20.17530752,  20.43933829,  20.70336907,
#[Out]#          20.96739985,  21.23143063,  21.49546141,  21.75949218,
#[Out]#          22.02352296,  22.28755374,  22.55158452,  22.8156153 ,
#[Out]#          23.07964608,  23.34367685,  23.60770763,  23.87173841,
#[Out]#          24.13576919,  24.39979997,  24.66383074,  24.92786152,
#[Out]#          25.1918923 ,  25.45592308,  25.71995386,  25.98398463,
#[Out]#          26.24801541,  26.51204619,  26.77607697,  27.04010775,  27.30413852]),
#[Out]#  <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:10:33
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb101be80>
# Fri, 11 Sep 2015 11:10:57
plt.title("Histogram: diabetes w mcc, national")
#[Out]# <matplotlib.text.Text at 0x7fefb102fb00>
# Fri, 11 Sep 2015 11:11:09
plt.savefig("diabetesmccnational")
# Fri, 11 Sep 2015 11:11:34
plt.clf()
# Fri, 11 Sep 2015 11:11:38
plt.hist(np.asarray(diabetescc["markup"]),bins=100)
#[Out]# (array([  13.,   44.,   18.,   28.,   36.,   52.,   59.,   74.,   81.,
#[Out]#          103.,   91.,   90.,   95.,  103.,   82.,   75.,   67.,   54.,
#[Out]#           51.,   43.,   39.,   57.,   31.,   39.,   33.,   29.,   20.,
#[Out]#           20.,   16.,   20.,   12.,   20.,    7.,   13.,   16.,    9.,
#[Out]#            6.,    9.,    5.,    9.,    4.,   12.,   10.,    1.,    2.,
#[Out]#            2.,    2.,    3.,    2.,    0.,    5.,    2.,    2.,    1.,
#[Out]#            0.,    1.,    1.,    1.,    1.,    0.,    1.,    1.,    0.,
#[Out]#            1.,    0.,    0.,    1.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            1.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    1.]),
#[Out]#  array([  0.90106072,   1.1650915 ,   1.42912227,   1.69315305,
#[Out]#           1.95718383,   2.22121461,   2.48524539,   2.74927616,
#[Out]#           3.01330694,   3.27733772,   3.5413685 ,   3.80539928,
#[Out]#           4.06943005,   4.33346083,   4.59749161,   4.86152239,
#[Out]#           5.12555317,   5.38958394,   5.65361472,   5.9176455 ,
#[Out]#           6.18167628,   6.44570706,   6.70973783,   6.97376861,
#[Out]#           7.23779939,   7.50183017,   7.76586095,   8.02989173,
#[Out]#           8.2939225 ,   8.55795328,   8.82198406,   9.08601484,
#[Out]#           9.35004562,   9.61407639,   9.87810717,  10.14213795,
#[Out]#          10.40616873,  10.67019951,  10.93423028,  11.19826106,
#[Out]#          11.46229184,  11.72632262,  11.9903534 ,  12.25438417,
#[Out]#          12.51841495,  12.78244573,  13.04647651,  13.31050729,
#[Out]#          13.57453806,  13.83856884,  14.10259962,  14.3666304 ,
#[Out]#          14.63066118,  14.89469195,  15.15872273,  15.42275351,
#[Out]#          15.68678429,  15.95081507,  16.21484585,  16.47887662,
#[Out]#          16.7429074 ,  17.00693818,  17.27096896,  17.53499974,
#[Out]#          17.79903051,  18.06306129,  18.32709207,  18.59112285,
#[Out]#          18.85515363,  19.1191844 ,  19.38321518,  19.64724596,
#[Out]#          19.91127674,  20.17530752,  20.43933829,  20.70336907,
#[Out]#          20.96739985,  21.23143063,  21.49546141,  21.75949218,
#[Out]#          22.02352296,  22.28755374,  22.55158452,  22.8156153 ,
#[Out]#          23.07964608,  23.34367685,  23.60770763,  23.87173841,
#[Out]#          24.13576919,  24.39979997,  24.66383074,  24.92786152,
#[Out]#          25.1918923 ,  25.45592308,  25.71995386,  25.98398463,
#[Out]#          26.24801541,  26.51204619,  26.77607697,  27.04010775,  27.30413852]),
#[Out]#  <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:11:41
plt.title("Histogram: diabetes w mcc, national")
#[Out]# <matplotlib.text.Text at 0x7fefb0d4d128>
# Fri, 11 Sep 2015 11:11:43
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb0d257f0>
# Fri, 11 Sep 2015 11:11:48
plt.savefig("diabetesmccnational")
# Fri, 11 Sep 2015 11:12:01
plt.clf()
# Fri, 11 Sep 2015 11:12:11
plt.hist(np.asarray(diabetesmcc["markup"]),bins=100)
#[Out]# (array([  2.,   1.,  23.,   7.,   5.,   7.,   4.,  13.,  16.,  18.,  24.,
#[Out]#          35.,  29.,  19.,  29.,  35.,  33.,  37.,  32.,  39.,  34.,  28.,
#[Out]#          25.,  20.,  20.,  20.,  19.,  11.,  14.,  13.,  13.,  16.,  12.,
#[Out]#          12.,   6.,   3.,   6.,   5.,   6.,  11.,   4.,   2.,   0.,   3.,
#[Out]#           6.,   4.,   4.,   5.,   5.,   1.,   1.,   0.,   4.,   3.,   2.,
#[Out]#           3.,   4.,   1.,   0.,   1.,   0.,   2.,   2.,   2.,   0.,   1.,
#[Out]#           0.,   0.,   1.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   1.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  0.73852453,   0.90979033,   1.08105613,   1.25232194,
#[Out]#           1.42358774,   1.59485354,   1.76611934,   1.93738514,
#[Out]#           2.10865094,   2.27991674,   2.45118255,   2.62244835,
#[Out]#           2.79371415,   2.96497995,   3.13624575,   3.30751155,
#[Out]#           3.47877735,   3.65004316,   3.82130896,   3.99257476,
#[Out]#           4.16384056,   4.33510636,   4.50637216,   4.67763796,
#[Out]#           4.84890377,   5.02016957,   5.19143537,   5.36270117,
#[Out]#           5.53396697,   5.70523277,   5.87649857,   6.04776438,
#[Out]#           6.21903018,   6.39029598,   6.56156178,   6.73282758,
#[Out]#           6.90409338,   7.07535918,   7.24662499,   7.41789079,
#[Out]#           7.58915659,   7.76042239,   7.93168819,   8.10295399,
#[Out]#           8.27421979,   8.4454856 ,   8.6167514 ,   8.7880172 ,
#[Out]#           8.959283  ,   9.1305488 ,   9.3018146 ,   9.4730804 ,
#[Out]#           9.64434621,   9.81561201,   9.98687781,  10.15814361,
#[Out]#          10.32940941,  10.50067521,  10.67194102,  10.84320682,
#[Out]#          11.01447262,  11.18573842,  11.35700422,  11.52827002,
#[Out]#          11.69953582,  11.87080163,  12.04206743,  12.21333323,
#[Out]#          12.38459903,  12.55586483,  12.72713063,  12.89839643,
#[Out]#          13.06966224,  13.24092804,  13.41219384,  13.58345964,
#[Out]#          13.75472544,  13.92599124,  14.09725704,  14.26852285,
#[Out]#          14.43978865,  14.61105445,  14.78232025,  14.95358605,
#[Out]#          15.12485185,  15.29611765,  15.46738346,  15.63864926,
#[Out]#          15.80991506,  15.98118086,  16.15244666,  16.32371246,
#[Out]#          16.49497826,  16.66624407,  16.83750987,  17.00877567,
#[Out]#          17.18004147,  17.35130727,  17.52257307,  17.69383887,  17.86510468]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:12:18
plt.title("Histogram: diabetes w mcc, national")
#[Out]# <matplotlib.text.Text at 0x7fefb41d6c88>
# Fri, 11 Sep 2015 11:12:20
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb0a08ac8>
# Fri, 11 Sep 2015 11:12:25
plt.savefig("diabetesmccnational")
# Fri, 11 Sep 2015 11:12:28
plt.clf()
# Fri, 11 Sep 2015 11:12:32
plt.hist(np.asarray(diabetescc["markup"]),bins=100)
#[Out]# (array([  13.,   44.,   18.,   28.,   36.,   52.,   59.,   74.,   81.,
#[Out]#          103.,   91.,   90.,   95.,  103.,   82.,   75.,   67.,   54.,
#[Out]#           51.,   43.,   39.,   57.,   31.,   39.,   33.,   29.,   20.,
#[Out]#           20.,   16.,   20.,   12.,   20.,    7.,   13.,   16.,    9.,
#[Out]#            6.,    9.,    5.,    9.,    4.,   12.,   10.,    1.,    2.,
#[Out]#            2.,    2.,    3.,    2.,    0.,    5.,    2.,    2.,    1.,
#[Out]#            0.,    1.,    1.,    1.,    1.,    0.,    1.,    1.,    0.,
#[Out]#            1.,    0.,    0.,    1.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,
#[Out]#            1.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    0.,    1.]),
#[Out]#  array([  0.90106072,   1.1650915 ,   1.42912227,   1.69315305,
#[Out]#           1.95718383,   2.22121461,   2.48524539,   2.74927616,
#[Out]#           3.01330694,   3.27733772,   3.5413685 ,   3.80539928,
#[Out]#           4.06943005,   4.33346083,   4.59749161,   4.86152239,
#[Out]#           5.12555317,   5.38958394,   5.65361472,   5.9176455 ,
#[Out]#           6.18167628,   6.44570706,   6.70973783,   6.97376861,
#[Out]#           7.23779939,   7.50183017,   7.76586095,   8.02989173,
#[Out]#           8.2939225 ,   8.55795328,   8.82198406,   9.08601484,
#[Out]#           9.35004562,   9.61407639,   9.87810717,  10.14213795,
#[Out]#          10.40616873,  10.67019951,  10.93423028,  11.19826106,
#[Out]#          11.46229184,  11.72632262,  11.9903534 ,  12.25438417,
#[Out]#          12.51841495,  12.78244573,  13.04647651,  13.31050729,
#[Out]#          13.57453806,  13.83856884,  14.10259962,  14.3666304 ,
#[Out]#          14.63066118,  14.89469195,  15.15872273,  15.42275351,
#[Out]#          15.68678429,  15.95081507,  16.21484585,  16.47887662,
#[Out]#          16.7429074 ,  17.00693818,  17.27096896,  17.53499974,
#[Out]#          17.79903051,  18.06306129,  18.32709207,  18.59112285,
#[Out]#          18.85515363,  19.1191844 ,  19.38321518,  19.64724596,
#[Out]#          19.91127674,  20.17530752,  20.43933829,  20.70336907,
#[Out]#          20.96739985,  21.23143063,  21.49546141,  21.75949218,
#[Out]#          22.02352296,  22.28755374,  22.55158452,  22.8156153 ,
#[Out]#          23.07964608,  23.34367685,  23.60770763,  23.87173841,
#[Out]#          24.13576919,  24.39979997,  24.66383074,  24.92786152,
#[Out]#          25.1918923 ,  25.45592308,  25.71995386,  25.98398463,
#[Out]#          26.24801541,  26.51204619,  26.77607697,  27.04010775,  27.30413852]),
#[Out]#  <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:12:39
plt.title("Histogram: diabetes w cc, national")
#[Out]# <matplotlib.text.Text at 0x7fefb164fdd8>
# Fri, 11 Sep 2015 11:12:46
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb1688240>
# Fri, 11 Sep 2015 11:12:52
plt.savefig("diabetesccnational")
# Fri, 11 Sep 2015 11:12:57
plt.clf()
# Fri, 11 Sep 2015 11:13:44
hypertension=data[data["proc code"]=="305"]
# Fri, 11 Sep 2015 11:13:55
plt.hist(np.asarray(hypertension["markup"]),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:14:04
plt.title("Histogram: hypertension, national")
#[Out]# <matplotlib.text.Text at 0x7fefb14ffa20>
# Fri, 11 Sep 2015 11:14:07
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb14d3898>
# Fri, 11 Sep 2015 11:14:17
plt.savefig("hypertensionnational")
# Fri, 11 Sep 2015 11:14:28
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 11:14:36
data["Hospital Referral Region (HRR) Description"]
#[Out]# 0             AL - Dothan
#[Out]# 1         AL - Birmingham
#[Out]# 2         AL - Birmingham
#[Out]# 3         AL - Birmingham
#[Out]# 4         AL - Birmingham
#[Out]# 5         AL - Montgomery
#[Out]# 6         AL - Montgomery
#[Out]# 7         AL - Birmingham
#[Out]# 8         AL - Birmingham
#[Out]# 9         AL - Huntsville
#[Out]# 10        AL - Birmingham
#[Out]# 11        AL - Birmingham
#[Out]# 12            AL - Dothan
#[Out]# 13        AL - Birmingham
#[Out]# 14        AL - Birmingham
#[Out]# 15        AL - Huntsville
#[Out]# 16            AL - Mobile
#[Out]# 17        AL - Tuscaloosa
#[Out]# 18            AL - Mobile
#[Out]# 19        AL - Birmingham
#[Out]# 20        AL - Birmingham
#[Out]# 21            AL - Mobile
#[Out]# 22        AL - Birmingham
#[Out]# 23         AK - Anchorage
#[Out]# 24           AZ - Phoenix
#[Out]# 25            AZ - Tucson
#[Out]# 26           AZ - Phoenix
#[Out]# 27            AZ - Tucson
#[Out]# 28            AZ - Tucson
#[Out]# 29           AZ - Phoenix
#[Out]#                ...       
#[Out]# 157717                NaN
#[Out]# 157718                NaN
#[Out]# 157719                NaN
#[Out]# 157720                NaN
#[Out]# 157721                NaN
#[Out]# 157722                NaN
#[Out]# 157723                NaN
#[Out]# 157724                NaN
#[Out]# 157725                NaN
#[Out]# 157726                NaN
#[Out]# 157727                NaN
#[Out]# 157728                NaN
#[Out]# 157729                NaN
#[Out]# 157730                NaN
#[Out]# 157731                NaN
#[Out]# 157732                NaN
#[Out]# 157733                NaN
#[Out]# 157734                NaN
#[Out]# 157735                NaN
#[Out]# 157736                NaN
#[Out]# 157737                NaN
#[Out]# 157738                NaN
#[Out]# 157739                NaN
#[Out]# 157740                NaN
#[Out]# 157741                NaN
#[Out]# 157742                NaN
#[Out]# 157743                NaN
#[Out]# 157744                NaN
#[Out]# 157745                NaN
#[Out]# 157746                NaN
#[Out]# Name: Hospital Referral Region (HRR) Description, dtype: object
# Fri, 11 Sep 2015 11:14:41
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 11:14:53
data["Provider State"]
#[Out]# 0          AL
#[Out]# 1          AL
#[Out]# 2          AL
#[Out]# 3          AL
#[Out]# 4          AL
#[Out]# 5          AL
#[Out]# 6          AL
#[Out]# 7          AL
#[Out]# 8          AL
#[Out]# 9          AL
#[Out]# 10         AL
#[Out]# 11         AL
#[Out]# 12         AL
#[Out]# 13         AL
#[Out]# 14         AL
#[Out]# 15         AL
#[Out]# 16         AL
#[Out]# 17         AL
#[Out]# 18         AL
#[Out]# 19         AL
#[Out]# 20         AL
#[Out]# 21         AL
#[Out]# 22         AL
#[Out]# 23         AK
#[Out]# 24         AZ
#[Out]# 25         AZ
#[Out]# 26         AZ
#[Out]# 27         AZ
#[Out]# 28         AZ
#[Out]# 29         AZ
#[Out]#          ... 
#[Out]# 157717    NaN
#[Out]# 157718    NaN
#[Out]# 157719    NaN
#[Out]# 157720    NaN
#[Out]# 157721    NaN
#[Out]# 157722    NaN
#[Out]# 157723    NaN
#[Out]# 157724    NaN
#[Out]# 157725    NaN
#[Out]# 157726    NaN
#[Out]# 157727    NaN
#[Out]# 157728    NaN
#[Out]# 157729    NaN
#[Out]# 157730    NaN
#[Out]# 157731    NaN
#[Out]# 157732    NaN
#[Out]# 157733    NaN
#[Out]# 157734    NaN
#[Out]# 157735    NaN
#[Out]# 157736    NaN
#[Out]# 157737    NaN
#[Out]# 157738    NaN
#[Out]# 157739    NaN
#[Out]# 157740    NaN
#[Out]# 157741    NaN
#[Out]# 157742    NaN
#[Out]# 157743    NaN
#[Out]# 157744    NaN
#[Out]# 157745    NaN
#[Out]# 157746    NaN
#[Out]# Name: Provider State, dtype: object
# Fri, 11 Sep 2015 11:15:29
hypertension[hypertension["Provider State"]=="NY"]
#[Out]#                    DRG Definition  Provider Id  \
#[Out]# 65630  305 - HYPERTENSION W/O MCC       330004   
#[Out]# 65631  305 - HYPERTENSION W/O MCC       330005   
#[Out]# 65632  305 - HYPERTENSION W/O MCC       330009   
#[Out]# 65633  305 - HYPERTENSION W/O MCC       330011   
#[Out]# 65634  305 - HYPERTENSION W/O MCC       330013   
#[Out]# 65635  305 - HYPERTENSION W/O MCC       330014   
#[Out]# 65636  305 - HYPERTENSION W/O MCC       330019   
#[Out]# 65637  305 - HYPERTENSION W/O MCC       330023   
#[Out]# 65638  305 - HYPERTENSION W/O MCC       330024   
#[Out]# 65639  305 - HYPERTENSION W/O MCC       330027   
#[Out]# 65640  305 - HYPERTENSION W/O MCC       330028   
#[Out]# 65641  305 - HYPERTENSION W/O MCC       330043   
#[Out]# 65642  305 - HYPERTENSION W/O MCC       330045   
#[Out]# 65643  305 - HYPERTENSION W/O MCC       330046   
#[Out]# 65644  305 - HYPERTENSION W/O MCC       330055   
#[Out]# 65645  305 - HYPERTENSION W/O MCC       330057   
#[Out]# 65646  305 - HYPERTENSION W/O MCC       330059   
#[Out]# 65647  305 - HYPERTENSION W/O MCC       330061   
#[Out]# 65648  305 - HYPERTENSION W/O MCC       330086   
#[Out]# 65649  305 - HYPERTENSION W/O MCC       330090   
#[Out]# 65650  305 - HYPERTENSION W/O MCC       330101   
#[Out]# 65651  305 - HYPERTENSION W/O MCC       330106   
#[Out]# 65652  305 - HYPERTENSION W/O MCC       330119   
#[Out]# 65653  305 - HYPERTENSION W/O MCC       330125   
#[Out]# 65654  305 - HYPERTENSION W/O MCC       330126   
#[Out]# 65655  305 - HYPERTENSION W/O MCC       330128   
#[Out]# 65656  305 - HYPERTENSION W/O MCC       330140   
#[Out]# 65657  305 - HYPERTENSION W/O MCC       330141   
#[Out]# 65658  305 - HYPERTENSION W/O MCC       330157   
#[Out]# 65659  305 - HYPERTENSION W/O MCC       330158   
#[Out]# ...                           ...          ...   
#[Out]# 65663  305 - HYPERTENSION W/O MCC       330181   
#[Out]# 65664  305 - HYPERTENSION W/O MCC       330182   
#[Out]# 65665  305 - HYPERTENSION W/O MCC       330185   
#[Out]# 65666  305 - HYPERTENSION W/O MCC       330193   
#[Out]# 65667  305 - HYPERTENSION W/O MCC       330194   
#[Out]# 65668  305 - HYPERTENSION W/O MCC       330195   
#[Out]# 65669  305 - HYPERTENSION W/O MCC       330196   
#[Out]# 65670  305 - HYPERTENSION W/O MCC       330198   
#[Out]# 65671  305 - HYPERTENSION W/O MCC       330201   
#[Out]# 65672  305 - HYPERTENSION W/O MCC       330202   
#[Out]# 65673  305 - HYPERTENSION W/O MCC       330221   
#[Out]# 65674  305 - HYPERTENSION W/O MCC       330231   
#[Out]# 65675  305 - HYPERTENSION W/O MCC       330236   
#[Out]# 65676  305 - HYPERTENSION W/O MCC       330240   
#[Out]# 65677  305 - HYPERTENSION W/O MCC       330245   
#[Out]# 65678  305 - HYPERTENSION W/O MCC       330246   
#[Out]# 65679  305 - HYPERTENSION W/O MCC       330264   
#[Out]# 65680  305 - HYPERTENSION W/O MCC       330285   
#[Out]# 65681  305 - HYPERTENSION W/O MCC       330286   
#[Out]# 65682  305 - HYPERTENSION W/O MCC       330304   
#[Out]# 65683  305 - HYPERTENSION W/O MCC       330306   
#[Out]# 65684  305 - HYPERTENSION W/O MCC       330331   
#[Out]# 65685  305 - HYPERTENSION W/O MCC       330350   
#[Out]# 65686  305 - HYPERTENSION W/O MCC       330353   
#[Out]# 65687  305 - HYPERTENSION W/O MCC       330372   
#[Out]# 65688  305 - HYPERTENSION W/O MCC       330393   
#[Out]# 65689  305 - HYPERTENSION W/O MCC       330394   
#[Out]# 65690  305 - HYPERTENSION W/O MCC       330395   
#[Out]# 65691  305 - HYPERTENSION W/O MCC       330399   
#[Out]# 65692  305 - HYPERTENSION W/O MCC       330401   
#[Out]# 
#[Out]#                                            Provider Name  \
#[Out]# 65630           HEALTH ALLIANCE HOSPITAL BROADWAY CAMPUS   
#[Out]# 65631                                     KALEIDA HEALTH   
#[Out]# 65632                      BRONX-LEBANON HOSPITAL CENTER   
#[Out]# 65633         OUR LADY OF LOURDES MEMORIAL HOSPITAL, INC   
#[Out]# 65634                     ALBANY MEDICAL CENTER HOSPITAL   
#[Out]# 65635                    JAMAICA HOSPITAL MEDICAL CENTER   
#[Out]# 65636      NEW YORK COMMUNITY HOSPITAL OF BROOKLYN, INC.   
#[Out]# 65637                     VASSAR BROTHERS MEDICAL CENTER   
#[Out]# 65638                               MOUNT SINAI HOSPITAL   
#[Out]# 65639                   NASSAU UNIVERSITY MEDICAL CENTER   
#[Out]# 65640                 RICHMOND UNIVERSITY MEDICAL CENTER   
#[Out]# 65641                                 SOUTHSIDE HOSPITAL   
#[Out]# 65642                                HUNTINGTON HOSPITAL   
#[Out]# 65643                       ST LUKE'S ROOSEVELT HOSPITAL   
#[Out]# 65644         NEW YORK HOSPITAL MEDICAL CENTER OF QUEENS   
#[Out]# 65645                                ST PETER'S HOSPITAL   
#[Out]# 65646                          MONTEFIORE MEDICAL CENTER   
#[Out]# 65647                           LAWRENCE HOSPITAL CENTER   
#[Out]# 65648                   MONTEFIORE MOUNT VERNON HOSPITAL   
#[Out]# 65649                         ARNOT OGDEN MEDICAL CENTER   
#[Out]# 65650                     NEW YORK-PRESBYTERIAN HOSPITAL   
#[Out]# 65651                    NORTH SHORE UNIVERSITY HOSPITAL   
#[Out]# 65652                                LENOX HILL HOSPITAL   
#[Out]# 65653                         ROCHESTER GENERAL HOSPITAL   
#[Out]# 65654                     ORANGE REGIONAL MEDICAL CENTER   
#[Out]# 65655                           ELMHURST HOSPITAL CENTER   
#[Out]# 65656                 ST JOSEPH'S HOSPITAL HEALTH CENTER   
#[Out]# 65657        BROOKHAVEN MEMORIAL HOSPITAL MEDICAL CENTER   
#[Out]# 65658                           SAMARITAN MEDICAL CENTER   
#[Out]# 65659                 GOOD SAMARITAN HOSPITAL OF SUFFERN   
#[Out]# ...                                                  ...   
#[Out]# 65663                                 GLEN COVE HOSPITAL   
#[Out]# 65664                        ST FRANCIS HOSPITAL, ROSLYN   
#[Out]# 65665  JOHN T MATHER MEMORIAL HOSPITAL  OF PORT JEFFE...   
#[Out]# 65666                   FLUSHING HOSPITAL MEDICAL CENTER   
#[Out]# 65667                          MAIMONIDES MEDICAL CENTER   
#[Out]# 65668                  LONG ISLAND JEWISH MEDICAL CENTER   
#[Out]# 65669                              CONEY ISLAND HOSPITAL   
#[Out]# 65670                  SOUTH NASSAU COMMUNITIES HOSPITAL   
#[Out]# 65671                   KINGSBROOK JEWISH MEDICAL CENTER   
#[Out]# 65672                       KINGS COUNTY HOSPITAL CENTER   
#[Out]# 65673                     WYCKOFF HEIGHTS MEDICAL CENTER   
#[Out]# 65674                             QUEENS HOSPITAL CENTER   
#[Out]# 65675                        NEW YORK METHODIST HOSPITAL   
#[Out]# 65676                             HARLEM HOSPITAL CENTER   
#[Out]# 65677                        ST ELIZABETH MEDICAL CENTER   
#[Out]# 65678                                ST CHARLES HOSPITAL   
#[Out]# 65679                        ST LUKE'S CORNWALL HOSPITAL   
#[Out]# 65680                           STRONG MEMORIAL HOSPITAL   
#[Out]# 65681             GOOD SAMARITAN HOSPITAL MEDICAL CENTER   
#[Out]# 65682                       WHITE PLAINS HOSPITAL CENTER   
#[Out]# 65683                            LUTHERAN MEDICAL CENTER   
#[Out]# 65684                                 PLAINVIEW HOSPITAL   
#[Out]# 65685      UNIVERSITY HOSPITAL OF BROOKLYN ( DOWNSTATE )   
#[Out]# 65686                              FOREST HILLS HOSPITAL   
#[Out]# 65687                                  FRANKLIN HOSPITAL   
#[Out]# 65688                UNIVERSITY HOSPITAL ( STONY BROOK )   
#[Out]# 65689              UNITED HEALTH SERVICES HOSPITALS, INC   
#[Out]# 65690        ST JOHN'S EPISCOPAL HOSPITAL AT SOUTH SHORE   
#[Out]# 65691                               ST BARNABAS HOSPITAL   
#[Out]# 65692                     ST CATHERINE OF SIENA HOSPITAL   
#[Out]# 
#[Out]#                    Provider Street Address   Provider City Provider State  \
#[Out]# 65630                         396 BROADWAY        KINGSTON             NY   
#[Out]# 65631       726 EXCHANGE STREET, SUITE 522         BUFFALO             NY   
#[Out]# 65632                   1276 FULTON AVENUE           BRONX             NY   
#[Out]# 65633                  169 RIVERSIDE DRIVE      BINGHAMTON             NY   
#[Out]# 65634               43 NEW SCOTLAND AVENUE          ALBANY             NY   
#[Out]# 65635  89TH AVENUE AND VAN WYCK EXPRESSWAY         JAMAICA             NY   
#[Out]# 65636                   2525 KINGS HIGHWAY        BROOKLYN             NY   
#[Out]# 65637                       45 READE PLACE    POUGHKEEPSIE             NY   
#[Out]# 65638             ONE GUSTAVE L LEVY PLACE        NEW YORK             NY   
#[Out]# 65639              2201 HEMPSTEAD TURNPIKE     EAST MEADOW             NY   
#[Out]# 65640                      355 BARD AVENUE   STATEN ISLAND             NY   
#[Out]# 65641                 301 EAST MAIN STREET       BAY SHORE             NY   
#[Out]# 65642                      270 PARK AVENUE      HUNTINGTON             NY   
#[Out]# 65643                1111 AMSTERDAM AVENUE        NEW YORK             NY   
#[Out]# 65644                    56-45 MAIN STREET        FLUSHING             NY   
#[Out]# 65645          315 SOUTH MANNING BOULEVARD          ALBANY             NY   
#[Out]# 65646                111 EAST 210TH STREET           BRONX             NY   
#[Out]# 65647                     55 PALMER AVENUE      BRONXVILLE             NY   
#[Out]# 65648                  12 NORTH 7TH AVENUE    MOUNT VERNON             NY   
#[Out]# 65649                       600 ROE AVENUE          ELMIRA             NY   
#[Out]# 65650                 525 EAST 68TH STREET        NEW YORK             NY   
#[Out]# 65651                  300 COMMUNITY DRIVE       MANHASSET             NY   
#[Out]# 65652                 100 EAST 77TH STREET        NEW YORK             NY   
#[Out]# 65653                 1425 PORTLAND AVENUE       ROCHESTER             NY   
#[Out]# 65654                 707 EAST MAIN STREET      MIDDLETOWN             NY   
#[Out]# 65655                       79-01 BROADWAY        ELMHURST             NY   
#[Out]# 65656                  301 PROSPECT AVENUE        SYRACUSE             NY   
#[Out]# 65657                    101 HOSPITAL ROAD       PATCHOGUE             NY   
#[Out]# 65658                830 WASHINGTON STREET       WATERTOWN             NY   
#[Out]# 65659                 255 LAFAYETTE AVENUE         SUFFERN             NY   
#[Out]# ...                                    ...             ...            ...   
#[Out]# 65663                  101 ST ANDREWS LANE       GLEN COVE             NY   
#[Out]# 65664        100 PORT WASHINGTON BOULEVARD          ROSLYN             NY   
#[Out]# 65665                75 NORTH COUNTRY ROAD  PORT JEFFERSON             NY   
#[Out]# 65666    45TH AVENUE AND PARSONS BOULEVARD        FLUSHING             NY   
#[Out]# 65667                    4802 TENTH AVENUE        BROOKLYN             NY   
#[Out]# 65668                 270 - 05 76TH AVENUE   NEW HYDE PARK             NY   
#[Out]# 65669                   2601 OCEAN PARKWAY        BROOKLYN             NY   
#[Out]# 65670                      ONE HEALTHY WAY       OCEANSIDE             NY   
#[Out]# 65671               585 SCHENECTADY AVENUE        BROOKLYN             NY   
#[Out]# 65672                  451 CLARKSON AVENUE        BROOKLYN             NY   
#[Out]# 65673                 374 STOCKHOLM STREET        BROOKLYN             NY   
#[Out]# 65674                   82-68 164TH STREET         JAMAICA             NY   
#[Out]# 65675                     506 SIXTH STREET        BROOKLYN             NY   
#[Out]# 65676                     506 LENOX AVENUE        NEW YORK             NY   
#[Out]# 65677                  2209 GENESEE STREET           UTICA             NY   
#[Out]# 65678                 200 BELLE TERRE ROAD  PORT JEFFERSON             NY   
#[Out]# 65679                     70 DUBOIS STREET        NEWBURGH             NY   
#[Out]# 65680                      601 ELMWOOD AVE       ROCHESTER             NY   
#[Out]# 65681                 1000 MONTAUK HIGHWAY      WEST ISLIP             NY   
#[Out]# 65682                    41 EAST POST R0AD    WHITE PLAINS             NY   
#[Out]# 65683                      150 55TH STREET        BROOKLYN             NY   
#[Out]# 65684                 888 OLD COUNTRY ROAD       PLAINVIEW             NY   
#[Out]# 65685                       445 LENOX ROAD        BROOKLYN             NY   
#[Out]# 65686                   102 - 01 66TH ROAD    FOREST HILLS             NY   
#[Out]# 65687                  900 FRANKLIN AVENUE   VALLEY STREAM             NY   
#[Out]# 65688          HEALTH SCIENCES CENTER SUNY     STONY BROOK             NY   
#[Out]# 65689                33-57 HARRISON STREET    JOHNSON CITY             NY   
#[Out]# 65690                327 BEACH 19TH STREET    FAR ROCKAWAY             NY   
#[Out]# 65691                    4422 THIRD AVENUE           BRONX             NY   
#[Out]# 65692                         50 ROUTE 25A       SMITHTOWN             NY   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 65630              12401                                NY - Albany   
#[Out]# 65631              14210                               NY - Buffalo   
#[Out]# 65632              10456                                 NY - Bronx   
#[Out]# 65633              13905                            NY - Binghamton   
#[Out]# 65634              12208                                NY - Albany   
#[Out]# 65635              11418                      NY - East Long Island   
#[Out]# 65636              11229                             NY - Manhattan   
#[Out]# 65637              12601                                NY - Albany   
#[Out]# 65638              10029                             NY - Manhattan   
#[Out]# 65639              11554                      NY - East Long Island   
#[Out]# 65640              10304                             NY - Manhattan   
#[Out]# 65641              11706                      NY - East Long Island   
#[Out]# 65642              11743                      NY - East Long Island   
#[Out]# 65643              10025                             NY - Manhattan   
#[Out]# 65644              11355                      NY - East Long Island   
#[Out]# 65645              12208                                NY - Albany   
#[Out]# 65646              10467                                 NY - Bronx   
#[Out]# 65647              10708                          NY - White Plains   
#[Out]# 65648              10550                          NY - White Plains   
#[Out]# 65649              14905                                NY - Elmira   
#[Out]# 65650              10021                             NY - Manhattan   
#[Out]# 65651              11030                      NY - East Long Island   
#[Out]# 65652              10021                             NY - Manhattan   
#[Out]# 65653              14621                             NY - Rochester   
#[Out]# 65654              10940                          NY - White Plains   
#[Out]# 65655              11373                      NY - East Long Island   
#[Out]# 65656              13203                              NY - Syracuse   
#[Out]# 65657              11772                      NY - East Long Island   
#[Out]# 65658              13601                              NY - Syracuse   
#[Out]# 65659              10901                             NJ - Ridgewood   
#[Out]# ...                  ...                                        ...   
#[Out]# 65663              11542                      NY - East Long Island   
#[Out]# 65664              11576                      NY - East Long Island   
#[Out]# 65665              11777                      NY - East Long Island   
#[Out]# 65666              11355                      NY - East Long Island   
#[Out]# 65667              11219                             NY - Manhattan   
#[Out]# 65668              11040                      NY - East Long Island   
#[Out]# 65669              11235                             NY - Manhattan   
#[Out]# 65670              11572                      NY - East Long Island   
#[Out]# 65671              11203                             NY - Manhattan   
#[Out]# 65672              11203                             NY - Manhattan   
#[Out]# 65673              11237                             NY - Manhattan   
#[Out]# 65674              11432                      NY - East Long Island   
#[Out]# 65675              11215                             NY - Manhattan   
#[Out]# 65676              10037                             NY - Manhattan   
#[Out]# 65677              13501                                NY - Elmira   
#[Out]# 65678              11777                      NY - East Long Island   
#[Out]# 65679              12550                          NY - White Plains   
#[Out]# 65680              14642                             NY - Rochester   
#[Out]# 65681              11795                      NY - East Long Island   
#[Out]# 65682              10601                          NY - White Plains   
#[Out]# 65683              11220                             NY - Manhattan   
#[Out]# 65684              11803                      NY - East Long Island   
#[Out]# 65685              11203                             NY - Manhattan   
#[Out]# 65686              11375                      NY - East Long Island   
#[Out]# 65687              11580                      NY - East Long Island   
#[Out]# 65688              11794                      NY - East Long Island   
#[Out]# 65689              13790                            NY - Binghamton   
#[Out]# 65690              11691                      NY - East Long Island   
#[Out]# 65691              10457                                 NY - Bronx   
#[Out]# 65692              11787                      NY - East Long Island   
#[Out]# 
#[Out]#        Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 65630                11             13919.545450             4459.727273   
#[Out]# 65631                13             10272.846150             4994.769231   
#[Out]# 65632                28              7672.785714             8973.678571   
#[Out]# 65633                19              8438.210526             3638.842105   
#[Out]# 65634                35             18526.057140             5329.400000   
#[Out]# 65635                19              9172.263158             7595.842105   
#[Out]# 65636                73             10614.753420             6118.191781   
#[Out]# 65637                12             26497.583330             4375.416667   
#[Out]# 65638                42             17330.238100             7647.119048   
#[Out]# 65639                39             15238.282050             7646.538462   
#[Out]# 65640                16             20277.937500             6481.687500   
#[Out]# 65641                17             22585.176470             5553.647059   
#[Out]# 65642                23             17301.739130             4303.304348   
#[Out]# 65643                35             23515.085710             7174.400000   
#[Out]# 65644                32             21779.875000             6432.156250   
#[Out]# 65645                16             16352.625000             3808.937500   
#[Out]# 65646                54             31534.925930             8035.500000   
#[Out]# 65647                12             14430.583330             4841.083333   
#[Out]# 65648                13             15449.076920             6512.076923   
#[Out]# 65649                13             12037.000000             3507.153846   
#[Out]# 65650               101             23287.217820             7309.118812   
#[Out]# 65651                39             31155.076920             6192.923077   
#[Out]# 65652                50             32373.220000             6119.300000   
#[Out]# 65653                11             13153.090910             4021.000000   
#[Out]# 65654                35             25320.657140             4840.714286   
#[Out]# 65655                15              8311.133333             8038.200000   
#[Out]# 65656                18             10296.555560             4241.166667   
#[Out]# 65657                21             28188.761900             4825.761905   
#[Out]# 65658                11             20447.000000             8075.454545   
#[Out]# 65659                19             26970.684210             4972.000000   
#[Out]# ...                 ...                      ...                     ...   
#[Out]# 65663                14             16052.285710             6033.857143   
#[Out]# 65664                14             20140.285710             4644.928571   
#[Out]# 65665                15             20072.400000             4527.666667   
#[Out]# 65666                20             13339.500000             7171.800000   
#[Out]# 65667                33             21031.545450             7966.121212   
#[Out]# 65668                33             27006.060610             7116.333333   
#[Out]# 65669                18             14345.888890             7556.277778   
#[Out]# 65670                22             19044.863640             5046.909091   
#[Out]# 65671                16             25650.687500             6594.687500   
#[Out]# 65672                17             21031.823530            11858.823530   
#[Out]# 65673                15             11807.133330             7628.866667   
#[Out]# 65674                11             21435.181820             8670.454545   
#[Out]# 65675                59             13357.288140             6928.559322   
#[Out]# 65676                13              8365.769231             8793.692308   
#[Out]# 65677                14              9972.357143             3915.142857   
#[Out]# 65678                12             45968.583330             4831.250000   
#[Out]# 65679                15             21333.466670             4646.533333   
#[Out]# 65680                11              8086.818182             6168.000000   
#[Out]# 65681                11             29312.545450             4630.454545   
#[Out]# 65682                18             11794.666670             4505.611111   
#[Out]# 65683                40             10641.200000             8166.650000   
#[Out]# 65684                20             17220.400000             4944.250000   
#[Out]# 65685                20             12721.650000             7882.800000   
#[Out]# 65686                47             15073.765960             5958.574468   
#[Out]# 65687                15             14390.533330             4717.066667   
#[Out]# 65688                50             22041.800000             6672.480000   
#[Out]# 65689                17             12250.352940             4181.529412   
#[Out]# 65690                31             17940.258060             9086.000000   
#[Out]# 65691                20             10610.450000             8532.050000   
#[Out]# 65692                30             29148.666670             4201.966667   
#[Out]# 
#[Out]#        Average Medicare Payments                        proc proc code  \
#[Out]# 65630                3118.272727  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65631                3674.846154  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65632                7556.642857  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65633                2562.421053  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65634                3472.885714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65635                6222.684211  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65636                4999.794521  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65637                3294.750000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65638                5474.690476  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65639                6218.589744  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65640                5201.875000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65641                3683.058824  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65642                3317.652174  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65643                5805.314286  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65644                5099.093750  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65645                2660.625000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65646                6162.296296  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65647                2947.916667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65648                4301.461538  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65649                2367.307692  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65650                5457.148515  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65651                4031.128205  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65652                4415.120000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65653                2787.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65654                3204.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65655                6600.200000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65656                2781.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65657                3330.666667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65658                6536.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65659                4230.105263  305 - HYPERTENSION W/O MCC       305   
#[Out]# ...                          ...                         ...       ...   
#[Out]# 65663                3082.642857  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65664                2994.214286  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65665                2907.733333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65666                5392.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65667                6347.696970  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65668                5715.030303  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65669                6449.277778  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65670                3180.272727  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65671                5285.187500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65672                9430.352941  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65673                6002.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65674                6562.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65675                5346.271186  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65676                7251.923077  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65677                2652.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65678                3698.250000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65679                3468.133333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65680                4150.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65681                3138.090909  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65682                3191.222222  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65683                6536.325000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65684                3268.100000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65685                5564.700000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65686                4743.425532  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65687                3682.600000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65688                5094.480000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65689                2825.647059  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65690                6416.387097  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65691                6968.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65692                3306.666667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 
#[Out]#           markup  
#[Out]# 65630   4.463864  
#[Out]# 65631   2.795449  
#[Out]# 65632   1.015370  
#[Out]# 65633   3.293062  
#[Out]# 65634   5.334485  
#[Out]# 65635   1.474004  
#[Out]# 65636   2.123038  
#[Out]# 65637   8.042365  
#[Out]# 65638   3.165519  
#[Out]# 65639   2.450440  
#[Out]# 65640   3.898198  
#[Out]# 65641   6.132179  
#[Out]# 65642   5.215055  
#[Out]# 65643   4.050614  
#[Out]# 65644   4.271323  
#[Out]# 65645   6.146159  
#[Out]# 65646   5.117399  
#[Out]# 65647   4.895180  
#[Out]# 65648   3.591588  
#[Out]# 65649   5.084679  
#[Out]# 65650   4.267287  
#[Out]# 65651   7.728625  
#[Out]# 65652   7.332353  
#[Out]# 65653   4.719444  
#[Out]# 65654   7.902122  
#[Out]# 65655   1.259224  
#[Out]# 65656   3.702465  
#[Out]# 65657   8.463399  
#[Out]# 65658   3.127931  
#[Out]# 65659   6.375890  
#[Out]# ...          ...  
#[Out]# 65663   5.207313  
#[Out]# 65664   6.726401  
#[Out]# 65665   6.903109  
#[Out]# 65666   2.473943  
#[Out]# 65667   3.313256  
#[Out]# 65668   4.725445  
#[Out]# 65669   2.224418  
#[Out]# 65670   5.988437  
#[Out]# 65671   4.853316  
#[Out]# 65672   2.230227  
#[Out]# 65673   1.967091  
#[Out]# 65674   3.266110  
#[Out]# 65675   2.498431  
#[Out]# 65676   1.153593  
#[Out]# 65677   3.759911  
#[Out]# 65678  12.429820  
#[Out]# 65679   6.151282  
#[Out]# 65680   1.948247  
#[Out]# 65681   9.340885  
#[Out]# 65682   3.695972  
#[Out]# 65683   1.628010  
#[Out]# 65684   5.269239  
#[Out]# 65685   2.286134  
#[Out]# 65686   3.177823  
#[Out]# 65687   3.907710  
#[Out]# 65688   4.326604  
#[Out]# 65689   4.335415  
#[Out]# 65690   2.796006  
#[Out]# 65691   1.522740  
#[Out]# 65692   8.815121  
#[Out]# 
#[Out]# [63 rows x 15 columns]
# Fri, 11 Sep 2015 11:15:39
hypertension["markup"][hypertension["Provider State"]=="NY"]
#[Out]# 65630     4.463864
#[Out]# 65631     2.795449
#[Out]# 65632     1.015370
#[Out]# 65633     3.293062
#[Out]# 65634     5.334485
#[Out]# 65635     1.474004
#[Out]# 65636     2.123038
#[Out]# 65637     8.042365
#[Out]# 65638     3.165519
#[Out]# 65639     2.450440
#[Out]# 65640     3.898198
#[Out]# 65641     6.132179
#[Out]# 65642     5.215055
#[Out]# 65643     4.050614
#[Out]# 65644     4.271323
#[Out]# 65645     6.146159
#[Out]# 65646     5.117399
#[Out]# 65647     4.895180
#[Out]# 65648     3.591588
#[Out]# 65649     5.084679
#[Out]# 65650     4.267287
#[Out]# 65651     7.728625
#[Out]# 65652     7.332353
#[Out]# 65653     4.719444
#[Out]# 65654     7.902122
#[Out]# 65655     1.259224
#[Out]# 65656     3.702465
#[Out]# 65657     8.463399
#[Out]# 65658     3.127931
#[Out]# 65659     6.375890
#[Out]#            ...    
#[Out]# 65663     5.207313
#[Out]# 65664     6.726401
#[Out]# 65665     6.903109
#[Out]# 65666     2.473943
#[Out]# 65667     3.313256
#[Out]# 65668     4.725445
#[Out]# 65669     2.224418
#[Out]# 65670     5.988437
#[Out]# 65671     4.853316
#[Out]# 65672     2.230227
#[Out]# 65673     1.967091
#[Out]# 65674     3.266110
#[Out]# 65675     2.498431
#[Out]# 65676     1.153593
#[Out]# 65677     3.759911
#[Out]# 65678    12.429820
#[Out]# 65679     6.151282
#[Out]# 65680     1.948247
#[Out]# 65681     9.340885
#[Out]# 65682     3.695972
#[Out]# 65683     1.628010
#[Out]# 65684     5.269239
#[Out]# 65685     2.286134
#[Out]# 65686     3.177823
#[Out]# 65687     3.907710
#[Out]# 65688     4.326604
#[Out]# 65689     4.335415
#[Out]# 65690     2.796006
#[Out]# 65691     1.522740
#[Out]# 65692     8.815121
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:15:55
plt.hist(np.asarray(_),bins=100)
#[Out]# (array([ 1.,  1.,  1.,  0.,  2.,  1.,  0.,  0.,  2.,  1.,  2.,  1.,  3.,
#[Out]#          0.,  0.,  2.,  0.,  0.,  3.,  2.,  1.,  0.,  1.,  2.,  1.,  3.,
#[Out]#          1.,  0.,  2.,  2.,  1.,  0.,  3.,  2.,  0.,  2.,  2.,  2.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  1.,  3.,  0.,  1.,  0.,  0.,  0.,  1.,  1.,
#[Out]#          0.,  0.,  0.,  1.,  0.,  0.,  1.,  0.,  1.,  1.,  0.,  0.,  0.,
#[Out]#          1.,  0.,  0.,  1.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.12951414,   1.24365865,   1.35780316,
#[Out]#           1.47194767,   1.58609218,   1.70023668,   1.81438119,
#[Out]#           1.9285257 ,   2.04267021,   2.15681471,   2.27095922,
#[Out]#           2.38510373,   2.49924824,   2.61339275,   2.72753725,
#[Out]#           2.84168176,   2.95582627,   3.06997078,   3.18411528,
#[Out]#           3.29825979,   3.4124043 ,   3.52654881,   3.64069331,
#[Out]#           3.75483782,   3.86898233,   3.98312684,   4.09727135,
#[Out]#           4.21141585,   4.32556036,   4.43970487,   4.55384938,
#[Out]#           4.66799388,   4.78213839,   4.8962829 ,   5.01042741,
#[Out]#           5.12457192,   5.23871642,   5.35286093,   5.46700544,
#[Out]#           5.58114995,   5.69529445,   5.80943896,   5.92358347,
#[Out]#           6.03772798,   6.15187248,   6.26601699,   6.3801615 ,
#[Out]#           6.49430601,   6.60845052,   6.72259502,   6.83673953,
#[Out]#           6.95088404,   7.06502855,   7.17917305,   7.29331756,
#[Out]#           7.40746207,   7.52160658,   7.63575109,   7.74989559,
#[Out]#           7.8640401 ,   7.97818461,   8.09232912,   8.20647362,
#[Out]#           8.32061813,   8.43476264,   8.54890715,   8.66305165,
#[Out]#           8.77719616,   8.89134067,   9.00548518,   9.11962969,
#[Out]#           9.23377419,   9.3479187 ,   9.46206321,   9.57620772,
#[Out]#           9.69035222,   9.80449673,   9.91864124,  10.03278575,
#[Out]#          10.14693026,  10.26107476,  10.37521927,  10.48936378,
#[Out]#          10.60350829,  10.71765279,  10.8317973 ,  10.94594181,
#[Out]#          11.06008632,  11.17423082,  11.28837533,  11.40251984,
#[Out]#          11.51666435,  11.63080886,  11.74495336,  11.85909787,
#[Out]#          11.97324238,  12.08738689,  12.20153139,  12.3156759 ,  12.42982041]),
#[Out]#  <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:16:17
plt.hist(np.asarray(_),bins=10)
# Fri, 11 Sep 2015 11:16:28
hypertension["markup"][hypertension["Provider State"]=="NY"]
#[Out]# 65630     4.463864
#[Out]# 65631     2.795449
#[Out]# 65632     1.015370
#[Out]# 65633     3.293062
#[Out]# 65634     5.334485
#[Out]# 65635     1.474004
#[Out]# 65636     2.123038
#[Out]# 65637     8.042365
#[Out]# 65638     3.165519
#[Out]# 65639     2.450440
#[Out]# 65640     3.898198
#[Out]# 65641     6.132179
#[Out]# 65642     5.215055
#[Out]# 65643     4.050614
#[Out]# 65644     4.271323
#[Out]# 65645     6.146159
#[Out]# 65646     5.117399
#[Out]# 65647     4.895180
#[Out]# 65648     3.591588
#[Out]# 65649     5.084679
#[Out]# 65650     4.267287
#[Out]# 65651     7.728625
#[Out]# 65652     7.332353
#[Out]# 65653     4.719444
#[Out]# 65654     7.902122
#[Out]# 65655     1.259224
#[Out]# 65656     3.702465
#[Out]# 65657     8.463399
#[Out]# 65658     3.127931
#[Out]# 65659     6.375890
#[Out]#            ...    
#[Out]# 65663     5.207313
#[Out]# 65664     6.726401
#[Out]# 65665     6.903109
#[Out]# 65666     2.473943
#[Out]# 65667     3.313256
#[Out]# 65668     4.725445
#[Out]# 65669     2.224418
#[Out]# 65670     5.988437
#[Out]# 65671     4.853316
#[Out]# 65672     2.230227
#[Out]# 65673     1.967091
#[Out]# 65674     3.266110
#[Out]# 65675     2.498431
#[Out]# 65676     1.153593
#[Out]# 65677     3.759911
#[Out]# 65678    12.429820
#[Out]# 65679     6.151282
#[Out]# 65680     1.948247
#[Out]# 65681     9.340885
#[Out]# 65682     3.695972
#[Out]# 65683     1.628010
#[Out]# 65684     5.269239
#[Out]# 65685     2.286134
#[Out]# 65686     3.177823
#[Out]# 65687     3.907710
#[Out]# 65688     4.326604
#[Out]# 65689     4.335415
#[Out]# 65690     2.796006
#[Out]# 65691     1.522740
#[Out]# 65692     8.815121
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:16:31
len(_)
#[Out]# 63
# Fri, 11 Sep 2015 11:16:43
temp=hypertension["markup"][hypertension["Provider State"]=="N
# Fri, 11 Sep 2015 11:16:48
temp=hypertension["markup"][hypertension["Provider State"]=="NY"]
# Fri, 11 Sep 2015 11:16:50
temp
#[Out]# 65630     4.463864
#[Out]# 65631     2.795449
#[Out]# 65632     1.015370
#[Out]# 65633     3.293062
#[Out]# 65634     5.334485
#[Out]# 65635     1.474004
#[Out]# 65636     2.123038
#[Out]# 65637     8.042365
#[Out]# 65638     3.165519
#[Out]# 65639     2.450440
#[Out]# 65640     3.898198
#[Out]# 65641     6.132179
#[Out]# 65642     5.215055
#[Out]# 65643     4.050614
#[Out]# 65644     4.271323
#[Out]# 65645     6.146159
#[Out]# 65646     5.117399
#[Out]# 65647     4.895180
#[Out]# 65648     3.591588
#[Out]# 65649     5.084679
#[Out]# 65650     4.267287
#[Out]# 65651     7.728625
#[Out]# 65652     7.332353
#[Out]# 65653     4.719444
#[Out]# 65654     7.902122
#[Out]# 65655     1.259224
#[Out]# 65656     3.702465
#[Out]# 65657     8.463399
#[Out]# 65658     3.127931
#[Out]# 65659     6.375890
#[Out]#            ...    
#[Out]# 65663     5.207313
#[Out]# 65664     6.726401
#[Out]# 65665     6.903109
#[Out]# 65666     2.473943
#[Out]# 65667     3.313256
#[Out]# 65668     4.725445
#[Out]# 65669     2.224418
#[Out]# 65670     5.988437
#[Out]# 65671     4.853316
#[Out]# 65672     2.230227
#[Out]# 65673     1.967091
#[Out]# 65674     3.266110
#[Out]# 65675     2.498431
#[Out]# 65676     1.153593
#[Out]# 65677     3.759911
#[Out]# 65678    12.429820
#[Out]# 65679     6.151282
#[Out]# 65680     1.948247
#[Out]# 65681     9.340885
#[Out]# 65682     3.695972
#[Out]# 65683     1.628010
#[Out]# 65684     5.269239
#[Out]# 65685     2.286134
#[Out]# 65686     3.177823
#[Out]# 65687     3.907710
#[Out]# 65688     4.326604
#[Out]# 65689     4.335415
#[Out]# 65690     2.796006
#[Out]# 65691     1.522740
#[Out]# 65692     8.815121
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:16:56
plt.hist(temp,bins=10)
# Fri, 11 Sep 2015 11:17:06
plt.hist(np.asarray(temp),bins=10)
#[Out]# (array([  9.,  13.,  13.,  12.,   5.,   4.,   4.,   1.,   1.,   1.]),
#[Out]#  array([  1.01536964,   2.15681471,   3.29825979,   4.43970487,
#[Out]#           5.58114995,   6.72259502,   7.8640401 ,   9.00548518,
#[Out]#          10.14693026,  11.28837533,  12.42982041]),
#[Out]#  <a list of 10 Patch objects>)
# Fri, 11 Sep 2015 11:17:19
plt.hist(np.asarray(temp),bins=20)
#[Out]# (array([ 5.,  4.,  6.,  7.,  5.,  8.,  6.,  6.,  4.,  1.,  2.,  2.,  2.,
#[Out]#          2.,  1.,  0.,  0.,  1.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.58609218,   2.15681471,   2.72753725,
#[Out]#           3.29825979,   3.86898233,   4.43970487,   5.01042741,
#[Out]#           5.58114995,   6.15187248,   6.72259502,   7.29331756,
#[Out]#           7.8640401 ,   8.43476264,   9.00548518,   9.57620772,
#[Out]#          10.14693026,  10.71765279,  11.28837533,  11.85909787,  12.42982041]),
#[Out]#  <a list of 20 Patch objects>)
# Fri, 11 Sep 2015 11:17:29
plt.hist(np.asarray(temp),bins=30)
#[Out]# (array([ 3.,  3.,  3.,  6.,  2.,  5.,  2.,  7.,  4.,  4.,  4.,  4.,  0.,
#[Out]#          4.,  1.,  2.,  1.,  1.,  2.,  1.,  1.,  1.,  0.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.39585133,   1.77633302,   2.15681471,
#[Out]#           2.53729641,   2.9177781 ,   3.29825979,   3.67874148,
#[Out]#           4.05922318,   4.43970487,   4.82018656,   5.20066825,
#[Out]#           5.58114995,   5.96163164,   6.34211333,   6.72259502,
#[Out]#           7.10307672,   7.48355841,   7.8640401 ,   8.24452179,
#[Out]#           8.62500349,   9.00548518,   9.38596687,   9.76644856,
#[Out]#          10.14693026,  10.52741195,  10.90789364,  11.28837533,
#[Out]#          11.66885702,  12.04933872,  12.42982041]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:18:25
temp
#[Out]# 65630     4.463864
#[Out]# 65631     2.795449
#[Out]# 65632     1.015370
#[Out]# 65633     3.293062
#[Out]# 65634     5.334485
#[Out]# 65635     1.474004
#[Out]# 65636     2.123038
#[Out]# 65637     8.042365
#[Out]# 65638     3.165519
#[Out]# 65639     2.450440
#[Out]# 65640     3.898198
#[Out]# 65641     6.132179
#[Out]# 65642     5.215055
#[Out]# 65643     4.050614
#[Out]# 65644     4.271323
#[Out]# 65645     6.146159
#[Out]# 65646     5.117399
#[Out]# 65647     4.895180
#[Out]# 65648     3.591588
#[Out]# 65649     5.084679
#[Out]# 65650     4.267287
#[Out]# 65651     7.728625
#[Out]# 65652     7.332353
#[Out]# 65653     4.719444
#[Out]# 65654     7.902122
#[Out]# 65655     1.259224
#[Out]# 65656     3.702465
#[Out]# 65657     8.463399
#[Out]# 65658     3.127931
#[Out]# 65659     6.375890
#[Out]#            ...    
#[Out]# 65663     5.207313
#[Out]# 65664     6.726401
#[Out]# 65665     6.903109
#[Out]# 65666     2.473943
#[Out]# 65667     3.313256
#[Out]# 65668     4.725445
#[Out]# 65669     2.224418
#[Out]# 65670     5.988437
#[Out]# 65671     4.853316
#[Out]# 65672     2.230227
#[Out]# 65673     1.967091
#[Out]# 65674     3.266110
#[Out]# 65675     2.498431
#[Out]# 65676     1.153593
#[Out]# 65677     3.759911
#[Out]# 65678    12.429820
#[Out]# 65679     6.151282
#[Out]# 65680     1.948247
#[Out]# 65681     9.340885
#[Out]# 65682     3.695972
#[Out]# 65683     1.628010
#[Out]# 65684     5.269239
#[Out]# 65685     2.286134
#[Out]# 65686     3.177823
#[Out]# 65687     3.907710
#[Out]# 65688     4.326604
#[Out]# 65689     4.335415
#[Out]# 65690     2.796006
#[Out]# 65691     1.522740
#[Out]# 65692     8.815121
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:18:46
plt.title("Markups: Hypertension, NY")
#[Out]# <matplotlib.text.Text at 0x7fefb011ce10>
# Fri, 11 Sep 2015 11:18:52
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb010d358>
# Fri, 11 Sep 2015 11:19:03
plt.savefig("hypertensionNY")
# Fri, 11 Sep 2015 11:19:08
plt.clf()
# Fri, 11 Sep 2015 11:19:42
plt.hist(np.asarray(hypertension["markup"],bins=100))
# Fri, 11 Sep 2015 11:19:46
plt.hist(np.asarray(hypertension["markup"]),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:20:04
plt.hist(np.asarray(hypertension["markup"][hypertens]),bins=100)
# Fri, 11 Sep 2015 11:20:18
plt.hist(np.asarray(hypertension["markup"][hypertension["Provi]),bins=100)
# Fri, 11 Sep 2015 11:20:24
plt.hist(np.asarray(hypertension["markup"][hypertension["Provider]),bins=100)
# Fri, 11 Sep 2015 11:20:42
hypertension
#[Out]#                    DRG Definition  Provider Id  \
#[Out]# 65060  305 - HYPERTENSION W/O MCC        10001   
#[Out]# 65061  305 - HYPERTENSION W/O MCC        10006   
#[Out]# 65062  305 - HYPERTENSION W/O MCC        10011   
#[Out]# 65063  305 - HYPERTENSION W/O MCC        10016   
#[Out]# 65064  305 - HYPERTENSION W/O MCC        10019   
#[Out]# 65065  305 - HYPERTENSION W/O MCC        10023   
#[Out]# 65066  305 - HYPERTENSION W/O MCC        10024   
#[Out]# 65067  305 - HYPERTENSION W/O MCC        10033   
#[Out]# 65068  305 - HYPERTENSION W/O MCC        10039   
#[Out]# 65069  305 - HYPERTENSION W/O MCC        10040   
#[Out]# 65070  305 - HYPERTENSION W/O MCC        10055   
#[Out]# 65071  305 - HYPERTENSION W/O MCC        10078   
#[Out]# 65072  305 - HYPERTENSION W/O MCC        10090   
#[Out]# 65073  305 - HYPERTENSION W/O MCC        10092   
#[Out]# 65074  305 - HYPERTENSION W/O MCC        10103   
#[Out]# 65075  305 - HYPERTENSION W/O MCC        10112   
#[Out]# 65076  305 - HYPERTENSION W/O MCC        10113   
#[Out]# 65077  305 - HYPERTENSION W/O MCC        10139   
#[Out]# 65078  305 - HYPERTENSION W/O MCC        10144   
#[Out]# 65079  305 - HYPERTENSION W/O MCC        30006   
#[Out]# 65080  305 - HYPERTENSION W/O MCC        30014   
#[Out]# 65081  305 - HYPERTENSION W/O MCC        30023   
#[Out]# 65082  305 - HYPERTENSION W/O MCC        30036   
#[Out]# 65083  305 - HYPERTENSION W/O MCC        30043   
#[Out]# 65084  305 - HYPERTENSION W/O MCC        30055   
#[Out]# 65085  305 - HYPERTENSION W/O MCC        30061   
#[Out]# 65086  305 - HYPERTENSION W/O MCC        30065   
#[Out]# 65087  305 - HYPERTENSION W/O MCC        30088   
#[Out]# 65088  305 - HYPERTENSION W/O MCC        30089   
#[Out]# 65089  305 - HYPERTENSION W/O MCC        30092   
#[Out]# ...                           ...          ...   
#[Out]# 65999  305 - HYPERTENSION W/O MCC       500108   
#[Out]# 66000  305 - HYPERTENSION W/O MCC       500119   
#[Out]# 66001  305 - HYPERTENSION W/O MCC       500124   
#[Out]# 66002  305 - HYPERTENSION W/O MCC       500129   
#[Out]# 66003  305 - HYPERTENSION W/O MCC       500150   
#[Out]# 66004  305 - HYPERTENSION W/O MCC       500151   
#[Out]# 66005  305 - HYPERTENSION W/O MCC       510001   
#[Out]# 66006  305 - HYPERTENSION W/O MCC       510006   
#[Out]# 66007  305 - HYPERTENSION W/O MCC       510007   
#[Out]# 66008  305 - HYPERTENSION W/O MCC       510022   
#[Out]# 66009  305 - HYPERTENSION W/O MCC       510030   
#[Out]# 66010  305 - HYPERTENSION W/O MCC       510058   
#[Out]# 66011  305 - HYPERTENSION W/O MCC       510062   
#[Out]# 66012  305 - HYPERTENSION W/O MCC       510070   
#[Out]# 66013  305 - HYPERTENSION W/O MCC       520008   
#[Out]# 66014  305 - HYPERTENSION W/O MCC       520021   
#[Out]# 66015  305 - HYPERTENSION W/O MCC       520051   
#[Out]# 66016  305 - HYPERTENSION W/O MCC       520083   
#[Out]# 66017  305 - HYPERTENSION W/O MCC       520136   
#[Out]# 66018  305 - HYPERTENSION W/O MCC       520138   
#[Out]# 66019  305 - HYPERTENSION W/O MCC       520139   
#[Out]# 66020  305 - HYPERTENSION W/O MCC       520177   
#[Out]# 66021  305 - HYPERTENSION W/O MCC       530012   
#[Out]# 66022  305 - HYPERTENSION W/O MCC       530014   
#[Out]# 66023  305 - HYPERTENSION W/O MCC       670023   
#[Out]# 66024  305 - HYPERTENSION W/O MCC       670024   
#[Out]# 66025  305 - HYPERTENSION W/O MCC       670041   
#[Out]# 66026  305 - HYPERTENSION W/O MCC       670055   
#[Out]# 66027  305 - HYPERTENSION W/O MCC       670056   
#[Out]# 66028  305 - HYPERTENSION W/O MCC       670060   
#[Out]# 
#[Out]#                                     Provider Name  \
#[Out]# 65060            SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 65061              ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 65062                           ST VINCENT'S EAST   
#[Out]# 65063               SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 65064              HELEN KELLER MEMORIAL HOSPITAL   
#[Out]# 65065                BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 65066               JACKSON HOSPITAL & CLINIC INC   
#[Out]# 65067              UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 65068                         HUNTSVILLE HOSPITAL   
#[Out]# 65069             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 65070                            FLOWERS HOSPITAL   
#[Out]# 65071       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 65072                         PROVIDENCE HOSPITAL   
#[Out]# 65073               D C H REGIONAL MEDICAL CENTER   
#[Out]# 65074            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 65075              BRYAN W WHITFIELD MEM HOSP INC   
#[Out]# 65076                            MOBILE INFIRMARY   
#[Out]# 65077                    BROOKWOOD MEDICAL CENTER   
#[Out]# 65078                   SPRINGHILL MEDICAL CENTER   
#[Out]# 65079                       TUCSON MEDICAL CENTER   
#[Out]# 65080      JOHN C LINCOLN NORTH MOUNTAIN HOSPITAL   
#[Out]# 65081                    FLAGSTAFF MEDICAL CENTER   
#[Out]# 65082            CHANDLER REGIONAL MEDICAL CENTER   
#[Out]# 65083         SIERRA VISTA REGIONAL HEALTH CENTER   
#[Out]# 65084             KINGMAN REGIONAL MEDICAL CENTER   
#[Out]# 65085               BANNER BOSWELL MEDICAL CENTER   
#[Out]# 65086                BANNER DESERT MEDICAL CENTER   
#[Out]# 65087               BANNER BAYWOOD MEDICAL CENTER   
#[Out]# 65088           BANNER THUNDERBIRD MEDICAL CENTER   
#[Out]# 65089         JOHN C LINCOLN DEER VALLEY HOSPITAL   
#[Out]# ...                                           ...   
#[Out]# 65999                    ST JOSEPH MEDICAL CENTER   
#[Out]# 66000                             VALLEY HOSPITAL   
#[Out]# 66001           EVERGREEN HOSPITAL MEDICAL CENTER   
#[Out]# 66002           TACOMA GENERAL ALLENMORE HOSPITAL   
#[Out]# 66003          LEGACY SALMON CREEK MEDICAL CENTER   
#[Out]# 66004                         ST ANTHONY HOSPITAL   
#[Out]# 66005          WEST VIRGINIA UNIVERSITY HOSPITALS   
#[Out]# 66006                      UNITED HOSPITAL CENTER   
#[Out]# 66007                    ST MARY'S MEDICAL CENTER   
#[Out]# 66008              CHARLESTON AREA MEDICAL CENTER   
#[Out]# 66009                     DAVIS MEMORIAL HOSPITAL   
#[Out]# 66010                 CAMDEN CLARK MEDICAL CENTER   
#[Out]# 66011                        BECKLEY ARH HOSPITAL   
#[Out]# 66012                    RALEIGH GENERAL HOSPITAL   
#[Out]# 66013                  WAUKESHA MEMORIAL HOSPITAL   
#[Out]# 66014                      UNITED HOSPITAL SYSTEM   
#[Out]# 66015        COLUMBIA ST MARYS HOSPITAL MILWAUKEE   
#[Out]# 66016                           ST MARYS HOSPITAL   
#[Out]# 66017                WHEATON FRANCISCAN ST JOSEPH   
#[Out]# 66018              AURORA ST LUKES MEDICAL CENTER   
#[Out]# 66019            AURORA WEST ALLIS MEDICAL CENTER   
#[Out]# 66020                FROEDTERT MEM LUTHERAN HSPTL   
#[Out]# 66021                      WYOMING MEDICAL CENTER   
#[Out]# 66022            CHEYENNE REGIONAL MEDICAL CENTER   
#[Out]# 66023          METHODIST MANSFIELD MEDICAL CENTER   
#[Out]# 66024                NORTH CYPRESS MEDICAL CENTER   
#[Out]# 66025             SETON MEDICAL CENTER WILLIAMSON   
#[Out]# 66026                METHODIST STONE OAK HOSPITAL   
#[Out]# 66027                   SETON MEDICAL CENTER HAYS   
#[Out]# 66028  TEXAS REGIONAL MEDICAL CENTER AT SUNNYVALE   
#[Out]# 
#[Out]#                    Provider Street Address Provider City Provider State  \
#[Out]# 65060               1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 65061                   205 MARENGO STREET      FLORENCE             AL   
#[Out]# 65062           50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 65063              1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 65064         1300 SOUTH MONTGOMERY AVENUE     SHEFFIELD             AL   
#[Out]# 65065            2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 65066                     1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 65067                619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 65068                        101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 65069                 1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 65070                4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 65071                 400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 65072               6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 65073        809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 65074       701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 65075                  105 HIGHWAY 80 EAST     DEMOPOLIS             AL   
#[Out]# 65076            5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 65077  2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 65078                  3719 DAUPHIN STREET        MOBILE             AL   
#[Out]# 65079                 5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 65080               250 EAST DUNLAP AVENUE       PHOENIX             AZ   
#[Out]# 65081             1200 NORTH BEAVER STREET     FLAGSTAFF             AZ   
#[Out]# 65082                  1955 WEST FRYE ROAD      CHANDLER             AZ   
#[Out]# 65083                   300 EL CAMINO REAL  SIERRA VISTA             AZ   
#[Out]# 65084              3269 STOCKTON HILL ROAD       KINGMAN             AZ   
#[Out]# 65085     10401 WEST THUNDERBIRD BOULEVARD      SUN CITY             AZ   
#[Out]# 65086              1400 SOUTH  DOBSON ROAD          MESA             AZ   
#[Out]# 65087             6644 EAST BAYWOOD AVENUE          MESA             AZ   
#[Out]# 65088           5555 WEST THUNDERBIRD ROAD      GLENDALE             AZ   
#[Out]# 65089              19829 NORTH 27TH AVENUE       PHOENIX             AZ   
#[Out]# ...                                    ...           ...            ...   
#[Out]# 65999                  1717 SOUTH J STREET        TACOMA             WA   
#[Out]# 66000            12606 EAST MISSION AVENUE       SPOKANE             WA   
#[Out]# 66001                12040 NE 128TH STREET      KIRKLAND             WA   
#[Out]# 66002                     315 S MLK JR WAY        TACOMA             WA   
#[Out]# 66003                 2211 NE 139TH STREET     VANCOUVER             WA   
#[Out]# 66004        11567 CANTERWOOD BOULEVARD NW    GIG HARBOR             WA   
#[Out]# 66005                 MEDICAL CENTER DRIVE    MORGANTOWN             WV   
#[Out]# 66006               327 MEDICAL PARK DRIVE    BRIDGEPORT             WV   
#[Out]# 66007                      2900 1ST AVENUE    HUNTINGTON             WV   
#[Out]# 66008                    501 MORRIS STREET    CHARLESTON             WV   
#[Out]# 66009                          PO BOX 1484        ELKINS             WV   
#[Out]# 66010                     800 GARFIELD AVE   PARKERSBURG             WV   
#[Out]# 66011                   306 STANAFORD ROAD       BECKLEY             WV   
#[Out]# 66012                     1710 HARPER ROAD       BECKLEY             WV   
#[Out]# 66013                     725 AMERICAN AVE      WAUKESHA             WI   
#[Out]# 66014                      6308 EIGHTH AVE       KENOSHA             WI   
#[Out]# 66015                       2323 N LAKE DR     MILWAUKEE             WI   
#[Out]# 66016                    700 SOUTH PARK ST       MADISON             WI   
#[Out]# 66017                   5000 W CHAMBERS ST     MILWAUKEE             WI   
#[Out]# 66018                  2900 W OKLAHOMA AVE     MILWAUKEE             WI   
#[Out]# 66019                   8901 W LINCOLN AVE    WEST ALLIS             WI   
#[Out]# 66020                 9200 W WISCONSIN AVE     MILWAUKEE             WI   
#[Out]# 66021                     1233 EAST 2ND ST        CASPER             WY   
#[Out]# 66022                 214 EAST 23RD STREET      CHEYENNE             WY   
#[Out]# 66023                  2700 E BROAD STREET     MANSFIELD             TX   
#[Out]# 66024              21214 NORTHWEST FREEWAY       CYPRESS             TX   
#[Out]# 66025                    201 SETON PARKWAY    ROUND ROCK             TX   
#[Out]# 66026                 1139 E SONTERRA BLVD   SAN ANTONIO             TX   
#[Out]# 66027                       6001 KYLE PKWY          KYLE             TX   
#[Out]# 66028               231 SOUTH COLLINS ROAD     SUNNYVALE             TX   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 65060              36301                                AL - Dothan   
#[Out]# 65061              35631                            AL - Birmingham   
#[Out]# 65062              35235                            AL - Birmingham   
#[Out]# 65063              35007                            AL - Birmingham   
#[Out]# 65064              35660                            AL - Birmingham   
#[Out]# 65065              36116                            AL - Montgomery   
#[Out]# 65066              36106                            AL - Montgomery   
#[Out]# 65067              35233                            AL - Birmingham   
#[Out]# 65068              35801                            AL - Huntsville   
#[Out]# 65069              35903                            AL - Birmingham   
#[Out]# 65070              36305                                AL - Dothan   
#[Out]# 65071              36207                            AL - Birmingham   
#[Out]# 65072              36608                                AL - Mobile   
#[Out]# 65073              35401                            AL - Tuscaloosa   
#[Out]# 65074              35211                            AL - Birmingham   
#[Out]# 65075              36732                            AL - Birmingham   
#[Out]# 65076              36652                                AL - Mobile   
#[Out]# 65077              35209                            AL - Birmingham   
#[Out]# 65078              36608                                AL - Mobile   
#[Out]# 65079              85712                                AZ - Tucson   
#[Out]# 65080              85020                               AZ - Phoenix   
#[Out]# 65081              86001                               AZ - Phoenix   
#[Out]# 65082              85224                                  AZ - Mesa   
#[Out]# 65083              85635                                AZ - Tucson   
#[Out]# 65084              86409                               AZ - Phoenix   
#[Out]# 65085              85351                              AZ - Sun City   
#[Out]# 65086              85202                                  AZ - Mesa   
#[Out]# 65087              85206                                  AZ - Mesa   
#[Out]# 65088              85306                               AZ - Phoenix   
#[Out]# 65089              85027                               AZ - Phoenix   
#[Out]# ...                  ...                                        ...   
#[Out]# 65999              98405                                WA - Tacoma   
#[Out]# 66000              99216                               WA - Spokane   
#[Out]# 66001              98034                               WA - Seattle   
#[Out]# 66002              98415                                WA - Tacoma   
#[Out]# 66003              98686                              OR - Portland   
#[Out]# 66004              98332                                WA - Tacoma   
#[Out]# 66005              26506                            WV - Morgantown   
#[Out]# 66006              26330                            WV - Morgantown   
#[Out]# 66007              25701                            WV - Huntington   
#[Out]# 66008              25301                            WV - Charleston   
#[Out]# 66009              26241                            WV - Morgantown   
#[Out]# 66010              26101                            WV - Charleston   
#[Out]# 66011              25801                            WV - Charleston   
#[Out]# 66012              25801                            WV - Charleston   
#[Out]# 66013              53188                             WI - Milwaukee   
#[Out]# 66014              53143                             WI - Milwaukee   
#[Out]# 66015              53211                             WI - Milwaukee   
#[Out]# 66016              53715                               WI - Madison   
#[Out]# 66017              53210                             WI - Milwaukee   
#[Out]# 66018              53215                             WI - Milwaukee   
#[Out]# 66019              53227                             WI - Milwaukee   
#[Out]# 66020              53226                             WI - Milwaukee   
#[Out]# 66021              82601                                WY - Casper   
#[Out]# 66022              82001                          CO - Fort Collins   
#[Out]# 66023              76063                            TX - Fort Worth   
#[Out]# 66024              77429                               TX - Houston   
#[Out]# 66025              78664                                TX - Austin   
#[Out]# 66026              78258                           TX - San Antonio   
#[Out]# 66027              78640                                TX - Austin   
#[Out]# 66028              75182                                TX - Dallas   
#[Out]# 
#[Out]#        Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 65060                21             17187.666670             3532.476190   
#[Out]# 65061                13             16818.384620             3557.230769   
#[Out]# 65062                11             21731.545450             3530.272727   
#[Out]# 65063                19             17048.263160             3494.736842   
#[Out]# 65064                11             17953.818180             3861.909091   
#[Out]# 65065                11             10451.090910             3962.545455   
#[Out]# 65066                11             13748.363640             3421.636364   
#[Out]# 65067                38             15960.000000             4642.578947   
#[Out]# 65068                25             23610.600000             3634.280000   
#[Out]# 65069                19             41036.263160             3571.789474   
#[Out]# 65070                11             21801.454550             3285.272727   
#[Out]# 65071                17              9504.411765             3329.176471   
#[Out]# 65072                11             11451.636360             4659.181818   
#[Out]# 65073                82             15648.707320             3957.939024   
#[Out]# 65074                11             19641.181820             3761.636364   
#[Out]# 65075                17              5397.117647             3276.411765   
#[Out]# 65076                37             14267.216220             3397.459459   
#[Out]# 65077                32             50541.531250             3484.906250   
#[Out]# 65078                11             12068.181820             3002.545455   
#[Out]# 65079                11             16036.363640             4523.000000   
#[Out]# 65080                31             21633.612900             4088.870968   
#[Out]# 65081                21             20513.142860             6458.619048   
#[Out]# 65082                16             30256.437500             4104.562500   
#[Out]# 65083                15             15548.066670             5431.000000   
#[Out]# 65084                16             22512.375000             4672.062500   
#[Out]# 65085                33             24754.060610             3708.424242   
#[Out]# 65086                15             17849.800000             5014.133333   
#[Out]# 65087                27             18233.444440             3777.222222   
#[Out]# 65088                15             21456.466670             4409.866667   
#[Out]# 65089                12             21351.500000             4057.083333   
#[Out]# ...                 ...                      ...                     ...   
#[Out]# 65999                18             28179.222220             4818.055556   
#[Out]# 66000                18             19485.055560             3946.277778   
#[Out]# 66001                13             23903.615380             4174.076923   
#[Out]# 66002                12             25560.666670             9616.000000   
#[Out]# 66003                13             20224.538460             5035.769231   
#[Out]# 66004                11             19109.000000             4008.909091   
#[Out]# 66005                44             12360.181820             5590.363636   
#[Out]# 66006                11             10338.545450             3934.272727   
#[Out]# 66007                31             13822.032260             4333.387097   
#[Out]# 66008                22             18664.045450             4311.727273   
#[Out]# 66009                13              7266.692308             3556.461538   
#[Out]# 66010                14              9910.285714             3532.428571   
#[Out]# 66011                13              9601.846154             3624.615385   
#[Out]# 66012                54              8335.407407             3736.648148   
#[Out]# 66013                14             14119.285710             4432.714286   
#[Out]# 66014                19             15567.526320             3918.789474   
#[Out]# 66015                12             14715.083330             4714.583333   
#[Out]# 66016                12             17924.500000             4575.833333   
#[Out]# 66017                16             13459.625000             5628.562500   
#[Out]# 66018                37             16401.891890             4636.567568   
#[Out]# 66019                13             16192.076920             3822.230769   
#[Out]# 66020                13             13989.230770             5578.615385   
#[Out]# 66021                19             13095.578950             4156.052632   
#[Out]# 66022                21             12831.571430             5433.142857   
#[Out]# 66023                13             22199.076920             3484.538462   
#[Out]# 66024                14             47589.000000             3542.142857   
#[Out]# 66025                11             26933.727270             3820.454545   
#[Out]# 66026                16             23698.375000             3691.312500   
#[Out]# 66027                12             38615.666670             3646.500000   
#[Out]# 66028                12             25933.833330             3564.833333   
#[Out]# 
#[Out]#        Average Medicare Payments                        proc proc code  \
#[Out]# 65060                2579.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65061                2006.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65062                2079.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65063                2315.157895  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65064                2108.727273  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65065                2901.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65066                2668.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65067                3462.605263  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65068                2501.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65069                2640.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65070                2216.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65071                2450.352941  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65072                2060.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65073                2838.012195  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65074                2687.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65075                2236.647059  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65076                2374.216216  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65077                2529.906250  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65078                1933.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65079                3214.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65080                3135.064516  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65081                5447.761905  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65082                2999.812500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65083                3610.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65084                3464.500000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65085                2687.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65086                3161.533333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65087                2772.777778  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65088                3207.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65089                3174.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# ...                          ...                         ...       ...   
#[Out]# 65999                3297.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66000                2766.944444  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66001                3146.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66002                3331.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66003                3360.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66004                3160.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66005                3840.386364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66006                2572.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66007                2535.612903  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66008                2869.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66009                2543.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66010                2177.714286  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66011                2767.153846  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66012                2442.555556  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66013                2271.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66014                2932.052632  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66015                3591.416667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66016                3146.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66017                3308.187500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66018                2996.864865  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66019                3015.461538  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66020                4063.692308  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66021                2807.315789  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66022                4372.571429  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66023                2563.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66024                2529.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66025                2953.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66026                1931.250000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66027                2559.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66028                2686.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 
#[Out]#           markup  
#[Out]# 65060   6.663608  
#[Out]# 65061   8.381469  
#[Out]# 65062  10.451970  
#[Out]# 65063   7.363758  
#[Out]# 65064   8.514054  
#[Out]# 65065   3.602018  
#[Out]# 65066   5.152709  
#[Out]# 65067   4.609246  
#[Out]# 65068   9.440464  
#[Out]# 65069  15.544039  
#[Out]# 65070   9.835780  
#[Out]# 65071   3.878793  
#[Out]# 65072   5.557330  
#[Out]# 65073   5.513968  
#[Out]# 65074   7.307482  
#[Out]# 65075   2.413039  
#[Out]# 65076   6.009232  
#[Out]# 65077  19.977630  
#[Out]# 65078   6.240598  
#[Out]# 65079   4.988829  
#[Out]# 65080   6.900532  
#[Out]# 65081   3.765426  
#[Out]# 65082  10.086110  
#[Out]# 65083   4.306546  
#[Out]# 65084   6.498016  
#[Out]# 65085   9.209411  
#[Out]# 65086   5.645931  
#[Out]# 65087   6.575877  
#[Out]# 65088   6.690510  
#[Out]# 65089   6.727001  
#[Out]# ...          ...  
#[Out]# 65999   8.544768  
#[Out]# 66000   7.042084  
#[Out]# 66001   7.596612  
#[Out]# 66002   7.673572  
#[Out]# 66003   6.019208  
#[Out]# 66004   6.046108  
#[Out]# 66005   3.218474  
#[Out]# 66006   4.018800  
#[Out]# 66007   5.451160  
#[Out]# 66008   6.503976  
#[Out]# 66009   2.857527  
#[Out]# 66010   4.550774  
#[Out]# 66011   3.469936  
#[Out]# 66012   3.412576  
#[Out]# 66013   6.216429  
#[Out]# 66014   5.309429  
#[Out]# 66015   4.097292  
#[Out]# 66016   5.696044  
#[Out]# 66017   4.068580  
#[Out]# 66018   5.473017  
#[Out]# 66019   5.369684  
#[Out]# 66020   3.442493  
#[Out]# 66021   4.664804  
#[Out]# 66022   2.934560  
#[Out]# 66023   8.659285  
#[Out]# 66024  18.815193  
#[Out]# 66025   9.117995  
#[Out]# 66026  12.271003  
#[Out]# 66027  15.089157  
#[Out]# 66028   9.654588  
#[Out]# 
#[Out]# [969 rows x 15 columns]
# Fri, 11 Sep 2015 11:20:55
first=hypertension["markup"]
# Fri, 11 Sep 2015 11:21:23
local=hypertension["markup"][hypertension["Provider State"]=="NY"]
# Fri, 11 Sep 2015 11:21:25
local
#[Out]# 65630     4.463864
#[Out]# 65631     2.795449
#[Out]# 65632     1.015370
#[Out]# 65633     3.293062
#[Out]# 65634     5.334485
#[Out]# 65635     1.474004
#[Out]# 65636     2.123038
#[Out]# 65637     8.042365
#[Out]# 65638     3.165519
#[Out]# 65639     2.450440
#[Out]# 65640     3.898198
#[Out]# 65641     6.132179
#[Out]# 65642     5.215055
#[Out]# 65643     4.050614
#[Out]# 65644     4.271323
#[Out]# 65645     6.146159
#[Out]# 65646     5.117399
#[Out]# 65647     4.895180
#[Out]# 65648     3.591588
#[Out]# 65649     5.084679
#[Out]# 65650     4.267287
#[Out]# 65651     7.728625
#[Out]# 65652     7.332353
#[Out]# 65653     4.719444
#[Out]# 65654     7.902122
#[Out]# 65655     1.259224
#[Out]# 65656     3.702465
#[Out]# 65657     8.463399
#[Out]# 65658     3.127931
#[Out]# 65659     6.375890
#[Out]#            ...    
#[Out]# 65663     5.207313
#[Out]# 65664     6.726401
#[Out]# 65665     6.903109
#[Out]# 65666     2.473943
#[Out]# 65667     3.313256
#[Out]# 65668     4.725445
#[Out]# 65669     2.224418
#[Out]# 65670     5.988437
#[Out]# 65671     4.853316
#[Out]# 65672     2.230227
#[Out]# 65673     1.967091
#[Out]# 65674     3.266110
#[Out]# 65675     2.498431
#[Out]# 65676     1.153593
#[Out]# 65677     3.759911
#[Out]# 65678    12.429820
#[Out]# 65679     6.151282
#[Out]# 65680     1.948247
#[Out]# 65681     9.340885
#[Out]# 65682     3.695972
#[Out]# 65683     1.628010
#[Out]# 65684     5.269239
#[Out]# 65685     2.286134
#[Out]# 65686     3.177823
#[Out]# 65687     3.907710
#[Out]# 65688     4.326604
#[Out]# 65689     4.335415
#[Out]# 65690     2.796006
#[Out]# 65691     1.522740
#[Out]# 65692     8.815121
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:21:34
national=hypertension["markup"]
# Fri, 11 Sep 2015 11:21:48
plt.hist(local,bins=10)
# Fri, 11 Sep 2015 11:21:55
plt.hist(np.asarray(local),bins=10)
#[Out]# (array([  9.,  13.,  13.,  12.,   5.,   4.,   4.,   1.,   1.,   1.]),
#[Out]#  array([  1.01536964,   2.15681471,   3.29825979,   4.43970487,
#[Out]#           5.58114995,   6.72259502,   7.8640401 ,   9.00548518,
#[Out]#          10.14693026,  11.28837533,  12.42982041]),
#[Out]#  <a list of 10 Patch objects>)
# Fri, 11 Sep 2015 11:22:05
plt.hist(np.asarray(local),bins=20)
#[Out]# (array([ 5.,  4.,  6.,  7.,  5.,  8.,  6.,  6.,  4.,  1.,  2.,  2.,  2.,
#[Out]#          2.,  1.,  0.,  0.,  1.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.58609218,   2.15681471,   2.72753725,
#[Out]#           3.29825979,   3.86898233,   4.43970487,   5.01042741,
#[Out]#           5.58114995,   6.15187248,   6.72259502,   7.29331756,
#[Out]#           7.8640401 ,   8.43476264,   9.00548518,   9.57620772,
#[Out]#          10.14693026,  10.71765279,  11.28837533,  11.85909787,  12.42982041]),
#[Out]#  <a list of 20 Patch objects>)
# Fri, 11 Sep 2015 11:22:09
plt.hist(np.asarray(local),bins=30)
#[Out]# (array([ 3.,  3.,  3.,  6.,  2.,  5.,  2.,  7.,  4.,  4.,  4.,  4.,  0.,
#[Out]#          4.,  1.,  2.,  1.,  1.,  2.,  1.,  1.,  1.,  0.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.39585133,   1.77633302,   2.15681471,
#[Out]#           2.53729641,   2.9177781 ,   3.29825979,   3.67874148,
#[Out]#           4.05922318,   4.43970487,   4.82018656,   5.20066825,
#[Out]#           5.58114995,   5.96163164,   6.34211333,   6.72259502,
#[Out]#           7.10307672,   7.48355841,   7.8640401 ,   8.24452179,
#[Out]#           8.62500349,   9.00548518,   9.38596687,   9.76644856,
#[Out]#          10.14693026,  10.52741195,  10.90789364,  11.28837533,
#[Out]#          11.66885702,  12.04933872,  12.42982041]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:22:35
plt.title("Markups: hypertension; national vs NY")
#[Out]# <matplotlib.text.Text at 0x7fefb00436a0>
# Fri, 11 Sep 2015 11:22:46
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefb00e46a0>
# Fri, 11 Sep 2015 11:22:55
plt.savefig("hypertension comparison")
# Fri, 11 Sep 2015 11:24:22
nationaldiabetes=diabetes["markup"]
# Fri, 11 Sep 2015 11:24:25
nationaldiabetes=diabetes["markup"]
# Fri, 11 Sep 2015 11:24:27
diabetes
# Fri, 11 Sep 2015 11:25:07
diabetesmccnational=diabetesmcc["markup"]
# Fri, 11 Sep 2015 11:25:26
diabetesmccNY=diabetesmcc["markup"][diabetesmcc["Provider State"]=="N
# Fri, 11 Sep 2015 11:25:32
diabetesmccNY=diabetesmcc["markup"][diabetesmcc["Provider State"]=="NY"]
# Fri, 11 Sep 2015 11:25:44
diabetesmccnational
#[Out]# 118210     4.252082
#[Out]# 118211     4.350966
#[Out]# 118212     5.178436
#[Out]# 118213     4.197407
#[Out]# 118214     4.804503
#[Out]# 118215     6.071612
#[Out]# 118216     2.133313
#[Out]# 118217     5.772740
#[Out]# 118218     5.142182
#[Out]# 118219     4.106965
#[Out]# 118220     8.393442
#[Out]# 118221     4.479573
#[Out]# 118222     3.650863
#[Out]# 118223     4.197495
#[Out]# 118224     5.165595
#[Out]# 118225     3.455930
#[Out]# 118226     2.940485
#[Out]# 118227     4.513848
#[Out]# 118228     4.124426
#[Out]# 118229     5.615263
#[Out]# 118230     4.918253
#[Out]# 118231     4.151149
#[Out]# 118232     4.324748
#[Out]# 118233     3.556893
#[Out]# 118234     2.656044
#[Out]# 118235     3.173933
#[Out]# 118236     4.003997
#[Out]# 118237     3.878045
#[Out]# 118238     4.248939
#[Out]# 118239     4.640586
#[Out]#             ...    
#[Out]# 118949     4.070976
#[Out]# 118950    10.414994
#[Out]# 118951     7.751603
#[Out]# 118952     2.718412
#[Out]# 118953     3.484375
#[Out]# 118954     4.631264
#[Out]# 118955     2.924096
#[Out]# 118956     2.022685
#[Out]# 118957     2.099979
#[Out]# 118958     4.716606
#[Out]# 118959     3.400929
#[Out]# 118960     4.775731
#[Out]# 118961     3.882812
#[Out]# 118962     5.874427
#[Out]# 118963     3.653387
#[Out]# 118964     2.872969
#[Out]# 118965     4.848930
#[Out]# 118966     2.664886
#[Out]# 118967     4.158325
#[Out]# 118968     2.756745
#[Out]# 118969     2.527383
#[Out]# 118970     3.331725
#[Out]# 118971     3.068000
#[Out]# 118972     3.900030
#[Out]# 118973     3.629257
#[Out]# 118974     3.065028
#[Out]# 118975     3.635318
#[Out]# 118976     2.829200
#[Out]# 118977     3.376437
#[Out]# 118978     3.362292
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:26:08
plt.hist(diabetesmccnational,bins=100)
# Fri, 11 Sep 2015 11:26:16
diabetesmccnational
#[Out]# 118210     4.252082
#[Out]# 118211     4.350966
#[Out]# 118212     5.178436
#[Out]# 118213     4.197407
#[Out]# 118214     4.804503
#[Out]# 118215     6.071612
#[Out]# 118216     2.133313
#[Out]# 118217     5.772740
#[Out]# 118218     5.142182
#[Out]# 118219     4.106965
#[Out]# 118220     8.393442
#[Out]# 118221     4.479573
#[Out]# 118222     3.650863
#[Out]# 118223     4.197495
#[Out]# 118224     5.165595
#[Out]# 118225     3.455930
#[Out]# 118226     2.940485
#[Out]# 118227     4.513848
#[Out]# 118228     4.124426
#[Out]# 118229     5.615263
#[Out]# 118230     4.918253
#[Out]# 118231     4.151149
#[Out]# 118232     4.324748
#[Out]# 118233     3.556893
#[Out]# 118234     2.656044
#[Out]# 118235     3.173933
#[Out]# 118236     4.003997
#[Out]# 118237     3.878045
#[Out]# 118238     4.248939
#[Out]# 118239     4.640586
#[Out]#             ...    
#[Out]# 118949     4.070976
#[Out]# 118950    10.414994
#[Out]# 118951     7.751603
#[Out]# 118952     2.718412
#[Out]# 118953     3.484375
#[Out]# 118954     4.631264
#[Out]# 118955     2.924096
#[Out]# 118956     2.022685
#[Out]# 118957     2.099979
#[Out]# 118958     4.716606
#[Out]# 118959     3.400929
#[Out]# 118960     4.775731
#[Out]# 118961     3.882812
#[Out]# 118962     5.874427
#[Out]# 118963     3.653387
#[Out]# 118964     2.872969
#[Out]# 118965     4.848930
#[Out]# 118966     2.664886
#[Out]# 118967     4.158325
#[Out]# 118968     2.756745
#[Out]# 118969     2.527383
#[Out]# 118970     3.331725
#[Out]# 118971     3.068000
#[Out]# 118972     3.900030
#[Out]# 118973     3.629257
#[Out]# 118974     3.065028
#[Out]# 118975     3.635318
#[Out]# 118976     2.829200
#[Out]# 118977     3.376437
#[Out]# 118978     3.362292
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:26:19
len(_)
#[Out]# 769
# Fri, 11 Sep 2015 11:26:28
plt.hist(np.asarray(diabetesmccnational),bins=100)
#[Out]# (array([  2.,   1.,  23.,   7.,   5.,   7.,   4.,  13.,  16.,  18.,  24.,
#[Out]#          35.,  29.,  19.,  29.,  35.,  33.,  37.,  32.,  39.,  34.,  28.,
#[Out]#          25.,  20.,  20.,  20.,  19.,  11.,  14.,  13.,  13.,  16.,  12.,
#[Out]#          12.,   6.,   3.,   6.,   5.,   6.,  11.,   4.,   2.,   0.,   3.,
#[Out]#           6.,   4.,   4.,   5.,   5.,   1.,   1.,   0.,   4.,   3.,   2.,
#[Out]#           3.,   4.,   1.,   0.,   1.,   0.,   2.,   2.,   2.,   0.,   1.,
#[Out]#           0.,   0.,   1.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   1.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  0.73852453,   0.90979033,   1.08105613,   1.25232194,
#[Out]#           1.42358774,   1.59485354,   1.76611934,   1.93738514,
#[Out]#           2.10865094,   2.27991674,   2.45118255,   2.62244835,
#[Out]#           2.79371415,   2.96497995,   3.13624575,   3.30751155,
#[Out]#           3.47877735,   3.65004316,   3.82130896,   3.99257476,
#[Out]#           4.16384056,   4.33510636,   4.50637216,   4.67763796,
#[Out]#           4.84890377,   5.02016957,   5.19143537,   5.36270117,
#[Out]#           5.53396697,   5.70523277,   5.87649857,   6.04776438,
#[Out]#           6.21903018,   6.39029598,   6.56156178,   6.73282758,
#[Out]#           6.90409338,   7.07535918,   7.24662499,   7.41789079,
#[Out]#           7.58915659,   7.76042239,   7.93168819,   8.10295399,
#[Out]#           8.27421979,   8.4454856 ,   8.6167514 ,   8.7880172 ,
#[Out]#           8.959283  ,   9.1305488 ,   9.3018146 ,   9.4730804 ,
#[Out]#           9.64434621,   9.81561201,   9.98687781,  10.15814361,
#[Out]#          10.32940941,  10.50067521,  10.67194102,  10.84320682,
#[Out]#          11.01447262,  11.18573842,  11.35700422,  11.52827002,
#[Out]#          11.69953582,  11.87080163,  12.04206743,  12.21333323,
#[Out]#          12.38459903,  12.55586483,  12.72713063,  12.89839643,
#[Out]#          13.06966224,  13.24092804,  13.41219384,  13.58345964,
#[Out]#          13.75472544,  13.92599124,  14.09725704,  14.26852285,
#[Out]#          14.43978865,  14.61105445,  14.78232025,  14.95358605,
#[Out]#          15.12485185,  15.29611765,  15.46738346,  15.63864926,
#[Out]#          15.80991506,  15.98118086,  16.15244666,  16.32371246,
#[Out]#          16.49497826,  16.66624407,  16.83750987,  17.00877567,
#[Out]#          17.18004147,  17.35130727,  17.52257307,  17.69383887,  17.86510468]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:26:40
plt.hist(np.asarray(diabetesmccNY),bins=10)
#[Out]# (array([ 4.,  4.,  8.,  9.,  9.,  3.,  4.,  3.,  3.,  2.]),
#[Out]#  array([ 0.89046316,  1.5357632 ,  2.18106325,  2.82636329,  3.47166333,
#[Out]#          4.11696338,  4.76226342,  5.40756346,  6.05286351,  6.69816355,
#[Out]#          7.34346359]),
#[Out]#  <a list of 10 Patch objects>)
# Fri, 11 Sep 2015 11:26:44
plt.hist(np.asarray(diabetesmccNY),bins=20)
#[Out]# (array([ 2.,  2.,  2.,  2.,  2.,  6.,  5.,  4.,  5.,  4.,  1.,  2.,  4.,
#[Out]#          0.,  0.,  3.,  1.,  2.,  1.,  1.]),
#[Out]#  array([ 0.89046316,  1.21311318,  1.5357632 ,  1.85841322,  2.18106325,
#[Out]#          2.50371327,  2.82636329,  3.14901331,  3.47166333,  3.79431335,
#[Out]#          4.11696338,  4.4396134 ,  4.76226342,  5.08491344,  5.40756346,
#[Out]#          5.73021348,  6.05286351,  6.37551353,  6.69816355,  7.02081357,
#[Out]#          7.34346359]),
#[Out]#  <a list of 20 Patch objects>)
# Fri, 11 Sep 2015 11:26:46
plt.hist(np.asarray(diabetesmccNY),bins=30)
#[Out]# (array([ 2.,  1.,  1.,  2.,  0.,  2.,  2.,  1.,  5.,  5.,  2.,  2.,  3.,
#[Out]#          3.,  3.,  0.,  2.,  1.,  2.,  2.,  0.,  0.,  2.,  1.,  1.,  1.,
#[Out]#          1.,  0.,  1.,  1.]),
#[Out]#  array([ 0.89046316,  1.10556317,  1.32066319,  1.5357632 ,  1.75086322,
#[Out]#          1.96596323,  2.18106325,  2.39616326,  2.61126327,  2.82636329,
#[Out]#          3.0414633 ,  3.25656332,  3.47166333,  3.68676335,  3.90186336,
#[Out]#          4.11696338,  4.33206339,  4.5471634 ,  4.76226342,  4.97736343,
#[Out]#          5.19246345,  5.40756346,  5.62266348,  5.83776349,  6.05286351,
#[Out]#          6.26796352,  6.48306353,  6.69816355,  6.91326356,  7.12836358,
#[Out]#          7.34346359]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:34:48
plt.title("Markups diabetes w mcc; national vs NY")
#[Out]# <matplotlib.text.Text at 0x7fefa1a88da0>
# Fri, 11 Sep 2015 11:34:57
plt.xlabel("markup")
#[Out]# <matplotlib.text.Text at 0x7fefa1ae5390>
# Fri, 11 Sep 2015 11:35:11
plt.savefig("diabetesmcc comparison")
# Fri, 11 Sep 2015 11:35:16
plt.clf()
# Fri, 11 Sep 2015 11:35:43
hyp=data[data["proc code"]=="305"]
# Fri, 11 Sep 2015 11:35:45
hyp
#[Out]#                    DRG Definition  Provider Id  \
#[Out]# 65060  305 - HYPERTENSION W/O MCC        10001   
#[Out]# 65061  305 - HYPERTENSION W/O MCC        10006   
#[Out]# 65062  305 - HYPERTENSION W/O MCC        10011   
#[Out]# 65063  305 - HYPERTENSION W/O MCC        10016   
#[Out]# 65064  305 - HYPERTENSION W/O MCC        10019   
#[Out]# 65065  305 - HYPERTENSION W/O MCC        10023   
#[Out]# 65066  305 - HYPERTENSION W/O MCC        10024   
#[Out]# 65067  305 - HYPERTENSION W/O MCC        10033   
#[Out]# 65068  305 - HYPERTENSION W/O MCC        10039   
#[Out]# 65069  305 - HYPERTENSION W/O MCC        10040   
#[Out]# 65070  305 - HYPERTENSION W/O MCC        10055   
#[Out]# 65071  305 - HYPERTENSION W/O MCC        10078   
#[Out]# 65072  305 - HYPERTENSION W/O MCC        10090   
#[Out]# 65073  305 - HYPERTENSION W/O MCC        10092   
#[Out]# 65074  305 - HYPERTENSION W/O MCC        10103   
#[Out]# 65075  305 - HYPERTENSION W/O MCC        10112   
#[Out]# 65076  305 - HYPERTENSION W/O MCC        10113   
#[Out]# 65077  305 - HYPERTENSION W/O MCC        10139   
#[Out]# 65078  305 - HYPERTENSION W/O MCC        10144   
#[Out]# 65079  305 - HYPERTENSION W/O MCC        30006   
#[Out]# 65080  305 - HYPERTENSION W/O MCC        30014   
#[Out]# 65081  305 - HYPERTENSION W/O MCC        30023   
#[Out]# 65082  305 - HYPERTENSION W/O MCC        30036   
#[Out]# 65083  305 - HYPERTENSION W/O MCC        30043   
#[Out]# 65084  305 - HYPERTENSION W/O MCC        30055   
#[Out]# 65085  305 - HYPERTENSION W/O MCC        30061   
#[Out]# 65086  305 - HYPERTENSION W/O MCC        30065   
#[Out]# 65087  305 - HYPERTENSION W/O MCC        30088   
#[Out]# 65088  305 - HYPERTENSION W/O MCC        30089   
#[Out]# 65089  305 - HYPERTENSION W/O MCC        30092   
#[Out]# ...                           ...          ...   
#[Out]# 65999  305 - HYPERTENSION W/O MCC       500108   
#[Out]# 66000  305 - HYPERTENSION W/O MCC       500119   
#[Out]# 66001  305 - HYPERTENSION W/O MCC       500124   
#[Out]# 66002  305 - HYPERTENSION W/O MCC       500129   
#[Out]# 66003  305 - HYPERTENSION W/O MCC       500150   
#[Out]# 66004  305 - HYPERTENSION W/O MCC       500151   
#[Out]# 66005  305 - HYPERTENSION W/O MCC       510001   
#[Out]# 66006  305 - HYPERTENSION W/O MCC       510006   
#[Out]# 66007  305 - HYPERTENSION W/O MCC       510007   
#[Out]# 66008  305 - HYPERTENSION W/O MCC       510022   
#[Out]# 66009  305 - HYPERTENSION W/O MCC       510030   
#[Out]# 66010  305 - HYPERTENSION W/O MCC       510058   
#[Out]# 66011  305 - HYPERTENSION W/O MCC       510062   
#[Out]# 66012  305 - HYPERTENSION W/O MCC       510070   
#[Out]# 66013  305 - HYPERTENSION W/O MCC       520008   
#[Out]# 66014  305 - HYPERTENSION W/O MCC       520021   
#[Out]# 66015  305 - HYPERTENSION W/O MCC       520051   
#[Out]# 66016  305 - HYPERTENSION W/O MCC       520083   
#[Out]# 66017  305 - HYPERTENSION W/O MCC       520136   
#[Out]# 66018  305 - HYPERTENSION W/O MCC       520138   
#[Out]# 66019  305 - HYPERTENSION W/O MCC       520139   
#[Out]# 66020  305 - HYPERTENSION W/O MCC       520177   
#[Out]# 66021  305 - HYPERTENSION W/O MCC       530012   
#[Out]# 66022  305 - HYPERTENSION W/O MCC       530014   
#[Out]# 66023  305 - HYPERTENSION W/O MCC       670023   
#[Out]# 66024  305 - HYPERTENSION W/O MCC       670024   
#[Out]# 66025  305 - HYPERTENSION W/O MCC       670041   
#[Out]# 66026  305 - HYPERTENSION W/O MCC       670055   
#[Out]# 66027  305 - HYPERTENSION W/O MCC       670056   
#[Out]# 66028  305 - HYPERTENSION W/O MCC       670060   
#[Out]# 
#[Out]#                                     Provider Name  \
#[Out]# 65060            SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 65061              ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 65062                           ST VINCENT'S EAST   
#[Out]# 65063               SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 65064              HELEN KELLER MEMORIAL HOSPITAL   
#[Out]# 65065                BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 65066               JACKSON HOSPITAL & CLINIC INC   
#[Out]# 65067              UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 65068                         HUNTSVILLE HOSPITAL   
#[Out]# 65069             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 65070                            FLOWERS HOSPITAL   
#[Out]# 65071       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 65072                         PROVIDENCE HOSPITAL   
#[Out]# 65073               D C H REGIONAL MEDICAL CENTER   
#[Out]# 65074            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 65075              BRYAN W WHITFIELD MEM HOSP INC   
#[Out]# 65076                            MOBILE INFIRMARY   
#[Out]# 65077                    BROOKWOOD MEDICAL CENTER   
#[Out]# 65078                   SPRINGHILL MEDICAL CENTER   
#[Out]# 65079                       TUCSON MEDICAL CENTER   
#[Out]# 65080      JOHN C LINCOLN NORTH MOUNTAIN HOSPITAL   
#[Out]# 65081                    FLAGSTAFF MEDICAL CENTER   
#[Out]# 65082            CHANDLER REGIONAL MEDICAL CENTER   
#[Out]# 65083         SIERRA VISTA REGIONAL HEALTH CENTER   
#[Out]# 65084             KINGMAN REGIONAL MEDICAL CENTER   
#[Out]# 65085               BANNER BOSWELL MEDICAL CENTER   
#[Out]# 65086                BANNER DESERT MEDICAL CENTER   
#[Out]# 65087               BANNER BAYWOOD MEDICAL CENTER   
#[Out]# 65088           BANNER THUNDERBIRD MEDICAL CENTER   
#[Out]# 65089         JOHN C LINCOLN DEER VALLEY HOSPITAL   
#[Out]# ...                                           ...   
#[Out]# 65999                    ST JOSEPH MEDICAL CENTER   
#[Out]# 66000                             VALLEY HOSPITAL   
#[Out]# 66001           EVERGREEN HOSPITAL MEDICAL CENTER   
#[Out]# 66002           TACOMA GENERAL ALLENMORE HOSPITAL   
#[Out]# 66003          LEGACY SALMON CREEK MEDICAL CENTER   
#[Out]# 66004                         ST ANTHONY HOSPITAL   
#[Out]# 66005          WEST VIRGINIA UNIVERSITY HOSPITALS   
#[Out]# 66006                      UNITED HOSPITAL CENTER   
#[Out]# 66007                    ST MARY'S MEDICAL CENTER   
#[Out]# 66008              CHARLESTON AREA MEDICAL CENTER   
#[Out]# 66009                     DAVIS MEMORIAL HOSPITAL   
#[Out]# 66010                 CAMDEN CLARK MEDICAL CENTER   
#[Out]# 66011                        BECKLEY ARH HOSPITAL   
#[Out]# 66012                    RALEIGH GENERAL HOSPITAL   
#[Out]# 66013                  WAUKESHA MEMORIAL HOSPITAL   
#[Out]# 66014                      UNITED HOSPITAL SYSTEM   
#[Out]# 66015        COLUMBIA ST MARYS HOSPITAL MILWAUKEE   
#[Out]# 66016                           ST MARYS HOSPITAL   
#[Out]# 66017                WHEATON FRANCISCAN ST JOSEPH   
#[Out]# 66018              AURORA ST LUKES MEDICAL CENTER   
#[Out]# 66019            AURORA WEST ALLIS MEDICAL CENTER   
#[Out]# 66020                FROEDTERT MEM LUTHERAN HSPTL   
#[Out]# 66021                      WYOMING MEDICAL CENTER   
#[Out]# 66022            CHEYENNE REGIONAL MEDICAL CENTER   
#[Out]# 66023          METHODIST MANSFIELD MEDICAL CENTER   
#[Out]# 66024                NORTH CYPRESS MEDICAL CENTER   
#[Out]# 66025             SETON MEDICAL CENTER WILLIAMSON   
#[Out]# 66026                METHODIST STONE OAK HOSPITAL   
#[Out]# 66027                   SETON MEDICAL CENTER HAYS   
#[Out]# 66028  TEXAS REGIONAL MEDICAL CENTER AT SUNNYVALE   
#[Out]# 
#[Out]#                    Provider Street Address Provider City Provider State  \
#[Out]# 65060               1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 65061                   205 MARENGO STREET      FLORENCE             AL   
#[Out]# 65062           50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 65063              1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 65064         1300 SOUTH MONTGOMERY AVENUE     SHEFFIELD             AL   
#[Out]# 65065            2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 65066                     1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 65067                619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 65068                        101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 65069                 1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 65070                4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 65071                 400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 65072               6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 65073        809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 65074       701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 65075                  105 HIGHWAY 80 EAST     DEMOPOLIS             AL   
#[Out]# 65076            5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 65077  2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 65078                  3719 DAUPHIN STREET        MOBILE             AL   
#[Out]# 65079                 5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 65080               250 EAST DUNLAP AVENUE       PHOENIX             AZ   
#[Out]# 65081             1200 NORTH BEAVER STREET     FLAGSTAFF             AZ   
#[Out]# 65082                  1955 WEST FRYE ROAD      CHANDLER             AZ   
#[Out]# 65083                   300 EL CAMINO REAL  SIERRA VISTA             AZ   
#[Out]# 65084              3269 STOCKTON HILL ROAD       KINGMAN             AZ   
#[Out]# 65085     10401 WEST THUNDERBIRD BOULEVARD      SUN CITY             AZ   
#[Out]# 65086              1400 SOUTH  DOBSON ROAD          MESA             AZ   
#[Out]# 65087             6644 EAST BAYWOOD AVENUE          MESA             AZ   
#[Out]# 65088           5555 WEST THUNDERBIRD ROAD      GLENDALE             AZ   
#[Out]# 65089              19829 NORTH 27TH AVENUE       PHOENIX             AZ   
#[Out]# ...                                    ...           ...            ...   
#[Out]# 65999                  1717 SOUTH J STREET        TACOMA             WA   
#[Out]# 66000            12606 EAST MISSION AVENUE       SPOKANE             WA   
#[Out]# 66001                12040 NE 128TH STREET      KIRKLAND             WA   
#[Out]# 66002                     315 S MLK JR WAY        TACOMA             WA   
#[Out]# 66003                 2211 NE 139TH STREET     VANCOUVER             WA   
#[Out]# 66004        11567 CANTERWOOD BOULEVARD NW    GIG HARBOR             WA   
#[Out]# 66005                 MEDICAL CENTER DRIVE    MORGANTOWN             WV   
#[Out]# 66006               327 MEDICAL PARK DRIVE    BRIDGEPORT             WV   
#[Out]# 66007                      2900 1ST AVENUE    HUNTINGTON             WV   
#[Out]# 66008                    501 MORRIS STREET    CHARLESTON             WV   
#[Out]# 66009                          PO BOX 1484        ELKINS             WV   
#[Out]# 66010                     800 GARFIELD AVE   PARKERSBURG             WV   
#[Out]# 66011                   306 STANAFORD ROAD       BECKLEY             WV   
#[Out]# 66012                     1710 HARPER ROAD       BECKLEY             WV   
#[Out]# 66013                     725 AMERICAN AVE      WAUKESHA             WI   
#[Out]# 66014                      6308 EIGHTH AVE       KENOSHA             WI   
#[Out]# 66015                       2323 N LAKE DR     MILWAUKEE             WI   
#[Out]# 66016                    700 SOUTH PARK ST       MADISON             WI   
#[Out]# 66017                   5000 W CHAMBERS ST     MILWAUKEE             WI   
#[Out]# 66018                  2900 W OKLAHOMA AVE     MILWAUKEE             WI   
#[Out]# 66019                   8901 W LINCOLN AVE    WEST ALLIS             WI   
#[Out]# 66020                 9200 W WISCONSIN AVE     MILWAUKEE             WI   
#[Out]# 66021                     1233 EAST 2ND ST        CASPER             WY   
#[Out]# 66022                 214 EAST 23RD STREET      CHEYENNE             WY   
#[Out]# 66023                  2700 E BROAD STREET     MANSFIELD             TX   
#[Out]# 66024              21214 NORTHWEST FREEWAY       CYPRESS             TX   
#[Out]# 66025                    201 SETON PARKWAY    ROUND ROCK             TX   
#[Out]# 66026                 1139 E SONTERRA BLVD   SAN ANTONIO             TX   
#[Out]# 66027                       6001 KYLE PKWY          KYLE             TX   
#[Out]# 66028               231 SOUTH COLLINS ROAD     SUNNYVALE             TX   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 65060              36301                                AL - Dothan   
#[Out]# 65061              35631                            AL - Birmingham   
#[Out]# 65062              35235                            AL - Birmingham   
#[Out]# 65063              35007                            AL - Birmingham   
#[Out]# 65064              35660                            AL - Birmingham   
#[Out]# 65065              36116                            AL - Montgomery   
#[Out]# 65066              36106                            AL - Montgomery   
#[Out]# 65067              35233                            AL - Birmingham   
#[Out]# 65068              35801                            AL - Huntsville   
#[Out]# 65069              35903                            AL - Birmingham   
#[Out]# 65070              36305                                AL - Dothan   
#[Out]# 65071              36207                            AL - Birmingham   
#[Out]# 65072              36608                                AL - Mobile   
#[Out]# 65073              35401                            AL - Tuscaloosa   
#[Out]# 65074              35211                            AL - Birmingham   
#[Out]# 65075              36732                            AL - Birmingham   
#[Out]# 65076              36652                                AL - Mobile   
#[Out]# 65077              35209                            AL - Birmingham   
#[Out]# 65078              36608                                AL - Mobile   
#[Out]# 65079              85712                                AZ - Tucson   
#[Out]# 65080              85020                               AZ - Phoenix   
#[Out]# 65081              86001                               AZ - Phoenix   
#[Out]# 65082              85224                                  AZ - Mesa   
#[Out]# 65083              85635                                AZ - Tucson   
#[Out]# 65084              86409                               AZ - Phoenix   
#[Out]# 65085              85351                              AZ - Sun City   
#[Out]# 65086              85202                                  AZ - Mesa   
#[Out]# 65087              85206                                  AZ - Mesa   
#[Out]# 65088              85306                               AZ - Phoenix   
#[Out]# 65089              85027                               AZ - Phoenix   
#[Out]# ...                  ...                                        ...   
#[Out]# 65999              98405                                WA - Tacoma   
#[Out]# 66000              99216                               WA - Spokane   
#[Out]# 66001              98034                               WA - Seattle   
#[Out]# 66002              98415                                WA - Tacoma   
#[Out]# 66003              98686                              OR - Portland   
#[Out]# 66004              98332                                WA - Tacoma   
#[Out]# 66005              26506                            WV - Morgantown   
#[Out]# 66006              26330                            WV - Morgantown   
#[Out]# 66007              25701                            WV - Huntington   
#[Out]# 66008              25301                            WV - Charleston   
#[Out]# 66009              26241                            WV - Morgantown   
#[Out]# 66010              26101                            WV - Charleston   
#[Out]# 66011              25801                            WV - Charleston   
#[Out]# 66012              25801                            WV - Charleston   
#[Out]# 66013              53188                             WI - Milwaukee   
#[Out]# 66014              53143                             WI - Milwaukee   
#[Out]# 66015              53211                             WI - Milwaukee   
#[Out]# 66016              53715                               WI - Madison   
#[Out]# 66017              53210                             WI - Milwaukee   
#[Out]# 66018              53215                             WI - Milwaukee   
#[Out]# 66019              53227                             WI - Milwaukee   
#[Out]# 66020              53226                             WI - Milwaukee   
#[Out]# 66021              82601                                WY - Casper   
#[Out]# 66022              82001                          CO - Fort Collins   
#[Out]# 66023              76063                            TX - Fort Worth   
#[Out]# 66024              77429                               TX - Houston   
#[Out]# 66025              78664                                TX - Austin   
#[Out]# 66026              78258                           TX - San Antonio   
#[Out]# 66027              78640                                TX - Austin   
#[Out]# 66028              75182                                TX - Dallas   
#[Out]# 
#[Out]#        Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 65060                21             17187.666670             3532.476190   
#[Out]# 65061                13             16818.384620             3557.230769   
#[Out]# 65062                11             21731.545450             3530.272727   
#[Out]# 65063                19             17048.263160             3494.736842   
#[Out]# 65064                11             17953.818180             3861.909091   
#[Out]# 65065                11             10451.090910             3962.545455   
#[Out]# 65066                11             13748.363640             3421.636364   
#[Out]# 65067                38             15960.000000             4642.578947   
#[Out]# 65068                25             23610.600000             3634.280000   
#[Out]# 65069                19             41036.263160             3571.789474   
#[Out]# 65070                11             21801.454550             3285.272727   
#[Out]# 65071                17              9504.411765             3329.176471   
#[Out]# 65072                11             11451.636360             4659.181818   
#[Out]# 65073                82             15648.707320             3957.939024   
#[Out]# 65074                11             19641.181820             3761.636364   
#[Out]# 65075                17              5397.117647             3276.411765   
#[Out]# 65076                37             14267.216220             3397.459459   
#[Out]# 65077                32             50541.531250             3484.906250   
#[Out]# 65078                11             12068.181820             3002.545455   
#[Out]# 65079                11             16036.363640             4523.000000   
#[Out]# 65080                31             21633.612900             4088.870968   
#[Out]# 65081                21             20513.142860             6458.619048   
#[Out]# 65082                16             30256.437500             4104.562500   
#[Out]# 65083                15             15548.066670             5431.000000   
#[Out]# 65084                16             22512.375000             4672.062500   
#[Out]# 65085                33             24754.060610             3708.424242   
#[Out]# 65086                15             17849.800000             5014.133333   
#[Out]# 65087                27             18233.444440             3777.222222   
#[Out]# 65088                15             21456.466670             4409.866667   
#[Out]# 65089                12             21351.500000             4057.083333   
#[Out]# ...                 ...                      ...                     ...   
#[Out]# 65999                18             28179.222220             4818.055556   
#[Out]# 66000                18             19485.055560             3946.277778   
#[Out]# 66001                13             23903.615380             4174.076923   
#[Out]# 66002                12             25560.666670             9616.000000   
#[Out]# 66003                13             20224.538460             5035.769231   
#[Out]# 66004                11             19109.000000             4008.909091   
#[Out]# 66005                44             12360.181820             5590.363636   
#[Out]# 66006                11             10338.545450             3934.272727   
#[Out]# 66007                31             13822.032260             4333.387097   
#[Out]# 66008                22             18664.045450             4311.727273   
#[Out]# 66009                13              7266.692308             3556.461538   
#[Out]# 66010                14              9910.285714             3532.428571   
#[Out]# 66011                13              9601.846154             3624.615385   
#[Out]# 66012                54              8335.407407             3736.648148   
#[Out]# 66013                14             14119.285710             4432.714286   
#[Out]# 66014                19             15567.526320             3918.789474   
#[Out]# 66015                12             14715.083330             4714.583333   
#[Out]# 66016                12             17924.500000             4575.833333   
#[Out]# 66017                16             13459.625000             5628.562500   
#[Out]# 66018                37             16401.891890             4636.567568   
#[Out]# 66019                13             16192.076920             3822.230769   
#[Out]# 66020                13             13989.230770             5578.615385   
#[Out]# 66021                19             13095.578950             4156.052632   
#[Out]# 66022                21             12831.571430             5433.142857   
#[Out]# 66023                13             22199.076920             3484.538462   
#[Out]# 66024                14             47589.000000             3542.142857   
#[Out]# 66025                11             26933.727270             3820.454545   
#[Out]# 66026                16             23698.375000             3691.312500   
#[Out]# 66027                12             38615.666670             3646.500000   
#[Out]# 66028                12             25933.833330             3564.833333   
#[Out]# 
#[Out]#        Average Medicare Payments                        proc proc code  \
#[Out]# 65060                2579.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65061                2006.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65062                2079.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65063                2315.157895  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65064                2108.727273  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65065                2901.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65066                2668.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65067                3462.605263  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65068                2501.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65069                2640.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65070                2216.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65071                2450.352941  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65072                2060.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65073                2838.012195  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65074                2687.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65075                2236.647059  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65076                2374.216216  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65077                2529.906250  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65078                1933.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65079                3214.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65080                3135.064516  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65081                5447.761905  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65082                2999.812500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65083                3610.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65084                3464.500000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65085                2687.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65086                3161.533333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65087                2772.777778  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65088                3207.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65089                3174.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# ...                          ...                         ...       ...   
#[Out]# 65999                3297.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66000                2766.944444  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66001                3146.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66002                3331.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66003                3360.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66004                3160.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66005                3840.386364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66006                2572.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66007                2535.612903  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66008                2869.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66009                2543.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66010                2177.714286  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66011                2767.153846  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66012                2442.555556  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66013                2271.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66014                2932.052632  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66015                3591.416667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66016                3146.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66017                3308.187500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66018                2996.864865  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66019                3015.461538  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66020                4063.692308  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66021                2807.315789  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66022                4372.571429  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66023                2563.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66024                2529.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66025                2953.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66026                1931.250000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66027                2559.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66028                2686.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 
#[Out]#           markup  
#[Out]# 65060   6.663608  
#[Out]# 65061   8.381469  
#[Out]# 65062  10.451970  
#[Out]# 65063   7.363758  
#[Out]# 65064   8.514054  
#[Out]# 65065   3.602018  
#[Out]# 65066   5.152709  
#[Out]# 65067   4.609246  
#[Out]# 65068   9.440464  
#[Out]# 65069  15.544039  
#[Out]# 65070   9.835780  
#[Out]# 65071   3.878793  
#[Out]# 65072   5.557330  
#[Out]# 65073   5.513968  
#[Out]# 65074   7.307482  
#[Out]# 65075   2.413039  
#[Out]# 65076   6.009232  
#[Out]# 65077  19.977630  
#[Out]# 65078   6.240598  
#[Out]# 65079   4.988829  
#[Out]# 65080   6.900532  
#[Out]# 65081   3.765426  
#[Out]# 65082  10.086110  
#[Out]# 65083   4.306546  
#[Out]# 65084   6.498016  
#[Out]# 65085   9.209411  
#[Out]# 65086   5.645931  
#[Out]# 65087   6.575877  
#[Out]# 65088   6.690510  
#[Out]# 65089   6.727001  
#[Out]# ...          ...  
#[Out]# 65999   8.544768  
#[Out]# 66000   7.042084  
#[Out]# 66001   7.596612  
#[Out]# 66002   7.673572  
#[Out]# 66003   6.019208  
#[Out]# 66004   6.046108  
#[Out]# 66005   3.218474  
#[Out]# 66006   4.018800  
#[Out]# 66007   5.451160  
#[Out]# 66008   6.503976  
#[Out]# 66009   2.857527  
#[Out]# 66010   4.550774  
#[Out]# 66011   3.469936  
#[Out]# 66012   3.412576  
#[Out]# 66013   6.216429  
#[Out]# 66014   5.309429  
#[Out]# 66015   4.097292  
#[Out]# 66016   5.696044  
#[Out]# 66017   4.068580  
#[Out]# 66018   5.473017  
#[Out]# 66019   5.369684  
#[Out]# 66020   3.442493  
#[Out]# 66021   4.664804  
#[Out]# 66022   2.934560  
#[Out]# 66023   8.659285  
#[Out]# 66024  18.815193  
#[Out]# 66025   9.117995  
#[Out]# 66026  12.271003  
#[Out]# 66027  15.089157  
#[Out]# 66028   9.654588  
#[Out]# 
#[Out]# [969 rows x 15 columns]
# Fri, 11 Sep 2015 11:35:49
hyp.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 11:48:03
hyp
#[Out]#                    DRG Definition  Provider Id  \
#[Out]# 65060  305 - HYPERTENSION W/O MCC        10001   
#[Out]# 65061  305 - HYPERTENSION W/O MCC        10006   
#[Out]# 65062  305 - HYPERTENSION W/O MCC        10011   
#[Out]# 65063  305 - HYPERTENSION W/O MCC        10016   
#[Out]# 65064  305 - HYPERTENSION W/O MCC        10019   
#[Out]# 65065  305 - HYPERTENSION W/O MCC        10023   
#[Out]# 65066  305 - HYPERTENSION W/O MCC        10024   
#[Out]# 65067  305 - HYPERTENSION W/O MCC        10033   
#[Out]# 65068  305 - HYPERTENSION W/O MCC        10039   
#[Out]# 65069  305 - HYPERTENSION W/O MCC        10040   
#[Out]# 65070  305 - HYPERTENSION W/O MCC        10055   
#[Out]# 65071  305 - HYPERTENSION W/O MCC        10078   
#[Out]# 65072  305 - HYPERTENSION W/O MCC        10090   
#[Out]# 65073  305 - HYPERTENSION W/O MCC        10092   
#[Out]# 65074  305 - HYPERTENSION W/O MCC        10103   
#[Out]# 65075  305 - HYPERTENSION W/O MCC        10112   
#[Out]# 65076  305 - HYPERTENSION W/O MCC        10113   
#[Out]# 65077  305 - HYPERTENSION W/O MCC        10139   
#[Out]# 65078  305 - HYPERTENSION W/O MCC        10144   
#[Out]# 65079  305 - HYPERTENSION W/O MCC        30006   
#[Out]# 65080  305 - HYPERTENSION W/O MCC        30014   
#[Out]# 65081  305 - HYPERTENSION W/O MCC        30023   
#[Out]# 65082  305 - HYPERTENSION W/O MCC        30036   
#[Out]# 65083  305 - HYPERTENSION W/O MCC        30043   
#[Out]# 65084  305 - HYPERTENSION W/O MCC        30055   
#[Out]# 65085  305 - HYPERTENSION W/O MCC        30061   
#[Out]# 65086  305 - HYPERTENSION W/O MCC        30065   
#[Out]# 65087  305 - HYPERTENSION W/O MCC        30088   
#[Out]# 65088  305 - HYPERTENSION W/O MCC        30089   
#[Out]# 65089  305 - HYPERTENSION W/O MCC        30092   
#[Out]# ...                           ...          ...   
#[Out]# 65999  305 - HYPERTENSION W/O MCC       500108   
#[Out]# 66000  305 - HYPERTENSION W/O MCC       500119   
#[Out]# 66001  305 - HYPERTENSION W/O MCC       500124   
#[Out]# 66002  305 - HYPERTENSION W/O MCC       500129   
#[Out]# 66003  305 - HYPERTENSION W/O MCC       500150   
#[Out]# 66004  305 - HYPERTENSION W/O MCC       500151   
#[Out]# 66005  305 - HYPERTENSION W/O MCC       510001   
#[Out]# 66006  305 - HYPERTENSION W/O MCC       510006   
#[Out]# 66007  305 - HYPERTENSION W/O MCC       510007   
#[Out]# 66008  305 - HYPERTENSION W/O MCC       510022   
#[Out]# 66009  305 - HYPERTENSION W/O MCC       510030   
#[Out]# 66010  305 - HYPERTENSION W/O MCC       510058   
#[Out]# 66011  305 - HYPERTENSION W/O MCC       510062   
#[Out]# 66012  305 - HYPERTENSION W/O MCC       510070   
#[Out]# 66013  305 - HYPERTENSION W/O MCC       520008   
#[Out]# 66014  305 - HYPERTENSION W/O MCC       520021   
#[Out]# 66015  305 - HYPERTENSION W/O MCC       520051   
#[Out]# 66016  305 - HYPERTENSION W/O MCC       520083   
#[Out]# 66017  305 - HYPERTENSION W/O MCC       520136   
#[Out]# 66018  305 - HYPERTENSION W/O MCC       520138   
#[Out]# 66019  305 - HYPERTENSION W/O MCC       520139   
#[Out]# 66020  305 - HYPERTENSION W/O MCC       520177   
#[Out]# 66021  305 - HYPERTENSION W/O MCC       530012   
#[Out]# 66022  305 - HYPERTENSION W/O MCC       530014   
#[Out]# 66023  305 - HYPERTENSION W/O MCC       670023   
#[Out]# 66024  305 - HYPERTENSION W/O MCC       670024   
#[Out]# 66025  305 - HYPERTENSION W/O MCC       670041   
#[Out]# 66026  305 - HYPERTENSION W/O MCC       670055   
#[Out]# 66027  305 - HYPERTENSION W/O MCC       670056   
#[Out]# 66028  305 - HYPERTENSION W/O MCC       670060   
#[Out]# 
#[Out]#                                     Provider Name  \
#[Out]# 65060            SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 65061              ELIZA COFFEE MEMORIAL HOSPITAL   
#[Out]# 65062                           ST VINCENT'S EAST   
#[Out]# 65063               SHELBY BAPTIST MEDICAL CENTER   
#[Out]# 65064              HELEN KELLER MEMORIAL HOSPITAL   
#[Out]# 65065                BAPTIST MEDICAL CENTER SOUTH   
#[Out]# 65066               JACKSON HOSPITAL & CLINIC INC   
#[Out]# 65067              UNIVERSITY OF ALABAMA HOSPITAL   
#[Out]# 65068                         HUNTSVILLE HOSPITAL   
#[Out]# 65069             GADSDEN REGIONAL MEDICAL CENTER   
#[Out]# 65070                            FLOWERS HOSPITAL   
#[Out]# 65071       NORTHEAST ALABAMA REGIONAL MED CENTER   
#[Out]# 65072                         PROVIDENCE HOSPITAL   
#[Out]# 65073               D C H REGIONAL MEDICAL CENTER   
#[Out]# 65074            BAPTIST MEDICAL CENTER-PRINCETON   
#[Out]# 65075              BRYAN W WHITFIELD MEM HOSP INC   
#[Out]# 65076                            MOBILE INFIRMARY   
#[Out]# 65077                    BROOKWOOD MEDICAL CENTER   
#[Out]# 65078                   SPRINGHILL MEDICAL CENTER   
#[Out]# 65079                       TUCSON MEDICAL CENTER   
#[Out]# 65080      JOHN C LINCOLN NORTH MOUNTAIN HOSPITAL   
#[Out]# 65081                    FLAGSTAFF MEDICAL CENTER   
#[Out]# 65082            CHANDLER REGIONAL MEDICAL CENTER   
#[Out]# 65083         SIERRA VISTA REGIONAL HEALTH CENTER   
#[Out]# 65084             KINGMAN REGIONAL MEDICAL CENTER   
#[Out]# 65085               BANNER BOSWELL MEDICAL CENTER   
#[Out]# 65086                BANNER DESERT MEDICAL CENTER   
#[Out]# 65087               BANNER BAYWOOD MEDICAL CENTER   
#[Out]# 65088           BANNER THUNDERBIRD MEDICAL CENTER   
#[Out]# 65089         JOHN C LINCOLN DEER VALLEY HOSPITAL   
#[Out]# ...                                           ...   
#[Out]# 65999                    ST JOSEPH MEDICAL CENTER   
#[Out]# 66000                             VALLEY HOSPITAL   
#[Out]# 66001           EVERGREEN HOSPITAL MEDICAL CENTER   
#[Out]# 66002           TACOMA GENERAL ALLENMORE HOSPITAL   
#[Out]# 66003          LEGACY SALMON CREEK MEDICAL CENTER   
#[Out]# 66004                         ST ANTHONY HOSPITAL   
#[Out]# 66005          WEST VIRGINIA UNIVERSITY HOSPITALS   
#[Out]# 66006                      UNITED HOSPITAL CENTER   
#[Out]# 66007                    ST MARY'S MEDICAL CENTER   
#[Out]# 66008              CHARLESTON AREA MEDICAL CENTER   
#[Out]# 66009                     DAVIS MEMORIAL HOSPITAL   
#[Out]# 66010                 CAMDEN CLARK MEDICAL CENTER   
#[Out]# 66011                        BECKLEY ARH HOSPITAL   
#[Out]# 66012                    RALEIGH GENERAL HOSPITAL   
#[Out]# 66013                  WAUKESHA MEMORIAL HOSPITAL   
#[Out]# 66014                      UNITED HOSPITAL SYSTEM   
#[Out]# 66015        COLUMBIA ST MARYS HOSPITAL MILWAUKEE   
#[Out]# 66016                           ST MARYS HOSPITAL   
#[Out]# 66017                WHEATON FRANCISCAN ST JOSEPH   
#[Out]# 66018              AURORA ST LUKES MEDICAL CENTER   
#[Out]# 66019            AURORA WEST ALLIS MEDICAL CENTER   
#[Out]# 66020                FROEDTERT MEM LUTHERAN HSPTL   
#[Out]# 66021                      WYOMING MEDICAL CENTER   
#[Out]# 66022            CHEYENNE REGIONAL MEDICAL CENTER   
#[Out]# 66023          METHODIST MANSFIELD MEDICAL CENTER   
#[Out]# 66024                NORTH CYPRESS MEDICAL CENTER   
#[Out]# 66025             SETON MEDICAL CENTER WILLIAMSON   
#[Out]# 66026                METHODIST STONE OAK HOSPITAL   
#[Out]# 66027                   SETON MEDICAL CENTER HAYS   
#[Out]# 66028  TEXAS REGIONAL MEDICAL CENTER AT SUNNYVALE   
#[Out]# 
#[Out]#                    Provider Street Address Provider City Provider State  \
#[Out]# 65060               1108 ROSS CLARK CIRCLE        DOTHAN             AL   
#[Out]# 65061                   205 MARENGO STREET      FLORENCE             AL   
#[Out]# 65062           50 MEDICAL PARK EAST DRIVE    BIRMINGHAM             AL   
#[Out]# 65063              1000 FIRST STREET NORTH     ALABASTER             AL   
#[Out]# 65064         1300 SOUTH MONTGOMERY AVENUE     SHEFFIELD             AL   
#[Out]# 65065            2105 EAST SOUTH BOULEVARD    MONTGOMERY             AL   
#[Out]# 65066                     1725 PINE STREET    MONTGOMERY             AL   
#[Out]# 65067                619 SOUTH 19TH STREET    BIRMINGHAM             AL   
#[Out]# 65068                        101 SIVLEY RD    HUNTSVILLE             AL   
#[Out]# 65069                 1007 GOODYEAR AVENUE       GADSDEN             AL   
#[Out]# 65070                4370 WEST MAIN STREET        DOTHAN             AL   
#[Out]# 65071                 400 EAST 10TH STREET      ANNISTON             AL   
#[Out]# 65072               6801 AIRPORT BOULEVARD        MOBILE             AL   
#[Out]# 65073        809 UNIVERSITY BOULEVARD EAST    TUSCALOOSA             AL   
#[Out]# 65074       701 PRINCETON AVENUE SOUTHWEST    BIRMINGHAM             AL   
#[Out]# 65075                  105 HIGHWAY 80 EAST     DEMOPOLIS             AL   
#[Out]# 65076            5 MOBILE INFIRMARY CIRCLE        MOBILE             AL   
#[Out]# 65077  2010 BROOKWOOD MEDICAL CENTER DRIVE    BIRMINGHAM             AL   
#[Out]# 65078                  3719 DAUPHIN STREET        MOBILE             AL   
#[Out]# 65079                 5301 EAST GRANT ROAD        TUCSON             AZ   
#[Out]# 65080               250 EAST DUNLAP AVENUE       PHOENIX             AZ   
#[Out]# 65081             1200 NORTH BEAVER STREET     FLAGSTAFF             AZ   
#[Out]# 65082                  1955 WEST FRYE ROAD      CHANDLER             AZ   
#[Out]# 65083                   300 EL CAMINO REAL  SIERRA VISTA             AZ   
#[Out]# 65084              3269 STOCKTON HILL ROAD       KINGMAN             AZ   
#[Out]# 65085     10401 WEST THUNDERBIRD BOULEVARD      SUN CITY             AZ   
#[Out]# 65086              1400 SOUTH  DOBSON ROAD          MESA             AZ   
#[Out]# 65087             6644 EAST BAYWOOD AVENUE          MESA             AZ   
#[Out]# 65088           5555 WEST THUNDERBIRD ROAD      GLENDALE             AZ   
#[Out]# 65089              19829 NORTH 27TH AVENUE       PHOENIX             AZ   
#[Out]# ...                                    ...           ...            ...   
#[Out]# 65999                  1717 SOUTH J STREET        TACOMA             WA   
#[Out]# 66000            12606 EAST MISSION AVENUE       SPOKANE             WA   
#[Out]# 66001                12040 NE 128TH STREET      KIRKLAND             WA   
#[Out]# 66002                     315 S MLK JR WAY        TACOMA             WA   
#[Out]# 66003                 2211 NE 139TH STREET     VANCOUVER             WA   
#[Out]# 66004        11567 CANTERWOOD BOULEVARD NW    GIG HARBOR             WA   
#[Out]# 66005                 MEDICAL CENTER DRIVE    MORGANTOWN             WV   
#[Out]# 66006               327 MEDICAL PARK DRIVE    BRIDGEPORT             WV   
#[Out]# 66007                      2900 1ST AVENUE    HUNTINGTON             WV   
#[Out]# 66008                    501 MORRIS STREET    CHARLESTON             WV   
#[Out]# 66009                          PO BOX 1484        ELKINS             WV   
#[Out]# 66010                     800 GARFIELD AVE   PARKERSBURG             WV   
#[Out]# 66011                   306 STANAFORD ROAD       BECKLEY             WV   
#[Out]# 66012                     1710 HARPER ROAD       BECKLEY             WV   
#[Out]# 66013                     725 AMERICAN AVE      WAUKESHA             WI   
#[Out]# 66014                      6308 EIGHTH AVE       KENOSHA             WI   
#[Out]# 66015                       2323 N LAKE DR     MILWAUKEE             WI   
#[Out]# 66016                    700 SOUTH PARK ST       MADISON             WI   
#[Out]# 66017                   5000 W CHAMBERS ST     MILWAUKEE             WI   
#[Out]# 66018                  2900 W OKLAHOMA AVE     MILWAUKEE             WI   
#[Out]# 66019                   8901 W LINCOLN AVE    WEST ALLIS             WI   
#[Out]# 66020                 9200 W WISCONSIN AVE     MILWAUKEE             WI   
#[Out]# 66021                     1233 EAST 2ND ST        CASPER             WY   
#[Out]# 66022                 214 EAST 23RD STREET      CHEYENNE             WY   
#[Out]# 66023                  2700 E BROAD STREET     MANSFIELD             TX   
#[Out]# 66024              21214 NORTHWEST FREEWAY       CYPRESS             TX   
#[Out]# 66025                    201 SETON PARKWAY    ROUND ROCK             TX   
#[Out]# 66026                 1139 E SONTERRA BLVD   SAN ANTONIO             TX   
#[Out]# 66027                       6001 KYLE PKWY          KYLE             TX   
#[Out]# 66028               231 SOUTH COLLINS ROAD     SUNNYVALE             TX   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 65060              36301                                AL - Dothan   
#[Out]# 65061              35631                            AL - Birmingham   
#[Out]# 65062              35235                            AL - Birmingham   
#[Out]# 65063              35007                            AL - Birmingham   
#[Out]# 65064              35660                            AL - Birmingham   
#[Out]# 65065              36116                            AL - Montgomery   
#[Out]# 65066              36106                            AL - Montgomery   
#[Out]# 65067              35233                            AL - Birmingham   
#[Out]# 65068              35801                            AL - Huntsville   
#[Out]# 65069              35903                            AL - Birmingham   
#[Out]# 65070              36305                                AL - Dothan   
#[Out]# 65071              36207                            AL - Birmingham   
#[Out]# 65072              36608                                AL - Mobile   
#[Out]# 65073              35401                            AL - Tuscaloosa   
#[Out]# 65074              35211                            AL - Birmingham   
#[Out]# 65075              36732                            AL - Birmingham   
#[Out]# 65076              36652                                AL - Mobile   
#[Out]# 65077              35209                            AL - Birmingham   
#[Out]# 65078              36608                                AL - Mobile   
#[Out]# 65079              85712                                AZ - Tucson   
#[Out]# 65080              85020                               AZ - Phoenix   
#[Out]# 65081              86001                               AZ - Phoenix   
#[Out]# 65082              85224                                  AZ - Mesa   
#[Out]# 65083              85635                                AZ - Tucson   
#[Out]# 65084              86409                               AZ - Phoenix   
#[Out]# 65085              85351                              AZ - Sun City   
#[Out]# 65086              85202                                  AZ - Mesa   
#[Out]# 65087              85206                                  AZ - Mesa   
#[Out]# 65088              85306                               AZ - Phoenix   
#[Out]# 65089              85027                               AZ - Phoenix   
#[Out]# ...                  ...                                        ...   
#[Out]# 65999              98405                                WA - Tacoma   
#[Out]# 66000              99216                               WA - Spokane   
#[Out]# 66001              98034                               WA - Seattle   
#[Out]# 66002              98415                                WA - Tacoma   
#[Out]# 66003              98686                              OR - Portland   
#[Out]# 66004              98332                                WA - Tacoma   
#[Out]# 66005              26506                            WV - Morgantown   
#[Out]# 66006              26330                            WV - Morgantown   
#[Out]# 66007              25701                            WV - Huntington   
#[Out]# 66008              25301                            WV - Charleston   
#[Out]# 66009              26241                            WV - Morgantown   
#[Out]# 66010              26101                            WV - Charleston   
#[Out]# 66011              25801                            WV - Charleston   
#[Out]# 66012              25801                            WV - Charleston   
#[Out]# 66013              53188                             WI - Milwaukee   
#[Out]# 66014              53143                             WI - Milwaukee   
#[Out]# 66015              53211                             WI - Milwaukee   
#[Out]# 66016              53715                               WI - Madison   
#[Out]# 66017              53210                             WI - Milwaukee   
#[Out]# 66018              53215                             WI - Milwaukee   
#[Out]# 66019              53227                             WI - Milwaukee   
#[Out]# 66020              53226                             WI - Milwaukee   
#[Out]# 66021              82601                                WY - Casper   
#[Out]# 66022              82001                          CO - Fort Collins   
#[Out]# 66023              76063                            TX - Fort Worth   
#[Out]# 66024              77429                               TX - Houston   
#[Out]# 66025              78664                                TX - Austin   
#[Out]# 66026              78258                           TX - San Antonio   
#[Out]# 66027              78640                                TX - Austin   
#[Out]# 66028              75182                                TX - Dallas   
#[Out]# 
#[Out]#        Total Discharges  Average Covered Charges  Average Total Payments  \
#[Out]# 65060                21             17187.666670             3532.476190   
#[Out]# 65061                13             16818.384620             3557.230769   
#[Out]# 65062                11             21731.545450             3530.272727   
#[Out]# 65063                19             17048.263160             3494.736842   
#[Out]# 65064                11             17953.818180             3861.909091   
#[Out]# 65065                11             10451.090910             3962.545455   
#[Out]# 65066                11             13748.363640             3421.636364   
#[Out]# 65067                38             15960.000000             4642.578947   
#[Out]# 65068                25             23610.600000             3634.280000   
#[Out]# 65069                19             41036.263160             3571.789474   
#[Out]# 65070                11             21801.454550             3285.272727   
#[Out]# 65071                17              9504.411765             3329.176471   
#[Out]# 65072                11             11451.636360             4659.181818   
#[Out]# 65073                82             15648.707320             3957.939024   
#[Out]# 65074                11             19641.181820             3761.636364   
#[Out]# 65075                17              5397.117647             3276.411765   
#[Out]# 65076                37             14267.216220             3397.459459   
#[Out]# 65077                32             50541.531250             3484.906250   
#[Out]# 65078                11             12068.181820             3002.545455   
#[Out]# 65079                11             16036.363640             4523.000000   
#[Out]# 65080                31             21633.612900             4088.870968   
#[Out]# 65081                21             20513.142860             6458.619048   
#[Out]# 65082                16             30256.437500             4104.562500   
#[Out]# 65083                15             15548.066670             5431.000000   
#[Out]# 65084                16             22512.375000             4672.062500   
#[Out]# 65085                33             24754.060610             3708.424242   
#[Out]# 65086                15             17849.800000             5014.133333   
#[Out]# 65087                27             18233.444440             3777.222222   
#[Out]# 65088                15             21456.466670             4409.866667   
#[Out]# 65089                12             21351.500000             4057.083333   
#[Out]# ...                 ...                      ...                     ...   
#[Out]# 65999                18             28179.222220             4818.055556   
#[Out]# 66000                18             19485.055560             3946.277778   
#[Out]# 66001                13             23903.615380             4174.076923   
#[Out]# 66002                12             25560.666670             9616.000000   
#[Out]# 66003                13             20224.538460             5035.769231   
#[Out]# 66004                11             19109.000000             4008.909091   
#[Out]# 66005                44             12360.181820             5590.363636   
#[Out]# 66006                11             10338.545450             3934.272727   
#[Out]# 66007                31             13822.032260             4333.387097   
#[Out]# 66008                22             18664.045450             4311.727273   
#[Out]# 66009                13              7266.692308             3556.461538   
#[Out]# 66010                14              9910.285714             3532.428571   
#[Out]# 66011                13              9601.846154             3624.615385   
#[Out]# 66012                54              8335.407407             3736.648148   
#[Out]# 66013                14             14119.285710             4432.714286   
#[Out]# 66014                19             15567.526320             3918.789474   
#[Out]# 66015                12             14715.083330             4714.583333   
#[Out]# 66016                12             17924.500000             4575.833333   
#[Out]# 66017                16             13459.625000             5628.562500   
#[Out]# 66018                37             16401.891890             4636.567568   
#[Out]# 66019                13             16192.076920             3822.230769   
#[Out]# 66020                13             13989.230770             5578.615385   
#[Out]# 66021                19             13095.578950             4156.052632   
#[Out]# 66022                21             12831.571430             5433.142857   
#[Out]# 66023                13             22199.076920             3484.538462   
#[Out]# 66024                14             47589.000000             3542.142857   
#[Out]# 66025                11             26933.727270             3820.454545   
#[Out]# 66026                16             23698.375000             3691.312500   
#[Out]# 66027                12             38615.666670             3646.500000   
#[Out]# 66028                12             25933.833330             3564.833333   
#[Out]# 
#[Out]#        Average Medicare Payments                        proc proc code  \
#[Out]# 65060                2579.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65061                2006.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65062                2079.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65063                2315.157895  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65064                2108.727273  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65065                2901.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65066                2668.181818  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65067                3462.605263  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65068                2501.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65069                2640.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65070                2216.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65071                2450.352941  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65072                2060.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65073                2838.012195  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65074                2687.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65075                2236.647059  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65076                2374.216216  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65077                2529.906250  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65078                1933.818182  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65079                3214.454545  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65080                3135.064516  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65081                5447.761905  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65082                2999.812500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65083                3610.333333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65084                3464.500000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65085                2687.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65086                3161.533333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65087                2772.777778  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65088                3207.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 65089                3174.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# ...                          ...                         ...       ...   
#[Out]# 65999                3297.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66000                2766.944444  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66001                3146.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66002                3331.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66003                3360.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66004                3160.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66005                3840.386364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66006                2572.545455  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66007                2535.612903  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66008                2869.636364  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66009                2543.000000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66010                2177.714286  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66011                2767.153846  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66012                2442.555556  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66013                2271.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66014                2932.052632  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66015                3591.416667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66016                3146.833333  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66017                3308.187500  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66018                2996.864865  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66019                3015.461538  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66020                4063.692308  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66021                2807.315789  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66022                4372.571429  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66023                2563.615385  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66024                2529.285714  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66025                2953.909091  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66026                1931.250000  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66027                2559.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 66028                2686.166667  305 - HYPERTENSION W/O MCC       305   
#[Out]# 
#[Out]#           markup  
#[Out]# 65060   6.663608  
#[Out]# 65061   8.381469  
#[Out]# 65062  10.451970  
#[Out]# 65063   7.363758  
#[Out]# 65064   8.514054  
#[Out]# 65065   3.602018  
#[Out]# 65066   5.152709  
#[Out]# 65067   4.609246  
#[Out]# 65068   9.440464  
#[Out]# 65069  15.544039  
#[Out]# 65070   9.835780  
#[Out]# 65071   3.878793  
#[Out]# 65072   5.557330  
#[Out]# 65073   5.513968  
#[Out]# 65074   7.307482  
#[Out]# 65075   2.413039  
#[Out]# 65076   6.009232  
#[Out]# 65077  19.977630  
#[Out]# 65078   6.240598  
#[Out]# 65079   4.988829  
#[Out]# 65080   6.900532  
#[Out]# 65081   3.765426  
#[Out]# 65082  10.086110  
#[Out]# 65083   4.306546  
#[Out]# 65084   6.498016  
#[Out]# 65085   9.209411  
#[Out]# 65086   5.645931  
#[Out]# 65087   6.575877  
#[Out]# 65088   6.690510  
#[Out]# 65089   6.727001  
#[Out]# ...          ...  
#[Out]# 65999   8.544768  
#[Out]# 66000   7.042084  
#[Out]# 66001   7.596612  
#[Out]# 66002   7.673572  
#[Out]# 66003   6.019208  
#[Out]# 66004   6.046108  
#[Out]# 66005   3.218474  
#[Out]# 66006   4.018800  
#[Out]# 66007   5.451160  
#[Out]# 66008   6.503976  
#[Out]# 66009   2.857527  
#[Out]# 66010   4.550774  
#[Out]# 66011   3.469936  
#[Out]# 66012   3.412576  
#[Out]# 66013   6.216429  
#[Out]# 66014   5.309429  
#[Out]# 66015   4.097292  
#[Out]# 66016   5.696044  
#[Out]# 66017   4.068580  
#[Out]# 66018   5.473017  
#[Out]# 66019   5.369684  
#[Out]# 66020   3.442493  
#[Out]# 66021   4.664804  
#[Out]# 66022   2.934560  
#[Out]# 66023   8.659285  
#[Out]# 66024  18.815193  
#[Out]# 66025   9.117995  
#[Out]# 66026  12.271003  
#[Out]# 66027  15.089157  
#[Out]# 66028   9.654588  
#[Out]# 
#[Out]# [969 rows x 15 columns]
# Fri, 11 Sep 2015 11:48:06
hyp.col
# Fri, 11 Sep 2015 11:48:07
hyp.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'proc', 'proc code', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 11:48:30
hypfl=hyp[hyp["Provider State"]=="FL"]
# Fri, 11 Sep 2015 11:48:37
hypfl["markup"]
#[Out]# 65173     5.036840
#[Out]# 65174     8.451448
#[Out]# 65175     8.100361
#[Out]# 65176     7.293335
#[Out]# 65177    12.330226
#[Out]# 65178     6.081064
#[Out]# 65179     7.378786
#[Out]# 65180     5.988956
#[Out]# 65181     8.883900
#[Out]# 65182     4.410948
#[Out]# 65183     8.591667
#[Out]# 65184     5.252081
#[Out]# 65185     6.086634
#[Out]# 65186     9.849312
#[Out]# 65187    10.977484
#[Out]# 65188     7.632329
#[Out]# 65189     6.380390
#[Out]# 65190     5.285149
#[Out]# 65191     6.584336
#[Out]# 65192     9.338148
#[Out]# 65193     8.471736
#[Out]# 65194    11.138714
#[Out]# 65195     8.263698
#[Out]# 65196     7.649865
#[Out]# 65197     6.763949
#[Out]# 65198     7.265508
#[Out]# 65199     7.537204
#[Out]# 65200     6.800563
#[Out]# 65201     9.038997
#[Out]# 65202     8.979939
#[Out]#            ...    
#[Out]# 65237     5.863829
#[Out]# 65238    13.950660
#[Out]# 65239    15.042902
#[Out]# 65240    12.526431
#[Out]# 65241    13.730924
#[Out]# 65242    12.671421
#[Out]# 65243    11.890344
#[Out]# 65244    14.203454
#[Out]# 65245     5.812540
#[Out]# 65246    12.062069
#[Out]# 65247    11.414437
#[Out]# 65248     7.381223
#[Out]# 65249    10.272046
#[Out]# 65250     8.812616
#[Out]# 65251     6.577573
#[Out]# 65252    14.557245
#[Out]# 65253    11.157051
#[Out]# 65254    16.827184
#[Out]# 65255    11.712540
#[Out]# 65256    14.710331
#[Out]# 65257     7.136926
#[Out]# 65258    15.769518
#[Out]# 65259     8.457409
#[Out]# 65260     5.409009
#[Out]# 65261     3.944347
#[Out]# 65262    12.044994
#[Out]# 65263     4.331689
#[Out]# 65264     6.356596
#[Out]# 65265    12.299882
#[Out]# 65266     6.328674
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 11:48:49
hypFL=hypfl
# Fri, 11 Sep 2015 11:49:03
hypNY=hyp[hyp["State Provider"]=="NY"]
# Fri, 11 Sep 2015 11:49:10
hypNY=hyp[hyp["Provider State"]=="NY"]
# Fri, 11 Sep 2015 11:49:21
hypNat=hyp["markup"]
# Fri, 11 Sep 2015 11:49:30
hypNY=hyp["markup"][hyp["Provider State"]=="NY"]
# Fri, 11 Sep 2015 11:49:48
hypfl=hyp["markup"][hyp["Provider State"]=="FL"]
# Fri, 11 Sep 2015 11:49:50
hypFL=hypfl
# Fri, 11 Sep 2015 11:49:56
plt.clf()
# Fri, 11 Sep 2015 11:50:09
plt.hist(np.asarray(hypNat),bins=1000)
#[Out]# (array([ 1.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  1.,  0.,  2.,  1.,
#[Out]#          5.,  3.,  1.,  3.,  2.,  1.,  6.,  1.,  0.,  0.,  2.,  2.,  0.,
#[Out]#          1.,  0.,  1.,  1.,  1.,  0.,  4.,  0.,  0.,  1.,  0.,  1.,  0.,
#[Out]#          1.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  1.,  3.,  1.,  0.,
#[Out]#          1.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  1.,  1.,  3.,  3.,
#[Out]#          0.,  0.,  2.,  1.,  1.,  0.,  0.,  1.,  1.,  1.,  2.,  2.,  1.,
#[Out]#          1.,  1.,  1.,  1.,  0.,  0.,  3.,  4.,  1.,  0.,  0.,  0.,  3.,
#[Out]#          0.,  2.,  2.,  2.,  1.,  1.,  1.,  1.,  1.,  2.,  1.,  0.,  0.,
#[Out]#          2.,  1.,  1.,  0.,  0.,  3.,  1.,  2.,  3.,  3.,  4.,  1.,  4.,
#[Out]#          1.,  1.,  2.,  3.,  3.,  2.,  1.,  1.,  0.,  3.,  2.,  1.,  3.,
#[Out]#          2.,  1.,  2.,  1.,  1.,  3.,  1.,  1.,  1.,  1.,  2.,  5.,  2.,
#[Out]#          2.,  1.,  1.,  1.,  5.,  0.,  0.,  4.,  3.,  3.,  0.,  3.,  0.,
#[Out]#          2.,  2.,  1.,  1.,  3.,  2.,  1.,  3.,  2.,  0.,  5.,  6.,  4.,
#[Out]#          1.,  4.,  3.,  0.,  5.,  4.,  2.,  0.,  2.,  4.,  1.,  2.,  6.,
#[Out]#          3.,  1.,  0.,  4.,  5.,  4.,  3.,  2.,  4.,  3.,  2.,  2.,  7.,
#[Out]#          2.,  0.,  3.,  4.,  3.,  1.,  3.,  3.,  2.,  5.,  4.,  6.,  5.,
#[Out]#          1.,  3.,  1.,  2.,  5.,  1.,  5.,  0.,  1.,  5.,  4.,  4.,  4.,
#[Out]#          4.,  5.,  3.,  6.,  4.,  2.,  3.,  3.,  1.,  1.,  4.,  2.,  3.,
#[Out]#          5.,  4.,  2.,  3.,  2.,  2.,  5.,  0.,  1.,  3.,  3.,  2.,  5.,
#[Out]#          1.,  1.,  1.,  1.,  1.,  3.,  1.,  3.,  4.,  0.,  2.,  0.,  3.,
#[Out]#          1.,  1.,  4.,  3.,  1.,  3.,  1.,  4.,  1.,  5.,  3.,  0.,  2.,
#[Out]#          1.,  2.,  2.,  3.,  1.,  3.,  5.,  3.,  5.,  3.,  3.,  2.,  3.,
#[Out]#          3.,  1.,  1.,  3.,  2.,  0.,  3.,  4.,  2.,  0.,  5.,  3.,  2.,
#[Out]#          3.,  2.,  3.,  0.,  2.,  0.,  3.,  1.,  1.,  2.,  0.,  5.,  1.,
#[Out]#          0.,  0.,  6.,  1.,  2.,  5.,  2.,  1.,  1.,  1.,  3.,  2.,  2.,
#[Out]#          1.,  5.,  2.,  0.,  2.,  3.,  4.,  0.,  2.,  2.,  3.,  1.,  3.,
#[Out]#          1.,  2.,  1.,  1.,  1.,  3.,  1.,  0.,  1.,  3.,  5.,  1.,  1.,
#[Out]#          3.,  2.,  4.,  3.,  2.,  1.,  2.,  2.,  3.,  3.,  0.,  1.,  3.,
#[Out]#          1.,  0.,  1.,  2.,  0.,  0.,  4.,  1.,  0.,  2.,  2.,  3.,  1.,
#[Out]#          2.,  1.,  0.,  2.,  2.,  1.,  2.,  1.,  1.,  3.,  1.,  3.,  0.,
#[Out]#          1.,  0.,  4.,  1.,  0.,  2.,  1.,  2.,  2.,  4.,  0.,  2.,  0.,
#[Out]#          2.,  0.,  1.,  4.,  2.,  1.,  0.,  0.,  2.,  1.,  0.,  1.,  0.,
#[Out]#          1.,  0.,  0.,  0.,  5.,  1.,  2.,  4.,  0.,  2.,  0.,  1.,  1.,
#[Out]#          3.,  0.,  2.,  1.,  1.,  1.,  1.,  0.,  1.,  1.,  1.,  0.,  1.,
#[Out]#          1.,  0.,  4.,  1.,  1.,  2.,  0.,  0.,  4.,  0.,  1.,  0.,  1.,
#[Out]#          1.,  0.,  0.,  1.,  4.,  0.,  0.,  0.,  1.,  0.,  2.,  1.,  1.,
#[Out]#          0.,  3.,  0.,  2.,  2.,  2.,  1.,  3.,  1.,  0.,  2.,  1.,  0.,
#[Out]#          1.,  1.,  0.,  1.,  1.,  0.,  2.,  1.,  0.,  1.,  0.,  1.,  1.,
#[Out]#          0.,  1.,  0.,  2.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  2.,  0.,
#[Out]#          0.,  1.,  0.,  0.,  2.,  0.,  2.,  1.,  0.,  2.,  0.,  0.,  0.,
#[Out]#          1.,  1.,  1.,  0.,  0.,  1.,  3.,  0.,  0.,  2.,  0.,  0.,  1.,
#[Out]#          2.,  1.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  1.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  2.,  2.,  0.,  0.,
#[Out]#          1.,  0.,  0.,  0.,  0.,  3.,  0.,  0.,  1.,  0.,  0.,  3.,  0.,
#[Out]#          0.,  1.,  1.,  0.,  0.,  1.,  0.,  1.,  0.,  1.,  1.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  2.,  0.,  2.,  1.,  1.,
#[Out]#          0.,  2.,  0.,  1.,  0.,  0.,  0.,  0.,  1.,  2.,  1.,  0.,  1.,
#[Out]#          0.,  1.,  0.,  2.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  1.,  0.,  0.,  0.,  0.,  0.,  1.,  3.,  1.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  2.,  0.,  0.,  2.,  1.,  0.,  0.,  0.,  1.,  0.,
#[Out]#          3.,  1.,  0.,  0.,  0.,  0.,  1.,  1.,  2.,  1.,  0.,  1.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  1.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  1.,  0.,  0.,  1.,  1.,  1.,  1.,  0.,  0.,  1.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,
#[Out]#          0.,  2.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  1.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.,
#[Out]#          1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
#[Out]#          0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.0343319 ,   1.05329416, ...,  19.93970558,
#[Out]#          19.95866784,  19.9776301 ]),
#[Out]#  <a list of 1000 Patch objects>)
# Fri, 11 Sep 2015 11:50:19
plt.hist(np.asarray(hypNat),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:50:20
plt.clf()
# Fri, 11 Sep 2015 11:50:22
plt.hist(np.asarray(hypNat),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:50:31
plt.hist(np.asarray(hypFL),bins=10)
#[Out]# (array([  4.,   7.,  14.,  19.,  14.,  11.,  11.,   7.,   5.,   2.]),
#[Out]#  array([  2.63843407,   4.05730908,   5.4761841 ,   6.89505911,
#[Out]#           8.31393413,   9.73280914,  11.15168416,  12.57055917,
#[Out]#          13.98943419,  15.4083092 ,  16.82718421]),
#[Out]#  <a list of 10 Patch objects>)
# Fri, 11 Sep 2015 11:50:37
plt.hist(np.asarray(hypFL),bins=30)
#[Out]# (array([ 2.,  0.,  2.,  3.,  0.,  4.,  2.,  7.,  5.,  6.,  9.,  4.,  6.,
#[Out]#          4.,  4.,  4.,  1.,  6.,  2.,  4.,  5.,  2.,  1.,  4.,  1.,  3.,
#[Out]#          1.,  1.,  0.,  1.]),
#[Out]#  array([  2.63843407,   3.11139241,   3.58435074,   4.05730908,
#[Out]#           4.53026742,   5.00322576,   5.4761841 ,   5.94914244,
#[Out]#           6.42210077,   6.89505911,   7.36801745,   7.84097579,
#[Out]#           8.31393413,   8.78689246,   9.2598508 ,   9.73280914,
#[Out]#          10.20576748,  10.67872582,  11.15168416,  11.62464249,
#[Out]#          12.09760083,  12.57055917,  13.04351751,  13.51647585,
#[Out]#          13.98943419,  14.46239252,  14.93535086,  15.4083092 ,
#[Out]#          15.88126754,  16.35422588,  16.82718421]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:50:42
plt.clf()
# Fri, 11 Sep 2015 11:50:44
plt.hist(np.asarray(hypNat),bins=100)
#[Out]# (array([  3.,  24.,   8.,   8.,   5.,   3.,  12.,  10.,  10.,  14.,  10.,
#[Out]#          22.,  19.,  14.,  19.,  19.,  27.,  25.,  30.,  30.,  33.,  28.,
#[Out]#          35.,  28.,  22.,  18.,  24.,  22.,  27.,  24.,  14.,  23.,  19.,
#[Out]#          21.,  17.,  23.,  11.,  16.,  16.,  17.,  12.,   5.,  19.,   9.,
#[Out]#          10.,  12.,   8.,  14.,   7.,   6.,   3.,   7.,   9.,   5.,   2.,
#[Out]#           5.,   4.,   7.,   2.,   9.,   5.,   5.,   1.,   6.,   2.,   5.,
#[Out]#           6.,   5.,   1.,   2.,   5.,   2.,   1.,   2.,   3.,   0.,   3.,
#[Out]#           2.,   1.,   0.,   1.,   0.,   0.,   1.,   0.,   1.,   0.,   2.,
#[Out]#           0.,   0.,   0.,   0.,   0.,   1.,   0.,   0.,   0.,   0.,   0.,
#[Out]#           1.]), array([  1.01536964,   1.20499224,   1.39461485,   1.58423745,
#[Out]#           1.77386006,   1.96348266,   2.15310526,   2.34272787,
#[Out]#           2.53235047,   2.72197308,   2.91159568,   3.10121829,
#[Out]#           3.29084089,   3.4804635 ,   3.6700861 ,   3.85970871,
#[Out]#           4.04933131,   4.23895392,   4.42857652,   4.61819912,
#[Out]#           4.80782173,   4.99744433,   5.18706694,   5.37668954,
#[Out]#           5.56631215,   5.75593475,   5.94555736,   6.13517996,
#[Out]#           6.32480257,   6.51442517,   6.70404778,   6.89367038,
#[Out]#           7.08329299,   7.27291559,   7.46253819,   7.6521608 ,
#[Out]#           7.8417834 ,   8.03140601,   8.22102861,   8.41065122,
#[Out]#           8.60027382,   8.78989643,   8.97951903,   9.16914164,
#[Out]#           9.35876424,   9.54838685,   9.73800945,   9.92763205,
#[Out]#          10.11725466,  10.30687726,  10.49649987,  10.68612247,
#[Out]#          10.87574508,  11.06536768,  11.25499029,  11.44461289,
#[Out]#          11.6342355 ,  11.8238581 ,  12.01348071,  12.20310331,
#[Out]#          12.39272591,  12.58234852,  12.77197112,  12.96159373,
#[Out]#          13.15121633,  13.34083894,  13.53046154,  13.72008415,
#[Out]#          13.90970675,  14.09932936,  14.28895196,  14.47857457,
#[Out]#          14.66819717,  14.85781977,  15.04744238,  15.23706498,
#[Out]#          15.42668759,  15.61631019,  15.8059328 ,  15.9955554 ,
#[Out]#          16.18517801,  16.37480061,  16.56442322,  16.75404582,
#[Out]#          16.94366843,  17.13329103,  17.32291364,  17.51253624,
#[Out]#          17.70215884,  17.89178145,  18.08140405,  18.27102666,
#[Out]#          18.46064926,  18.65027187,  18.83989447,  19.02951708,
#[Out]#          19.21913968,  19.40876229,  19.59838489,  19.7880075 ,  19.9776301 ]), <a list of 100 Patch objects>)
# Fri, 11 Sep 2015 11:50:50
plt.hist(np.asarray(hypFL),bins=30)
#[Out]# (array([ 2.,  0.,  2.,  3.,  0.,  4.,  2.,  7.,  5.,  6.,  9.,  4.,  6.,
#[Out]#          4.,  4.,  4.,  1.,  6.,  2.,  4.,  5.,  2.,  1.,  4.,  1.,  3.,
#[Out]#          1.,  1.,  0.,  1.]),
#[Out]#  array([  2.63843407,   3.11139241,   3.58435074,   4.05730908,
#[Out]#           4.53026742,   5.00322576,   5.4761841 ,   5.94914244,
#[Out]#           6.42210077,   6.89505911,   7.36801745,   7.84097579,
#[Out]#           8.31393413,   8.78689246,   9.2598508 ,   9.73280914,
#[Out]#          10.20576748,  10.67872582,  11.15168416,  11.62464249,
#[Out]#          12.09760083,  12.57055917,  13.04351751,  13.51647585,
#[Out]#          13.98943419,  14.46239252,  14.93535086,  15.4083092 ,
#[Out]#          15.88126754,  16.35422588,  16.82718421]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:50:54
plt.hist(np.asarray(hypNY),bins=30)
#[Out]# (array([ 3.,  3.,  3.,  6.,  2.,  5.,  2.,  7.,  4.,  4.,  4.,  4.,  0.,
#[Out]#          4.,  1.,  2.,  1.,  1.,  2.,  1.,  1.,  1.,  0.,  0.,  0.,  1.,
#[Out]#          0.,  0.,  0.,  1.]),
#[Out]#  array([  1.01536964,   1.39585133,   1.77633302,   2.15681471,
#[Out]#           2.53729641,   2.9177781 ,   3.29825979,   3.67874148,
#[Out]#           4.05922318,   4.43970487,   4.82018656,   5.20066825,
#[Out]#           5.58114995,   5.96163164,   6.34211333,   6.72259502,
#[Out]#           7.10307672,   7.48355841,   7.8640401 ,   8.24452179,
#[Out]#           8.62500349,   9.00548518,   9.38596687,   9.76644856,
#[Out]#          10.14693026,  10.52741195,  10.90789364,  11.28837533,
#[Out]#          11.66885702,  12.04933872,  12.42982041]),
#[Out]#  <a list of 30 Patch objects>)
# Fri, 11 Sep 2015 11:51:20
plt.title("Markups: National vs NY vs FL")
#[Out]# <matplotlib.text.Text at 0x7fefa07453c8>
# Fri, 11 Sep 2015 11:51:27
plt.xlabel("markups")
#[Out]# <matplotlib.text.Text at 0x7fefa04df320>
# Fri, 11 Sep 2015 11:51:40
plt.legend()
# Fri, 11 Sep 2015 11:59:10
plt.title("Markups: Hypertension National vs NY vs FL")
#[Out]# <matplotlib.text.Text at 0x7fefa07453c8>
# Fri, 11 Sep 2015 11:59:28
plt.title("Markups: Hypertension; National vs NY vs FL")
#[Out]# <matplotlib.text.Text at 0x7fefa07453c8>
# Fri, 11 Sep 2015 11:59:47
plt.savefig("hypertension nat NY FL")
# Fri, 11 Sep 2015 15:44:03
exit
