# IPython log file

import health
(all, train, test, validate, endog, office, outpatient, inpatient, er, w, exog, insurance)=health.prep_for_modeling()
all.columns
#[Out]# Index(['DWELLING UNIT ID', 'PERSON NUMBER', 'PERSON ID (DUID + PID)',
#[Out]#        'PANEL NUMBER', 'FAMILY ID (STUDENT MERGED IN) - R3/1',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - R4/2',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - R5/3',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - 12/31/13', 'ANNUAL FAMILY IDENTIFIER',
#[Out]#        'CPSFAMID', 
#[Out]#        ...
#[Out]#        'WEARS SEAT BELT (>15) - RD 5/3==5',
#[Out]#        'WEARS SEAT BELT (>15) - RD 5/3==4', 'SAQ: CURRENTLY SMOKE==2',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==1',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==2',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==3',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==6',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==4',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==5',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==7'],
#[Out]#       dtype='object', length=1837)
insurance.columns
#[Out]# Index(['Plan ID (standard component)',
#[Out]#        'Medical Deductible-individual-standard',
#[Out]#        'Drug Deductible-individual-standard',
#[Out]#        'Medical Maximum Out Of Pocket - individual - standard',
#[Out]#        'Drug Maximum Out of Pocket - individual - standard',
#[Out]#        'Primary Care Physician  - standard', 'Specialist  - standard',
#[Out]#        'Emergency Room  - standard', 'Inpatient Facility  - standard',
#[Out]#        'Inpatient Physician - standard', 'Generic Drugs - standard',
#[Out]#        'Preferred Brand Drugs - standard',
#[Out]#        'Non-preferred Brand Drugs - standard', 'Specialty Drugs - standard',
#[Out]#        'State', 'County', 'breakout of Primary Care Physician  - standard',
#[Out]#        'breakout of Specialist  - standard',
#[Out]#        'breakout of Inpatient Physician - standard',
#[Out]#        'breakout of Emergency Room  - standard', 'pcp copay before deductible',
#[Out]#        'pcp coinsurance before deductible', 'pcp copay after deductible',
#[Out]#        'pcp coinsurance after deductible',
#[Out]#        'specialist copay before deductible',
#[Out]#        'specialist coinsurance before deductible',
#[Out]#        'specialist copay after deductible',
#[Out]#        'specialist coinsurance after deductible',
#[Out]#        'inpatientdoc copay before deductible',
#[Out]#        'inpatientdoc coinsurance before deductible',
#[Out]#        'inpatientdoc copay after deductible',
#[Out]#        'inpatientdoc coinsurance after deductible',
#[Out]#        'emergency copay before deductible',
#[Out]#        'emergency coinsurance before deductible',
#[Out]#        'emergency copay after deductible',
#[Out]#        'emergency coinsurance after deductible'],
#[Out]#       dtype='object')
insurance.iloc[0]
#[Out]# Plan ID (standard component)                                                                38344AK0570001
#[Out]# Medical Deductible-individual-standard                                                                5250
#[Out]# Drug Deductible-individual-standard                                                                      0
#[Out]# Medical Maximum Out Of Pocket - individual - standard                                                 5250
#[Out]# Drug Maximum Out of Pocket - individual - standard                                                       0
#[Out]# Primary Care Physician  - standard                                              No Charge after Deductible
#[Out]# Specialist  - standard                                                          No Charge after Deductible
#[Out]# Emergency Room  - standard                                                      No Charge after Deductible
#[Out]# Inpatient Facility  - standard                                                  No Charge after Deductible
#[Out]# Inpatient Physician - standard                                                  No Charge after Deductible
#[Out]# Generic Drugs - standard                                                        No Charge after Deductible
#[Out]# Preferred Brand Drugs - standard                                                No Charge after Deductible
#[Out]# Non-preferred Brand Drugs - standard                                            No Charge after Deductible
#[Out]# Specialty Drugs - standard                                                      No Charge after Deductible
#[Out]# State                                                                                                   AK
#[Out]# County                                                                                      Aleutians East
#[Out]# breakout of Primary Care Physician  - standard           [[(copay, 0), (coinsurance, 1.0)], [(copay, 0)...
#[Out]# breakout of Specialist  - standard                       [[(copay, 0), (coinsurance, 1.0)], [(copay, 0)...
#[Out]# breakout of Inpatient Physician - standard               [[(copay, 0), (coinsurance, 1.0)], [(copay, 0)...
#[Out]# breakout of Emergency Room  - standard                   [[(copay, 0), (coinsurance, 1.0)], [(copay, 0)...
#[Out]# pcp copay before deductible                                                                              0
#[Out]# pcp coinsurance before deductible                                                                        1
#[Out]# pcp copay after deductible                                                                               0
#[Out]# pcp coinsurance after deductible                                                                         0
#[Out]# specialist copay before deductible                                                                       0
#[Out]# specialist coinsurance before deductible                                                                 1
#[Out]# specialist copay after deductible                                                                        0
#[Out]# specialist coinsurance after deductible                                                                  0
#[Out]# inpatientdoc copay before deductible                                                                     0
#[Out]# inpatientdoc coinsurance before deductible                                                               1
#[Out]# inpatientdoc copay after deductible                                                                      0
#[Out]# inpatientdoc coinsurance after deductible                                                                0
#[Out]# emergency copay before deductible                                                                        0
#[Out]# emergency coinsurance before deductible                                                                  1
#[Out]# emergency copay after deductible                                                                         0
#[Out]# emergency coinsurance after deductible                                                                   0
#[Out]# Name: 0, dtype: object
insurance.columns
#[Out]# Index(['Plan ID (standard component)',
#[Out]#        'Medical Deductible-individual-standard',
#[Out]#        'Drug Deductible-individual-standard',
#[Out]#        'Medical Maximum Out Of Pocket - individual - standard',
#[Out]#        'Drug Maximum Out of Pocket - individual - standard',
#[Out]#        'Primary Care Physician  - standard', 'Specialist  - standard',
#[Out]#        'Emergency Room  - standard', 'Inpatient Facility  - standard',
#[Out]#        'Inpatient Physician - standard', 'Generic Drugs - standard',
#[Out]#        'Preferred Brand Drugs - standard',
#[Out]#        'Non-preferred Brand Drugs - standard', 'Specialty Drugs - standard',
#[Out]#        'State', 'County', 'breakout of Primary Care Physician  - standard',
#[Out]#        'breakout of Specialist  - standard',
#[Out]#        'breakout of Inpatient Physician - standard',
#[Out]#        'breakout of Emergency Room  - standard', 'pcp copay before deductible',
#[Out]#        'pcp coinsurance before deductible', 'pcp copay after deductible',
#[Out]#        'pcp coinsurance after deductible',
#[Out]#        'specialist copay before deductible',
#[Out]#        'specialist coinsurance before deductible',
#[Out]#        'specialist copay after deductible',
#[Out]#        'specialist coinsurance after deductible',
#[Out]#        'inpatientdoc copay before deductible',
#[Out]#        'inpatientdoc coinsurance before deductible',
#[Out]#        'inpatientdoc copay after deductible',
#[Out]#        'inpatientdoc coinsurance after deductible',
#[Out]#        'emergency copay before deductible',
#[Out]#        'emergency coinsurance before deductible',
#[Out]#        'emergency copay after deductible',
#[Out]#        'emergency coinsurance after deductible'],
#[Out]#       dtype='object')
insurance["State"]=="NJ"
#[Out]# 0        False
#[Out]# 1        False
#[Out]# 2        False
#[Out]# 3        False
#[Out]# 4        False
#[Out]# 5        False
#[Out]# 6        False
#[Out]# 7        False
#[Out]# 8        False
#[Out]# 9        False
#[Out]# 10       False
#[Out]# 11       False
#[Out]# 12       False
#[Out]# 13       False
#[Out]# 14       False
#[Out]# 15       False
#[Out]# 16       False
#[Out]# 17       False
#[Out]# 18       False
#[Out]# 19       False
#[Out]# 20       False
#[Out]# 21       False
#[Out]# 22       False
#[Out]# 23       False
#[Out]# 24       False
#[Out]# 25       False
#[Out]# 26       False
#[Out]# 27       False
#[Out]# 28       False
#[Out]# 29       False
#[Out]#          ...  
#[Out]# 89886    False
#[Out]# 89887    False
#[Out]# 89888    False
#[Out]# 89889    False
#[Out]# 89890    False
#[Out]# 89891    False
#[Out]# 89892    False
#[Out]# 89893    False
#[Out]# 89894    False
#[Out]# 89895    False
#[Out]# 89896    False
#[Out]# 89897    False
#[Out]# 89898    False
#[Out]# 89899    False
#[Out]# 89900    False
#[Out]# 89901    False
#[Out]# 89902    False
#[Out]# 89903    False
#[Out]# 89904    False
#[Out]# 89905    False
#[Out]# 89906    False
#[Out]# 89907    False
#[Out]# 89908    False
#[Out]# 89909    False
#[Out]# 89910    False
#[Out]# 89911    False
#[Out]# 89912    False
#[Out]# 89913    False
#[Out]# 89914    False
#[Out]# 89915    False
#[Out]# Name: State, dtype: bool
insurance[insurance["State"]=="NJ"]
#[Out]#       Plan ID (standard component)  Medical Deductible-individual-standard  \
#[Out]# 48247               10191NJ0030001                                    2500   
#[Out]# 48248               10191NJ0030002                                    2000   
#[Out]# 48249               10191NJ0050001                                    2000   
#[Out]# 48250               10191NJ0050002                                    1500   
#[Out]# 48251               10191NJ0050003                                     750   
#[Out]# 48252               10191NJ0070001                                    2500   
#[Out]# 48253               10191NJ0070002                                    2000   
#[Out]# 48254               10191NJ0070003                                    1500   
#[Out]# 48255               10191NJ0150001                                    6600   
#[Out]# 48256               10191NJ0190001                                    2500   
#[Out]# 48257               10191NJ0190002                                    2000   
#[Out]# 48258               10191NJ0190003                                    1500   
#[Out]# 48259               10191NJ0190004                                       0   
#[Out]# 48260               10191NJ0290001                                    2500   
#[Out]# 48261               10191NJ0290002                                    2000   
#[Out]# 48262               10191NJ0290003                                    1800   
#[Out]# 48263               10191NJ0290004                                       0   
#[Out]# 48264               48834NJ0080001                                     200   
#[Out]# 48265               48834NJ0080002                                    1000   
#[Out]# 48266               48834NJ0080003                                     500   
#[Out]# 48267               48834NJ0080004                                    2500   
#[Out]# 48268               48834NJ0080005                                    1500   
#[Out]# 48269               48834NJ0080006                                    2500   
#[Out]# 48270               77606NJ0040001                                    2500   
#[Out]# 48271               77606NJ0040002                                    2000   
#[Out]# 48272               91661NJ2260002                                    1000   
#[Out]# 48273               91661NJ2260003                                    1500   
#[Out]# 48274               91661NJ2260006                                    2500   
#[Out]# 48275               91661NJ2260007                                    2000   
#[Out]# 48276               91661NJ2270001                                    2000   
#[Out]# ...                            ...                                     ...   
#[Out]# 49252               48834NJ0080001                                     200   
#[Out]# 49253               48834NJ0080002                                    1000   
#[Out]# 49254               48834NJ0080003                                     500   
#[Out]# 49255               48834NJ0080004                                    2500   
#[Out]# 49256               48834NJ0080005                                    1500   
#[Out]# 49257               48834NJ0080006                                    2500   
#[Out]# 49258               77606NJ0040001                                    2500   
#[Out]# 49259               77606NJ0040002                                    2000   
#[Out]# 49260               91661NJ2260002                                    1000   
#[Out]# 49261               91661NJ2260003                                    1500   
#[Out]# 49262               91661NJ2260006                                    2500   
#[Out]# 49263               91661NJ2260007                                    2000   
#[Out]# 49264               91661NJ2270001                                    2000   
#[Out]# 49265               91661NJ2270002                                    2500   
#[Out]# 49266               91661NJ2270003                                    1000   
#[Out]# 49267               91661NJ2270004                                    2000   
#[Out]# 49268               91661NJ2280001                                    6600   
#[Out]# 49269               91762NJ0070001                                    2500   
#[Out]# 49270               91762NJ0070002                                    2500   
#[Out]# 49271               91762NJ0070003                                    2500   
#[Out]# 49272               91762NJ0070004                                    2500   
#[Out]# 49273               91762NJ0070006                                    1800   
#[Out]# 49274               91762NJ0070007                                    1350   
#[Out]# 49275               91762NJ0070010                                    1000   
#[Out]# 49276               91762NJ0070012                                    1300   
#[Out]# 49277               91762NJ0070014                                    6600   
#[Out]# 49278               91762NJ0070015                                    6600   
#[Out]# 49279               91762NJ0070080                                    1000   
#[Out]# 49280               91762NJ0110001                                       0   
#[Out]# 49281               91762NJ0110002                                    2500   
#[Out]# 
#[Out]#        Drug Deductible-individual-standard  \
#[Out]# 48247                                    0   
#[Out]# 48248                                    0   
#[Out]# 48249                                    0   
#[Out]# 48250                                    0   
#[Out]# 48251                                    0   
#[Out]# 48252                                    0   
#[Out]# 48253                                    0   
#[Out]# 48254                                    0   
#[Out]# 48255                                    0   
#[Out]# 48256                                    0   
#[Out]# 48257                                    0   
#[Out]# 48258                                    0   
#[Out]# 48259                                    0   
#[Out]# 48260                                    0   
#[Out]# 48261                                    0   
#[Out]# 48262                                    0   
#[Out]# 48263                                    0   
#[Out]# 48264                                    0   
#[Out]# 48265                                    0   
#[Out]# 48266                                    0   
#[Out]# 48267                                    0   
#[Out]# 48268                                    0   
#[Out]# 48269                                    0   
#[Out]# 48270                                    0   
#[Out]# 48271                                    0   
#[Out]# 48272                                    0   
#[Out]# 48273                                    0   
#[Out]# 48274                                    0   
#[Out]# 48275                                    0   
#[Out]# 48276                                    0   
#[Out]# ...                                    ...   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#        Medical Maximum Out Of Pocket - individual - standard  \
#[Out]# 48247                                               6600       
#[Out]# 48248                                               4500       
#[Out]# 48249                                               4500       
#[Out]# 48250                                               3500       
#[Out]# 48251                                               1500       
#[Out]# 48252                                               6450       
#[Out]# 48253                                               4000       
#[Out]# 48254                                               2500       
#[Out]# 48255                                               6600       
#[Out]# 48256                                               6600       
#[Out]# 48257                                               6000       
#[Out]# 48258                                               3000       
#[Out]# 48259                                               1250       
#[Out]# 48260                                               6550       
#[Out]# 48261                                               5000       
#[Out]# 48262                                               3000       
#[Out]# 48263                                               2000       
#[Out]# 48264                                               2000       
#[Out]# 48265                                               3000       
#[Out]# 48266                                               6600       
#[Out]# 48267                                               6350       
#[Out]# 48268                                               5500       
#[Out]# 48269                                               6350       
#[Out]# 48270                                               6350       
#[Out]# 48271                                               4650       
#[Out]# 48272                                               2500       
#[Out]# 48273                                               5000       
#[Out]# 48274                                               6600       
#[Out]# 48275                                               5000       
#[Out]# 48276                                               6350       
#[Out]# ...                                                  ...       
#[Out]# 49252                                               2000       
#[Out]# 49253                                               3000       
#[Out]# 49254                                               6600       
#[Out]# 49255                                               6350       
#[Out]# 49256                                               5500       
#[Out]# 49257                                               6350       
#[Out]# 49258                                               6350       
#[Out]# 49259                                               4650       
#[Out]# 49260                                               2500       
#[Out]# 49261                                               5000       
#[Out]# 49262                                               6600       
#[Out]# 49263                                               5000       
#[Out]# 49264                                               6350       
#[Out]# 49265                                               6350       
#[Out]# 49266                                               4000       
#[Out]# 49267                                               5000       
#[Out]# 49268                                               6600       
#[Out]# 49269                                               6450       
#[Out]# 49270                                               6450       
#[Out]# 49271                                               6450       
#[Out]# 49272                                               6450       
#[Out]# 49273                                               4500       
#[Out]# 49274                                               5750       
#[Out]# 49275                                               5000       
#[Out]# 49276                                               2500       
#[Out]# 49277                                               6600       
#[Out]# 49278                                               6600       
#[Out]# 49279                                               5000       
#[Out]# 49280                                               4000       
#[Out]# 49281                                               6350       
#[Out]# 
#[Out]#        Drug Maximum Out of Pocket - individual - standard  \
#[Out]# 48247                                                  0    
#[Out]# 48248                                                  0    
#[Out]# 48249                                                  0    
#[Out]# 48250                                                  0    
#[Out]# 48251                                                  0    
#[Out]# 48252                                                  0    
#[Out]# 48253                                                  0    
#[Out]# 48254                                                  0    
#[Out]# 48255                                                  0    
#[Out]# 48256                                                  0    
#[Out]# 48257                                                  0    
#[Out]# 48258                                                  0    
#[Out]# 48259                                                  0    
#[Out]# 48260                                                  0    
#[Out]# 48261                                                  0    
#[Out]# 48262                                                  0    
#[Out]# 48263                                                  0    
#[Out]# 48264                                                  0    
#[Out]# 48265                                                  0    
#[Out]# 48266                                                  0    
#[Out]# 48267                                                  0    
#[Out]# 48268                                                  0    
#[Out]# 48269                                                  0    
#[Out]# 48270                                                  0    
#[Out]# 48271                                                  0    
#[Out]# 48272                                                  0    
#[Out]# 48273                                                  0    
#[Out]# 48274                                                  0    
#[Out]# 48275                                                  0    
#[Out]# 48276                                                  0    
#[Out]# ...                                                  ...    
#[Out]# 49252                                                  0    
#[Out]# 49253                                                  0    
#[Out]# 49254                                                  0    
#[Out]# 49255                                                  0    
#[Out]# 49256                                                  0    
#[Out]# 49257                                                  0    
#[Out]# 49258                                                  0    
#[Out]# 49259                                                  0    
#[Out]# 49260                                                  0    
#[Out]# 49261                                                  0    
#[Out]# 49262                                                  0    
#[Out]# 49263                                                  0    
#[Out]# 49264                                                  0    
#[Out]# 49265                                                  0    
#[Out]# 49266                                                  0    
#[Out]# 49267                                                  0    
#[Out]# 49268                                                  0    
#[Out]# 49269                                                  0    
#[Out]# 49270                                                  0    
#[Out]# 49271                                                  0    
#[Out]# 49272                                                  0    
#[Out]# 49273                                                  0    
#[Out]# 49274                                                  0    
#[Out]# 49275                                                  0    
#[Out]# 49276                                                  0    
#[Out]# 49277                                                  0    
#[Out]# 49278                                                  0    
#[Out]# 49279                                                  0    
#[Out]# 49280                                                  0    
#[Out]# 49281                                                  0    
#[Out]# 
#[Out]#       Primary Care Physician  - standard            Specialist  - standard  \
#[Out]# 48247   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48248   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48249                                $25                               $50   
#[Out]# 48250                                $10                               $25   
#[Out]# 48251                                 $5                               $10   
#[Out]# 48252   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48253   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48254                                30%                               30%   
#[Out]# 48255         No Charge after Deductible        No Charge after Deductible   
#[Out]# 48256         $10 Copay after deductible        $75 Copay after deductible   
#[Out]# 48257                                $10                               $50   
#[Out]# 48258                                $10                               $25   
#[Out]# 48259                                $10                               $10   
#[Out]# 48260         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 48261                                $25                               $75   
#[Out]# 48262                                $15                               $50   
#[Out]# 48263                                $10                               $25   
#[Out]# 48264                                $15                               $30   
#[Out]# 48265                                $20                               $40   
#[Out]# 48266                                $20                               $40   
#[Out]# 48267                                $30                               $60   
#[Out]# 48268         $25 Copay after deductible        $50 Copay after deductible   
#[Out]# 48269   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48270                                $50                               $75   
#[Out]# 48271                                $15                               $30   
#[Out]# 48272                                $15                               $30   
#[Out]# 48273                                $30  30% Coinsurance after deductible   
#[Out]# 48274         $40 Copay after deductible  40% Coinsurance after deductible   
#[Out]# 48275                                $40  30% Coinsurance after deductible   
#[Out]# 48276                                $25                               $50   
#[Out]# ...                                  ...                               ...   
#[Out]# 49252                                $15                               $30   
#[Out]# 49253                                $20                               $40   
#[Out]# 49254                                $20                               $40   
#[Out]# 49255                                $30                               $60   
#[Out]# 49256         $25 Copay after deductible        $50 Copay after deductible   
#[Out]# 49257   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258                                $50                               $75   
#[Out]# 49259                                $15                               $30   
#[Out]# 49260                                $15                               $30   
#[Out]# 49261                                $30  30% Coinsurance after deductible   
#[Out]# 49262         $40 Copay after deductible  40% Coinsurance after deductible   
#[Out]# 49263                                $40  30% Coinsurance after deductible   
#[Out]# 49264                                $25                               $50   
#[Out]# 49265         $30 Copay after deductible  50% Coinsurance after deductible   
#[Out]# 49266                                $15                               $30   
#[Out]# 49267                                $20  30% Coinsurance after deductible   
#[Out]# 49268         No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49273         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49274         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49275                                $30                               $50   
#[Out]# 49276   20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277                                $30        No Charge after Deductible   
#[Out]# 49278                                $30        No Charge after Deductible   
#[Out]# 49279                                $30                               $50   
#[Out]# 49280                                $15                               $25   
#[Out]# 49281                                $40                               $50   
#[Out]# 
#[Out]#                               Emergency Room  - standard  \
#[Out]# 48247  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 48248  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 48249  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 48250  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 48251                                               $100   
#[Out]# 48252  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 48253  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 48254  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 48255                         No Charge after Deductible   
#[Out]# 48256  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 48257  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 48258  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 48259                     $100 Copay and 20% Coinsurance   
#[Out]# 48260                        $100 Copay after deductible   
#[Out]# 48261                                               $100   
#[Out]# 48262                                               $100   
#[Out]# 48263                                               $100   
#[Out]# 48264                                               $100   
#[Out]# 48265  $100 Copay before deductible and 10% Coinsuran...   
#[Out]# 48266  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 48267  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 48268                   20% Coinsurance after deductible   
#[Out]# 48269                   50% Coinsurance after deductible   
#[Out]# 48270                        $100 Copay after deductible   
#[Out]# 48271                                               $100   
#[Out]# 48272  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 48273  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 48274  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 48275  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 48276  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# ...                                                  ...   
#[Out]# 49252                                               $100   
#[Out]# 49253  $100 Copay before deductible and 10% Coinsuran...   
#[Out]# 49254  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49255  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49256                   20% Coinsurance after deductible   
#[Out]# 49257                   50% Coinsurance after deductible   
#[Out]# 49258                        $100 Copay after deductible   
#[Out]# 49259                                               $100   
#[Out]# 49260  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49261  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49262  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49263  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49264  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 49265  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49266  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49267  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49268                         No Charge after Deductible   
#[Out]# 49269                   50% Coinsurance after deductible   
#[Out]# 49270                   50% Coinsurance after deductible   
#[Out]# 49271                   50% Coinsurance after deductible   
#[Out]# 49272                   50% Coinsurance after deductible   
#[Out]# 49273                        $100 Copay after deductible   
#[Out]# 49274                        $100 Copay after deductible   
#[Out]# 49275                                               $100   
#[Out]# 49276                   20% Coinsurance after deductible   
#[Out]# 49277                         No Charge after Deductible   
#[Out]# 49278                         No Charge after Deductible   
#[Out]# 49279                                               $100   
#[Out]# 49280                                               $100   
#[Out]# 49281                        $100 Copay after deductible   
#[Out]# 
#[Out]#          Inpatient Facility  - standard    Inpatient Physician - standard  \
#[Out]# 48247  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48248  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48249  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48250  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 48251  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 48252  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48253  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48254  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 48255        No Charge after Deductible        No Charge after Deductible   
#[Out]# 48256  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48257                $500 Copay per Day  40% Coinsurance after deductible   
#[Out]# 48258                $250 Copay per Day  30% Coinsurance after deductible   
#[Out]# 48259                $100 Copay per Day                               20%   
#[Out]# 48260  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48261  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48262  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 48263                               20%                               20%   
#[Out]# 48264  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 48265                $100 Copay per Day  10% Coinsurance after deductible   
#[Out]# 48266  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 48267  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48268  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 48269  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48270  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 48271  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48272  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 48273  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 48274  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 48275  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 48276  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# ...                                 ...                               ...   
#[Out]# 49252  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49253                $100 Copay per Day  10% Coinsurance after deductible   
#[Out]# 49254  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49255  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49256  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49257  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49259  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49260  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49261  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49262  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49263  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49264  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49265  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49266  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49267  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49268        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49273                $500 Copay per Day        No Charge after Deductible   
#[Out]# 49274  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49275  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49276  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49278        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49279  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49280                $300 Copay per Day                         No Charge   
#[Out]# 49281  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 
#[Out]#                        ...                    \
#[Out]# 48247                  ...                     
#[Out]# 48248                  ...                     
#[Out]# 48249                  ...                     
#[Out]# 48250                  ...                     
#[Out]# 48251                  ...                     
#[Out]# 48252                  ...                     
#[Out]# 48253                  ...                     
#[Out]# 48254                  ...                     
#[Out]# 48255                  ...                     
#[Out]# 48256                  ...                     
#[Out]# 48257                  ...                     
#[Out]# 48258                  ...                     
#[Out]# 48259                  ...                     
#[Out]# 48260                  ...                     
#[Out]# 48261                  ...                     
#[Out]# 48262                  ...                     
#[Out]# 48263                  ...                     
#[Out]# 48264                  ...                     
#[Out]# 48265                  ...                     
#[Out]# 48266                  ...                     
#[Out]# 48267                  ...                     
#[Out]# 48268                  ...                     
#[Out]# 48269                  ...                     
#[Out]# 48270                  ...                     
#[Out]# 48271                  ...                     
#[Out]# 48272                  ...                     
#[Out]# 48273                  ...                     
#[Out]# 48274                  ...                     
#[Out]# 48275                  ...                     
#[Out]# 48276                  ...                     
#[Out]# ...                    ...                     
#[Out]# 49252                  ...                     
#[Out]# 49253                  ...                     
#[Out]# 49254                  ...                     
#[Out]# 49255                  ...                     
#[Out]# 49256                  ...                     
#[Out]# 49257                  ...                     
#[Out]# 49258                  ...                     
#[Out]# 49259                  ...                     
#[Out]# 49260                  ...                     
#[Out]# 49261                  ...                     
#[Out]# 49262                  ...                     
#[Out]# 49263                  ...                     
#[Out]# 49264                  ...                     
#[Out]# 49265                  ...                     
#[Out]# 49266                  ...                     
#[Out]# 49267                  ...                     
#[Out]# 49268                  ...                     
#[Out]# 49269                  ...                     
#[Out]# 49270                  ...                     
#[Out]# 49271                  ...                     
#[Out]# 49272                  ...                     
#[Out]# 49273                  ...                     
#[Out]# 49274                  ...                     
#[Out]# 49275                  ...                     
#[Out]# 49276                  ...                     
#[Out]# 49277                  ...                     
#[Out]# 49278                  ...                     
#[Out]# 49279                  ...                     
#[Out]# 49280                  ...                     
#[Out]# 49281                  ...                     
#[Out]# 
#[Out]#       specialist copay after deductible  \
#[Out]# 48247                                 0   
#[Out]# 48248                                 0   
#[Out]# 48249                                50   
#[Out]# 48250                                25   
#[Out]# 48251                                10   
#[Out]# 48252                                 0   
#[Out]# 48253                                 0   
#[Out]# 48254                                 0   
#[Out]# 48255                                 0   
#[Out]# 48256                                75   
#[Out]# 48257                                50   
#[Out]# 48258                                25   
#[Out]# 48259                                10   
#[Out]# 48260                                75   
#[Out]# 48261                                75   
#[Out]# 48262                                50   
#[Out]# 48263                                25   
#[Out]# 48264                                30   
#[Out]# 48265                                40   
#[Out]# 48266                                40   
#[Out]# 48267                                60   
#[Out]# 48268                                50   
#[Out]# 48269                                 0   
#[Out]# 48270                                75   
#[Out]# 48271                                30   
#[Out]# 48272                                30   
#[Out]# 48273                                 0   
#[Out]# 48274                                 0   
#[Out]# 48275                                 0   
#[Out]# 48276                                50   
#[Out]# ...                                 ...   
#[Out]# 49252                                30   
#[Out]# 49253                                40   
#[Out]# 49254                                40   
#[Out]# 49255                                60   
#[Out]# 49256                                50   
#[Out]# 49257                                 0   
#[Out]# 49258                                75   
#[Out]# 49259                                30   
#[Out]# 49260                                30   
#[Out]# 49261                                 0   
#[Out]# 49262                                 0   
#[Out]# 49263                                 0   
#[Out]# 49264                                50   
#[Out]# 49265                                 0   
#[Out]# 49266                                30   
#[Out]# 49267                                 0   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                75   
#[Out]# 49273                                75   
#[Out]# 49274                                75   
#[Out]# 49275                                50   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                                50   
#[Out]# 49280                                25   
#[Out]# 49281                                50   
#[Out]# 
#[Out]#       specialist coinsurance after deductible  \
#[Out]# 48247                                     0.5   
#[Out]# 48248                                     0.4   
#[Out]# 48249                                     0.0   
#[Out]# 48250                                     0.0   
#[Out]# 48251                                     0.0   
#[Out]# 48252                                     0.5   
#[Out]# 48253                                     0.4   
#[Out]# 48254                                     0.3   
#[Out]# 48255                                     0.0   
#[Out]# 48256                                     0.0   
#[Out]# 48257                                     0.0   
#[Out]# 48258                                     0.0   
#[Out]# 48259                                     0.0   
#[Out]# 48260                                     0.0   
#[Out]# 48261                                     0.0   
#[Out]# 48262                                     0.0   
#[Out]# 48263                                     0.0   
#[Out]# 48264                                     0.0   
#[Out]# 48265                                     0.0   
#[Out]# 48266                                     0.0   
#[Out]# 48267                                     0.0   
#[Out]# 48268                                     0.0   
#[Out]# 48269                                     0.5   
#[Out]# 48270                                     0.0   
#[Out]# 48271                                     0.0   
#[Out]# 48272                                     0.0   
#[Out]# 48273                                     0.3   
#[Out]# 48274                                     0.4   
#[Out]# 48275                                     0.3   
#[Out]# 48276                                     0.0   
#[Out]# ...                                       ...   
#[Out]# 49252                                     0.0   
#[Out]# 49253                                     0.0   
#[Out]# 49254                                     0.0   
#[Out]# 49255                                     0.0   
#[Out]# 49256                                     0.0   
#[Out]# 49257                                     0.5   
#[Out]# 49258                                     0.0   
#[Out]# 49259                                     0.0   
#[Out]# 49260                                     0.0   
#[Out]# 49261                                     0.3   
#[Out]# 49262                                     0.4   
#[Out]# 49263                                     0.3   
#[Out]# 49264                                     0.0   
#[Out]# 49265                                     0.5   
#[Out]# 49266                                     0.0   
#[Out]# 49267                                     0.3   
#[Out]# 49268                                     0.0   
#[Out]# 49269                                     0.5   
#[Out]# 49270                                     0.5   
#[Out]# 49271                                     0.5   
#[Out]# 49272                                     0.0   
#[Out]# 49273                                     0.0   
#[Out]# 49274                                     0.0   
#[Out]# 49275                                     0.0   
#[Out]# 49276                                     0.2   
#[Out]# 49277                                     0.0   
#[Out]# 49278                                     0.0   
#[Out]# 49279                                     0.0   
#[Out]# 49280                                     0.0   
#[Out]# 49281                                     0.0   
#[Out]# 
#[Out]#       inpatientdoc copay before deductible  \
#[Out]# 48247                                    0   
#[Out]# 48248                                    0   
#[Out]# 48249                                    0   
#[Out]# 48250                                    0   
#[Out]# 48251                                    0   
#[Out]# 48252                                    0   
#[Out]# 48253                                    0   
#[Out]# 48254                                    0   
#[Out]# 48255                                    0   
#[Out]# 48256                                    0   
#[Out]# 48257                                    0   
#[Out]# 48258                                    0   
#[Out]# 48259                                    0   
#[Out]# 48260                                    0   
#[Out]# 48261                                    0   
#[Out]# 48262                                    0   
#[Out]# 48263                                    0   
#[Out]# 48264                                    0   
#[Out]# 48265                                    0   
#[Out]# 48266                                    0   
#[Out]# 48267                                    0   
#[Out]# 48268                                    0   
#[Out]# 48269                                    0   
#[Out]# 48270                                    0   
#[Out]# 48271                                    0   
#[Out]# 48272                                    0   
#[Out]# 48273                                    0   
#[Out]# 48274                                    0   
#[Out]# 48275                                    0   
#[Out]# 48276                                    0   
#[Out]# ...                                    ...   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance before deductible  \
#[Out]# 48247                                        1.0   
#[Out]# 48248                                        1.0   
#[Out]# 48249                                        1.0   
#[Out]# 48250                                        1.0   
#[Out]# 48251                                        1.0   
#[Out]# 48252                                        1.0   
#[Out]# 48253                                        1.0   
#[Out]# 48254                                        1.0   
#[Out]# 48255                                        1.0   
#[Out]# 48256                                        1.0   
#[Out]# 48257                                        1.0   
#[Out]# 48258                                        1.0   
#[Out]# 48259                                        0.2   
#[Out]# 48260                                        1.0   
#[Out]# 48261                                        1.0   
#[Out]# 48262                                        1.0   
#[Out]# 48263                                        0.2   
#[Out]# 48264                                        1.0   
#[Out]# 48265                                        1.0   
#[Out]# 48266                                        1.0   
#[Out]# 48267                                        1.0   
#[Out]# 48268                                        1.0   
#[Out]# 48269                                        1.0   
#[Out]# 48270                                        1.0   
#[Out]# 48271                                        1.0   
#[Out]# 48272                                        1.0   
#[Out]# 48273                                        1.0   
#[Out]# 48274                                        1.0   
#[Out]# 48275                                        1.0   
#[Out]# 48276                                        1.0   
#[Out]# ...                                          ...   
#[Out]# 49252                                        1.0   
#[Out]# 49253                                        1.0   
#[Out]# 49254                                        1.0   
#[Out]# 49255                                        1.0   
#[Out]# 49256                                        1.0   
#[Out]# 49257                                        1.0   
#[Out]# 49258                                        1.0   
#[Out]# 49259                                        1.0   
#[Out]# 49260                                        1.0   
#[Out]# 49261                                        1.0   
#[Out]# 49262                                        1.0   
#[Out]# 49263                                        1.0   
#[Out]# 49264                                        1.0   
#[Out]# 49265                                        1.0   
#[Out]# 49266                                        1.0   
#[Out]# 49267                                        1.0   
#[Out]# 49268                                        1.0   
#[Out]# 49269                                        1.0   
#[Out]# 49270                                        1.0   
#[Out]# 49271                                        1.0   
#[Out]# 49272                                        1.0   
#[Out]# 49273                                        1.0   
#[Out]# 49274                                        1.0   
#[Out]# 49275                                        1.0   
#[Out]# 49276                                        1.0   
#[Out]# 49277                                        1.0   
#[Out]# 49278                                        1.0   
#[Out]# 49279                                        1.0   
#[Out]# 49280                                        0.0   
#[Out]# 49281                                        1.0   
#[Out]# 
#[Out]#       inpatientdoc copay after deductible  \
#[Out]# 48247                                   0   
#[Out]# 48248                                   0   
#[Out]# 48249                                   0   
#[Out]# 48250                                   0   
#[Out]# 48251                                   0   
#[Out]# 48252                                   0   
#[Out]# 48253                                   0   
#[Out]# 48254                                   0   
#[Out]# 48255                                   0   
#[Out]# 48256                                   0   
#[Out]# 48257                                   0   
#[Out]# 48258                                   0   
#[Out]# 48259                                   0   
#[Out]# 48260                                   0   
#[Out]# 48261                                   0   
#[Out]# 48262                                   0   
#[Out]# 48263                                   0   
#[Out]# 48264                                   0   
#[Out]# 48265                                   0   
#[Out]# 48266                                   0   
#[Out]# 48267                                   0   
#[Out]# 48268                                   0   
#[Out]# 48269                                   0   
#[Out]# 48270                                   0   
#[Out]# 48271                                   0   
#[Out]# 48272                                   0   
#[Out]# 48273                                   0   
#[Out]# 48274                                   0   
#[Out]# 48275                                   0   
#[Out]# 48276                                   0   
#[Out]# ...                                   ...   
#[Out]# 49252                                   0   
#[Out]# 49253                                   0   
#[Out]# 49254                                   0   
#[Out]# 49255                                   0   
#[Out]# 49256                                   0   
#[Out]# 49257                                   0   
#[Out]# 49258                                   0   
#[Out]# 49259                                   0   
#[Out]# 49260                                   0   
#[Out]# 49261                                   0   
#[Out]# 49262                                   0   
#[Out]# 49263                                   0   
#[Out]# 49264                                   0   
#[Out]# 49265                                   0   
#[Out]# 49266                                   0   
#[Out]# 49267                                   0   
#[Out]# 49268                                   0   
#[Out]# 49269                                   0   
#[Out]# 49270                                   0   
#[Out]# 49271                                   0   
#[Out]# 49272                                   0   
#[Out]# 49273                                   0   
#[Out]# 49274                                   0   
#[Out]# 49275                                   0   
#[Out]# 49276                                   0   
#[Out]# 49277                                   0   
#[Out]# 49278                                   0   
#[Out]# 49279                                   0   
#[Out]# 49280                                   0   
#[Out]# 49281                                   0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance after deductible  \
#[Out]# 48247                                       0.5   
#[Out]# 48248                                       0.4   
#[Out]# 48249                                       0.4   
#[Out]# 48250                                       0.3   
#[Out]# 48251                                       0.2   
#[Out]# 48252                                       0.5   
#[Out]# 48253                                       0.4   
#[Out]# 48254                                       0.3   
#[Out]# 48255                                       0.0   
#[Out]# 48256                                       0.5   
#[Out]# 48257                                       0.4   
#[Out]# 48258                                       0.3   
#[Out]# 48259                                       0.2   
#[Out]# 48260                                       0.5   
#[Out]# 48261                                       0.4   
#[Out]# 48262                                       0.3   
#[Out]# 48263                                       0.2   
#[Out]# 48264                                       0.1   
#[Out]# 48265                                       0.1   
#[Out]# 48266                                       0.2   
#[Out]# 48267                                       0.5   
#[Out]# 48268                                       0.2   
#[Out]# 48269                                       0.5   
#[Out]# 48270                                       0.5   
#[Out]# 48271                                       0.4   
#[Out]# 48272                                       0.2   
#[Out]# 48273                                       0.3   
#[Out]# 48274                                       0.4   
#[Out]# 48275                                       0.3   
#[Out]# 48276                                       0.4   
#[Out]# ...                                         ...   
#[Out]# 49252                                       0.1   
#[Out]# 49253                                       0.1   
#[Out]# 49254                                       0.2   
#[Out]# 49255                                       0.5   
#[Out]# 49256                                       0.2   
#[Out]# 49257                                       0.5   
#[Out]# 49258                                       0.5   
#[Out]# 49259                                       0.4   
#[Out]# 49260                                       0.2   
#[Out]# 49261                                       0.3   
#[Out]# 49262                                       0.4   
#[Out]# 49263                                       0.3   
#[Out]# 49264                                       0.4   
#[Out]# 49265                                       0.5   
#[Out]# 49266                                       0.2   
#[Out]# 49267                                       0.3   
#[Out]# 49268                                       0.0   
#[Out]# 49269                                       0.5   
#[Out]# 49270                                       0.5   
#[Out]# 49271                                       0.5   
#[Out]# 49272                                       0.2   
#[Out]# 49273                                       0.0   
#[Out]# 49274                                       0.1   
#[Out]# 49275                                       0.2   
#[Out]# 49276                                       0.2   
#[Out]# 49277                                       0.0   
#[Out]# 49278                                       0.0   
#[Out]# 49279                                       0.2   
#[Out]# 49280                                       0.0   
#[Out]# 49281                                       0.3   
#[Out]# 
#[Out]#       emergency copay before deductible  \
#[Out]# 48247                                 0   
#[Out]# 48248                                 0   
#[Out]# 48249                               100   
#[Out]# 48250                               100   
#[Out]# 48251                               100   
#[Out]# 48252                                 0   
#[Out]# 48253                                 0   
#[Out]# 48254                                 0   
#[Out]# 48255                                 0   
#[Out]# 48256                               100   
#[Out]# 48257                                 0   
#[Out]# 48258                                 0   
#[Out]# 48259                               100   
#[Out]# 48260                                 0   
#[Out]# 48261                               100   
#[Out]# 48262                               100   
#[Out]# 48263                               100   
#[Out]# 48264                               100   
#[Out]# 48265                               100   
#[Out]# 48266                               100   
#[Out]# 48267                               100   
#[Out]# 48268                                 0   
#[Out]# 48269                                 0   
#[Out]# 48270                                 0   
#[Out]# 48271                               100   
#[Out]# 48272                               100   
#[Out]# 48273                               100   
#[Out]# 48274                                 0   
#[Out]# 48275                               100   
#[Out]# 48276                               100   
#[Out]# ...                                 ...   
#[Out]# 49252                               100   
#[Out]# 49253                               100   
#[Out]# 49254                               100   
#[Out]# 49255                               100   
#[Out]# 49256                                 0   
#[Out]# 49257                                 0   
#[Out]# 49258                                 0   
#[Out]# 49259                               100   
#[Out]# 49260                               100   
#[Out]# 49261                               100   
#[Out]# 49262                                 0   
#[Out]# 49263                               100   
#[Out]# 49264                               100   
#[Out]# 49265                               100   
#[Out]# 49266                               100   
#[Out]# 49267                               100   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                 0   
#[Out]# 49273                                 0   
#[Out]# 49274                                 0   
#[Out]# 49275                               100   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                               100   
#[Out]# 49280                               100   
#[Out]# 49281                                 0   
#[Out]# 
#[Out]#       emergency coinsurance before deductible  \
#[Out]# 48247                                     1.0   
#[Out]# 48248                                     1.0   
#[Out]# 48249                                     1.0   
#[Out]# 48250                                     1.0   
#[Out]# 48251                                     1.0   
#[Out]# 48252                                     1.0   
#[Out]# 48253                                     1.0   
#[Out]# 48254                                     1.0   
#[Out]# 48255                                     1.0   
#[Out]# 48256                                     1.0   
#[Out]# 48257                                     1.0   
#[Out]# 48258                                     1.0   
#[Out]# 48259                                     0.2   
#[Out]# 48260                                     1.0   
#[Out]# 48261                                     1.0   
#[Out]# 48262                                     1.0   
#[Out]# 48263                                     1.0   
#[Out]# 48264                                     1.0   
#[Out]# 48265                                     1.0   
#[Out]# 48266                                     1.0   
#[Out]# 48267                                     1.0   
#[Out]# 48268                                     1.0   
#[Out]# 48269                                     1.0   
#[Out]# 48270                                     1.0   
#[Out]# 48271                                     1.0   
#[Out]# 48272                                     1.0   
#[Out]# 48273                                     1.0   
#[Out]# 48274                                     1.0   
#[Out]# 48275                                     1.0   
#[Out]# 48276                                     1.0   
#[Out]# ...                                       ...   
#[Out]# 49252                                     1.0   
#[Out]# 49253                                     1.0   
#[Out]# 49254                                     1.0   
#[Out]# 49255                                     1.0   
#[Out]# 49256                                     1.0   
#[Out]# 49257                                     1.0   
#[Out]# 49258                                     1.0   
#[Out]# 49259                                     1.0   
#[Out]# 49260                                     1.0   
#[Out]# 49261                                     1.0   
#[Out]# 49262                                     1.0   
#[Out]# 49263                                     1.0   
#[Out]# 49264                                     1.0   
#[Out]# 49265                                     1.0   
#[Out]# 49266                                     1.0   
#[Out]# 49267                                     1.0   
#[Out]# 49268                                     1.0   
#[Out]# 49269                                     1.0   
#[Out]# 49270                                     1.0   
#[Out]# 49271                                     1.0   
#[Out]# 49272                                     1.0   
#[Out]# 49273                                     1.0   
#[Out]# 49274                                     1.0   
#[Out]# 49275                                     1.0   
#[Out]# 49276                                     1.0   
#[Out]# 49277                                     1.0   
#[Out]# 49278                                     1.0   
#[Out]# 49279                                     1.0   
#[Out]# 49280                                     1.0   
#[Out]# 49281                                     1.0   
#[Out]# 
#[Out]#       emergency copay after deductible emergency coinsurance after deductible  
#[Out]# 48247                              100                                    0.5  
#[Out]# 48248                              100                                    0.4  
#[Out]# 48249                                0                                    0.4  
#[Out]# 48250                                0                                    0.3  
#[Out]# 48251                              100                                    0.0  
#[Out]# 48252                              100                                    0.5  
#[Out]# 48253                              100                                    0.4  
#[Out]# 48254                              100                                    0.3  
#[Out]# 48255                                0                                    0.0  
#[Out]# 48256                                0                                    0.5  
#[Out]# 48257                              100                                    0.4  
#[Out]# 48258                              100                                    0.3  
#[Out]# 48259                              100                                    0.2  
#[Out]# 48260                              100                                    0.0  
#[Out]# 48261                              100                                    0.0  
#[Out]# 48262                              100                                    0.0  
#[Out]# 48263                              100                                    0.0  
#[Out]# 48264                              100                                    0.0  
#[Out]# 48265                                0                                    0.1  
#[Out]# 48266                                0                                    0.2  
#[Out]# 48267                                0                                    0.5  
#[Out]# 48268                                0                                    0.2  
#[Out]# 48269                                0                                    0.5  
#[Out]# 48270                              100                                    0.0  
#[Out]# 48271                              100                                    0.0  
#[Out]# 48272                                0                                    0.2  
#[Out]# 48273                                0                                    0.3  
#[Out]# 48274                              100                                    0.4  
#[Out]# 48275                                0                                    0.3  
#[Out]# 48276                                0                                    0.4  
#[Out]# ...                                ...                                    ...  
#[Out]# 49252                              100                                    0.0  
#[Out]# 49253                                0                                    0.1  
#[Out]# 49254                                0                                    0.2  
#[Out]# 49255                                0                                    0.5  
#[Out]# 49256                                0                                    0.2  
#[Out]# 49257                                0                                    0.5  
#[Out]# 49258                              100                                    0.0  
#[Out]# 49259                              100                                    0.0  
#[Out]# 49260                                0                                    0.2  
#[Out]# 49261                                0                                    0.3  
#[Out]# 49262                              100                                    0.4  
#[Out]# 49263                                0                                    0.3  
#[Out]# 49264                                0                                    0.4  
#[Out]# 49265                                0                                    0.5  
#[Out]# 49266                                0                                    0.2  
#[Out]# 49267                                0                                    0.3  
#[Out]# 49268                                0                                    0.0  
#[Out]# 49269                                0                                    0.5  
#[Out]# 49270                                0                                    0.5  
#[Out]# 49271                                0                                    0.5  
#[Out]# 49272                                0                                    0.5  
#[Out]# 49273                              100                                    0.0  
#[Out]# 49274                              100                                    0.0  
#[Out]# 49275                              100                                    0.0  
#[Out]# 49276                                0                                    0.2  
#[Out]# 49277                                0                                    0.0  
#[Out]# 49278                                0                                    0.0  
#[Out]# 49279                              100                                    0.0  
#[Out]# 49280                              100                                    0.0  
#[Out]# 49281                              100                                    0.0  
#[Out]# 
#[Out]# [1035 rows x 36 columns]
insurance[insurance["State"]=="NJ"].col
insurance[insurance["State"]=="NJ"].columns
#[Out]# Index(['Plan ID (standard component)',
#[Out]#        'Medical Deductible-individual-standard',
#[Out]#        'Drug Deductible-individual-standard',
#[Out]#        'Medical Maximum Out Of Pocket - individual - standard',
#[Out]#        'Drug Maximum Out of Pocket - individual - standard',
#[Out]#        'Primary Care Physician  - standard', 'Specialist  - standard',
#[Out]#        'Emergency Room  - standard', 'Inpatient Facility  - standard',
#[Out]#        'Inpatient Physician - standard', 'Generic Drugs - standard',
#[Out]#        'Preferred Brand Drugs - standard',
#[Out]#        'Non-preferred Brand Drugs - standard', 'Specialty Drugs - standard',
#[Out]#        'State', 'County', 'breakout of Primary Care Physician  - standard',
#[Out]#        'breakout of Specialist  - standard',
#[Out]#        'breakout of Inpatient Physician - standard',
#[Out]#        'breakout of Emergency Room  - standard', 'pcp copay before deductible',
#[Out]#        'pcp coinsurance before deductible', 'pcp copay after deductible',
#[Out]#        'pcp coinsurance after deductible',
#[Out]#        'specialist copay before deductible',
#[Out]#        'specialist coinsurance before deductible',
#[Out]#        'specialist copay after deductible',
#[Out]#        'specialist coinsurance after deductible',
#[Out]#        'inpatientdoc copay before deductible',
#[Out]#        'inpatientdoc coinsurance before deductible',
#[Out]#        'inpatientdoc copay after deductible',
#[Out]#        'inpatientdoc coinsurance after deductible',
#[Out]#        'emergency copay before deductible',
#[Out]#        'emergency coinsurance before deductible',
#[Out]#        'emergency copay after deductible',
#[Out]#        'emergency coinsurance after deductible'],
#[Out]#       dtype='object')
insurance[insurance["State"]=="NJ"]["County"]
#[Out]# 48247    Atlantic
#[Out]# 48248    Atlantic
#[Out]# 48249    Atlantic
#[Out]# 48250    Atlantic
#[Out]# 48251    Atlantic
#[Out]# 48252    Atlantic
#[Out]# 48253    Atlantic
#[Out]# 48254    Atlantic
#[Out]# 48255    Atlantic
#[Out]# 48256    Atlantic
#[Out]# 48257    Atlantic
#[Out]# 48258    Atlantic
#[Out]# 48259    Atlantic
#[Out]# 48260    Atlantic
#[Out]# 48261    Atlantic
#[Out]# 48262    Atlantic
#[Out]# 48263    Atlantic
#[Out]# 48264    Atlantic
#[Out]# 48265    Atlantic
#[Out]# 48266    Atlantic
#[Out]# 48267    Atlantic
#[Out]# 48268    Atlantic
#[Out]# 48269    Atlantic
#[Out]# 48270    Atlantic
#[Out]# 48271    Atlantic
#[Out]# 48272    Atlantic
#[Out]# 48273    Atlantic
#[Out]# 48274    Atlantic
#[Out]# 48275    Atlantic
#[Out]# 48276    Atlantic
#[Out]#            ...   
#[Out]# 49252      Warren
#[Out]# 49253      Warren
#[Out]# 49254      Warren
#[Out]# 49255      Warren
#[Out]# 49256      Warren
#[Out]# 49257      Warren
#[Out]# 49258      Warren
#[Out]# 49259      Warren
#[Out]# 49260      Warren
#[Out]# 49261      Warren
#[Out]# 49262      Warren
#[Out]# 49263      Warren
#[Out]# 49264      Warren
#[Out]# 49265      Warren
#[Out]# 49266      Warren
#[Out]# 49267      Warren
#[Out]# 49268      Warren
#[Out]# 49269      Warren
#[Out]# 49270      Warren
#[Out]# 49271      Warren
#[Out]# 49272      Warren
#[Out]# 49273      Warren
#[Out]# 49274      Warren
#[Out]# 49275      Warren
#[Out]# 49276      Warren
#[Out]# 49277      Warren
#[Out]# 49278      Warren
#[Out]# 49279      Warren
#[Out]# 49280      Warren
#[Out]# 49281      Warren
#[Out]# Name: County, dtype: object
insurance[(insurance["State"]=="NJ") & (insurance["County"]=="Warren")]
#[Out]#       Plan ID (standard component)  Medical Deductible-individual-standard  \
#[Out]# 49235               10191NJ0030001                                    2500   
#[Out]# 49236               10191NJ0030002                                    2000   
#[Out]# 49237               10191NJ0050001                                    2000   
#[Out]# 49238               10191NJ0050002                                    1500   
#[Out]# 49239               10191NJ0050003                                     750   
#[Out]# 49240               10191NJ0070001                                    2500   
#[Out]# 49241               10191NJ0070002                                    2000   
#[Out]# 49242               10191NJ0070003                                    1500   
#[Out]# 49243               10191NJ0150001                                    6600   
#[Out]# 49244               10191NJ0190001                                    2500   
#[Out]# 49245               10191NJ0190002                                    2000   
#[Out]# 49246               10191NJ0190003                                    1500   
#[Out]# 49247               10191NJ0190004                                       0   
#[Out]# 49248               10191NJ0290001                                    2500   
#[Out]# 49249               10191NJ0290002                                    2000   
#[Out]# 49250               10191NJ0290003                                    1800   
#[Out]# 49251               10191NJ0290004                                       0   
#[Out]# 49252               48834NJ0080001                                     200   
#[Out]# 49253               48834NJ0080002                                    1000   
#[Out]# 49254               48834NJ0080003                                     500   
#[Out]# 49255               48834NJ0080004                                    2500   
#[Out]# 49256               48834NJ0080005                                    1500   
#[Out]# 49257               48834NJ0080006                                    2500   
#[Out]# 49258               77606NJ0040001                                    2500   
#[Out]# 49259               77606NJ0040002                                    2000   
#[Out]# 49260               91661NJ2260002                                    1000   
#[Out]# 49261               91661NJ2260003                                    1500   
#[Out]# 49262               91661NJ2260006                                    2500   
#[Out]# 49263               91661NJ2260007                                    2000   
#[Out]# 49264               91661NJ2270001                                    2000   
#[Out]# 49265               91661NJ2270002                                    2500   
#[Out]# 49266               91661NJ2270003                                    1000   
#[Out]# 49267               91661NJ2270004                                    2000   
#[Out]# 49268               91661NJ2280001                                    6600   
#[Out]# 49269               91762NJ0070001                                    2500   
#[Out]# 49270               91762NJ0070002                                    2500   
#[Out]# 49271               91762NJ0070003                                    2500   
#[Out]# 49272               91762NJ0070004                                    2500   
#[Out]# 49273               91762NJ0070006                                    1800   
#[Out]# 49274               91762NJ0070007                                    1350   
#[Out]# 49275               91762NJ0070010                                    1000   
#[Out]# 49276               91762NJ0070012                                    1300   
#[Out]# 49277               91762NJ0070014                                    6600   
#[Out]# 49278               91762NJ0070015                                    6600   
#[Out]# 49279               91762NJ0070080                                    1000   
#[Out]# 49280               91762NJ0110001                                       0   
#[Out]# 49281               91762NJ0110002                                    2500   
#[Out]# 
#[Out]#        Drug Deductible-individual-standard  \
#[Out]# 49235                                    0   
#[Out]# 49236                                    0   
#[Out]# 49237                                    0   
#[Out]# 49238                                    0   
#[Out]# 49239                                    0   
#[Out]# 49240                                    0   
#[Out]# 49241                                    0   
#[Out]# 49242                                    0   
#[Out]# 49243                                    0   
#[Out]# 49244                                    0   
#[Out]# 49245                                    0   
#[Out]# 49246                                    0   
#[Out]# 49247                                    0   
#[Out]# 49248                                    0   
#[Out]# 49249                                    0   
#[Out]# 49250                                    0   
#[Out]# 49251                                    0   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#        Medical Maximum Out Of Pocket - individual - standard  \
#[Out]# 49235                                               6600       
#[Out]# 49236                                               4500       
#[Out]# 49237                                               4500       
#[Out]# 49238                                               3500       
#[Out]# 49239                                               1500       
#[Out]# 49240                                               6450       
#[Out]# 49241                                               4000       
#[Out]# 49242                                               2500       
#[Out]# 49243                                               6600       
#[Out]# 49244                                               6600       
#[Out]# 49245                                               6000       
#[Out]# 49246                                               3000       
#[Out]# 49247                                               1250       
#[Out]# 49248                                               6550       
#[Out]# 49249                                               5000       
#[Out]# 49250                                               3000       
#[Out]# 49251                                               2000       
#[Out]# 49252                                               2000       
#[Out]# 49253                                               3000       
#[Out]# 49254                                               6600       
#[Out]# 49255                                               6350       
#[Out]# 49256                                               5500       
#[Out]# 49257                                               6350       
#[Out]# 49258                                               6350       
#[Out]# 49259                                               4650       
#[Out]# 49260                                               2500       
#[Out]# 49261                                               5000       
#[Out]# 49262                                               6600       
#[Out]# 49263                                               5000       
#[Out]# 49264                                               6350       
#[Out]# 49265                                               6350       
#[Out]# 49266                                               4000       
#[Out]# 49267                                               5000       
#[Out]# 49268                                               6600       
#[Out]# 49269                                               6450       
#[Out]# 49270                                               6450       
#[Out]# 49271                                               6450       
#[Out]# 49272                                               6450       
#[Out]# 49273                                               4500       
#[Out]# 49274                                               5750       
#[Out]# 49275                                               5000       
#[Out]# 49276                                               2500       
#[Out]# 49277                                               6600       
#[Out]# 49278                                               6600       
#[Out]# 49279                                               5000       
#[Out]# 49280                                               4000       
#[Out]# 49281                                               6350       
#[Out]# 
#[Out]#        Drug Maximum Out of Pocket - individual - standard  \
#[Out]# 49235                                                  0    
#[Out]# 49236                                                  0    
#[Out]# 49237                                                  0    
#[Out]# 49238                                                  0    
#[Out]# 49239                                                  0    
#[Out]# 49240                                                  0    
#[Out]# 49241                                                  0    
#[Out]# 49242                                                  0    
#[Out]# 49243                                                  0    
#[Out]# 49244                                                  0    
#[Out]# 49245                                                  0    
#[Out]# 49246                                                  0    
#[Out]# 49247                                                  0    
#[Out]# 49248                                                  0    
#[Out]# 49249                                                  0    
#[Out]# 49250                                                  0    
#[Out]# 49251                                                  0    
#[Out]# 49252                                                  0    
#[Out]# 49253                                                  0    
#[Out]# 49254                                                  0    
#[Out]# 49255                                                  0    
#[Out]# 49256                                                  0    
#[Out]# 49257                                                  0    
#[Out]# 49258                                                  0    
#[Out]# 49259                                                  0    
#[Out]# 49260                                                  0    
#[Out]# 49261                                                  0    
#[Out]# 49262                                                  0    
#[Out]# 49263                                                  0    
#[Out]# 49264                                                  0    
#[Out]# 49265                                                  0    
#[Out]# 49266                                                  0    
#[Out]# 49267                                                  0    
#[Out]# 49268                                                  0    
#[Out]# 49269                                                  0    
#[Out]# 49270                                                  0    
#[Out]# 49271                                                  0    
#[Out]# 49272                                                  0    
#[Out]# 49273                                                  0    
#[Out]# 49274                                                  0    
#[Out]# 49275                                                  0    
#[Out]# 49276                                                  0    
#[Out]# 49277                                                  0    
#[Out]# 49278                                                  0    
#[Out]# 49279                                                  0    
#[Out]# 49280                                                  0    
#[Out]# 49281                                                  0    
#[Out]# 
#[Out]#       Primary Care Physician  - standard            Specialist  - standard  \
#[Out]# 49235   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49236   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49237                                $25                               $50   
#[Out]# 49238                                $10                               $25   
#[Out]# 49239                                 $5                               $10   
#[Out]# 49240   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49241   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49242                                30%                               30%   
#[Out]# 49243         No Charge after Deductible        No Charge after Deductible   
#[Out]# 49244         $10 Copay after deductible        $75 Copay after deductible   
#[Out]# 49245                                $10                               $50   
#[Out]# 49246                                $10                               $25   
#[Out]# 49247                                $10                               $10   
#[Out]# 49248         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49249                                $25                               $75   
#[Out]# 49250                                $15                               $50   
#[Out]# 49251                                $10                               $25   
#[Out]# 49252                                $15                               $30   
#[Out]# 49253                                $20                               $40   
#[Out]# 49254                                $20                               $40   
#[Out]# 49255                                $30                               $60   
#[Out]# 49256         $25 Copay after deductible        $50 Copay after deductible   
#[Out]# 49257   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258                                $50                               $75   
#[Out]# 49259                                $15                               $30   
#[Out]# 49260                                $15                               $30   
#[Out]# 49261                                $30  30% Coinsurance after deductible   
#[Out]# 49262         $40 Copay after deductible  40% Coinsurance after deductible   
#[Out]# 49263                                $40  30% Coinsurance after deductible   
#[Out]# 49264                                $25                               $50   
#[Out]# 49265         $30 Copay after deductible  50% Coinsurance after deductible   
#[Out]# 49266                                $15                               $30   
#[Out]# 49267                                $20  30% Coinsurance after deductible   
#[Out]# 49268         No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49273         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49274         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49275                                $30                               $50   
#[Out]# 49276   20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277                                $30        No Charge after Deductible   
#[Out]# 49278                                $30        No Charge after Deductible   
#[Out]# 49279                                $30                               $50   
#[Out]# 49280                                $15                               $25   
#[Out]# 49281                                $40                               $50   
#[Out]# 
#[Out]#                               Emergency Room  - standard  \
#[Out]# 49235  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 49236  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49237  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 49238  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49239                                               $100   
#[Out]# 49240  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 49241  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49242  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 49243                         No Charge after Deductible   
#[Out]# 49244  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49245  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49246  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 49247                     $100 Copay and 20% Coinsurance   
#[Out]# 49248                        $100 Copay after deductible   
#[Out]# 49249                                               $100   
#[Out]# 49250                                               $100   
#[Out]# 49251                                               $100   
#[Out]# 49252                                               $100   
#[Out]# 49253  $100 Copay before deductible and 10% Coinsuran...   
#[Out]# 49254  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49255  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49256                   20% Coinsurance after deductible   
#[Out]# 49257                   50% Coinsurance after deductible   
#[Out]# 49258                        $100 Copay after deductible   
#[Out]# 49259                                               $100   
#[Out]# 49260  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49261  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49262  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49263  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49264  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 49265  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49266  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49267  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49268                         No Charge after Deductible   
#[Out]# 49269                   50% Coinsurance after deductible   
#[Out]# 49270                   50% Coinsurance after deductible   
#[Out]# 49271                   50% Coinsurance after deductible   
#[Out]# 49272                   50% Coinsurance after deductible   
#[Out]# 49273                        $100 Copay after deductible   
#[Out]# 49274                        $100 Copay after deductible   
#[Out]# 49275                                               $100   
#[Out]# 49276                   20% Coinsurance after deductible   
#[Out]# 49277                         No Charge after Deductible   
#[Out]# 49278                         No Charge after Deductible   
#[Out]# 49279                                               $100   
#[Out]# 49280                                               $100   
#[Out]# 49281                        $100 Copay after deductible   
#[Out]# 
#[Out]#          Inpatient Facility  - standard    Inpatient Physician - standard  \
#[Out]# 49235  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49236  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49237  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49238  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49239  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49240  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49241  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49242  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49243        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49244  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49245                $500 Copay per Day  40% Coinsurance after deductible   
#[Out]# 49246                $250 Copay per Day  30% Coinsurance after deductible   
#[Out]# 49247                $100 Copay per Day                               20%   
#[Out]# 49248  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49249  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49250  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49251                               20%                               20%   
#[Out]# 49252  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49253                $100 Copay per Day  10% Coinsurance after deductible   
#[Out]# 49254  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49255  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49256  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49257  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49259  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49260  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49261  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49262  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49263  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49264  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49265  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49266  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49267  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49268        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49273                $500 Copay per Day        No Charge after Deductible   
#[Out]# 49274  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49275  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49276  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49278        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49279  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49280                $300 Copay per Day                         No Charge   
#[Out]# 49281  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 
#[Out]#                        ...                    \
#[Out]# 49235                  ...                     
#[Out]# 49236                  ...                     
#[Out]# 49237                  ...                     
#[Out]# 49238                  ...                     
#[Out]# 49239                  ...                     
#[Out]# 49240                  ...                     
#[Out]# 49241                  ...                     
#[Out]# 49242                  ...                     
#[Out]# 49243                  ...                     
#[Out]# 49244                  ...                     
#[Out]# 49245                  ...                     
#[Out]# 49246                  ...                     
#[Out]# 49247                  ...                     
#[Out]# 49248                  ...                     
#[Out]# 49249                  ...                     
#[Out]# 49250                  ...                     
#[Out]# 49251                  ...                     
#[Out]# 49252                  ...                     
#[Out]# 49253                  ...                     
#[Out]# 49254                  ...                     
#[Out]# 49255                  ...                     
#[Out]# 49256                  ...                     
#[Out]# 49257                  ...                     
#[Out]# 49258                  ...                     
#[Out]# 49259                  ...                     
#[Out]# 49260                  ...                     
#[Out]# 49261                  ...                     
#[Out]# 49262                  ...                     
#[Out]# 49263                  ...                     
#[Out]# 49264                  ...                     
#[Out]# 49265                  ...                     
#[Out]# 49266                  ...                     
#[Out]# 49267                  ...                     
#[Out]# 49268                  ...                     
#[Out]# 49269                  ...                     
#[Out]# 49270                  ...                     
#[Out]# 49271                  ...                     
#[Out]# 49272                  ...                     
#[Out]# 49273                  ...                     
#[Out]# 49274                  ...                     
#[Out]# 49275                  ...                     
#[Out]# 49276                  ...                     
#[Out]# 49277                  ...                     
#[Out]# 49278                  ...                     
#[Out]# 49279                  ...                     
#[Out]# 49280                  ...                     
#[Out]# 49281                  ...                     
#[Out]# 
#[Out]#       specialist copay after deductible  \
#[Out]# 49235                                 0   
#[Out]# 49236                                 0   
#[Out]# 49237                                50   
#[Out]# 49238                                25   
#[Out]# 49239                                10   
#[Out]# 49240                                 0   
#[Out]# 49241                                 0   
#[Out]# 49242                                 0   
#[Out]# 49243                                 0   
#[Out]# 49244                                75   
#[Out]# 49245                                50   
#[Out]# 49246                                25   
#[Out]# 49247                                10   
#[Out]# 49248                                75   
#[Out]# 49249                                75   
#[Out]# 49250                                50   
#[Out]# 49251                                25   
#[Out]# 49252                                30   
#[Out]# 49253                                40   
#[Out]# 49254                                40   
#[Out]# 49255                                60   
#[Out]# 49256                                50   
#[Out]# 49257                                 0   
#[Out]# 49258                                75   
#[Out]# 49259                                30   
#[Out]# 49260                                30   
#[Out]# 49261                                 0   
#[Out]# 49262                                 0   
#[Out]# 49263                                 0   
#[Out]# 49264                                50   
#[Out]# 49265                                 0   
#[Out]# 49266                                30   
#[Out]# 49267                                 0   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                75   
#[Out]# 49273                                75   
#[Out]# 49274                                75   
#[Out]# 49275                                50   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                                50   
#[Out]# 49280                                25   
#[Out]# 49281                                50   
#[Out]# 
#[Out]#       specialist coinsurance after deductible  \
#[Out]# 49235                                     0.5   
#[Out]# 49236                                     0.4   
#[Out]# 49237                                     0.0   
#[Out]# 49238                                     0.0   
#[Out]# 49239                                     0.0   
#[Out]# 49240                                     0.5   
#[Out]# 49241                                     0.4   
#[Out]# 49242                                     0.3   
#[Out]# 49243                                     0.0   
#[Out]# 49244                                     0.0   
#[Out]# 49245                                     0.0   
#[Out]# 49246                                     0.0   
#[Out]# 49247                                     0.0   
#[Out]# 49248                                     0.0   
#[Out]# 49249                                     0.0   
#[Out]# 49250                                     0.0   
#[Out]# 49251                                     0.0   
#[Out]# 49252                                     0.0   
#[Out]# 49253                                     0.0   
#[Out]# 49254                                     0.0   
#[Out]# 49255                                     0.0   
#[Out]# 49256                                     0.0   
#[Out]# 49257                                     0.5   
#[Out]# 49258                                     0.0   
#[Out]# 49259                                     0.0   
#[Out]# 49260                                     0.0   
#[Out]# 49261                                     0.3   
#[Out]# 49262                                     0.4   
#[Out]# 49263                                     0.3   
#[Out]# 49264                                     0.0   
#[Out]# 49265                                     0.5   
#[Out]# 49266                                     0.0   
#[Out]# 49267                                     0.3   
#[Out]# 49268                                     0.0   
#[Out]# 49269                                     0.5   
#[Out]# 49270                                     0.5   
#[Out]# 49271                                     0.5   
#[Out]# 49272                                     0.0   
#[Out]# 49273                                     0.0   
#[Out]# 49274                                     0.0   
#[Out]# 49275                                     0.0   
#[Out]# 49276                                     0.2   
#[Out]# 49277                                     0.0   
#[Out]# 49278                                     0.0   
#[Out]# 49279                                     0.0   
#[Out]# 49280                                     0.0   
#[Out]# 49281                                     0.0   
#[Out]# 
#[Out]#       inpatientdoc copay before deductible  \
#[Out]# 49235                                    0   
#[Out]# 49236                                    0   
#[Out]# 49237                                    0   
#[Out]# 49238                                    0   
#[Out]# 49239                                    0   
#[Out]# 49240                                    0   
#[Out]# 49241                                    0   
#[Out]# 49242                                    0   
#[Out]# 49243                                    0   
#[Out]# 49244                                    0   
#[Out]# 49245                                    0   
#[Out]# 49246                                    0   
#[Out]# 49247                                    0   
#[Out]# 49248                                    0   
#[Out]# 49249                                    0   
#[Out]# 49250                                    0   
#[Out]# 49251                                    0   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance before deductible  \
#[Out]# 49235                                        1.0   
#[Out]# 49236                                        1.0   
#[Out]# 49237                                        1.0   
#[Out]# 49238                                        1.0   
#[Out]# 49239                                        1.0   
#[Out]# 49240                                        1.0   
#[Out]# 49241                                        1.0   
#[Out]# 49242                                        1.0   
#[Out]# 49243                                        1.0   
#[Out]# 49244                                        1.0   
#[Out]# 49245                                        1.0   
#[Out]# 49246                                        1.0   
#[Out]# 49247                                        0.2   
#[Out]# 49248                                        1.0   
#[Out]# 49249                                        1.0   
#[Out]# 49250                                        1.0   
#[Out]# 49251                                        0.2   
#[Out]# 49252                                        1.0   
#[Out]# 49253                                        1.0   
#[Out]# 49254                                        1.0   
#[Out]# 49255                                        1.0   
#[Out]# 49256                                        1.0   
#[Out]# 49257                                        1.0   
#[Out]# 49258                                        1.0   
#[Out]# 49259                                        1.0   
#[Out]# 49260                                        1.0   
#[Out]# 49261                                        1.0   
#[Out]# 49262                                        1.0   
#[Out]# 49263                                        1.0   
#[Out]# 49264                                        1.0   
#[Out]# 49265                                        1.0   
#[Out]# 49266                                        1.0   
#[Out]# 49267                                        1.0   
#[Out]# 49268                                        1.0   
#[Out]# 49269                                        1.0   
#[Out]# 49270                                        1.0   
#[Out]# 49271                                        1.0   
#[Out]# 49272                                        1.0   
#[Out]# 49273                                        1.0   
#[Out]# 49274                                        1.0   
#[Out]# 49275                                        1.0   
#[Out]# 49276                                        1.0   
#[Out]# 49277                                        1.0   
#[Out]# 49278                                        1.0   
#[Out]# 49279                                        1.0   
#[Out]# 49280                                        0.0   
#[Out]# 49281                                        1.0   
#[Out]# 
#[Out]#       inpatientdoc copay after deductible  \
#[Out]# 49235                                   0   
#[Out]# 49236                                   0   
#[Out]# 49237                                   0   
#[Out]# 49238                                   0   
#[Out]# 49239                                   0   
#[Out]# 49240                                   0   
#[Out]# 49241                                   0   
#[Out]# 49242                                   0   
#[Out]# 49243                                   0   
#[Out]# 49244                                   0   
#[Out]# 49245                                   0   
#[Out]# 49246                                   0   
#[Out]# 49247                                   0   
#[Out]# 49248                                   0   
#[Out]# 49249                                   0   
#[Out]# 49250                                   0   
#[Out]# 49251                                   0   
#[Out]# 49252                                   0   
#[Out]# 49253                                   0   
#[Out]# 49254                                   0   
#[Out]# 49255                                   0   
#[Out]# 49256                                   0   
#[Out]# 49257                                   0   
#[Out]# 49258                                   0   
#[Out]# 49259                                   0   
#[Out]# 49260                                   0   
#[Out]# 49261                                   0   
#[Out]# 49262                                   0   
#[Out]# 49263                                   0   
#[Out]# 49264                                   0   
#[Out]# 49265                                   0   
#[Out]# 49266                                   0   
#[Out]# 49267                                   0   
#[Out]# 49268                                   0   
#[Out]# 49269                                   0   
#[Out]# 49270                                   0   
#[Out]# 49271                                   0   
#[Out]# 49272                                   0   
#[Out]# 49273                                   0   
#[Out]# 49274                                   0   
#[Out]# 49275                                   0   
#[Out]# 49276                                   0   
#[Out]# 49277                                   0   
#[Out]# 49278                                   0   
#[Out]# 49279                                   0   
#[Out]# 49280                                   0   
#[Out]# 49281                                   0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance after deductible  \
#[Out]# 49235                                       0.5   
#[Out]# 49236                                       0.4   
#[Out]# 49237                                       0.4   
#[Out]# 49238                                       0.3   
#[Out]# 49239                                       0.2   
#[Out]# 49240                                       0.5   
#[Out]# 49241                                       0.4   
#[Out]# 49242                                       0.3   
#[Out]# 49243                                       0.0   
#[Out]# 49244                                       0.5   
#[Out]# 49245                                       0.4   
#[Out]# 49246                                       0.3   
#[Out]# 49247                                       0.2   
#[Out]# 49248                                       0.5   
#[Out]# 49249                                       0.4   
#[Out]# 49250                                       0.3   
#[Out]# 49251                                       0.2   
#[Out]# 49252                                       0.1   
#[Out]# 49253                                       0.1   
#[Out]# 49254                                       0.2   
#[Out]# 49255                                       0.5   
#[Out]# 49256                                       0.2   
#[Out]# 49257                                       0.5   
#[Out]# 49258                                       0.5   
#[Out]# 49259                                       0.4   
#[Out]# 49260                                       0.2   
#[Out]# 49261                                       0.3   
#[Out]# 49262                                       0.4   
#[Out]# 49263                                       0.3   
#[Out]# 49264                                       0.4   
#[Out]# 49265                                       0.5   
#[Out]# 49266                                       0.2   
#[Out]# 49267                                       0.3   
#[Out]# 49268                                       0.0   
#[Out]# 49269                                       0.5   
#[Out]# 49270                                       0.5   
#[Out]# 49271                                       0.5   
#[Out]# 49272                                       0.2   
#[Out]# 49273                                       0.0   
#[Out]# 49274                                       0.1   
#[Out]# 49275                                       0.2   
#[Out]# 49276                                       0.2   
#[Out]# 49277                                       0.0   
#[Out]# 49278                                       0.0   
#[Out]# 49279                                       0.2   
#[Out]# 49280                                       0.0   
#[Out]# 49281                                       0.3   
#[Out]# 
#[Out]#       emergency copay before deductible  \
#[Out]# 49235                                 0   
#[Out]# 49236                                 0   
#[Out]# 49237                               100   
#[Out]# 49238                               100   
#[Out]# 49239                               100   
#[Out]# 49240                                 0   
#[Out]# 49241                                 0   
#[Out]# 49242                                 0   
#[Out]# 49243                                 0   
#[Out]# 49244                               100   
#[Out]# 49245                                 0   
#[Out]# 49246                                 0   
#[Out]# 49247                               100   
#[Out]# 49248                                 0   
#[Out]# 49249                               100   
#[Out]# 49250                               100   
#[Out]# 49251                               100   
#[Out]# 49252                               100   
#[Out]# 49253                               100   
#[Out]# 49254                               100   
#[Out]# 49255                               100   
#[Out]# 49256                                 0   
#[Out]# 49257                                 0   
#[Out]# 49258                                 0   
#[Out]# 49259                               100   
#[Out]# 49260                               100   
#[Out]# 49261                               100   
#[Out]# 49262                                 0   
#[Out]# 49263                               100   
#[Out]# 49264                               100   
#[Out]# 49265                               100   
#[Out]# 49266                               100   
#[Out]# 49267                               100   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                 0   
#[Out]# 49273                                 0   
#[Out]# 49274                                 0   
#[Out]# 49275                               100   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                               100   
#[Out]# 49280                               100   
#[Out]# 49281                                 0   
#[Out]# 
#[Out]#       emergency coinsurance before deductible  \
#[Out]# 49235                                     1.0   
#[Out]# 49236                                     1.0   
#[Out]# 49237                                     1.0   
#[Out]# 49238                                     1.0   
#[Out]# 49239                                     1.0   
#[Out]# 49240                                     1.0   
#[Out]# 49241                                     1.0   
#[Out]# 49242                                     1.0   
#[Out]# 49243                                     1.0   
#[Out]# 49244                                     1.0   
#[Out]# 49245                                     1.0   
#[Out]# 49246                                     1.0   
#[Out]# 49247                                     0.2   
#[Out]# 49248                                     1.0   
#[Out]# 49249                                     1.0   
#[Out]# 49250                                     1.0   
#[Out]# 49251                                     1.0   
#[Out]# 49252                                     1.0   
#[Out]# 49253                                     1.0   
#[Out]# 49254                                     1.0   
#[Out]# 49255                                     1.0   
#[Out]# 49256                                     1.0   
#[Out]# 49257                                     1.0   
#[Out]# 49258                                     1.0   
#[Out]# 49259                                     1.0   
#[Out]# 49260                                     1.0   
#[Out]# 49261                                     1.0   
#[Out]# 49262                                     1.0   
#[Out]# 49263                                     1.0   
#[Out]# 49264                                     1.0   
#[Out]# 49265                                     1.0   
#[Out]# 49266                                     1.0   
#[Out]# 49267                                     1.0   
#[Out]# 49268                                     1.0   
#[Out]# 49269                                     1.0   
#[Out]# 49270                                     1.0   
#[Out]# 49271                                     1.0   
#[Out]# 49272                                     1.0   
#[Out]# 49273                                     1.0   
#[Out]# 49274                                     1.0   
#[Out]# 49275                                     1.0   
#[Out]# 49276                                     1.0   
#[Out]# 49277                                     1.0   
#[Out]# 49278                                     1.0   
#[Out]# 49279                                     1.0   
#[Out]# 49280                                     1.0   
#[Out]# 49281                                     1.0   
#[Out]# 
#[Out]#       emergency copay after deductible emergency coinsurance after deductible  
#[Out]# 49235                              100                                    0.5  
#[Out]# 49236                              100                                    0.4  
#[Out]# 49237                                0                                    0.4  
#[Out]# 49238                                0                                    0.3  
#[Out]# 49239                              100                                    0.0  
#[Out]# 49240                              100                                    0.5  
#[Out]# 49241                              100                                    0.4  
#[Out]# 49242                              100                                    0.3  
#[Out]# 49243                                0                                    0.0  
#[Out]# 49244                                0                                    0.5  
#[Out]# 49245                              100                                    0.4  
#[Out]# 49246                              100                                    0.3  
#[Out]# 49247                              100                                    0.2  
#[Out]# 49248                              100                                    0.0  
#[Out]# 49249                              100                                    0.0  
#[Out]# 49250                              100                                    0.0  
#[Out]# 49251                              100                                    0.0  
#[Out]# 49252                              100                                    0.0  
#[Out]# 49253                                0                                    0.1  
#[Out]# 49254                                0                                    0.2  
#[Out]# 49255                                0                                    0.5  
#[Out]# 49256                                0                                    0.2  
#[Out]# 49257                                0                                    0.5  
#[Out]# 49258                              100                                    0.0  
#[Out]# 49259                              100                                    0.0  
#[Out]# 49260                                0                                    0.2  
#[Out]# 49261                                0                                    0.3  
#[Out]# 49262                              100                                    0.4  
#[Out]# 49263                                0                                    0.3  
#[Out]# 49264                                0                                    0.4  
#[Out]# 49265                                0                                    0.5  
#[Out]# 49266                                0                                    0.2  
#[Out]# 49267                                0                                    0.3  
#[Out]# 49268                                0                                    0.0  
#[Out]# 49269                                0                                    0.5  
#[Out]# 49270                                0                                    0.5  
#[Out]# 49271                                0                                    0.5  
#[Out]# 49272                                0                                    0.5  
#[Out]# 49273                              100                                    0.0  
#[Out]# 49274                              100                                    0.0  
#[Out]# 49275                              100                                    0.0  
#[Out]# 49276                                0                                    0.2  
#[Out]# 49277                                0                                    0.0  
#[Out]# 49278                                0                                    0.0  
#[Out]# 49279                              100                                    0.0  
#[Out]# 49280                              100                                    0.0  
#[Out]# 49281                              100                                    0.0  
#[Out]# 
#[Out]# [47 rows x 36 columns]
insurance[(insurance["State"]=="NJ") & (insurance["County"]=="Warren")].columns
#[Out]# Index(['Plan ID (standard component)',
#[Out]#        'Medical Deductible-individual-standard',
#[Out]#        'Drug Deductible-individual-standard',
#[Out]#        'Medical Maximum Out Of Pocket - individual - standard',
#[Out]#        'Drug Maximum Out of Pocket - individual - standard',
#[Out]#        'Primary Care Physician  - standard', 'Specialist  - standard',
#[Out]#        'Emergency Room  - standard', 'Inpatient Facility  - standard',
#[Out]#        'Inpatient Physician - standard', 'Generic Drugs - standard',
#[Out]#        'Preferred Brand Drugs - standard',
#[Out]#        'Non-preferred Brand Drugs - standard', 'Specialty Drugs - standard',
#[Out]#        'State', 'County', 'breakout of Primary Care Physician  - standard',
#[Out]#        'breakout of Specialist  - standard',
#[Out]#        'breakout of Inpatient Physician - standard',
#[Out]#        'breakout of Emergency Room  - standard', 'pcp copay before deductible',
#[Out]#        'pcp coinsurance before deductible', 'pcp copay after deductible',
#[Out]#        'pcp coinsurance after deductible',
#[Out]#        'specialist copay before deductible',
#[Out]#        'specialist coinsurance before deductible',
#[Out]#        'specialist copay after deductible',
#[Out]#        'specialist coinsurance after deductible',
#[Out]#        'inpatientdoc copay before deductible',
#[Out]#        'inpatientdoc coinsurance before deductible',
#[Out]#        'inpatientdoc copay after deductible',
#[Out]#        'inpatientdoc coinsurance after deductible',
#[Out]#        'emergency copay before deductible',
#[Out]#        'emergency coinsurance before deductible',
#[Out]#        'emergency copay after deductible',
#[Out]#        'emergency coinsurance after deductible'],
#[Out]#       dtype='object')
insurance[(insurance["State"]=="NJ") & (insurance["County"]=="Warren")]
#[Out]#       Plan ID (standard component)  Medical Deductible-individual-standard  \
#[Out]# 49235               10191NJ0030001                                    2500   
#[Out]# 49236               10191NJ0030002                                    2000   
#[Out]# 49237               10191NJ0050001                                    2000   
#[Out]# 49238               10191NJ0050002                                    1500   
#[Out]# 49239               10191NJ0050003                                     750   
#[Out]# 49240               10191NJ0070001                                    2500   
#[Out]# 49241               10191NJ0070002                                    2000   
#[Out]# 49242               10191NJ0070003                                    1500   
#[Out]# 49243               10191NJ0150001                                    6600   
#[Out]# 49244               10191NJ0190001                                    2500   
#[Out]# 49245               10191NJ0190002                                    2000   
#[Out]# 49246               10191NJ0190003                                    1500   
#[Out]# 49247               10191NJ0190004                                       0   
#[Out]# 49248               10191NJ0290001                                    2500   
#[Out]# 49249               10191NJ0290002                                    2000   
#[Out]# 49250               10191NJ0290003                                    1800   
#[Out]# 49251               10191NJ0290004                                       0   
#[Out]# 49252               48834NJ0080001                                     200   
#[Out]# 49253               48834NJ0080002                                    1000   
#[Out]# 49254               48834NJ0080003                                     500   
#[Out]# 49255               48834NJ0080004                                    2500   
#[Out]# 49256               48834NJ0080005                                    1500   
#[Out]# 49257               48834NJ0080006                                    2500   
#[Out]# 49258               77606NJ0040001                                    2500   
#[Out]# 49259               77606NJ0040002                                    2000   
#[Out]# 49260               91661NJ2260002                                    1000   
#[Out]# 49261               91661NJ2260003                                    1500   
#[Out]# 49262               91661NJ2260006                                    2500   
#[Out]# 49263               91661NJ2260007                                    2000   
#[Out]# 49264               91661NJ2270001                                    2000   
#[Out]# 49265               91661NJ2270002                                    2500   
#[Out]# 49266               91661NJ2270003                                    1000   
#[Out]# 49267               91661NJ2270004                                    2000   
#[Out]# 49268               91661NJ2280001                                    6600   
#[Out]# 49269               91762NJ0070001                                    2500   
#[Out]# 49270               91762NJ0070002                                    2500   
#[Out]# 49271               91762NJ0070003                                    2500   
#[Out]# 49272               91762NJ0070004                                    2500   
#[Out]# 49273               91762NJ0070006                                    1800   
#[Out]# 49274               91762NJ0070007                                    1350   
#[Out]# 49275               91762NJ0070010                                    1000   
#[Out]# 49276               91762NJ0070012                                    1300   
#[Out]# 49277               91762NJ0070014                                    6600   
#[Out]# 49278               91762NJ0070015                                    6600   
#[Out]# 49279               91762NJ0070080                                    1000   
#[Out]# 49280               91762NJ0110001                                       0   
#[Out]# 49281               91762NJ0110002                                    2500   
#[Out]# 
#[Out]#        Drug Deductible-individual-standard  \
#[Out]# 49235                                    0   
#[Out]# 49236                                    0   
#[Out]# 49237                                    0   
#[Out]# 49238                                    0   
#[Out]# 49239                                    0   
#[Out]# 49240                                    0   
#[Out]# 49241                                    0   
#[Out]# 49242                                    0   
#[Out]# 49243                                    0   
#[Out]# 49244                                    0   
#[Out]# 49245                                    0   
#[Out]# 49246                                    0   
#[Out]# 49247                                    0   
#[Out]# 49248                                    0   
#[Out]# 49249                                    0   
#[Out]# 49250                                    0   
#[Out]# 49251                                    0   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#        Medical Maximum Out Of Pocket - individual - standard  \
#[Out]# 49235                                               6600       
#[Out]# 49236                                               4500       
#[Out]# 49237                                               4500       
#[Out]# 49238                                               3500       
#[Out]# 49239                                               1500       
#[Out]# 49240                                               6450       
#[Out]# 49241                                               4000       
#[Out]# 49242                                               2500       
#[Out]# 49243                                               6600       
#[Out]# 49244                                               6600       
#[Out]# 49245                                               6000       
#[Out]# 49246                                               3000       
#[Out]# 49247                                               1250       
#[Out]# 49248                                               6550       
#[Out]# 49249                                               5000       
#[Out]# 49250                                               3000       
#[Out]# 49251                                               2000       
#[Out]# 49252                                               2000       
#[Out]# 49253                                               3000       
#[Out]# 49254                                               6600       
#[Out]# 49255                                               6350       
#[Out]# 49256                                               5500       
#[Out]# 49257                                               6350       
#[Out]# 49258                                               6350       
#[Out]# 49259                                               4650       
#[Out]# 49260                                               2500       
#[Out]# 49261                                               5000       
#[Out]# 49262                                               6600       
#[Out]# 49263                                               5000       
#[Out]# 49264                                               6350       
#[Out]# 49265                                               6350       
#[Out]# 49266                                               4000       
#[Out]# 49267                                               5000       
#[Out]# 49268                                               6600       
#[Out]# 49269                                               6450       
#[Out]# 49270                                               6450       
#[Out]# 49271                                               6450       
#[Out]# 49272                                               6450       
#[Out]# 49273                                               4500       
#[Out]# 49274                                               5750       
#[Out]# 49275                                               5000       
#[Out]# 49276                                               2500       
#[Out]# 49277                                               6600       
#[Out]# 49278                                               6600       
#[Out]# 49279                                               5000       
#[Out]# 49280                                               4000       
#[Out]# 49281                                               6350       
#[Out]# 
#[Out]#        Drug Maximum Out of Pocket - individual - standard  \
#[Out]# 49235                                                  0    
#[Out]# 49236                                                  0    
#[Out]# 49237                                                  0    
#[Out]# 49238                                                  0    
#[Out]# 49239                                                  0    
#[Out]# 49240                                                  0    
#[Out]# 49241                                                  0    
#[Out]# 49242                                                  0    
#[Out]# 49243                                                  0    
#[Out]# 49244                                                  0    
#[Out]# 49245                                                  0    
#[Out]# 49246                                                  0    
#[Out]# 49247                                                  0    
#[Out]# 49248                                                  0    
#[Out]# 49249                                                  0    
#[Out]# 49250                                                  0    
#[Out]# 49251                                                  0    
#[Out]# 49252                                                  0    
#[Out]# 49253                                                  0    
#[Out]# 49254                                                  0    
#[Out]# 49255                                                  0    
#[Out]# 49256                                                  0    
#[Out]# 49257                                                  0    
#[Out]# 49258                                                  0    
#[Out]# 49259                                                  0    
#[Out]# 49260                                                  0    
#[Out]# 49261                                                  0    
#[Out]# 49262                                                  0    
#[Out]# 49263                                                  0    
#[Out]# 49264                                                  0    
#[Out]# 49265                                                  0    
#[Out]# 49266                                                  0    
#[Out]# 49267                                                  0    
#[Out]# 49268                                                  0    
#[Out]# 49269                                                  0    
#[Out]# 49270                                                  0    
#[Out]# 49271                                                  0    
#[Out]# 49272                                                  0    
#[Out]# 49273                                                  0    
#[Out]# 49274                                                  0    
#[Out]# 49275                                                  0    
#[Out]# 49276                                                  0    
#[Out]# 49277                                                  0    
#[Out]# 49278                                                  0    
#[Out]# 49279                                                  0    
#[Out]# 49280                                                  0    
#[Out]# 49281                                                  0    
#[Out]# 
#[Out]#       Primary Care Physician  - standard            Specialist  - standard  \
#[Out]# 49235   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49236   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49237                                $25                               $50   
#[Out]# 49238                                $10                               $25   
#[Out]# 49239                                 $5                               $10   
#[Out]# 49240   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49241   40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49242                                30%                               30%   
#[Out]# 49243         No Charge after Deductible        No Charge after Deductible   
#[Out]# 49244         $10 Copay after deductible        $75 Copay after deductible   
#[Out]# 49245                                $10                               $50   
#[Out]# 49246                                $10                               $25   
#[Out]# 49247                                $10                               $10   
#[Out]# 49248         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49249                                $25                               $75   
#[Out]# 49250                                $15                               $50   
#[Out]# 49251                                $10                               $25   
#[Out]# 49252                                $15                               $30   
#[Out]# 49253                                $20                               $40   
#[Out]# 49254                                $20                               $40   
#[Out]# 49255                                $30                               $60   
#[Out]# 49256         $25 Copay after deductible        $50 Copay after deductible   
#[Out]# 49257   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258                                $50                               $75   
#[Out]# 49259                                $15                               $30   
#[Out]# 49260                                $15                               $30   
#[Out]# 49261                                $30  30% Coinsurance after deductible   
#[Out]# 49262         $40 Copay after deductible  40% Coinsurance after deductible   
#[Out]# 49263                                $40  30% Coinsurance after deductible   
#[Out]# 49264                                $25                               $50   
#[Out]# 49265         $30 Copay after deductible  50% Coinsurance after deductible   
#[Out]# 49266                                $15                               $30   
#[Out]# 49267                                $20  30% Coinsurance after deductible   
#[Out]# 49268         No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271   50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49273         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49274         $50 Copay after deductible        $75 Copay after deductible   
#[Out]# 49275                                $30                               $50   
#[Out]# 49276   20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277                                $30        No Charge after Deductible   
#[Out]# 49278                                $30        No Charge after Deductible   
#[Out]# 49279                                $30                               $50   
#[Out]# 49280                                $15                               $25   
#[Out]# 49281                                $40                               $50   
#[Out]# 
#[Out]#                               Emergency Room  - standard  \
#[Out]# 49235  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 49236  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49237  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 49238  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49239                                               $100   
#[Out]# 49240  $100 Copay after deductible and 50% Coinsuranc...   
#[Out]# 49241  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49242  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 49243                         No Charge after Deductible   
#[Out]# 49244  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49245  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49246  $100 Copay after deductible and 30% Coinsuranc...   
#[Out]# 49247                     $100 Copay and 20% Coinsurance   
#[Out]# 49248                        $100 Copay after deductible   
#[Out]# 49249                                               $100   
#[Out]# 49250                                               $100   
#[Out]# 49251                                               $100   
#[Out]# 49252                                               $100   
#[Out]# 49253  $100 Copay before deductible and 10% Coinsuran...   
#[Out]# 49254  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49255  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49256                   20% Coinsurance after deductible   
#[Out]# 49257                   50% Coinsurance after deductible   
#[Out]# 49258                        $100 Copay after deductible   
#[Out]# 49259                                               $100   
#[Out]# 49260  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49261  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49262  $100 Copay after deductible and 40% Coinsuranc...   
#[Out]# 49263  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49264  $100 Copay before deductible and 40% Coinsuran...   
#[Out]# 49265  $100 Copay before deductible and 50% Coinsuran...   
#[Out]# 49266  $100 Copay before deductible and 20% Coinsuran...   
#[Out]# 49267  $100 Copay before deductible and 30% Coinsuran...   
#[Out]# 49268                         No Charge after Deductible   
#[Out]# 49269                   50% Coinsurance after deductible   
#[Out]# 49270                   50% Coinsurance after deductible   
#[Out]# 49271                   50% Coinsurance after deductible   
#[Out]# 49272                   50% Coinsurance after deductible   
#[Out]# 49273                        $100 Copay after deductible   
#[Out]# 49274                        $100 Copay after deductible   
#[Out]# 49275                                               $100   
#[Out]# 49276                   20% Coinsurance after deductible   
#[Out]# 49277                         No Charge after Deductible   
#[Out]# 49278                         No Charge after Deductible   
#[Out]# 49279                                               $100   
#[Out]# 49280                                               $100   
#[Out]# 49281                        $100 Copay after deductible   
#[Out]# 
#[Out]#          Inpatient Facility  - standard    Inpatient Physician - standard  \
#[Out]# 49235  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49236  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49237  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49238  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49239  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49240  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49241  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49242  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49243        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49244  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49245                $500 Copay per Day  40% Coinsurance after deductible   
#[Out]# 49246                $250 Copay per Day  30% Coinsurance after deductible   
#[Out]# 49247                $100 Copay per Day                               20%   
#[Out]# 49248  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49249  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49250  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49251                               20%                               20%   
#[Out]# 49252  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49253                $100 Copay per Day  10% Coinsurance after deductible   
#[Out]# 49254  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49255  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49256  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49257  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49258  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49259  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49260  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49261  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49262  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49263  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49264  40% Coinsurance after deductible  40% Coinsurance after deductible   
#[Out]# 49265  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49266  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49267  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 49268        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49269  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49270  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49271  50% Coinsurance after deductible  50% Coinsurance after deductible   
#[Out]# 49272  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49273                $500 Copay per Day        No Charge after Deductible   
#[Out]# 49274  10% Coinsurance after deductible  10% Coinsurance after deductible   
#[Out]# 49275  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49276  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49277        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49278        No Charge after Deductible        No Charge after Deductible   
#[Out]# 49279  20% Coinsurance after deductible  20% Coinsurance after deductible   
#[Out]# 49280                $300 Copay per Day                         No Charge   
#[Out]# 49281  30% Coinsurance after deductible  30% Coinsurance after deductible   
#[Out]# 
#[Out]#                        ...                    \
#[Out]# 49235                  ...                     
#[Out]# 49236                  ...                     
#[Out]# 49237                  ...                     
#[Out]# 49238                  ...                     
#[Out]# 49239                  ...                     
#[Out]# 49240                  ...                     
#[Out]# 49241                  ...                     
#[Out]# 49242                  ...                     
#[Out]# 49243                  ...                     
#[Out]# 49244                  ...                     
#[Out]# 49245                  ...                     
#[Out]# 49246                  ...                     
#[Out]# 49247                  ...                     
#[Out]# 49248                  ...                     
#[Out]# 49249                  ...                     
#[Out]# 49250                  ...                     
#[Out]# 49251                  ...                     
#[Out]# 49252                  ...                     
#[Out]# 49253                  ...                     
#[Out]# 49254                  ...                     
#[Out]# 49255                  ...                     
#[Out]# 49256                  ...                     
#[Out]# 49257                  ...                     
#[Out]# 49258                  ...                     
#[Out]# 49259                  ...                     
#[Out]# 49260                  ...                     
#[Out]# 49261                  ...                     
#[Out]# 49262                  ...                     
#[Out]# 49263                  ...                     
#[Out]# 49264                  ...                     
#[Out]# 49265                  ...                     
#[Out]# 49266                  ...                     
#[Out]# 49267                  ...                     
#[Out]# 49268                  ...                     
#[Out]# 49269                  ...                     
#[Out]# 49270                  ...                     
#[Out]# 49271                  ...                     
#[Out]# 49272                  ...                     
#[Out]# 49273                  ...                     
#[Out]# 49274                  ...                     
#[Out]# 49275                  ...                     
#[Out]# 49276                  ...                     
#[Out]# 49277                  ...                     
#[Out]# 49278                  ...                     
#[Out]# 49279                  ...                     
#[Out]# 49280                  ...                     
#[Out]# 49281                  ...                     
#[Out]# 
#[Out]#       specialist copay after deductible  \
#[Out]# 49235                                 0   
#[Out]# 49236                                 0   
#[Out]# 49237                                50   
#[Out]# 49238                                25   
#[Out]# 49239                                10   
#[Out]# 49240                                 0   
#[Out]# 49241                                 0   
#[Out]# 49242                                 0   
#[Out]# 49243                                 0   
#[Out]# 49244                                75   
#[Out]# 49245                                50   
#[Out]# 49246                                25   
#[Out]# 49247                                10   
#[Out]# 49248                                75   
#[Out]# 49249                                75   
#[Out]# 49250                                50   
#[Out]# 49251                                25   
#[Out]# 49252                                30   
#[Out]# 49253                                40   
#[Out]# 49254                                40   
#[Out]# 49255                                60   
#[Out]# 49256                                50   
#[Out]# 49257                                 0   
#[Out]# 49258                                75   
#[Out]# 49259                                30   
#[Out]# 49260                                30   
#[Out]# 49261                                 0   
#[Out]# 49262                                 0   
#[Out]# 49263                                 0   
#[Out]# 49264                                50   
#[Out]# 49265                                 0   
#[Out]# 49266                                30   
#[Out]# 49267                                 0   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                75   
#[Out]# 49273                                75   
#[Out]# 49274                                75   
#[Out]# 49275                                50   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                                50   
#[Out]# 49280                                25   
#[Out]# 49281                                50   
#[Out]# 
#[Out]#       specialist coinsurance after deductible  \
#[Out]# 49235                                     0.5   
#[Out]# 49236                                     0.4   
#[Out]# 49237                                     0.0   
#[Out]# 49238                                     0.0   
#[Out]# 49239                                     0.0   
#[Out]# 49240                                     0.5   
#[Out]# 49241                                     0.4   
#[Out]# 49242                                     0.3   
#[Out]# 49243                                     0.0   
#[Out]# 49244                                     0.0   
#[Out]# 49245                                     0.0   
#[Out]# 49246                                     0.0   
#[Out]# 49247                                     0.0   
#[Out]# 49248                                     0.0   
#[Out]# 49249                                     0.0   
#[Out]# 49250                                     0.0   
#[Out]# 49251                                     0.0   
#[Out]# 49252                                     0.0   
#[Out]# 49253                                     0.0   
#[Out]# 49254                                     0.0   
#[Out]# 49255                                     0.0   
#[Out]# 49256                                     0.0   
#[Out]# 49257                                     0.5   
#[Out]# 49258                                     0.0   
#[Out]# 49259                                     0.0   
#[Out]# 49260                                     0.0   
#[Out]# 49261                                     0.3   
#[Out]# 49262                                     0.4   
#[Out]# 49263                                     0.3   
#[Out]# 49264                                     0.0   
#[Out]# 49265                                     0.5   
#[Out]# 49266                                     0.0   
#[Out]# 49267                                     0.3   
#[Out]# 49268                                     0.0   
#[Out]# 49269                                     0.5   
#[Out]# 49270                                     0.5   
#[Out]# 49271                                     0.5   
#[Out]# 49272                                     0.0   
#[Out]# 49273                                     0.0   
#[Out]# 49274                                     0.0   
#[Out]# 49275                                     0.0   
#[Out]# 49276                                     0.2   
#[Out]# 49277                                     0.0   
#[Out]# 49278                                     0.0   
#[Out]# 49279                                     0.0   
#[Out]# 49280                                     0.0   
#[Out]# 49281                                     0.0   
#[Out]# 
#[Out]#       inpatientdoc copay before deductible  \
#[Out]# 49235                                    0   
#[Out]# 49236                                    0   
#[Out]# 49237                                    0   
#[Out]# 49238                                    0   
#[Out]# 49239                                    0   
#[Out]# 49240                                    0   
#[Out]# 49241                                    0   
#[Out]# 49242                                    0   
#[Out]# 49243                                    0   
#[Out]# 49244                                    0   
#[Out]# 49245                                    0   
#[Out]# 49246                                    0   
#[Out]# 49247                                    0   
#[Out]# 49248                                    0   
#[Out]# 49249                                    0   
#[Out]# 49250                                    0   
#[Out]# 49251                                    0   
#[Out]# 49252                                    0   
#[Out]# 49253                                    0   
#[Out]# 49254                                    0   
#[Out]# 49255                                    0   
#[Out]# 49256                                    0   
#[Out]# 49257                                    0   
#[Out]# 49258                                    0   
#[Out]# 49259                                    0   
#[Out]# 49260                                    0   
#[Out]# 49261                                    0   
#[Out]# 49262                                    0   
#[Out]# 49263                                    0   
#[Out]# 49264                                    0   
#[Out]# 49265                                    0   
#[Out]# 49266                                    0   
#[Out]# 49267                                    0   
#[Out]# 49268                                    0   
#[Out]# 49269                                    0   
#[Out]# 49270                                    0   
#[Out]# 49271                                    0   
#[Out]# 49272                                    0   
#[Out]# 49273                                    0   
#[Out]# 49274                                    0   
#[Out]# 49275                                    0   
#[Out]# 49276                                    0   
#[Out]# 49277                                    0   
#[Out]# 49278                                    0   
#[Out]# 49279                                    0   
#[Out]# 49280                                    0   
#[Out]# 49281                                    0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance before deductible  \
#[Out]# 49235                                        1.0   
#[Out]# 49236                                        1.0   
#[Out]# 49237                                        1.0   
#[Out]# 49238                                        1.0   
#[Out]# 49239                                        1.0   
#[Out]# 49240                                        1.0   
#[Out]# 49241                                        1.0   
#[Out]# 49242                                        1.0   
#[Out]# 49243                                        1.0   
#[Out]# 49244                                        1.0   
#[Out]# 49245                                        1.0   
#[Out]# 49246                                        1.0   
#[Out]# 49247                                        0.2   
#[Out]# 49248                                        1.0   
#[Out]# 49249                                        1.0   
#[Out]# 49250                                        1.0   
#[Out]# 49251                                        0.2   
#[Out]# 49252                                        1.0   
#[Out]# 49253                                        1.0   
#[Out]# 49254                                        1.0   
#[Out]# 49255                                        1.0   
#[Out]# 49256                                        1.0   
#[Out]# 49257                                        1.0   
#[Out]# 49258                                        1.0   
#[Out]# 49259                                        1.0   
#[Out]# 49260                                        1.0   
#[Out]# 49261                                        1.0   
#[Out]# 49262                                        1.0   
#[Out]# 49263                                        1.0   
#[Out]# 49264                                        1.0   
#[Out]# 49265                                        1.0   
#[Out]# 49266                                        1.0   
#[Out]# 49267                                        1.0   
#[Out]# 49268                                        1.0   
#[Out]# 49269                                        1.0   
#[Out]# 49270                                        1.0   
#[Out]# 49271                                        1.0   
#[Out]# 49272                                        1.0   
#[Out]# 49273                                        1.0   
#[Out]# 49274                                        1.0   
#[Out]# 49275                                        1.0   
#[Out]# 49276                                        1.0   
#[Out]# 49277                                        1.0   
#[Out]# 49278                                        1.0   
#[Out]# 49279                                        1.0   
#[Out]# 49280                                        0.0   
#[Out]# 49281                                        1.0   
#[Out]# 
#[Out]#       inpatientdoc copay after deductible  \
#[Out]# 49235                                   0   
#[Out]# 49236                                   0   
#[Out]# 49237                                   0   
#[Out]# 49238                                   0   
#[Out]# 49239                                   0   
#[Out]# 49240                                   0   
#[Out]# 49241                                   0   
#[Out]# 49242                                   0   
#[Out]# 49243                                   0   
#[Out]# 49244                                   0   
#[Out]# 49245                                   0   
#[Out]# 49246                                   0   
#[Out]# 49247                                   0   
#[Out]# 49248                                   0   
#[Out]# 49249                                   0   
#[Out]# 49250                                   0   
#[Out]# 49251                                   0   
#[Out]# 49252                                   0   
#[Out]# 49253                                   0   
#[Out]# 49254                                   0   
#[Out]# 49255                                   0   
#[Out]# 49256                                   0   
#[Out]# 49257                                   0   
#[Out]# 49258                                   0   
#[Out]# 49259                                   0   
#[Out]# 49260                                   0   
#[Out]# 49261                                   0   
#[Out]# 49262                                   0   
#[Out]# 49263                                   0   
#[Out]# 49264                                   0   
#[Out]# 49265                                   0   
#[Out]# 49266                                   0   
#[Out]# 49267                                   0   
#[Out]# 49268                                   0   
#[Out]# 49269                                   0   
#[Out]# 49270                                   0   
#[Out]# 49271                                   0   
#[Out]# 49272                                   0   
#[Out]# 49273                                   0   
#[Out]# 49274                                   0   
#[Out]# 49275                                   0   
#[Out]# 49276                                   0   
#[Out]# 49277                                   0   
#[Out]# 49278                                   0   
#[Out]# 49279                                   0   
#[Out]# 49280                                   0   
#[Out]# 49281                                   0   
#[Out]# 
#[Out]#       inpatientdoc coinsurance after deductible  \
#[Out]# 49235                                       0.5   
#[Out]# 49236                                       0.4   
#[Out]# 49237                                       0.4   
#[Out]# 49238                                       0.3   
#[Out]# 49239                                       0.2   
#[Out]# 49240                                       0.5   
#[Out]# 49241                                       0.4   
#[Out]# 49242                                       0.3   
#[Out]# 49243                                       0.0   
#[Out]# 49244                                       0.5   
#[Out]# 49245                                       0.4   
#[Out]# 49246                                       0.3   
#[Out]# 49247                                       0.2   
#[Out]# 49248                                       0.5   
#[Out]# 49249                                       0.4   
#[Out]# 49250                                       0.3   
#[Out]# 49251                                       0.2   
#[Out]# 49252                                       0.1   
#[Out]# 49253                                       0.1   
#[Out]# 49254                                       0.2   
#[Out]# 49255                                       0.5   
#[Out]# 49256                                       0.2   
#[Out]# 49257                                       0.5   
#[Out]# 49258                                       0.5   
#[Out]# 49259                                       0.4   
#[Out]# 49260                                       0.2   
#[Out]# 49261                                       0.3   
#[Out]# 49262                                       0.4   
#[Out]# 49263                                       0.3   
#[Out]# 49264                                       0.4   
#[Out]# 49265                                       0.5   
#[Out]# 49266                                       0.2   
#[Out]# 49267                                       0.3   
#[Out]# 49268                                       0.0   
#[Out]# 49269                                       0.5   
#[Out]# 49270                                       0.5   
#[Out]# 49271                                       0.5   
#[Out]# 49272                                       0.2   
#[Out]# 49273                                       0.0   
#[Out]# 49274                                       0.1   
#[Out]# 49275                                       0.2   
#[Out]# 49276                                       0.2   
#[Out]# 49277                                       0.0   
#[Out]# 49278                                       0.0   
#[Out]# 49279                                       0.2   
#[Out]# 49280                                       0.0   
#[Out]# 49281                                       0.3   
#[Out]# 
#[Out]#       emergency copay before deductible  \
#[Out]# 49235                                 0   
#[Out]# 49236                                 0   
#[Out]# 49237                               100   
#[Out]# 49238                               100   
#[Out]# 49239                               100   
#[Out]# 49240                                 0   
#[Out]# 49241                                 0   
#[Out]# 49242                                 0   
#[Out]# 49243                                 0   
#[Out]# 49244                               100   
#[Out]# 49245                                 0   
#[Out]# 49246                                 0   
#[Out]# 49247                               100   
#[Out]# 49248                                 0   
#[Out]# 49249                               100   
#[Out]# 49250                               100   
#[Out]# 49251                               100   
#[Out]# 49252                               100   
#[Out]# 49253                               100   
#[Out]# 49254                               100   
#[Out]# 49255                               100   
#[Out]# 49256                                 0   
#[Out]# 49257                                 0   
#[Out]# 49258                                 0   
#[Out]# 49259                               100   
#[Out]# 49260                               100   
#[Out]# 49261                               100   
#[Out]# 49262                                 0   
#[Out]# 49263                               100   
#[Out]# 49264                               100   
#[Out]# 49265                               100   
#[Out]# 49266                               100   
#[Out]# 49267                               100   
#[Out]# 49268                                 0   
#[Out]# 49269                                 0   
#[Out]# 49270                                 0   
#[Out]# 49271                                 0   
#[Out]# 49272                                 0   
#[Out]# 49273                                 0   
#[Out]# 49274                                 0   
#[Out]# 49275                               100   
#[Out]# 49276                                 0   
#[Out]# 49277                                 0   
#[Out]# 49278                                 0   
#[Out]# 49279                               100   
#[Out]# 49280                               100   
#[Out]# 49281                                 0   
#[Out]# 
#[Out]#       emergency coinsurance before deductible  \
#[Out]# 49235                                     1.0   
#[Out]# 49236                                     1.0   
#[Out]# 49237                                     1.0   
#[Out]# 49238                                     1.0   
#[Out]# 49239                                     1.0   
#[Out]# 49240                                     1.0   
#[Out]# 49241                                     1.0   
#[Out]# 49242                                     1.0   
#[Out]# 49243                                     1.0   
#[Out]# 49244                                     1.0   
#[Out]# 49245                                     1.0   
#[Out]# 49246                                     1.0   
#[Out]# 49247                                     0.2   
#[Out]# 49248                                     1.0   
#[Out]# 49249                                     1.0   
#[Out]# 49250                                     1.0   
#[Out]# 49251                                     1.0   
#[Out]# 49252                                     1.0   
#[Out]# 49253                                     1.0   
#[Out]# 49254                                     1.0   
#[Out]# 49255                                     1.0   
#[Out]# 49256                                     1.0   
#[Out]# 49257                                     1.0   
#[Out]# 49258                                     1.0   
#[Out]# 49259                                     1.0   
#[Out]# 49260                                     1.0   
#[Out]# 49261                                     1.0   
#[Out]# 49262                                     1.0   
#[Out]# 49263                                     1.0   
#[Out]# 49264                                     1.0   
#[Out]# 49265                                     1.0   
#[Out]# 49266                                     1.0   
#[Out]# 49267                                     1.0   
#[Out]# 49268                                     1.0   
#[Out]# 49269                                     1.0   
#[Out]# 49270                                     1.0   
#[Out]# 49271                                     1.0   
#[Out]# 49272                                     1.0   
#[Out]# 49273                                     1.0   
#[Out]# 49274                                     1.0   
#[Out]# 49275                                     1.0   
#[Out]# 49276                                     1.0   
#[Out]# 49277                                     1.0   
#[Out]# 49278                                     1.0   
#[Out]# 49279                                     1.0   
#[Out]# 49280                                     1.0   
#[Out]# 49281                                     1.0   
#[Out]# 
#[Out]#       emergency copay after deductible emergency coinsurance after deductible  
#[Out]# 49235                              100                                    0.5  
#[Out]# 49236                              100                                    0.4  
#[Out]# 49237                                0                                    0.4  
#[Out]# 49238                                0                                    0.3  
#[Out]# 49239                              100                                    0.0  
#[Out]# 49240                              100                                    0.5  
#[Out]# 49241                              100                                    0.4  
#[Out]# 49242                              100                                    0.3  
#[Out]# 49243                                0                                    0.0  
#[Out]# 49244                                0                                    0.5  
#[Out]# 49245                              100                                    0.4  
#[Out]# 49246                              100                                    0.3  
#[Out]# 49247                              100                                    0.2  
#[Out]# 49248                              100                                    0.0  
#[Out]# 49249                              100                                    0.0  
#[Out]# 49250                              100                                    0.0  
#[Out]# 49251                              100                                    0.0  
#[Out]# 49252                              100                                    0.0  
#[Out]# 49253                                0                                    0.1  
#[Out]# 49254                                0                                    0.2  
#[Out]# 49255                                0                                    0.5  
#[Out]# 49256                                0                                    0.2  
#[Out]# 49257                                0                                    0.5  
#[Out]# 49258                              100                                    0.0  
#[Out]# 49259                              100                                    0.0  
#[Out]# 49260                                0                                    0.2  
#[Out]# 49261                                0                                    0.3  
#[Out]# 49262                              100                                    0.4  
#[Out]# 49263                                0                                    0.3  
#[Out]# 49264                                0                                    0.4  
#[Out]# 49265                                0                                    0.5  
#[Out]# 49266                                0                                    0.2  
#[Out]# 49267                                0                                    0.3  
#[Out]# 49268                                0                                    0.0  
#[Out]# 49269                                0                                    0.5  
#[Out]# 49270                                0                                    0.5  
#[Out]# 49271                                0                                    0.5  
#[Out]# 49272                                0                                    0.5  
#[Out]# 49273                              100                                    0.0  
#[Out]# 49274                              100                                    0.0  
#[Out]# 49275                              100                                    0.0  
#[Out]# 49276                                0                                    0.2  
#[Out]# 49277                                0                                    0.0  
#[Out]# 49278                                0                                    0.0  
#[Out]# 49279                              100                                    0.0  
#[Out]# 49280                              100                                    0.0  
#[Out]# 49281                              100                                    0.0  
#[Out]# 
#[Out]# [47 rows x 36 columns]
insurance["County"][insurance["State"]=="
insurance["County"][insurance["State"]=="NJ"]
#[Out]# 48247    Atlantic
#[Out]# 48248    Atlantic
#[Out]# 48249    Atlantic
#[Out]# 48250    Atlantic
#[Out]# 48251    Atlantic
#[Out]# 48252    Atlantic
#[Out]# 48253    Atlantic
#[Out]# 48254    Atlantic
#[Out]# 48255    Atlantic
#[Out]# 48256    Atlantic
#[Out]# 48257    Atlantic
#[Out]# 48258    Atlantic
#[Out]# 48259    Atlantic
#[Out]# 48260    Atlantic
#[Out]# 48261    Atlantic
#[Out]# 48262    Atlantic
#[Out]# 48263    Atlantic
#[Out]# 48264    Atlantic
#[Out]# 48265    Atlantic
#[Out]# 48266    Atlantic
#[Out]# 48267    Atlantic
#[Out]# 48268    Atlantic
#[Out]# 48269    Atlantic
#[Out]# 48270    Atlantic
#[Out]# 48271    Atlantic
#[Out]# 48272    Atlantic
#[Out]# 48273    Atlantic
#[Out]# 48274    Atlantic
#[Out]# 48275    Atlantic
#[Out]# 48276    Atlantic
#[Out]#            ...   
#[Out]# 49252      Warren
#[Out]# 49253      Warren
#[Out]# 49254      Warren
#[Out]# 49255      Warren
#[Out]# 49256      Warren
#[Out]# 49257      Warren
#[Out]# 49258      Warren
#[Out]# 49259      Warren
#[Out]# 49260      Warren
#[Out]# 49261      Warren
#[Out]# 49262      Warren
#[Out]# 49263      Warren
#[Out]# 49264      Warren
#[Out]# 49265      Warren
#[Out]# 49266      Warren
#[Out]# 49267      Warren
#[Out]# 49268      Warren
#[Out]# 49269      Warren
#[Out]# 49270      Warren
#[Out]# 49271      Warren
#[Out]# 49272      Warren
#[Out]# 49273      Warren
#[Out]# 49274      Warren
#[Out]# 49275      Warren
#[Out]# 49276      Warren
#[Out]# 49277      Warren
#[Out]# 49278      Warren
#[Out]# 49279      Warren
#[Out]# 49280      Warren
#[Out]# 49281      Warren
#[Out]# Name: County, dtype: object
insurance["County"][insurance["State"]=="NJ"].unique()
#[Out]# array(['Atlantic', 'Bergen', 'Burlington', 'Camden', 'Cape May',
#[Out]#        'Cumberland', 'Essex', 'Gloucester', 'Hudson', 'Hunterdon',
#[Out]#        'Mercer', 'Middlesex', 'Monmouth', 'Morris', 'Ocean', 'Passaic',
#[Out]#        'Salem', 'Somerset', 'Sussex', 'Union', 'Warren'], dtype=object)
all.columns
#[Out]# Index(['DWELLING UNIT ID', 'PERSON NUMBER', 'PERSON ID (DUID + PID)',
#[Out]#        'PANEL NUMBER', 'FAMILY ID (STUDENT MERGED IN) - R3/1',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - R4/2',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - R5/3',
#[Out]#        'FAMILY ID (STUDENT MERGED IN) - 12/31/13', 'ANNUAL FAMILY IDENTIFIER',
#[Out]#        'CPSFAMID', 
#[Out]#        ...
#[Out]#        'WEARS SEAT BELT (>15) - RD 5/3==5',
#[Out]#        'WEARS SEAT BELT (>15) - RD 5/3==4', 'SAQ: CURRENTLY SMOKE==2',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==1',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==2',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==3',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==6',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==4',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==5',
#[Out]#        'FULL YEAR INSURANCE COVERAGE STATUS 2013==7'],
#[Out]#       dtype='object', length=1837)
rf_office=health.rf_modeling?
rf_office=health.rf_modeling(train,office,"./exog_building_rf_model.txt",100,w)
rf_office.?
rf_office?
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
health.rmse?
error
error=health.rmse?
error=health.rmse(rf_office,test,office,"./exog_building_rf_model.txt")
health.rmse?
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
error=health.rmse(rf_office,test,office,"./exog_building_rf_model.txt")
error
#[Out]# 3295.521821044641
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
health.rmse(rf_office,test,office,"./exog_building_rf_model.txt")
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
health.rmse(rf_office,test,office,"./exog_building_rf_model.txt")
#[Out]# (3295.521821044641, 4157.0059357142136)
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
health.rmse(rf_office,test,office,"./exog_building_rf_model.txt")
#[Out]# ('rmse = 3295.52182104', 'stddev in data = 4157.00593571')
rf_office.feature_importances_
#[Out]# array([  3.30306877e-02,   6.96496451e-04,   6.27741491e-03,
#[Out]#          2.65943375e-02,   5.05749087e-02,   3.47162021e-01,
#[Out]#          3.28996470e-02,   1.10378770e-02,   5.15364075e-03,
#[Out]#          6.36035473e-03,   9.47583413e-05,   7.43091849e-03,
#[Out]#          1.61509493e-02,   1.76302443e-02,   1.66753504e-01,
#[Out]#          8.33856671e-03,   2.55822407e-02,   7.77572913e-03,
#[Out]#          5.42416591e-03,   8.02600792e-03,   2.43439404e-03,
#[Out]#          3.59578348e-03,   2.59470245e-03,   2.69643106e-03,
#[Out]#          1.76704550e-03,   1.16670973e-03,   1.02670326e-02,
#[Out]#          3.51764998e-03,   5.72380692e-04,   1.06865239e-02,
#[Out]#          3.81710580e-04,   8.54847299e-03,   4.30082961e-04,
#[Out]#          4.17156321e-03,   1.27588120e-03,   7.26667232e-03,
#[Out]#          4.87659208e-04,   3.91199838e-03,   1.13650842e-04,
#[Out]#          1.51892001e-03,   3.32477811e-04,   2.07271663e-03,
#[Out]#          8.89236973e-03,   3.74007997e-03,   7.77624936e-04,
#[Out]#          2.08221352e-03,   1.24969781e-03,   7.93612459e-04,
#[Out]#          5.70764309e-04,   4.65817994e-04,   1.22371006e-03,
#[Out]#          4.85739645e-03,   2.58058776e-03,   1.27539810e-03,
#[Out]#          1.05641392e-03,   6.71331632e-04,   1.11927938e-03,
#[Out]#          6.05036102e-03,   2.23271089e-03,   2.39505898e-03,
#[Out]#          2.32577215e-02,   5.02632491e-04,   2.59891370e-03,
#[Out]#          5.49448370e-04,   6.74557866e-04,   2.06306277e-03,
#[Out]#          9.71627689e-04,   4.05546907e-04,   8.59934875e-04,
#[Out]#          5.72055917e-04,   8.06748530e-04,   3.50028945e-04,
#[Out]#          4.82704955e-04,   1.20607521e-03,   1.37448013e-03,
#[Out]#          5.08516489e-03,   1.03655904e-03,   1.59204358e-03,
#[Out]#          1.38787509e-03,   1.39508532e-03,   1.26407698e-03,
#[Out]#          3.25927154e-03,   3.24146671e-03,   2.14196623e-03,
#[Out]#          9.20821068e-04,   1.20815483e-03,   2.37661218e-03,
#[Out]#          1.08745462e-03,   1.25244727e-03,   1.83836466e-03,
#[Out]#          3.00847048e-03,   1.44805527e-03,   1.58555579e-03,
#[Out]#          1.86806635e-03,   1.50116054e-03,   3.00566719e-03,
#[Out]#          1.81265869e-03,   9.28864520e-04,   7.91084190e-04,
#[Out]#          1.03343597e-03,   2.35840687e-03,   1.18690057e-03,
#[Out]#          6.88332729e-04,   1.88687076e-03,   4.41618918e-03,
#[Out]#          3.95312310e-03,   3.46586533e-03,   4.45902985e-03])
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
d={rf_office.feature_importances_[place]:exog[place] for place in len(exog)}
d={rf_office.feature_importances_[place]:exog[place] for place in range(len(exog))}
d={rf_office.feature_importances_[place]:exog[place] for place in range(len(exog))}
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
d={rf_office.feature_importances_[place]:exog.iloc[place] for place in range(len(exog))}
d
#[Out]# {0.3471620209261288: ADULT BODY MASS INDEX (>17) - RD 5/3    # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# Name: 5, dtype: object, 0.00057076430936153842: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - LYMPHOMA (>17)
#[Out]# Name: 48, dtype: object, 0.003911998380495941: ADULT BODY MASS INDEX (>17) - RD 5/3    EMPHYSEMA DIAGNOSIS (>17)
#[Out]# Name: 37, dtype: object, 0.0018868707582693218: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# Name: 103, dtype: object, 0.00046581799445500032: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - MELANOMA (>17)
#[Out]# Name: 49, dtype: object, 0.0039531231026602746: ADULT BODY MASS INDEX (>17) - RD 5/3    PERSON WEARS HEARING AID - RD 4/2
#[Out]# Name: 105, dtype: object, 0.0015855557904456172: ADULT BODY MASS INDEX (>17) - RD 5/3    HOUSEWORK LIMITATION - RD 3/1
#[Out]# Name: 92, dtype: object, 0.0012237100581762347: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - OTHER (>17)
#[Out]# Name: 50, dtype: object, 0.0014480552721406294: ADULT BODY MASS INDEX (>17) - RD 5/3    WORK LIMITATION - RD 3/1
#[Out]# Name: 91, dtype: object, 0.0037400799698705852: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - BLADDER (>17)
#[Out]# Name: 43, dtype: object, 0.0013744801297412913: ADULT BODY MASS INDEX (>17) - RD 5/3    PREGNANT DURING REF PERIOD - RD 3/1
#[Out]# Name: 74, dtype: object, 0.050574908651201629: ADULT BODY MASS INDEX (>17) - RD 5/3    # OFFICE-BASED PROVIDER VISITS 13
#[Out]# Name: 4, dtype: object, 0.00085993487469838482: ADULT BODY MASS INDEX (>17) - RD 5/3    NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3
#[Out]# Name: 68, dtype: object, 0.0012758811952273755: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-OTHER HEART DISEASE
#[Out]# Name: 34, dtype: object, 0.023257721529462604: ADULT BODY MASS INDEX (>17) - RD 5/3    ASTHMA DIAGNOSIS
#[Out]# Name: 60, dtype: object, 0.010267032639699476: ADULT BODY MASS INDEX (>17) - RD 5/3    MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# Name: 26, dtype: object, 0.00057238069179789267: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# Name: 28, dtype: object, 0.00097162768910875777: ADULT BODY MASS INDEX (>17) - RD 5/3    USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3
#[Out]# Name: 66, dtype: object, 0.0010365590395975529: ADULT BODY MASS INDEX (>17) - RD 5/3    IADL HELP 3+ MONTHS - RD 3/1
#[Out]# Name: 76, dtype: object, 0.0011869005700702179: ADULT BODY MASS INDEX (>17) - RD 5/3    SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# Name: 101, dtype: object, 0.007775729131083502: ADULT BODY MASS INDEX (>17) - RD 5/3    MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# Name: 17, dtype: object, 0.0060503610196654019: ADULT BODY MASS INDEX (>17) - RD 5/3    JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1
#[Out]# Name: 57, dtype: object, 0.0012640769835168453: ADULT BODY MASS INDEX (>17) - RD 5/3    LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# Name: 80, dtype: object, 0.0020727166342049612: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-HIGH CHOLESTEROL
#[Out]# Name: 41, dtype: object, 0.00035002894495563702: ADULT BODY MASS INDEX (>17) - RD 5/3    WHEN LAST USED PEAK FLOW METER - RD 5/3
#[Out]# Name: 71, dtype: object, 0.0026964310610129378: ADULT BODY MASS INDEX (>17) - RD 5/3    YEARS PERSON LIVED IN THE US
#[Out]# Name: 23, dtype: object, 0.0044161891781093979: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# Name: 104, dtype: object, 0.00067455786572936461: ADULT BODY MASS INDEX (>17) - RD 5/3    WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1
#[Out]# Name: 64, dtype: object, 0.00105641391834468: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - UTERUS (>17)
#[Out]# Name: 54, dtype: object, 0.0015011605441199976: ADULT BODY MASS INDEX (>17) - RD 5/3    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# Name: 94, dtype: object, 0.0035957834849969224: ADULT BODY MASS INDEX (>17) - RD 5/3    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# Name: 21, dtype: object, 0.00092082106759974758: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# Name: 84, dtype: object, 0.0025947024508306275: ADULT BODY MASS INDEX (>17) - RD 5/3    PERSON BORN IN THE US
#[Out]# Name: 22, dtype: object, 0.00080674852982930189: ADULT BODY MASS INDEX (>17) - RD 5/3    EVER USED PEAK FLOW METER - RD 5/3
#[Out]# Name: 70, dtype: object, 0.001275398101429324: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)
#[Out]# Name: 53, dtype: object, 0.00048270495473304802: ADULT BODY MASS INDEX (>17) - RD 5/3    ADHDADD DIAGNOSIS (5-17)
#[Out]# Name: 72, dtype: object, 0.032899647016282431: ADULT BODY MASS INDEX (>17) - RD 5/3    # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# Name: 6, dtype: object, 0.00077762493582301422: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - BREAST (>17)
#[Out]# Name: 44, dtype: object, 0.002232710890408697: ADULT BODY MASS INDEX (>17) - RD 5/3    ARTHRITIS DIAGNOSIS (>17)
#[Out]# Name: 58, dtype: object, 0.0009288645204627385: ADULT BODY MASS INDEX (>17) - RD 5/3    SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# Name: 97, dtype: object, 0.0013950853193475483: ADULT BODY MASS INDEX (>17) - RD 5/3    USED ASSISTIVE DEVICES - RD 3/1
#[Out]# Name: 79, dtype: object, 0.0012496978075250804: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - COLON (>17)
#[Out]# Name: 46, dtype: object, 0.0005026324910203788: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-ASTHMA
#[Out]# Name: 61, dtype: object, 9.4758341343245176e-05: ADULT BODY MASS INDEX (>17) - RD 5/3    # HOSPITAL DISCHARGES 2013
#[Out]# Name: 10, dtype: object, 0.0034658653331003258: ADULT BODY MASS INDEX (>17) - RD 5/3    WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# Name: 106, dtype: object, 0.025582240691221322: ADULT BODY MASS INDEX (>17) - RD 5/3    RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# Name: 16, dtype: object, 0.033030687736769933: ADULT BODY MASS INDEX (>17) - RD 5/3    NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# Name: 0, dtype: object, 0.0020822135194508581: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - CERVIX (>17)
#[Out]# Name: 45, dtype: object, 0.00054944837013530708: ADULT BODY MASS INDEX (>17) - RD 5/3    ASTHMA ATTACK LAST 12 MOS - RD3/1
#[Out]# Name: 63, dtype: object, 0.00033247781060767806: ADULT BODY MASS INDEX (>17) - RD 5/3    HIGH CHOLESTEROL DIAGNOSIS (>17)
#[Out]# Name: 40, dtype: object, 0.0011192793801382721: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-DIABETES
#[Out]# Name: 56, dtype: object, 0.00040554690689092023: ADULT BODY MASS INDEX (>17) - RD 5/3    EVER USED PREV DAILY ASTHMA MEDS -RD 5/3
#[Out]# Name: 67, dtype: object, 0.0032592715350888072: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# Name: 81, dtype: object, 0.00068833272856688135: ADULT BODY MASS INDEX (>17) - RD 5/3    SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# Name: 102, dtype: object, 0.00069649645114383085: ADULT BODY MASS INDEX (>17) - RD 5/3    MONTHLY VALUE OF FOOD STAMPS
#[Out]# Name: 1, dtype: object, 0.0015920435837667958: ADULT BODY MASS INDEX (>17) - RD 5/3    ADL SCREENER - RD 3/1
#[Out]# Name: 77, dtype: object, 0.0023766121790393352: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# Name: 86, dtype: object, 0.0021419662293700702: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# Name: 83, dtype: object, 0.00079361245928815955: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - LUNG (>17)
#[Out]# Name: 47, dtype: object, 0.0023584068738974062: ADULT BODY MASS INDEX (>17) - RD 5/3    PERSON IS BLIND - RD 4/2
#[Out]# Name: 100, dtype: object, 0.0074309184900819836: ADULT BODY MASS INDEX (>17) - RD 5/3    # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# Name: 11, dtype: object, 0.00011365084210032706: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-EMPHYSEMA
#[Out]# Name: 38, dtype: object, 0.0088923697297333421: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSIS (>17)
#[Out]# Name: 42, dtype: object, 0.0080260079217410775: ADULT BODY MASS INDEX (>17) - RD 5/3    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# Name: 19, dtype: object, 0.16675350361943775: ADULT BODY MASS INDEX (>17) - RD 5/3    CENSUS REGION AS OF 12/31/13
#[Out]# Name: 14, dtype: object, 0.026594337532053043: ADULT BODY MASS INDEX (>17) - RD 5/3    FAMILY'S TOTAL INCOME
#[Out]# Name: 3, dtype: object, 0.0004876592082255299: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-STROKE
#[Out]# Name: 36, dtype: object, 0.00043008296090103271: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-HEART ATTACK(MI)
#[Out]# Name: 32, dtype: object, 0.0013878750891699973: ADULT BODY MASS INDEX (>17) - RD 5/3    ADL HELP 3+ MONTHS - RD 3/1
#[Out]# Name: 78, dtype: object, 0.0015189200078403289: ADULT BODY MASS INDEX (>17) - RD 5/3    CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1
#[Out]# Name: 39, dtype: object, 0.0017670455037161576: ADULT BODY MASS INDEX (>17) - RD 5/3    HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# Name: 24, dtype: object, 0.0012524472651287161: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# Name: 88, dtype: object, 0.003241466709865427: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# Name: 82, dtype: object, 0.00300566718784996: ADULT BODY MASS INDEX (>17) - RD 5/3    SOCIAL LIMITATIONS - RD 3/1
#[Out]# Name: 95, dtype: object, 0.001838364657371485: ADULT BODY MASS INDEX (>17) - RD 5/3    PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# Name: 89, dtype: object, 0.00067133163227591126: ADULT BODY MASS INDEX (>17) - RD 5/3    DIABETES DIAGNOSIS (>17)
#[Out]# Name: 55, dtype: object, 0.0020630627749840991: ADULT BODY MASS INDEX (>17) - RD 5/3    USED ACUTE PRES INHALER LAST 3 MOS-RD5/3
#[Out]# Name: 65, dtype: object, 0.017630244335874755: ADULT BODY MASS INDEX (>17) - RD 5/3    # HOME HEALTH PROVIDER DAYS 2013
#[Out]# Name: 13, dtype: object, 0.0018680663549180629: ADULT BODY MASS INDEX (>17) - RD 5/3    SCHOOL LIMITATION - RD 3/1
#[Out]# Name: 93, dtype: object, 0.00038171057960275859: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-ANGINA
#[Out]# Name: 30, dtype: object, 0.0054241659100721044: ADULT BODY MASS INDEX (>17) - RD 5/3    EDUCATION RECODE (EDITED)
#[Out]# Name: 18, dtype: object, 0.005153640745768598: ADULT BODY MASS INDEX (>17) - RD 5/3    # EMERGENCY ROOM VISITS 13
#[Out]# Name: 8, dtype: object, 0.0035176499827192786: ADULT BODY MASS INDEX (>17) - RD 5/3    CORONARY HRT DISEASE DIAG (>17)
#[Out]# Name: 27, dtype: object, 0.0025989137041525241: ADULT BODY MASS INDEX (>17) - RD 5/3    DOES PERSON STILL HAVE ASTHMA-RD3/1
#[Out]# Name: 62, dtype: object, 0.0030084704790707085: ADULT BODY MASS INDEX (>17) - RD 5/3    ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# Name: 90, dtype: object, 0.0062774149062354316: ADULT BODY MASS INDEX (>17) - RD 5/3    PERSON'S TOTAL INCOME
#[Out]# Name: 2, dtype: object, 0.001812658688018363: ADULT BODY MASS INDEX (>17) - RD 5/3    COGNITIVE LIMITATIONS - RD 3/1
#[Out]# Name: 96, dtype: object, 0.001206075205359203: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-ADHD/ADD
#[Out]# Name: 73, dtype: object, 0.0048573964494600178: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - PROSTATE (>17)
#[Out]# Name: 51, dtype: object, 0.0023950589844016035: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-ARTHRITIS
#[Out]# Name: 59, dtype: object, 0.010686523924629415: ADULT BODY MASS INDEX (>17) - RD 5/3    ANGINA DIAGNOSIS (>17)
#[Out]# Name: 29, dtype: object, 0.0025805877559059555: ADULT BODY MASS INDEX (>17) - RD 5/3    CANCER DIAGNOSED - SKIN-NONMELANO (>17)
#[Out]# Name: 52, dtype: object, 0.0012081548329251192: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# Name: 85, dtype: object, 0.016150949264163925: ADULT BODY MASS INDEX (>17) - RD 5/3    # DENTAL CARE VISITS 13
#[Out]# Name: 12, dtype: object, 0.011037877032044265: ADULT BODY MASS INDEX (>17) - RD 5/3    # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# Name: 7, dtype: object, 0.0050851648944117856: ADULT BODY MASS INDEX (>17) - RD 5/3    IADL SCREENER - RD 3/1
#[Out]# Name: 75, dtype: object, 0.0010334359661009367: ADULT BODY MASS INDEX (>17) - RD 5/3    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# Name: 99, dtype: object, 0.0041715632136641754: ADULT BODY MASS INDEX (>17) - RD 5/3    OTHER HEART DISEASE DIAG (>17)
#[Out]# Name: 33, dtype: object, 0.0010874546197405695: ADULT BODY MASS INDEX (>17) - RD 5/3    DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# Name: 87, dtype: object, 0.0085484729915109563: ADULT BODY MASS INDEX (>17) - RD 5/3    HEART ATTACK (MI) DIAG (>17)
#[Out]# Name: 31, dtype: object, 0.00057205591689190303: ADULT BODY MASS INDEX (>17) - RD 5/3    HAVE PEAK FLOW METER AT HOME - RD 5/3
#[Out]# Name: 69, dtype: object, 0.0011667097313107144: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# Name: 25, dtype: object, 0.00079108418979838291: ADULT BODY MASS INDEX (>17) - RD 5/3    PERSON IS DEAF - RD 4/2
#[Out]# Name: 98, dtype: object, 0.0083385667098019259: ADULT BODY MASS INDEX (>17) - RD 5/3    AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# Name: 15, dtype: object, 0.0072666723248089272: ADULT BODY MASS INDEX (>17) - RD 5/3    STROKE DIAGNOSIS (>17)
#[Out]# Name: 35, dtype: object, 0.006360354726926528: ADULT BODY MASS INDEX (>17) - RD 5/3    # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# Name: 9, dtype: object, 0.0024343940420483694: ADULT BODY MASS INDEX (>17) - RD 5/3    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# Name: 20, dtype: object}
exogs
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
candidates=list(exog)
candidates
#[Out]# ['ADULT BODY MASS INDEX (>17) - RD 5/3']
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
exog.head()
#[Out]#      ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0  NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1            MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                   PERSON'S TOTAL INCOME
#[Out]# 3                   FAMILY'S TOTAL INCOME
#[Out]# 4       # OFFICE-BASED PROVIDER VISITS 13
(all, train, test, validate, endog, office, outpatient, inpatient, er, w, exog, insurance)=health.prep_for_modeling()
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
exog.columns
#[Out]# Index(['ADULT BODY MASS INDEX (>17) - RD 5/3'], dtype='object')
(all, train, test, validate, endog, office, outpatient, inpatient, er, w, exog, insurance)=health.prep_for_modeling()
exog
#[Out]#          ADULT BODY MASS INDEX (>17) - RD 5/3
#[Out]# 0      NUMBER OF MONTHS FOOD STAMPS PURCHASED
#[Out]# 1                MONTHLY VALUE OF FOOD STAMPS
#[Out]# 2                       PERSON'S TOTAL INCOME
#[Out]# 3                       FAMILY'S TOTAL INCOME
#[Out]# 4           # OFFICE-BASED PROVIDER VISITS 13
#[Out]# 5          # OFFICE-BASED PHYSICIAN VISITS 13
#[Out]# 6        # OUTPATIENT DEPT PROVIDER VISITS 13
#[Out]# 7       # OUTPATIENT DEPT PHYSICIAN VISITS 13
#[Out]# 8                  # EMERGENCY ROOM VISITS 13
#[Out]# 9              # ZERO-NIGHT HOSPITAL STAYS 13
#[Out]# 10                 # HOSPITAL DISCHARGES 2013
#[Out]# 11       # NIGHTS IN HOSP FOR DISCHARGES 2013
#[Out]# 12                    # DENTAL CARE VISITS 13
#[Out]# 13           # HOME HEALTH PROVIDER DAYS 2013
#[Out]# 14               CENSUS REGION AS OF 12/31/13
#[Out]# 15        AGE AS OF 12/31/13 (EDITED/IMPUTED)
#[Out]# 16            RACE/ETHNICITY (EDITED/IMPUTED)
#[Out]# 17   MARITAL STATUS-12/31/13 (EDITED/IMPUTED)
#[Out]# 18                  EDUCATION RECODE (EDITED)
#[Out]# 19    STUDENT STATUS IF AGES 17-23 - 12/31/13
#[Out]# 20    IN FAMILY WITH SOMEONE SPKNG OTHER LANG
#[Out]# 21    LANGUAGE SPOKEN AT HOME OTHER THAN ENGL
#[Out]# 22                      PERSON BORN IN THE US
#[Out]# 23               YEARS PERSON LIVED IN THE US
#[Out]# 24             HIGH BLOOD PRESSURE DIAG (>17)
#[Out]# 25       AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE
#[Out]# 26           MULT DIAG HIGH BLOOD PRESS (>17)
#[Out]# 27            CORONARY HRT DISEASE DIAG (>17)
#[Out]# 28    AGE OF DIAGNOSIS-CORONARY HEART DISEASE
#[Out]# 29                     ANGINA DIAGNOSIS (>17)
#[Out]# ..                                        ...
#[Out]# 77                      ADL SCREENER - RD 3/1
#[Out]# 78                ADL HELP 3+ MONTHS - RD 3/1
#[Out]# 79            USED ASSISTIVE DEVICES - RD 3/1
#[Out]# 80   LIMITATION IN PHYSICAL FUNCTIONING-RD3/1
#[Out]# 81      DIFFICULTY LIFTING 10 POUNDS - RD 3/1
#[Out]# 82    DIFFICULTY WALKING UP 10 STEPS - RD 3/1
#[Out]# 83       DIFFICULTY WALKING 3 BLOCKS - RD 3/1
#[Out]# 84         DIFFICULTY WALKING A MILE - RD 3/1
#[Out]# 85    DIFFICULTY STANDING 20 MINUTES - RD 3/1
#[Out]# 86       DIFFICULTY BENDING/STOOPING - RD 3/1
#[Out]# 87      DIFFICULTY REACHING OVERHEAD - RD 3/1
#[Out]# 88   DIFFICULTY USING FINGERS TO GRASP-RD 3/1
#[Out]# 89   PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1
#[Out]# 90   ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1
#[Out]# 91                   WORK LIMITATION - RD 3/1
#[Out]# 92              HOUSEWORK LIMITATION - RD 3/1
#[Out]# 93                 SCHOOL LIMITATION - RD 3/1
#[Out]# 94    COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1
#[Out]# 95                SOCIAL LIMITATIONS - RD 3/1
#[Out]# 96             COGNITIVE LIMITATIONS - RD 3/1
#[Out]# 97          SERIOUS DIFFICULTY HEARING-RD 4/2
#[Out]# 98                    PERSON IS DEAF - RD 4/2
#[Out]# 99    SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2
#[Out]# 100                  PERSON IS BLIND - RD 4/2
#[Out]# 101     SERIOUS COGNITIVE DIFFICULTIES-RD 4/2
#[Out]# 102  SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2
#[Out]# 103        DIFFICULTY DRESSING/BATHING-RD 4/2
#[Out]# 104     DIFFICULTY DOING ERRANDS ALONE-RD 4/2
#[Out]# 105         PERSON WEARS HEARING AID - RD 4/2
#[Out]# 106     WEARS EYEGLASSES OR CONTACTS - RD 4/2
#[Out]# 
#[Out]# [107 rows x 1 columns]
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
(all, train, test, validate, endog, office, outpatient, inpatient, er, w, exog, insurance)=health.prep_for_modeling()
exog
#[Out]# ['ADULT BODY MASS INDEX (>17) - RD 5/3', 'NUMBER OF MONTHS FOOD STAMPS PURCHASED', 'MONTHLY VALUE OF FOOD STAMPS', "PERSON'S TOTAL INCOME", "FAMILY'S TOTAL INCOME", '# OFFICE-BASED PROVIDER VISITS 13', '# OFFICE-BASED PHYSICIAN VISITS 13', '# OUTPATIENT DEPT PROVIDER VISITS 13', '# OUTPATIENT DEPT PHYSICIAN VISITS 13', '# EMERGENCY ROOM VISITS 13', '# ZERO-NIGHT HOSPITAL STAYS 13', '# HOSPITAL DISCHARGES 2013', '# NIGHTS IN HOSP FOR DISCHARGES 2013', '# DENTAL CARE VISITS 13', '# HOME HEALTH PROVIDER DAYS 2013', 'CENSUS REGION AS OF 12/31/13', 'AGE AS OF 12/31/13 (EDITED/IMPUTED)', 'RACE/ETHNICITY (EDITED/IMPUTED)', 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)', 'EDUCATION RECODE (EDITED)', 'STUDENT STATUS IF AGES 17-23 - 12/31/13', 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG', 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL', 'PERSON BORN IN THE US', 'YEARS PERSON LIVED IN THE US', 'HIGH BLOOD PRESSURE DIAG (>17)', 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE', 'MULT DIAG HIGH BLOOD PRESS (>17)', 'CORONARY HRT DISEASE DIAG (>17)', 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE', 'ANGINA DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-ANGINA', 'HEART ATTACK (MI) DIAG (>17)', 'AGE OF DIAGNOSIS-HEART ATTACK(MI)', 'OTHER HEART DISEASE DIAG (>17)', 'AGE OF DIAGNOSIS-OTHER HEART DISEASE', 'STROKE DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-STROKE', 'EMPHYSEMA DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-EMPHYSEMA', 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1', 'HIGH CHOLESTEROL DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL', 'CANCER DIAGNOSIS (>17)', 'CANCER DIAGNOSED - BLADDER (>17)', 'CANCER DIAGNOSED - BREAST (>17)', 'CANCER DIAGNOSED - CERVIX (>17)', 'CANCER DIAGNOSED - COLON (>17)', 'CANCER DIAGNOSED - LUNG (>17)', 'CANCER DIAGNOSED - LYMPHOMA (>17)', 'CANCER DIAGNOSED - MELANOMA (>17)', 'CANCER DIAGNOSED - OTHER (>17)', 'CANCER DIAGNOSED - PROSTATE (>17)', 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)', 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)', 'CANCER DIAGNOSED - UTERUS (>17)', 'DIABETES DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-DIABETES', 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1', 'ARTHRITIS DIAGNOSIS (>17)', 'AGE OF DIAGNOSIS-ARTHRITIS', 'ASTHMA DIAGNOSIS', 'AGE OF DIAGNOSIS-ASTHMA', 'DOES PERSON STILL HAVE ASTHMA-RD3/1', 'ASTHMA ATTACK LAST 12 MOS - RD3/1', 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1', 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3', 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3', 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3', 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3', 'HAVE PEAK FLOW METER AT HOME - RD 5/3', 'EVER USED PEAK FLOW METER - RD 5/3', 'WHEN LAST USED PEAK FLOW METER - RD 5/3', 'ADHDADD DIAGNOSIS (5-17)', 'AGE OF DIAGNOSIS-ADHD/ADD', 'PREGNANT DURING REF PERIOD - RD 3/1', 'IADL SCREENER - RD 3/1', 'IADL HELP 3+ MONTHS - RD 3/1', 'ADL SCREENER - RD 3/1', 'ADL HELP 3+ MONTHS - RD 3/1', 'USED ASSISTIVE DEVICES - RD 3/1', 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1', 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1', 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1', 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1', 'DIFFICULTY WALKING A MILE - RD 3/1', 'DIFFICULTY STANDING 20 MINUTES - RD 3/1', 'DIFFICULTY BENDING/STOOPING - RD 3/1', 'DIFFICULTY REACHING OVERHEAD - RD 3/1', 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1', 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1', 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1', 'WORK LIMITATION - RD 3/1', 'HOUSEWORK LIMITATION - RD 3/1', 'SCHOOL LIMITATION - RD 3/1', 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1', 'SOCIAL LIMITATIONS - RD 3/1', 'COGNITIVE LIMITATIONS - RD 3/1', 'SERIOUS DIFFICULTY HEARING-RD 4/2', 'PERSON IS DEAF - RD 4/2', 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2', 'PERSON IS BLIND - RD 4/2', 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2', 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2', 'DIFFICULTY DRESSING/BATHING-RD 4/2', 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2', 'PERSON WEARS HEARING AID - RD 4/2', 'WEARS EYEGLASSES OR CONTACTS - RD 4/2']
d={rf_office.feature_importances_[place]:exog[place] for place in range(len(exog))}
d
#[Out]# {0.3471620209261288: '# OFFICE-BASED PROVIDER VISITS 13', 0.00057076430936153842: 'CANCER DIAGNOSED - LUNG (>17)', 0.003911998380495941: 'AGE OF DIAGNOSIS-STROKE', 0.0018868707582693218: 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2', 0.00046581799445500032: 'CANCER DIAGNOSED - LYMPHOMA (>17)', 0.0039531231026602746: 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2', 0.0015855557904456172: 'WORK LIMITATION - RD 3/1', 0.0012237100581762347: 'CANCER DIAGNOSED - MELANOMA (>17)', 0.0014480552721406294: 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1', 0.0037400799698705852: 'CANCER DIAGNOSIS (>17)', 0.0013744801297412913: 'AGE OF DIAGNOSIS-ADHD/ADD', 0.050574908651201629: "FAMILY'S TOTAL INCOME", 0.00085993487469838482: 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3', 0.0012758811952273755: 'OTHER HEART DISEASE DIAG (>17)', 0.023257721529462604: 'AGE OF DIAGNOSIS-ARTHRITIS', 0.010267032639699476: 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE', 0.00057238069179789267: 'CORONARY HRT DISEASE DIAG (>17)', 0.00097162768910875777: 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3', 0.0010365590395975529: 'IADL SCREENER - RD 3/1', 0.0011869005700702179: 'PERSON IS BLIND - RD 4/2', 0.007775729131083502: 'RACE/ETHNICITY (EDITED/IMPUTED)', 0.0060503610196654019: 'AGE OF DIAGNOSIS-DIABETES', 0.0012640769835168453: 'USED ASSISTIVE DEVICES - RD 3/1', 0.0020727166342049612: 'HIGH CHOLESTEROL DIAGNOSIS (>17)', 0.00035002894495563702: 'EVER USED PEAK FLOW METER - RD 5/3', 0.0026964310610129378: 'PERSON BORN IN THE US', 0.0044161891781093979: 'DIFFICULTY DRESSING/BATHING-RD 4/2', 0.00067455786572936461: 'ASTHMA ATTACK LAST 12 MOS - RD3/1', 0.00105641391834468: 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)', 0.0015011605441199976: 'SCHOOL LIMITATION - RD 3/1', 0.0035957834849969224: 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG', 0.00092082106759974758: 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1', 0.0025947024508306275: 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL', 0.00080674852982930189: 'HAVE PEAK FLOW METER AT HOME - RD 5/3', 0.001275398101429324: 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)', 0.00048270495473304802: 'WHEN LAST USED PEAK FLOW METER - RD 5/3', 0.032899647016282431: '# OFFICE-BASED PHYSICIAN VISITS 13', 0.00077762493582301422: 'CANCER DIAGNOSED - BLADDER (>17)', 0.002232710890408697: 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1', 0.0009288645204627385: 'COGNITIVE LIMITATIONS - RD 3/1', 0.0013950853193475483: 'ADL HELP 3+ MONTHS - RD 3/1', 0.0012496978075250804: 'CANCER DIAGNOSED - CERVIX (>17)', 0.0005026324910203788: 'ASTHMA DIAGNOSIS', 9.4758341343245176e-05: '# ZERO-NIGHT HOSPITAL STAYS 13', 0.0034658653331003258: 'PERSON WEARS HEARING AID - RD 4/2', 0.025582240691221322: 'AGE AS OF 12/31/13 (EDITED/IMPUTED)', 0.033030687736769933: 'ADULT BODY MASS INDEX (>17) - RD 5/3', 0.0020822135194508581: 'CANCER DIAGNOSED - BREAST (>17)', 0.00054944837013530708: 'DOES PERSON STILL HAVE ASTHMA-RD3/1', 0.00033247781060767806: 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1', 0.0011192793801382721: 'DIABETES DIAGNOSIS (>17)', 0.00040554690689092023: 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3', 0.0032592715350888072: 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1', 0.00068833272856688135: 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2', 0.00069649645114383085: 'NUMBER OF MONTHS FOOD STAMPS PURCHASED', 0.0015920435837667958: 'IADL HELP 3+ MONTHS - RD 3/1', 0.0023766121790393352: 'DIFFICULTY STANDING 20 MINUTES - RD 3/1', 0.0021419662293700702: 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1', 0.00079361245928815955: 'CANCER DIAGNOSED - COLON (>17)', 0.0023584068738974062: 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2', 0.0074309184900819836: '# HOSPITAL DISCHARGES 2013', 0.00011365084210032706: 'EMPHYSEMA DIAGNOSIS (>17)', 0.0088923697297333421: 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL', 0.0080260079217410775: 'EDUCATION RECODE (EDITED)', 0.16675350361943775: '# HOME HEALTH PROVIDER DAYS 2013', 0.026594337532053043: "PERSON'S TOTAL INCOME", 0.0004876592082255299: 'STROKE DIAGNOSIS (>17)', 0.00043008296090103271: 'HEART ATTACK (MI) DIAG (>17)', 0.0013878750891699973: 'ADL SCREENER - RD 3/1', 0.0015189200078403289: 'AGE OF DIAGNOSIS-EMPHYSEMA', 0.0017670455037161576: 'YEARS PERSON LIVED IN THE US', 0.0012524472651287161: 'DIFFICULTY REACHING OVERHEAD - RD 3/1', 0.003241466709865427: 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1', 0.00300566718784996: 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1', 0.001838364657371485: 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1', 0.00067133163227591126: 'CANCER DIAGNOSED - UTERUS (>17)', 0.0020630627749840991: 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1', 0.017630244335874755: '# DENTAL CARE VISITS 13', 0.0018680663549180629: 'HOUSEWORK LIMITATION - RD 3/1', 0.00038171057960275859: 'ANGINA DIAGNOSIS (>17)', 0.0054241659100721044: 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)', 0.005153640745768598: '# OUTPATIENT DEPT PHYSICIAN VISITS 13', 0.0035176499827192786: 'MULT DIAG HIGH BLOOD PRESS (>17)', 0.0025989137041525241: 'AGE OF DIAGNOSIS-ASTHMA', 0.0030084704790707085: 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1', 0.0062774149062354316: 'MONTHLY VALUE OF FOOD STAMPS', 0.001812658688018363: 'SOCIAL LIMITATIONS - RD 3/1', 0.001206075205359203: 'ADHDADD DIAGNOSIS (5-17)', 0.0048573964494600178: 'CANCER DIAGNOSED - OTHER (>17)', 0.0023950589844016035: 'ARTHRITIS DIAGNOSIS (>17)', 0.010686523924629415: 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE', 0.0025805877559059555: 'CANCER DIAGNOSED - PROSTATE (>17)', 0.0012081548329251192: 'DIFFICULTY WALKING A MILE - RD 3/1', 0.0044590298517266222: 'WEARS EYEGLASSES OR CONTACTS - RD 4/2', 0.016150949264163925: '# NIGHTS IN HOSP FOR DISCHARGES 2013', 0.011037877032044265: '# OUTPATIENT DEPT PROVIDER VISITS 13', 0.0050851648944117856: 'PREGNANT DURING REF PERIOD - RD 3/1', 0.0010334359661009367: 'PERSON IS DEAF - RD 4/2', 0.0041715632136641754: 'AGE OF DIAGNOSIS-HEART ATTACK(MI)', 0.0010874546197405695: 'DIFFICULTY BENDING/STOOPING - RD 3/1', 0.0085484729915109563: 'AGE OF DIAGNOSIS-ANGINA', 0.00057205591689190303: 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3', 0.0011667097313107144: 'HIGH BLOOD PRESSURE DIAG (>17)', 0.00079108418979838291: 'SERIOUS DIFFICULTY HEARING-RD 4/2', 0.0083385667098019259: 'CENSUS REGION AS OF 12/31/13', 0.0072666723248089272: 'AGE OF DIAGNOSIS-OTHER HEART DISEASE', 0.006360354726926528: '# EMERGENCY ROOM VISITS 13', 0.0024343940420483694: 'STUDENT STATUS IF AGES 17-23 - 12/31/13'}
d.sort()
sorted(d)
#[Out]# [9.4758341343245176e-05, 0.00011365084210032706, 0.00033247781060767806, 0.00035002894495563702, 0.00038171057960275859, 0.00040554690689092023, 0.00043008296090103271, 0.00046581799445500032, 0.00048270495473304802, 0.0004876592082255299, 0.0005026324910203788, 0.00054944837013530708, 0.00057076430936153842, 0.00057205591689190303, 0.00057238069179789267, 0.00067133163227591126, 0.00067455786572936461, 0.00068833272856688135, 0.00069649645114383085, 0.00077762493582301422, 0.00079108418979838291, 0.00079361245928815955, 0.00080674852982930189, 0.00085993487469838482, 0.00092082106759974758, 0.0009288645204627385, 0.00097162768910875777, 0.0010334359661009367, 0.0010365590395975529, 0.00105641391834468, 0.0010874546197405695, 0.0011192793801382721, 0.0011667097313107144, 0.0011869005700702179, 0.001206075205359203, 0.0012081548329251192, 0.0012237100581762347, 0.0012496978075250804, 0.0012524472651287161, 0.0012640769835168453, 0.001275398101429324, 0.0012758811952273755, 0.0013744801297412913, 0.0013878750891699973, 0.0013950853193475483, 0.0014480552721406294, 0.0015011605441199976, 0.0015189200078403289, 0.0015855557904456172, 0.0015920435837667958, 0.0017670455037161576, 0.001812658688018363, 0.001838364657371485, 0.0018680663549180629, 0.0018868707582693218, 0.0020630627749840991, 0.0020727166342049612, 0.0020822135194508581, 0.0021419662293700702, 0.002232710890408697, 0.0023584068738974062, 0.0023766121790393352, 0.0023950589844016035, 0.0024343940420483694, 0.0025805877559059555, 0.0025947024508306275, 0.0025989137041525241, 0.0026964310610129378, 0.00300566718784996, 0.0030084704790707085, 0.003241466709865427, 0.0032592715350888072, 0.0034658653331003258, 0.0035176499827192786, 0.0035957834849969224, 0.0037400799698705852, 0.003911998380495941, 0.0039531231026602746, 0.0041715632136641754, 0.0044161891781093979, 0.0044590298517266222, 0.0048573964494600178, 0.0050851648944117856, 0.005153640745768598, 0.0054241659100721044, 0.0060503610196654019, 0.0062774149062354316, 0.006360354726926528, 0.0072666723248089272, 0.0074309184900819836, 0.007775729131083502, 0.0080260079217410775, 0.0083385667098019259, 0.0085484729915109563, 0.0088923697297333421, 0.010267032639699476, 0.010686523924629415, 0.011037877032044265, 0.016150949264163925, 0.017630244335874755, 0.023257721529462604, 0.025582240691221322, 0.026594337532053043, 0.032899647016282431, 0.033030687736769933, 0.050574908651201629, 0.16675350361943775, 0.3471620209261288]
sorted(d)[:10]
#[Out]# [9.4758341343245176e-05, 0.00011365084210032706, 0.00033247781060767806, 0.00035002894495563702, 0.00038171057960275859, 0.00040554690689092023, 0.00043008296090103271, 0.00046581799445500032, 0.00048270495473304802, 0.0004876592082255299]
d=[(rf_office.feature_importances_[place]:exog[place]) for place in range(len(exog))]
d=[(rf_office.feature_importances_[place],exog[place]) for place in range(len(exog))]
d
#[Out]# [(9.4758341343245176e-05, '# ZERO-NIGHT HOSPITAL STAYS 13'), (0.00011365084210032706, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00033247781060767806, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00035002894495563702, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00038171057960275859, 'ANGINA DIAGNOSIS (>17)'), (0.00040554690689092023, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00043008296090103271, 'HEART ATTACK (MI) DIAG (>17)'), (0.00046581799445500032, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.00048270495473304802, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.0004876592082255299, 'STROKE DIAGNOSIS (>17)'), (0.0005026324910203788, 'ASTHMA DIAGNOSIS'), (0.00054944837013530708, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00057076430936153842, 'CANCER DIAGNOSED - LUNG (>17)'), (0.00057205591689190303, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00057238069179789267, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00067133163227591126, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00067455786572936461, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00068833272856688135, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.00069649645114383085, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00077762493582301422, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00079108418979838291, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00079361245928815955, 'CANCER DIAGNOSED - COLON (>17)'), (0.00080674852982930189, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.00085993487469838482, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00092082106759974758, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0009288645204627385, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.00097162768910875777, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.0010334359661009367, 'PERSON IS DEAF - RD 4/2'), (0.0010365590395975529, 'IADL SCREENER - RD 3/1'), (0.00105641391834468, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0010874546197405695, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011192793801382721, 'DIABETES DIAGNOSIS (>17)'), (0.0011667097313107144, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0011869005700702179, 'PERSON IS BLIND - RD 4/2'), (0.001206075205359203, 'ADHDADD DIAGNOSIS (5-17)'), (0.0012081548329251192, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0012237100581762347, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.0012496978075250804, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0012524472651287161, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0012640769835168453, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.001275398101429324, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0012758811952273755, 'OTHER HEART DISEASE DIAG (>17)'), (0.0013744801297412913, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0013878750891699973, 'ADL SCREENER - RD 3/1'), (0.0013950853193475483, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0014480552721406294, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.0015011605441199976, 'SCHOOL LIMITATION - RD 3/1'), (0.0015189200078403289, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0015855557904456172, 'WORK LIMITATION - RD 3/1'), (0.0015920435837667958, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0017670455037161576, 'YEARS PERSON LIVED IN THE US'), (0.001812658688018363, 'SOCIAL LIMITATIONS - RD 3/1'), (0.001838364657371485, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0018680663549180629, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0018868707582693218, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0020630627749840991, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0020727166342049612, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0020822135194508581, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0021419662293700702, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.002232710890408697, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.0023584068738974062, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0023766121790393352, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0023950589844016035, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0024343940420483694, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0025805877559059555, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0025947024508306275, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0025989137041525241, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0026964310610129378, 'PERSON BORN IN THE US'), (0.00300566718784996, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0030084704790707085, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.003241466709865427, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0032592715350888072, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.0034658653331003258, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0035176499827192786, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0035957834849969224, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0037400799698705852, 'CANCER DIAGNOSIS (>17)'), (0.003911998380495941, 'AGE OF DIAGNOSIS-STROKE'), (0.0039531231026602746, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0041715632136641754, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0044161891781093979, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0044590298517266222, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0048573964494600178, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0050851648944117856, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.005153640745768598, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0054241659100721044, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0060503610196654019, 'AGE OF DIAGNOSIS-DIABETES'), (0.0062774149062354316, 'MONTHLY VALUE OF FOOD STAMPS'), (0.006360354726926528, '# EMERGENCY ROOM VISITS 13'), (0.0072666723248089272, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0074309184900819836, '# HOSPITAL DISCHARGES 2013'), (0.007775729131083502, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0080260079217410775, 'EDUCATION RECODE (EDITED)'), (0.0083385667098019259, 'CENSUS REGION AS OF 12/31/13'), (0.0085484729915109563, 'AGE OF DIAGNOSIS-ANGINA'), (0.0088923697297333421, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.010267032639699476, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.010686523924629415, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.011037877032044265, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.016150949264163925, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.017630244335874755, '# DENTAL CARE VISITS 13'), (0.023257721529462604, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.025582240691221322, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.026594337532053043, "PERSON'S TOTAL INCOME"), (0.032899647016282431, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.033030687736769933, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.050574908651201629, "FAMILY'S TOTAL INCOME"), (0.16675350361943775, '# HOME HEALTH PROVIDER DAYS 2013'), (0.3471620209261288, '# OFFICE-BASED PROVIDER VISITS 13')]
d.sort()
d
#[Out]# [(9.4758341343245176e-05, '# ZERO-NIGHT HOSPITAL STAYS 13'), (0.00011365084210032706, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00033247781060767806, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00035002894495563702, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00038171057960275859, 'ANGINA DIAGNOSIS (>17)'), (0.00040554690689092023, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00043008296090103271, 'HEART ATTACK (MI) DIAG (>17)'), (0.00046581799445500032, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.00048270495473304802, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.0004876592082255299, 'STROKE DIAGNOSIS (>17)'), (0.0005026324910203788, 'ASTHMA DIAGNOSIS'), (0.00054944837013530708, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00057076430936153842, 'CANCER DIAGNOSED - LUNG (>17)'), (0.00057205591689190303, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00057238069179789267, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00067133163227591126, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00067455786572936461, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00068833272856688135, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.00069649645114383085, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00077762493582301422, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00079108418979838291, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00079361245928815955, 'CANCER DIAGNOSED - COLON (>17)'), (0.00080674852982930189, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.00085993487469838482, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00092082106759974758, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0009288645204627385, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.00097162768910875777, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.0010334359661009367, 'PERSON IS DEAF - RD 4/2'), (0.0010365590395975529, 'IADL SCREENER - RD 3/1'), (0.00105641391834468, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0010874546197405695, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011192793801382721, 'DIABETES DIAGNOSIS (>17)'), (0.0011667097313107144, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0011869005700702179, 'PERSON IS BLIND - RD 4/2'), (0.001206075205359203, 'ADHDADD DIAGNOSIS (5-17)'), (0.0012081548329251192, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0012237100581762347, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.0012496978075250804, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0012524472651287161, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0012640769835168453, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.001275398101429324, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0012758811952273755, 'OTHER HEART DISEASE DIAG (>17)'), (0.0013744801297412913, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0013878750891699973, 'ADL SCREENER - RD 3/1'), (0.0013950853193475483, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0014480552721406294, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.0015011605441199976, 'SCHOOL LIMITATION - RD 3/1'), (0.0015189200078403289, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0015855557904456172, 'WORK LIMITATION - RD 3/1'), (0.0015920435837667958, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0017670455037161576, 'YEARS PERSON LIVED IN THE US'), (0.001812658688018363, 'SOCIAL LIMITATIONS - RD 3/1'), (0.001838364657371485, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0018680663549180629, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0018868707582693218, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0020630627749840991, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0020727166342049612, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0020822135194508581, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0021419662293700702, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.002232710890408697, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.0023584068738974062, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0023766121790393352, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0023950589844016035, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0024343940420483694, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0025805877559059555, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0025947024508306275, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0025989137041525241, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0026964310610129378, 'PERSON BORN IN THE US'), (0.00300566718784996, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0030084704790707085, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.003241466709865427, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0032592715350888072, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.0034658653331003258, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0035176499827192786, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0035957834849969224, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0037400799698705852, 'CANCER DIAGNOSIS (>17)'), (0.003911998380495941, 'AGE OF DIAGNOSIS-STROKE'), (0.0039531231026602746, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0041715632136641754, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0044161891781093979, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0044590298517266222, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0048573964494600178, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0050851648944117856, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.005153640745768598, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0054241659100721044, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0060503610196654019, 'AGE OF DIAGNOSIS-DIABETES'), (0.0062774149062354316, 'MONTHLY VALUE OF FOOD STAMPS'), (0.006360354726926528, '# EMERGENCY ROOM VISITS 13'), (0.0072666723248089272, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0074309184900819836, '# HOSPITAL DISCHARGES 2013'), (0.007775729131083502, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0080260079217410775, 'EDUCATION RECODE (EDITED)'), (0.0083385667098019259, 'CENSUS REGION AS OF 12/31/13'), (0.0085484729915109563, 'AGE OF DIAGNOSIS-ANGINA'), (0.0088923697297333421, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.010267032639699476, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.010686523924629415, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.011037877032044265, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.016150949264163925, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.017630244335874755, '# DENTAL CARE VISITS 13'), (0.023257721529462604, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.025582240691221322, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.026594337532053043, "PERSON'S TOTAL INCOME"), (0.032899647016282431, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.033030687736769933, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.050574908651201629, "FAMILY'S TOTAL INCOME"), (0.16675350361943775, '# HOME HEALTH PROVIDER DAYS 2013'), (0.3471620209261288, '# OFFICE-BASED PROVIDER VISITS 13')]
rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
health.reload(health)
rf_office?
health.reload(health)
importance
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
result=_
result[0]
result[1]
_
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
result
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
rf_office[0]
#[Out]# RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
#[Out]#            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
#[Out]#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#[Out]#            n_estimators=100, n_jobs=1, oob_score=True, random_state=None,
#[Out]#            verbose=0, warm_start=False)
rf_office[1]
#[Out]# [(0.00011733085921730424, '# ZERO-NIGHT HOSPITAL STAYS 13'), (0.00014800227876293076, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00018986525003998028, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00029825609615534275, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00035807931796701011, 'HEART ATTACK (MI) DIAG (>17)'), (0.0003771138608431634, 'STROKE DIAGNOSIS (>17)'), (0.0004006015801068612, 'ADL SCREENER - RD 3/1'), (0.00043120776110109856, 'ANGINA DIAGNOSIS (>17)'), (0.00045049326902063617, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00048510229273666559, 'ASTHMA DIAGNOSIS'), (0.00048649122979911927, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00049049119393548639, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.0005022770342741859, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00050346197080457429, 'WORK LIMITATION - RD 3/1'), (0.00050499082569986458, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.000563688555408895, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00064542295239281044, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00065132884456767573, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00066474798311555135, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00069890076111163543, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00071765202775022035, 'OTHER HEART DISEASE DIAG (>17)'), (0.00074485880104860445, 'DIABETES DIAGNOSIS (>17)'), (0.00077224066232523176, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00077404396718273915, 'CANCER DIAGNOSED - BREAST (>17)'), (0.00079263955591504039, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00086225090322456302, 'PERSON IS DEAF - RD 4/2'), (0.00087713534722241859, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0008786578264769171, 'CANCER DIAGNOSED - LUNG (>17)'), (0.00087961270235120714, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00092417030254118122, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00094423645668648885, 'CANCER DIAGNOSED - COLON (>17)'), (0.00098030285855226005, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0010388158601847717, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0010578464087412003, 'IADL SCREENER - RD 3/1'), (0.0010727550951165725, 'ADHDADD DIAGNOSIS (5-17)'), (0.0010960312900969585, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.0011021392457607616, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0011284593738822252, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.0011624940425683595, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.001170465081611575, 'SCHOOL LIMITATION - RD 3/1'), (0.0011856395921336768, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0012186164717895142, 'YEARS PERSON LIVED IN THE US'), (0.0012252649672368473, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0012346013331354258, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0012440625428681564, 'PERSON IS BLIND - RD 4/2'), (0.001272535143348714, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.001296355861830859, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0013346351606628093, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.0013987552028699409, 'HOUSEWORK LIMITATION - RD 3/1'), (0.001453916361079999, 'SOCIAL LIMITATIONS - RD 3/1'), (0.001554618563886796, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0017803039028671459, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0018269323942316884, 'CANCER DIAGNOSIS (>17)'), (0.0019346662739956446, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0020536127484510804, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0022241189843547251, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0022557242382095534, 'PERSON BORN IN THE US'), (0.0022625493415379802, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0022693030411038949, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0022852422007428241, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0022933287136939996, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.002297275613108152, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0023055289637629191, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0025314363166528799, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0025977167584633276, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0026143002920209191, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0026459727150343611, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0026709150845496813, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0029388686359042623, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0029727565210206312, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0030739839013307103, 'AGE OF DIAGNOSIS-STROKE'), (0.0030877920597631526, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0031353718349899683, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0035872930493690337, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.003594009213122236, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.0036940651229405074, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0037177088071407884, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0038102593073017386, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.0038717595074923821, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0039389450033165485, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0043138820407428204, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0045174676066920124, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0047409257867666614, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0052495456849974151, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0063413288799199392, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0065024417910317413, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0066293438550646686, '# EMERGENCY ROOM VISITS 13'), (0.0070413881997016762, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0077915973857150345, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0078039440035226641, 'CENSUS REGION AS OF 12/31/13'), (0.0081584164710770471, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.0082745745442430792, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0085159454683159126, 'EDUCATION RECODE (EDITED)'), (0.0085168021066340527, 'AGE OF DIAGNOSIS-DIABETES'), (0.0085620338303683992, '# HOSPITAL DISCHARGES 2013'), (0.0095416847151044912, 'AGE OF DIAGNOSIS-ANGINA'), (0.011036867347921343, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.013012792347607788, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.016957570955529442, '# DENTAL CARE VISITS 13'), (0.017128378482059198, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.023286760516617253, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.023436745063418883, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.027399581388011907, "PERSON'S TOTAL INCOME"), (0.028815260775844578, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.03674918025748436, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.048038239591272208, "FAMILY'S TOTAL INCOME"), (0.18379612429359402, '# HOME HEALTH PROVIDER DAYS 2013'), (0.33520577313712602, '# OFFICE-BASED PROVIDER VISITS 13')]
rf_office[1].sorted()
rf_office[1].sort()
rf_office[1]
#[Out]# [(0.00011733085921730424, '# ZERO-NIGHT HOSPITAL STAYS 13'), (0.00014800227876293076, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00018986525003998028, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00029825609615534275, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00035807931796701011, 'HEART ATTACK (MI) DIAG (>17)'), (0.0003771138608431634, 'STROKE DIAGNOSIS (>17)'), (0.0004006015801068612, 'ADL SCREENER - RD 3/1'), (0.00043120776110109856, 'ANGINA DIAGNOSIS (>17)'), (0.00045049326902063617, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00048510229273666559, 'ASTHMA DIAGNOSIS'), (0.00048649122979911927, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00049049119393548639, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.0005022770342741859, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00050346197080457429, 'WORK LIMITATION - RD 3/1'), (0.00050499082569986458, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.000563688555408895, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00064542295239281044, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00065132884456767573, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00066474798311555135, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00069890076111163543, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00071765202775022035, 'OTHER HEART DISEASE DIAG (>17)'), (0.00074485880104860445, 'DIABETES DIAGNOSIS (>17)'), (0.00077224066232523176, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00077404396718273915, 'CANCER DIAGNOSED - BREAST (>17)'), (0.00079263955591504039, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00086225090322456302, 'PERSON IS DEAF - RD 4/2'), (0.00087713534722241859, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0008786578264769171, 'CANCER DIAGNOSED - LUNG (>17)'), (0.00087961270235120714, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00092417030254118122, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00094423645668648885, 'CANCER DIAGNOSED - COLON (>17)'), (0.00098030285855226005, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0010388158601847717, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0010578464087412003, 'IADL SCREENER - RD 3/1'), (0.0010727550951165725, 'ADHDADD DIAGNOSIS (5-17)'), (0.0010960312900969585, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.0011021392457607616, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0011284593738822252, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.0011624940425683595, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.001170465081611575, 'SCHOOL LIMITATION - RD 3/1'), (0.0011856395921336768, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0012186164717895142, 'YEARS PERSON LIVED IN THE US'), (0.0012252649672368473, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0012346013331354258, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0012440625428681564, 'PERSON IS BLIND - RD 4/2'), (0.001272535143348714, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.001296355861830859, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0013346351606628093, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.0013987552028699409, 'HOUSEWORK LIMITATION - RD 3/1'), (0.001453916361079999, 'SOCIAL LIMITATIONS - RD 3/1'), (0.001554618563886796, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0017803039028671459, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0018269323942316884, 'CANCER DIAGNOSIS (>17)'), (0.0019346662739956446, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0020536127484510804, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0022241189843547251, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0022557242382095534, 'PERSON BORN IN THE US'), (0.0022625493415379802, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0022693030411038949, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0022852422007428241, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0022933287136939996, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.002297275613108152, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0023055289637629191, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0025314363166528799, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0025977167584633276, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0026143002920209191, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0026459727150343611, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0026709150845496813, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0029388686359042623, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0029727565210206312, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0030739839013307103, 'AGE OF DIAGNOSIS-STROKE'), (0.0030877920597631526, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0031353718349899683, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0035872930493690337, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.003594009213122236, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.0036940651229405074, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0037177088071407884, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0038102593073017386, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.0038717595074923821, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0039389450033165485, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0043138820407428204, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0045174676066920124, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0047409257867666614, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0052495456849974151, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0063413288799199392, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0065024417910317413, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0066293438550646686, '# EMERGENCY ROOM VISITS 13'), (0.0070413881997016762, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0077915973857150345, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0078039440035226641, 'CENSUS REGION AS OF 12/31/13'), (0.0081584164710770471, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.0082745745442430792, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0085159454683159126, 'EDUCATION RECODE (EDITED)'), (0.0085168021066340527, 'AGE OF DIAGNOSIS-DIABETES'), (0.0085620338303683992, '# HOSPITAL DISCHARGES 2013'), (0.0095416847151044912, 'AGE OF DIAGNOSIS-ANGINA'), (0.011036867347921343, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.013012792347607788, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.016957570955529442, '# DENTAL CARE VISITS 13'), (0.017128378482059198, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.023286760516617253, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.023436745063418883, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.027399581388011907, "PERSON'S TOTAL INCOME"), (0.028815260775844578, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.03674918025748436, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.048038239591272208, "FAMILY'S TOTAL INCOME"), (0.18379612429359402, '# HOME HEALTH PROVIDER DAYS 2013'), (0.33520577313712602, '# OFFICE-BASED PROVIDER VISITS 13')]
rf_office[1][:10]
#[Out]# [(0.00011733085921730424, '# ZERO-NIGHT HOSPITAL STAYS 13'), (0.00014800227876293076, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00018986525003998028, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00029825609615534275, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00035807931796701011, 'HEART ATTACK (MI) DIAG (>17)'), (0.0003771138608431634, 'STROKE DIAGNOSIS (>17)'), (0.0004006015801068612, 'ADL SCREENER - RD 3/1'), (0.00043120776110109856, 'ANGINA DIAGNOSIS (>17)'), (0.00045049326902063617, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00048510229273666559, 'ASTHMA DIAGNOSIS')]
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
rf_office[1]
#[Out]# [(0.35459483623474247, '# OFFICE-BASED PROVIDER VISITS 13'), (0.15882052709607916, '# HOME HEALTH PROVIDER DAYS 2013'), (0.048854988534723691, "FAMILY'S TOTAL INCOME"), (0.0363312730609699, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.032065883381891126, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.026890158113365162, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.025517846966640167, "PERSON'S TOTAL INCOME"), (0.025201639119807079, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.017975634717962579, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.016720669804512648, '# DENTAL CARE VISITS 13'), (0.012349721463149782, 'AGE OF DIAGNOSIS-ANGINA'), (0.012263299192824048, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.0098787611127272839, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.008702149072179758, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0084195483495214038, '# HOSPITAL DISCHARGES 2013'), (0.0081992912827773407, 'CENSUS REGION AS OF 12/31/13'), (0.0081964299628839669, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.0075074236051212015, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0072467536916036067, 'AGE OF DIAGNOSIS-DIABETES'), (0.0071689781242127151, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0070658413873547522, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0066750512972838542, 'EDUCATION RECODE (EDITED)'), (0.0062508093988320641, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0058406573086959316, '# EMERGENCY ROOM VISITS 13'), (0.0054062271893428092, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0053374120116628536, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0049040721032139641, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0047261526287435043, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0040424806420257565, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0040309113475475679, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0036705453271598004, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0036259959442686675, 'AGE OF DIAGNOSIS-STROKE'), (0.003466717293407055, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.003384931000490211, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.003382903152221966, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0032594116043734023, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0032091893267110067, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.002848307013368823, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0027665870736377817, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0027505345613989892, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0027246804257537575, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0023958794620443803, 'CANCER DIAGNOSIS (>17)'), (0.0022789645511485674, 'PERSON BORN IN THE US'), (0.0021706161563821154, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0021101777912633725, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0020894929436617742, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0020568048538113712, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0020394761480753078, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0019670181702292257, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0019612935231774664, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0019470369194421484, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0018934492233430534, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.0018776597909081387, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0018386401160077588, 'YEARS PERSON LIVED IN THE US'), (0.0018009680318681459, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0017812794008740208, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0017561068433860916, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0017141805317999631, 'CANCER DIAGNOSED - COLON (>17)'), (0.0016885006507394718, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0014717743427874273, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0014608113157300172, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0013817441593937405, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0013457267246630811, 'SOCIAL LIMITATIONS - RD 3/1'), (0.0013438578040587583, 'CANCER DIAGNOSED - LUNG (>17)'), (0.0013350618767571393, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0013030444712099306, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.0012283373400305715, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.0012106537160051221, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.0012003047139841105, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0011950368231289851, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0011861398452536727, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0011738003301463215, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011422461238020036, 'PERSON IS DEAF - RD 4/2'), (0.0011387301178191589, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0011124941870675647, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.0010901467551505495, 'IADL SCREENER - RD 3/1'), (0.0010771826768150026, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.001043133061682331, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0010408348576277271, 'ADHDADD DIAGNOSIS (5-17)'), (0.0010342373137468537, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.0010042364215437601, 'SCHOOL LIMITATION - RD 3/1'), (0.00096877336243132587, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.00092960038730642526, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00092715277526721196, 'ADL SCREENER - RD 3/1'), (0.00092438131184764444, 'DIABETES DIAGNOSIS (>17)'), (0.00078362631353151298, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.00077936424343620647, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00076245235351956177, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00075085874328349332, 'WORK LIMITATION - RD 3/1'), (0.00072763877856162962, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00069278515414611321, 'PERSON IS BLIND - RD 4/2'), (0.00066731865352173572, 'HEART ATTACK (MI) DIAG (>17)'), (0.00063592860562447842, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00060293966104439634, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00059996525184563185, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00058120930686954622, 'OTHER HEART DISEASE DIAG (>17)'), (0.00055964700045487488, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00053890773027950173, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00053310221808893768, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00049765760109669475, 'STROKE DIAGNOSIS (>17)'), (0.0003866681280506847, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.0003772351627155148, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00036665426823597234, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00034240234231137353, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00029854346011166818, 'ASTHMA DIAGNOSIS'), (0.00029119418153688713, 'ANGINA DIAGNOSIS (>17)'), (0.00020148917261403757, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00011019282051100551, '# ZERO-NIGHT HOSPITAL STAYS 13')]
rf_office[1]
#[Out]# [(0.35459483623474247, '# OFFICE-BASED PROVIDER VISITS 13'), (0.15882052709607916, '# HOME HEALTH PROVIDER DAYS 2013'), (0.048854988534723691, "FAMILY'S TOTAL INCOME"), (0.0363312730609699, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.032065883381891126, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.026890158113365162, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.025517846966640167, "PERSON'S TOTAL INCOME"), (0.025201639119807079, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.017975634717962579, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.016720669804512648, '# DENTAL CARE VISITS 13'), (0.012349721463149782, 'AGE OF DIAGNOSIS-ANGINA'), (0.012263299192824048, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.0098787611127272839, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.008702149072179758, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0084195483495214038, '# HOSPITAL DISCHARGES 2013'), (0.0081992912827773407, 'CENSUS REGION AS OF 12/31/13'), (0.0081964299628839669, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.0075074236051212015, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0072467536916036067, 'AGE OF DIAGNOSIS-DIABETES'), (0.0071689781242127151, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0070658413873547522, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0066750512972838542, 'EDUCATION RECODE (EDITED)'), (0.0062508093988320641, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0058406573086959316, '# EMERGENCY ROOM VISITS 13'), (0.0054062271893428092, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0053374120116628536, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0049040721032139641, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0047261526287435043, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0040424806420257565, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0040309113475475679, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0036705453271598004, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0036259959442686675, 'AGE OF DIAGNOSIS-STROKE'), (0.003466717293407055, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.003384931000490211, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.003382903152221966, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0032594116043734023, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0032091893267110067, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.002848307013368823, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0027665870736377817, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0027505345613989892, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0027246804257537575, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0023958794620443803, 'CANCER DIAGNOSIS (>17)'), (0.0022789645511485674, 'PERSON BORN IN THE US'), (0.0021706161563821154, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0021101777912633725, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0020894929436617742, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0020568048538113712, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0020394761480753078, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0019670181702292257, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0019612935231774664, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0019470369194421484, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0018934492233430534, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.0018776597909081387, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0018386401160077588, 'YEARS PERSON LIVED IN THE US'), (0.0018009680318681459, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0017812794008740208, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0017561068433860916, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0017141805317999631, 'CANCER DIAGNOSED - COLON (>17)'), (0.0016885006507394718, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0014717743427874273, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0014608113157300172, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0013817441593937405, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0013457267246630811, 'SOCIAL LIMITATIONS - RD 3/1'), (0.0013438578040587583, 'CANCER DIAGNOSED - LUNG (>17)'), (0.0013350618767571393, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0013030444712099306, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.0012283373400305715, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.0012106537160051221, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.0012003047139841105, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0011950368231289851, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0011861398452536727, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0011738003301463215, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011422461238020036, 'PERSON IS DEAF - RD 4/2'), (0.0011387301178191589, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0011124941870675647, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.0010901467551505495, 'IADL SCREENER - RD 3/1'), (0.0010771826768150026, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.001043133061682331, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0010408348576277271, 'ADHDADD DIAGNOSIS (5-17)'), (0.0010342373137468537, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.0010042364215437601, 'SCHOOL LIMITATION - RD 3/1'), (0.00096877336243132587, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.00092960038730642526, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00092715277526721196, 'ADL SCREENER - RD 3/1'), (0.00092438131184764444, 'DIABETES DIAGNOSIS (>17)'), (0.00078362631353151298, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.00077936424343620647, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00076245235351956177, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00075085874328349332, 'WORK LIMITATION - RD 3/1'), (0.00072763877856162962, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00069278515414611321, 'PERSON IS BLIND - RD 4/2'), (0.00066731865352173572, 'HEART ATTACK (MI) DIAG (>17)'), (0.00063592860562447842, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00060293966104439634, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00059996525184563185, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00058120930686954622, 'OTHER HEART DISEASE DIAG (>17)'), (0.00055964700045487488, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00053890773027950173, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00053310221808893768, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00049765760109669475, 'STROKE DIAGNOSIS (>17)'), (0.0003866681280506847, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.0003772351627155148, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00036665426823597234, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00034240234231137353, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00029854346011166818, 'ASTHMA DIAGNOSIS'), (0.00029119418153688713, 'ANGINA DIAGNOSIS (>17)'), (0.00020148917261403757, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00011019282051100551, '# ZERO-NIGHT HOSPITAL STAYS 13')]
rf_office[1].reversed()
rf_office[1].reverse()
rf_office[1]
#[Out]# [(0.35459483623474247, '# OFFICE-BASED PROVIDER VISITS 13'), (0.15882052709607916, '# HOME HEALTH PROVIDER DAYS 2013'), (0.048854988534723691, "FAMILY'S TOTAL INCOME"), (0.0363312730609699, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.032065883381891126, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.026890158113365162, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.025517846966640167, "PERSON'S TOTAL INCOME"), (0.025201639119807079, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.017975634717962579, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.016720669804512648, '# DENTAL CARE VISITS 13'), (0.012349721463149782, 'AGE OF DIAGNOSIS-ANGINA'), (0.012263299192824048, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.0098787611127272839, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.008702149072179758, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0084195483495214038, '# HOSPITAL DISCHARGES 2013'), (0.0081992912827773407, 'CENSUS REGION AS OF 12/31/13'), (0.0081964299628839669, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.0075074236051212015, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0072467536916036067, 'AGE OF DIAGNOSIS-DIABETES'), (0.0071689781242127151, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.0070658413873547522, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0066750512972838542, 'EDUCATION RECODE (EDITED)'), (0.0062508093988320641, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0058406573086959316, '# EMERGENCY ROOM VISITS 13'), (0.0054062271893428092, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0053374120116628536, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0049040721032139641, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0047261526287435043, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0040424806420257565, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0040309113475475679, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0036705453271598004, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0036259959442686675, 'AGE OF DIAGNOSIS-STROKE'), (0.003466717293407055, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.003384931000490211, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.003382903152221966, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0032594116043734023, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0032091893267110067, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.002848307013368823, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0027665870736377817, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0027505345613989892, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0027246804257537575, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0023958794620443803, 'CANCER DIAGNOSIS (>17)'), (0.0022789645511485674, 'PERSON BORN IN THE US'), (0.0021706161563821154, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0021101777912633725, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0020894929436617742, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0020568048538113712, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0020394761480753078, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0019670181702292257, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0019612935231774664, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0019470369194421484, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0018934492233430534, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.0018776597909081387, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0018386401160077588, 'YEARS PERSON LIVED IN THE US'), (0.0018009680318681459, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0017812794008740208, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0017561068433860916, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0017141805317999631, 'CANCER DIAGNOSED - COLON (>17)'), (0.0016885006507394718, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0014717743427874273, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0014608113157300172, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0013817441593937405, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0013457267246630811, 'SOCIAL LIMITATIONS - RD 3/1'), (0.0013438578040587583, 'CANCER DIAGNOSED - LUNG (>17)'), (0.0013350618767571393, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0013030444712099306, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.0012283373400305715, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.0012106537160051221, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.0012003047139841105, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0011950368231289851, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0011861398452536727, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0011738003301463215, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011422461238020036, 'PERSON IS DEAF - RD 4/2'), (0.0011387301178191589, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0011124941870675647, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.0010901467551505495, 'IADL SCREENER - RD 3/1'), (0.0010771826768150026, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.001043133061682331, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.0010408348576277271, 'ADHDADD DIAGNOSIS (5-17)'), (0.0010342373137468537, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.0010042364215437601, 'SCHOOL LIMITATION - RD 3/1'), (0.00096877336243132587, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.00092960038730642526, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00092715277526721196, 'ADL SCREENER - RD 3/1'), (0.00092438131184764444, 'DIABETES DIAGNOSIS (>17)'), (0.00078362631353151298, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.00077936424343620647, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00076245235351956177, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00075085874328349332, 'WORK LIMITATION - RD 3/1'), (0.00072763877856162962, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00069278515414611321, 'PERSON IS BLIND - RD 4/2'), (0.00066731865352173572, 'HEART ATTACK (MI) DIAG (>17)'), (0.00063592860562447842, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00060293966104439634, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00059996525184563185, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00058120930686954622, 'OTHER HEART DISEASE DIAG (>17)'), (0.00055964700045487488, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00053890773027950173, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00053310221808893768, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00049765760109669475, 'STROKE DIAGNOSIS (>17)'), (0.0003866681280506847, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.0003772351627155148, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00036665426823597234, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00034240234231137353, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00029854346011166818, 'ASTHMA DIAGNOSIS'), (0.00029119418153688713, 'ANGINA DIAGNOSIS (>17)'), (0.00020148917261403757, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00011019282051100551, '# ZERO-NIGHT HOSPITAL STAYS 13')]
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
rf_office[1]
#[Out]# [(0.34988081591710196, '# OFFICE-BASED PROVIDER VISITS 13'), (0.15866072762608652, '# HOME HEALTH PROVIDER DAYS 2013'), (0.048458406208494974, "FAMILY'S TOTAL INCOME"), (0.038483565113807801, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.029791056958366625, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.028873154446443259, "PERSON'S TOTAL INCOME"), (0.024808763847370238, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.021422682163352164, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.018395055172035751, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.018121794101278444, '# DENTAL CARE VISITS 13'), (0.015844903746174772, 'AGE OF DIAGNOSIS-ANGINA'), (0.015181851830600706, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.011048927513000892, '# HOSPITAL DISCHARGES 2013'), (0.010189099330793563, 'EDUCATION RECODE (EDITED)'), (0.0089089723415059878, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.0084498019720766777, 'CENSUS REGION AS OF 12/31/13'), (0.0083607557969509853, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.0081243233375833529, '# EMERGENCY ROOM VISITS 13'), (0.0074386305944046504, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0069732320435263303, 'AGE OF DIAGNOSIS-DIABETES'), (0.0069371809329043677, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0067795601323496595, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0065937253544424033, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0052381245881855578, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.005064463510706778, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0048274492867804667, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0043277372284671305, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0039354141007580182, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0039058332174829458, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0036945206728810341, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0031446870283460193, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0030462741516457358, 'AGE OF DIAGNOSIS-STROKE'), (0.0030210741270114997, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0029772637606248335, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0027864102973035221, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0026447562660726147, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0026356896852996773, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0025924438233124476, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0025761954269220926, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0025727161687210142, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0025280373004653539, 'SOCIAL LIMITATIONS - RD 3/1'), (0.0024934816797537018, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0024119479169717691, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0023927052143093448, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0023859027869679908, 'OTHER HEART DISEASE DIAG (>17)'), (0.0023322988880987495, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0022453925708645957, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.0021475549184279213, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0021053706787415539, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0021026197759432667, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0020540045415509104, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0019247742595395025, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.001871049528337918, 'CANCER DIAGNOSED - LUNG (>17)'), (0.0018656272923110338, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0018136697711655587, 'PERSON BORN IN THE US'), (0.0017583097171758816, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0017251385197868117, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0016772891928429395, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0016600121239527192, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0016177661367301306, 'ADHDADD DIAGNOSIS (5-17)'), (0.0015815850872823148, 'YEARS PERSON LIVED IN THE US'), (0.0015414769092806715, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0015244276825630537, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0014649824443600868, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0014639891123839387, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0014452184059422528, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0014389975830754918, 'DIABETES DIAGNOSIS (>17)'), (0.0014029544377799161, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0013944735568109706, 'CANCER DIAGNOSIS (>17)'), (0.0013488774885298837, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0012578989968570296, 'HEART ATTACK (MI) DIAG (>17)'), (0.0012576326434829087, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0012009959271010316, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.0011928862882274211, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011235934537371826, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.00101498579452232, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00096751998091180178, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.00095219631071099848, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.00093981503263116772, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.00090876006258372205, 'PERSON IS BLIND - RD 4/2'), (0.00090421682663944754, 'CANCER DIAGNOSED - COLON (>17)'), (0.0008731026818846405, 'PERSON IS DEAF - RD 4/2'), (0.00086796749104174143, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00084842259804362259, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00082536442039062225, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.00082072616755960099, 'ADL SCREENER - RD 3/1'), (0.00079319125474625637, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.00078875886389055227, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00078520388914153638, 'IADL SCREENER - RD 3/1'), (0.00077670190400214525, 'SCHOOL LIMITATION - RD 3/1'), (0.00071978545012379503, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00068511422906050235, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00066379795567293764, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00066368255438444945, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00066180263543744037, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00056568939046981953, 'STROKE DIAGNOSIS (>17)'), (0.00054945594479851353, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00054120525222582487, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00052592931978740951, 'WORK LIMITATION - RD 3/1'), (0.00047178536084689144, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00039705226158816059, 'ASTHMA DIAGNOSIS'), (0.00039493772077816376, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00037501409668050373, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00035353896196916766, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00031382199363786149, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00026827917643762228, 'ANGINA DIAGNOSIS (>17)'), (0.00020847676594371652, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00010271499893161518, '# ZERO-NIGHT HOSPITAL STAYS 13')]
health.rmse?
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
rf_office
#[Out]# (RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
#[Out]#            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
#[Out]#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#[Out]#            n_estimators=100, n_jobs=1, oob_score=True, random_state=None,
#[Out]#            verbose=0, warm_start=False), [(0.34988081591710196, '# OFFICE-BASED PROVIDER VISITS 13'), (0.15866072762608652, '# HOME HEALTH PROVIDER DAYS 2013'), (0.048458406208494974, "FAMILY'S TOTAL INCOME"), (0.038483565113807801, 'ADULT BODY MASS INDEX (>17) - RD 5/3'), (0.029791056958366625, '# OFFICE-BASED PHYSICIAN VISITS 13'), (0.028873154446443259, "PERSON'S TOTAL INCOME"), (0.024808763847370238, 'AGE AS OF 12/31/13 (EDITED/IMPUTED)'), (0.021422682163352164, 'AGE OF DIAGNOSIS-ARTHRITIS'), (0.018395055172035751, '# NIGHTS IN HOSP FOR DISCHARGES 2013'), (0.018121794101278444, '# DENTAL CARE VISITS 13'), (0.015844903746174772, 'AGE OF DIAGNOSIS-ANGINA'), (0.015181851830600706, '# OUTPATIENT DEPT PROVIDER VISITS 13'), (0.011048927513000892, '# HOSPITAL DISCHARGES 2013'), (0.010189099330793563, 'EDUCATION RECODE (EDITED)'), (0.0089089723415059878, 'AGE OF DIAGNOSIS-HIGH CHOLESTEROL'), (0.0084498019720766777, 'CENSUS REGION AS OF 12/31/13'), (0.0083607557969509853, 'AGE OF DIAGNOSIS-HIGH BLOOD PRESSURE'), (0.0081243233375833529, '# EMERGENCY ROOM VISITS 13'), (0.0074386305944046504, 'PREGNANT DURING REF PERIOD - RD 3/1'), (0.0069732320435263303, 'AGE OF DIAGNOSIS-DIABETES'), (0.0069371809329043677, 'AGE OF DIAGNOSIS-CORONARY HEART DISEASE'), (0.0067795601323496595, 'MARITAL STATUS-12/31/13 (EDITED/IMPUTED)'), (0.0065937253544424033, '# OUTPATIENT DEPT PHYSICIAN VISITS 13'), (0.0052381245881855578, 'RACE/ETHNICITY (EDITED/IMPUTED)'), (0.005064463510706778, 'AGE OF DIAGNOSIS-OTHER HEART DISEASE'), (0.0048274492867804667, 'WEARS EYEGLASSES OR CONTACTS - RD 4/2'), (0.0043277372284671305, 'IN FAMILY WITH SOMEONE SPKNG OTHER LANG'), (0.0039354141007580182, 'MULT DIAG HIGH BLOOD PRESS (>17)'), (0.0039058332174829458, 'AGE OF DIAGNOSIS-HEART ATTACK(MI)'), (0.0036945206728810341, 'MONTHLY VALUE OF FOOD STAMPS'), (0.0031446870283460193, 'DIFFICULTY DRESSING/BATHING-RD 4/2'), (0.0030462741516457358, 'AGE OF DIAGNOSIS-STROKE'), (0.0030210741270114997, 'CANCER DIAGNOSED - PROSTATE (>17)'), (0.0029772637606248335, 'DIFFICULTY STANDING 20 MINUTES - RD 3/1'), (0.0027864102973035221, 'DIFFICULTY WALKING A MILE - RD 3/1'), (0.0026447562660726147, 'DIFFICULTY USING FINGERS TO GRASP-RD 3/1'), (0.0026356896852996773, 'SERIOUS DIFFICULTY SEE W/GLASSES-RD 4/2'), (0.0025924438233124476, 'PHYS FUNCTIONING HELP 3+ MONTHS - RD 3/1'), (0.0025761954269220926, 'AGE OF DIAGNOSIS-ASTHMA'), (0.0025727161687210142, 'ARTHRITIS DIAGNOSIS (>17)'), (0.0025280373004653539, 'SOCIAL LIMITATIONS - RD 3/1'), (0.0024934816797537018, 'STUDENT STATUS IF AGES 17-23 - 12/31/13'), (0.0024119479169717691, 'WHEN WAS LAST EPISODE OF ASTHMA - RD 3/1'), (0.0023927052143093448, 'DIFFICULTY DOING ERRANDS ALONE-RD 4/2'), (0.0023859027869679908, 'OTHER HEART DISEASE DIAG (>17)'), (0.0023322988880987495, 'HIGH CHOLESTEROL DIAGNOSIS (>17)'), (0.0022453925708645957, 'DIFFICULTY WALKING UP 10 STEPS - RD 3/1'), (0.0021475549184279213, 'COMPLETELY UNABLE TO DO ACTIVITY-RD 3/1'), (0.0021053706787415539, 'SERIOUS DIFCULTY WLK/CLIMB STAIRS-RD 4/2'), (0.0021026197759432667, 'PERSON WEARS HEARING AID - RD 4/2'), (0.0020540045415509104, 'LANGUAGE SPOKEN AT HOME OTHER THAN ENGL'), (0.0019247742595395025, 'JOINT PAIN LAST 12 MONTHS (>17) - RD 3/1'), (0.001871049528337918, 'CANCER DIAGNOSED - LUNG (>17)'), (0.0018656272923110338, 'CANCER DIAGNOSED - BREAST (>17)'), (0.0018136697711655587, 'PERSON BORN IN THE US'), (0.0017583097171758816, 'DIFFICULTY LIFTING 10 POUNDS - RD 3/1'), (0.0017251385197868117, 'CANCER DIAGNOSED - SKIN-NONMELANO (>17)'), (0.0016772891928429395, 'CANCER DIAGNOSED - OTHER (>17)'), (0.0016600121239527192, 'HIGH BLOOD PRESSURE DIAG (>17)'), (0.0016177661367301306, 'ADHDADD DIAGNOSIS (5-17)'), (0.0015815850872823148, 'YEARS PERSON LIVED IN THE US'), (0.0015414769092806715, 'AGE OF DIAGNOSIS-ADHD/ADD'), (0.0015244276825630537, 'AGE OF DIAGNOSIS-EMPHYSEMA'), (0.0014649824443600868, 'HOUSEWORK LIMITATION - RD 3/1'), (0.0014639891123839387, 'DIFFICULTY WALKING 3 BLOCKS - RD 3/1'), (0.0014452184059422528, 'IADL HELP 3+ MONTHS - RD 3/1'), (0.0014389975830754918, 'DIABETES DIAGNOSIS (>17)'), (0.0014029544377799161, 'DIFFICULTY REACHING OVERHEAD - RD 3/1'), (0.0013944735568109706, 'CANCER DIAGNOSIS (>17)'), (0.0013488774885298837, 'CANCER DIAGNOSED - CERVIX (>17)'), (0.0012578989968570296, 'HEART ATTACK (MI) DIAG (>17)'), (0.0012576326434829087, 'ADL HELP 3+ MONTHS - RD 3/1'), (0.0012009959271010316, 'HAVE PEAK FLOW METER AT HOME - RD 5/3'), (0.0011928862882274211, 'DIFFICULTY BENDING/STOOPING - RD 3/1'), (0.0011235934537371826, 'CANCER DIAGNOSED - MELANOMA (>17)'), (0.00101498579452232, 'ANY LIMITATION WORK/HOUSEWRK/SCHL-RD 3/1'), (0.00096751998091180178, 'SERIOUS COGNITIVE DIFFICULTIES-RD 4/2'), (0.00095219631071099848, 'LIMITATION IN PHYSICAL FUNCTIONING-RD3/1'), (0.00093981503263116772, 'COGNITIVE LIMITATIONS - RD 3/1'), (0.00090876006258372205, 'PERSON IS BLIND - RD 4/2'), (0.00090421682663944754, 'CANCER DIAGNOSED - COLON (>17)'), (0.0008731026818846405, 'PERSON IS DEAF - RD 4/2'), (0.00086796749104174143, 'USED ACUTE PRES INHALER LAST 3 MOS-RD5/3'), (0.00084842259804362259, 'USED ASSISTIVE DEVICES - RD 3/1'), (0.00082536442039062225, 'CANCER DIAGNOSED-SKIN-UNKNOWN TYPE (>17)'), (0.00082072616755960099, 'ADL SCREENER - RD 3/1'), (0.00079319125474625637, 'CANCER DIAGNOSED - LYMPHOMA (>17)'), (0.00078875886389055227, 'SERIOUS DIFFICULTY HEARING-RD 4/2'), (0.00078520388914153638, 'IADL SCREENER - RD 3/1'), (0.00077670190400214525, 'SCHOOL LIMITATION - RD 3/1'), (0.00071978545012379503, 'CANCER DIAGNOSED - UTERUS (>17)'), (0.00068511422906050235, 'CORONARY HRT DISEASE DIAG (>17)'), (0.00066379795567293764, 'DOES PERSON STILL HAVE ASTHMA-RD3/1'), (0.00066368255438444945, 'NUMBER OF MONTHS FOOD STAMPS PURCHASED'), (0.00066180263543744037, 'EVER USED PREV DAILY ASTHMA MEDS -RD 5/3'), (0.00056568939046981953, 'STROKE DIAGNOSIS (>17)'), (0.00054945594479851353, 'WHEN LAST USED PEAK FLOW METER - RD 5/3'), (0.00054120525222582487, 'NOW TAKE PREV DAILY ASTHMA MEDS - RD 5/3'), (0.00052592931978740951, 'WORK LIMITATION - RD 3/1'), (0.00047178536084689144, 'CANCER DIAGNOSED - BLADDER (>17)'), (0.00039705226158816059, 'ASTHMA DIAGNOSIS'), (0.00039493772077816376, 'ASTHMA ATTACK LAST 12 MOS - RD3/1'), (0.00037501409668050373, 'USED>3ACUTE CN PRES INH LAST 3 MOS-RD5/3'), (0.00035353896196916766, 'EVER USED PEAK FLOW METER - RD 5/3'), (0.00031382199363786149, 'EMPHYSEMA DIAGNOSIS (>17)'), (0.00026827917643762228, 'ANGINA DIAGNOSIS (>17)'), (0.00020847676594371652, 'CHRONC BRONCHITS LAST 12 MTHS (>17)-R3/1'), (0.00010271499893161518, '# ZERO-NIGHT HOSPITAL STAYS 13')])
rf_office[0]
#[Out]# RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
#[Out]#            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
#[Out]#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#[Out]#            n_estimators=100, n_jobs=1, oob_score=True, random_state=None,
#[Out]#            verbose=0, warm_start=False)
health.rmse(rf_office[0],test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 4257.88544802', 'stddev in data = 4157.00593571')
health.rmse(rf_office[0],test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 4257.88544802', 'stddev in data = 4157.00593571')
rf_office[0].oob_scores_
rf_office[0]?
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
rf_office?
rf_office.oob_score_
#[Out]# 0.29129123482810582
health.rmse?
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 3710.86188479', 'stddev in data = 4157.00593571')
# Sun, 27 Sep 2015 00:00:37
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
# Sun, 27 Sep 2015 00:03:23
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 3513.08347895', 'stddev in data = 4157.00593571')
# Sun, 27 Sep 2015 00:03:29
rf_office.oob_score_
#[Out]# 0.31386157646743718
# Sun, 27 Sep 2015 00:05:53
rf_outpatient=health.rf_modeling?
# Sun, 27 Sep 2015 00:05:55
rf_outpatient=health.rf_modeling?
# Sun, 27 Sep 2015 00:06:46
%time rf_outpatient=health.rf_modeling(train,outpatient,"./exog_building_rf_outpatient.txt"
# Sun, 27 Sep 2015 00:06:49
rf_outpatient=health.rf_modeling?
# Sun, 27 Sep 2015 00:07:12
%time rf_outpatient=health.rf_modeling(train,outpatient,"./exog_building_rf_outpatient.txt",100,w)
# Sun, 27 Sep 2015 00:22:18
health.rmse(rf_outpatient,test,outpatient,"./exog_building_rf_outpatient.txt")
#[Out]# ('rmse = 63452.3538812', 'stddev in data = 1636.96415983')
# Sun, 27 Sep 2015 00:22:40
rf_outpatient.oob_score_
#[Out]# -0.023492642332556102
# Sun, 27 Sep 2015 00:23:43
rf_outpatient.feature_importances_
#[Out]# array([  1.96617737e-02,   7.33492539e-04,   9.83365757e-04,
#[Out]#          3.65394572e-02,   2.52278167e-02,   1.25388529e-02,
#[Out]#          1.62320744e-02,   7.33836590e-02,   3.53642224e-01,
#[Out]#          2.29896404e-03,   5.99420464e-05,   2.99240397e-03,
#[Out]#          3.53404681e-03,   6.85133653e-03,   1.26041343e-02,
#[Out]#          1.19431482e-02,   2.86897383e-02,   5.15055809e-03,
#[Out]#          3.74041183e-02,   1.82311726e-02,   1.02181125e-03,
#[Out]#          2.19688490e-03,   6.05735576e-04,   1.54113282e-03,
#[Out]#          5.23830030e-04,   7.30578500e-03,   6.36918096e-03,
#[Out]#          4.47565725e-03,   1.60622049e-04,   2.89667687e-03,
#[Out]#          1.14889841e-04,   3.03027356e-04,   4.27350774e-03,
#[Out]#          1.91771011e-03,   5.40805581e-04,   2.61545532e-03,
#[Out]#          1.15638837e-04,   7.36651267e-04,   6.20881478e-05,
#[Out]#          2.37165413e-04,   5.02419762e-04,   6.72405215e-03,
#[Out]#          4.33103924e-03,   8.14339458e-04,   2.23832937e-04,
#[Out]#          4.37457326e-04,   1.00171963e-04,   2.50664066e-04,
#[Out]#          1.11948179e-04,   2.31641851e-04,   9.48333372e-04,
#[Out]#          3.99964346e-04,   1.29426938e-03,   7.03812818e-04,
#[Out]#          3.87083426e-04,   4.99181724e-04,   5.43910974e-03,
#[Out]#          6.44192329e-03,   1.27599607e-03,   6.23808775e-03,
#[Out]#          7.56385714e-03,   4.32444848e-03,   5.48528960e-02,
#[Out]#          2.82195931e-03,   3.69849304e-04,   1.00879741e-03,
#[Out]#          1.92447272e-02,   1.15029809e-02,   1.02401062e-02,
#[Out]#          3.41512408e-04,   5.33734867e-03,   9.80839367e-05,
#[Out]#          7.05116211e-04,   4.40241598e-03,   2.13608676e-03,
#[Out]#          3.12995962e-03,   3.98740894e-04,   5.38212714e-04,
#[Out]#          2.32381259e-04,   1.31167811e-02,   1.42143527e-02,
#[Out]#          1.99131880e-02,   2.33650251e-03,   8.32422891e-04,
#[Out]#          1.20104987e-03,   3.34115047e-03,   3.21512240e-03,
#[Out]#          8.60503270e-04,   4.28248798e-03,   3.31074425e-04,
#[Out]#          3.16382782e-04,   4.61600914e-03,   1.10806898e-04,
#[Out]#          6.60592043e-04,   6.73348387e-04,   3.15222719e-03,
#[Out]#          7.95512571e-04,   4.12385870e-03,   3.00977048e-04,
#[Out]#          2.60195640e-04,   3.02014204e-04,   2.17465727e-04,
#[Out]#          1.18331374e-02,   1.64451006e-02,   1.40982178e-02,
#[Out]#          8.41079509e-04,   4.59766775e-04,   3.82732692e-03])
# Sun, 27 Sep 2015 00:23:50
list(rf_outpatient.feature_importances_)
#[Out]# [0.019661773685263336,
#[Out]#  0.0007334925387726544,
#[Out]#  0.00098336575749281081,
#[Out]#  0.036539457217941183,
#[Out]#  0.02522781669563158,
#[Out]#  0.012538852885510631,
#[Out]#  0.016232074447715496,
#[Out]#  0.07338365901423885,
#[Out]#  0.35364222389809891,
#[Out]#  0.0022989640361285625,
#[Out]#  5.9942046415591717e-05,
#[Out]#  0.002992403965199134,
#[Out]#  0.00353404681402898,
#[Out]#  0.0068513365288100951,
#[Out]#  0.012604134320327776,
#[Out]#  0.011943148242233309,
#[Out]#  0.0286897383058317,
#[Out]#  0.0051505580907469537,
#[Out]#  0.037404118294942695,
#[Out]#  0.018231172574981355,
#[Out]#  0.0010218112508790309,
#[Out]#  0.0021968849009482085,
#[Out]#  0.00060573557607194182,
#[Out]#  0.0015411328150168574,
#[Out]#  0.00052383003010500646,
#[Out]#  0.0073057849982208559,
#[Out]#  0.0063691809566481941,
#[Out]#  0.0044756572526723085,
#[Out]#  0.0001606220485798263,
#[Out]#  0.0028966768696245225,
#[Out]#  0.00011488984093366677,
#[Out]#  0.00030302735599650374,
#[Out]#  0.0042735077379702164,
#[Out]#  0.0019177101054901624,
#[Out]#  0.00054080558059907782,
#[Out]#  0.0026154553232042199,
#[Out]#  0.00011563883677770299,
#[Out]#  0.00073665126733123457,
#[Out]#  6.20881477565857e-05,
#[Out]#  0.00023716541280801925,
#[Out]#  0.00050241976188577803,
#[Out]#  0.006724052146742935,
#[Out]#  0.0043310392449344079,
#[Out]#  0.00081433945775459624,
#[Out]#  0.00022383293650779583,
#[Out]#  0.00043745732557826328,
#[Out]#  0.00010017196294326786,
#[Out]#  0.00025066406561327203,
#[Out]#  0.00011194817880978819,
#[Out]#  0.00023164185066770087,
#[Out]#  0.00094833337215092974,
#[Out]#  0.00039996434631419747,
#[Out]#  0.0012942693839823078,
#[Out]#  0.00070381281750188168,
#[Out]#  0.00038708342577951243,
#[Out]#  0.00049918172374745077,
#[Out]#  0.0054391097402999551,
#[Out]#  0.0064419232904506041,
#[Out]#  0.0012759960746088735,
#[Out]#  0.006238087750518242,
#[Out]#  0.0075638571418765402,
#[Out]#  0.0043244484798198694,
#[Out]#  0.054852895967167775,
#[Out]#  0.0028219593120063636,
#[Out]#  0.00036984930362193261,
#[Out]#  0.0010087974060645974,
#[Out]#  0.019244727190170324,
#[Out]#  0.011502980947285753,
#[Out]#  0.010240106201201566,
#[Out]#  0.00034151240813634751,
#[Out]#  0.0053373486695038833,
#[Out]#  9.808393668012134e-05,
#[Out]#  0.00070511621117757509,
#[Out]#  0.0044024159770686931,
#[Out]#  0.0021360867627488638,
#[Out]#  0.0031299596158491138,
#[Out]#  0.0003987408937755408,
#[Out]#  0.00053821271448251692,
#[Out]#  0.00023238125898607167,
#[Out]#  0.013116781094293529,
#[Out]#  0.014214352749883778,
#[Out]#  0.019913188009981101,
#[Out]#  0.0023365025078832145,
#[Out]#  0.00083242289148899404,
#[Out]#  0.0012010498705002743,
#[Out]#  0.0033411504743744995,
#[Out]#  0.0032151224028265949,
#[Out]#  0.00086050327046841306,
#[Out]#  0.0042824879795543454,
#[Out]#  0.00033107442473744709,
#[Out]#  0.00031638278196407117,
#[Out]#  0.0046160091423432517,
#[Out]#  0.00011080689836120395,
#[Out]#  0.00066059204264916804,
#[Out]#  0.00067334838746949469,
#[Out]#  0.0031522271927255114,
#[Out]#  0.00079551257058144652,
#[Out]#  0.0041238587033008292,
#[Out]#  0.00030097704755505485,
#[Out]#  0.00026019563987382494,
#[Out]#  0.00030201420431468,
#[Out]#  0.00021746572675373734,
#[Out]#  0.011833137421325048,
#[Out]#  0.016445100646092833,
#[Out]#  0.014098217797072916,
#[Out]#  0.00084107950871964963,
#[Out]#  0.00045976677461725924,
#[Out]#  0.0038273269159270966]
# Sun, 27 Sep 2015 00:24:06
list(rf_outpatient.feature_importances_)[:10]
#[Out]# [0.019661773685263336,
#[Out]#  0.0007334925387726544,
#[Out]#  0.00098336575749281081,
#[Out]#  0.036539457217941183,
#[Out]#  0.02522781669563158,
#[Out]#  0.012538852885510631,
#[Out]#  0.016232074447715496,
#[Out]#  0.07338365901423885,
#[Out]#  0.35364222389809891,
#[Out]#  0.0022989640361285625]
# Sun, 27 Sep 2015 00:24:09
list(rf_outpatient.feature_importances_)[:20]
#[Out]# [0.019661773685263336,
#[Out]#  0.0007334925387726544,
#[Out]#  0.00098336575749281081,
#[Out]#  0.036539457217941183,
#[Out]#  0.02522781669563158,
#[Out]#  0.012538852885510631,
#[Out]#  0.016232074447715496,
#[Out]#  0.07338365901423885,
#[Out]#  0.35364222389809891,
#[Out]#  0.0022989640361285625,
#[Out]#  5.9942046415591717e-05,
#[Out]#  0.002992403965199134,
#[Out]#  0.00353404681402898,
#[Out]#  0.0068513365288100951,
#[Out]#  0.012604134320327776,
#[Out]#  0.011943148242233309,
#[Out]#  0.0286897383058317,
#[Out]#  0.0051505580907469537,
#[Out]#  0.037404118294942695,
#[Out]#  0.018231172574981355]
# Sun, 27 Sep 2015 00:24:11
list(rf_outpatient.feature_importances_)[:21]
#[Out]# [0.019661773685263336,
#[Out]#  0.0007334925387726544,
#[Out]#  0.00098336575749281081,
#[Out]#  0.036539457217941183,
#[Out]#  0.02522781669563158,
#[Out]#  0.012538852885510631,
#[Out]#  0.016232074447715496,
#[Out]#  0.07338365901423885,
#[Out]#  0.35364222389809891,
#[Out]#  0.0022989640361285625,
#[Out]#  5.9942046415591717e-05,
#[Out]#  0.002992403965199134,
#[Out]#  0.00353404681402898,
#[Out]#  0.0068513365288100951,
#[Out]#  0.012604134320327776,
#[Out]#  0.011943148242233309,
#[Out]#  0.0286897383058317,
#[Out]#  0.0051505580907469537,
#[Out]#  0.037404118294942695,
#[Out]#  0.018231172574981355,
#[Out]#  0.0010218112508790309]
# Sun, 27 Sep 2015 00:24:44
%time rf_outpatient=health.rf_modeling(train,outpatient,"./exog_building_rf_outpatient.txt",100,w)
# Sun, 27 Sep 2015 00:27:26
rf_outpatient.oob_score_
#[Out]# 0.028771123520677899
# Sun, 27 Sep 2015 00:27:30
health.rmse(rf_outpatient,test,outpatient,"./exog_building_rf_outpatient.txt")
#[Out]# ('rmse = 2991.24229453', 'stddev in data = 1636.96415983')
# Sun, 27 Sep 2015 00:27:53
%time rf_outpatient=health.rf_modeling(train,outpatient,"./exog_building_rf_outpatient.txt",100,w)
# Sun, 27 Sep 2015 00:28:45
health.rmse(rf_outpatient,test,outpatient,"./exog_building_rf_outpatient.txt")
#[Out]# ('rmse = 2157.45708824', 'stddev in data = 1636.96415983')
# Sun, 27 Sep 2015 00:28:51
rf_outpatient.oob_score_
#[Out]# 0.044370232128967402
# Sun, 27 Sep 2015 02:08:36
%time rf_inpatient=health.rf_modeling?
# Sun, 27 Sep 2015 02:09:07
%time rf_inpatient=health.rf_modeling(train,inpatient,"./exog_building_rf_inpatient.txt",100,w)
# Sun, 27 Sep 2015 02:11:17
rf_inpatient?
# Sun, 27 Sep 2015 02:11:34
list(rf_inpatient.feature_importances_)
#[Out]# [0.014307768932711556,
#[Out]#  0.0011982132925744805,
#[Out]#  0.0045338760758237015,
#[Out]#  0.018344763916216641,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.014794979566331055,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0055324088460278502,
#[Out]#  0.052826837807277559,
#[Out]#  0.00080761022856102205,
#[Out]#  0.081537512681626734,
#[Out]#  0.44196483074471266,
#[Out]#  0.0046646446431125284,
#[Out]#  0.0048114007201744621,
#[Out]#  0.01279641456324768,
#[Out]#  0.011123459890756639,
#[Out]#  0.0033733565829595557,
#[Out]#  0.0037553469094817869,
#[Out]#  0.0092132315626342384,
#[Out]#  0.00054603389866892847,
#[Out]#  0.0041775309254357016,
#[Out]#  0.0019055762567571199,
#[Out]#  0.0026038500691755763,
#[Out]#  0.00075375443551529498,
#[Out]#  0.00094990958020013368,
#[Out]#  0.012188176351441902,
#[Out]#  0.0032554744764037289,
#[Out]#  0.00043706211883904006,
#[Out]#  0.0018226012015011826,
#[Out]#  0.00071286417931450856,
#[Out]#  0.0027028798729806952,
#[Out]#  0.0019319400505911014,
#[Out]#  0.0073708397314112203,
#[Out]#  0.00088058268577424716,
#[Out]#  0.024377791932052452,
#[Out]#  0.00064908676383868143,
#[Out]#  0.0026185768828427543,
#[Out]#  0.0006819885743686038,
#[Out]#  0.00050555646082315925,
#[Out]#  0.00027742366390731286,
#[Out]#  0.00092322611839460236,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0066163177909811053,
#[Out]#  0.0018637719956641654,
#[Out]#  0.0025971662691488466,
#[Out]#  0.0011322350927577469,
#[Out]#  0.0015628972648922118,
#[Out]#  0.0023472266873461592,
#[Out]#  0.0037556336106575289,
#[Out]#  0.0023619756237352641,
#[Out]#  0.0020276223667640746,
#[Out]#  0.012695275625702263,
#[Out]#  0.0014661538361673279,
#[Out]#  0.0028560931816874291,
#[Out]#  0.00044731469704016211,
#[Out]#  0.0051490537739638373,
#[Out]#  0.017048746395083705,
#[Out]#  0.0013812907866131174,
#[Out]#  0.0021509464380104217,
#[Out]#  0.009087569780794464,
#[Out]#  0.00025602037594930677,
#[Out]#  0.0021389028189339968,
#[Out]#  0.0092571129627260166,
#[Out]#  0.00053097167258171155,
#[Out]#  0.0024198419761143884,
#[Out]#  0.0025357345460583157,
#[Out]#  0.00013067082892140381,
#[Out]#  0.0037888084799081868,
#[Out]#  0.00022961182995188755,
#[Out]#  0.00088186979912618197,
#[Out]#  0.0014942246655814959,
#[Out]#  0.00029791452255448859,
#[Out]#  0.00090770089564042814,
#[Out]#  0.00026560801996828204,
#[Out]#  0.001205664197063016,
#[Out]#  0.00094896978180574615,
#[Out]#  0.0010395827511020597,
#[Out]#  0.00052129530553082489,
#[Out]#  0.00020202231790879784,
#[Out]#  0.0011173846651903205,
#[Out]#  0.00055373134964798983,
#[Out]#  0.0021070265434577043,
#[Out]#  0.0028372493215243462,
#[Out]#  0.0025303377534068401,
#[Out]#  0.0005756294419753422,
#[Out]#  0.0012372564102186504,
#[Out]#  0.003777305272934493,
#[Out]#  0.0029691711561356865,
#[Out]#  0.0011237876182720453,
#[Out]#  0.0014134808995310299,
#[Out]#  0.00081311099762849807,
#[Out]#  0.00039872925815505111,
#[Out]#  0.0043745967145511211,
#[Out]#  0.0015219842630576847,
#[Out]#  0.0032025736548801282,
#[Out]#  0.0011592948241941773,
#[Out]#  0.00086258791834983081,
#[Out]#  0.00048266063086820792,
#[Out]#  0.00041116716919429544,
#[Out]#  0.00069036045107968738,
#[Out]#  0.00076085230534933882,
#[Out]#  0.0038706130932074962,
#[Out]#  0.0019097800911094935,
#[Out]#  0.0011364581171867778,
#[Out]#  0.00081447453685789997,
#[Out]#  0.00051462384597232189,
#[Out]#  0.0020596365812466755]
# Sun, 27 Sep 2015 02:11:39
list(rf_inpatient.feature_importances_)[:10]
#[Out]# [0.014307768932711556,
#[Out]#  0.0011982132925744805,
#[Out]#  0.0045338760758237015,
#[Out]#  0.018344763916216641,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.014794979566331055,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0055324088460278502,
#[Out]#  0.052826837807277559]
# Sun, 27 Sep 2015 02:11:51
list(rf_inpatient.feature_importances_.sort())[:10]
# Sun, 27 Sep 2015 02:11:59
list(rf_inpatient.feature_importances_)[:10]
#[Out]# [0.014307768932711556,
#[Out]#  0.0011982132925744805,
#[Out]#  0.0045338760758237015,
#[Out]#  0.018344763916216641,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.014794979566331055,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0055324088460278502,
#[Out]#  0.052826837807277559]
# Sun, 27 Sep 2015 02:12:41
temp
# Sun, 27 Sep 2015 02:12:47
temp=rf_inpatient.feature_importances_
# Sun, 27 Sep 2015 02:12:51
temp=rf_inpatient.feature_importances_.copy()
# Sun, 27 Sep 2015 02:12:55
temp.sort()
# Sun, 27 Sep 2015 02:12:56
temp
#[Out]# array([  1.30670829e-04,   2.02022318e-04,   2.29611830e-04,
#[Out]#          2.56020376e-04,   2.65608020e-04,   2.77423664e-04,
#[Out]#          2.97914523e-04,   3.98729258e-04,   4.11167169e-04,
#[Out]#          4.37062119e-04,   4.47314697e-04,   4.82660631e-04,
#[Out]#          5.05556461e-04,   5.14623846e-04,   5.21295306e-04,
#[Out]#          5.30971673e-04,   5.46033899e-04,   5.53731350e-04,
#[Out]#          5.75629442e-04,   6.49086764e-04,   6.81988574e-04,
#[Out]#          6.90360451e-04,   7.12864179e-04,   7.53754436e-04,
#[Out]#          7.60852305e-04,   8.07610229e-04,   8.13110998e-04,
#[Out]#          8.14474537e-04,   8.62587918e-04,   8.80582686e-04,
#[Out]#          8.81869799e-04,   9.07700896e-04,   9.23226118e-04,
#[Out]#          9.48969782e-04,   9.49909580e-04,   1.03958275e-03,
#[Out]#          1.11738467e-03,   1.12378762e-03,   1.13223509e-03,
#[Out]#          1.13645812e-03,   1.15929482e-03,   1.19821329e-03,
#[Out]#          1.20566420e-03,   1.23725641e-03,   1.38129079e-03,
#[Out]#          1.41348090e-03,   1.46615384e-03,   1.49422467e-03,
#[Out]#          1.52198426e-03,   1.56289726e-03,   1.82260120e-03,
#[Out]#          1.86377200e-03,   1.90557626e-03,   1.90978009e-03,
#[Out]#          1.93194005e-03,   2.02762237e-03,   2.05963658e-03,
#[Out]#          2.10702654e-03,   2.13890282e-03,   2.15094644e-03,
#[Out]#          2.34722669e-03,   2.36197562e-03,   2.41984198e-03,
#[Out]#          2.53033775e-03,   2.53573455e-03,   2.59716627e-03,
#[Out]#          2.60385007e-03,   2.61857688e-03,   2.70287987e-03,
#[Out]#          2.83724932e-03,   2.85609318e-03,   2.96917116e-03,
#[Out]#          3.20257365e-03,   3.25547448e-03,   3.37335658e-03,
#[Out]#          3.75534691e-03,   3.75563361e-03,   3.77730527e-03,
#[Out]#          3.78880848e-03,   3.87061309e-03,   4.17753093e-03,
#[Out]#          4.37459671e-03,   4.53387608e-03,   4.66464464e-03,
#[Out]#          4.67653138e-03,   4.81140072e-03,   5.14905377e-03,
#[Out]#          5.53240885e-03,   6.61631779e-03,   7.37083973e-03,
#[Out]#          8.35465412e-03,   9.08756978e-03,   9.21323156e-03,
#[Out]#          9.25711296e-03,   1.11234599e-02,   1.21881764e-02,
#[Out]#          1.26952756e-02,   1.27964146e-02,   1.43077689e-02,
#[Out]#          1.47949796e-02,   1.70487464e-02,   1.83447639e-02,
#[Out]#          2.43777919e-02,   2.72998501e-02,   5.09838974e-02,
#[Out]#          5.28268378e-02,   8.15375127e-02,   4.41964831e-01])
# Sun, 27 Sep 2015 02:13:02
temp.reverse()
# Sun, 27 Sep 2015 02:13:09
list(temp).reverse()
# Sun, 27 Sep 2015 02:13:12
_
#[Out]# array([  1.30670829e-04,   2.02022318e-04,   2.29611830e-04,
#[Out]#          2.56020376e-04,   2.65608020e-04,   2.77423664e-04,
#[Out]#          2.97914523e-04,   3.98729258e-04,   4.11167169e-04,
#[Out]#          4.37062119e-04,   4.47314697e-04,   4.82660631e-04,
#[Out]#          5.05556461e-04,   5.14623846e-04,   5.21295306e-04,
#[Out]#          5.30971673e-04,   5.46033899e-04,   5.53731350e-04,
#[Out]#          5.75629442e-04,   6.49086764e-04,   6.81988574e-04,
#[Out]#          6.90360451e-04,   7.12864179e-04,   7.53754436e-04,
#[Out]#          7.60852305e-04,   8.07610229e-04,   8.13110998e-04,
#[Out]#          8.14474537e-04,   8.62587918e-04,   8.80582686e-04,
#[Out]#          8.81869799e-04,   9.07700896e-04,   9.23226118e-04,
#[Out]#          9.48969782e-04,   9.49909580e-04,   1.03958275e-03,
#[Out]#          1.11738467e-03,   1.12378762e-03,   1.13223509e-03,
#[Out]#          1.13645812e-03,   1.15929482e-03,   1.19821329e-03,
#[Out]#          1.20566420e-03,   1.23725641e-03,   1.38129079e-03,
#[Out]#          1.41348090e-03,   1.46615384e-03,   1.49422467e-03,
#[Out]#          1.52198426e-03,   1.56289726e-03,   1.82260120e-03,
#[Out]#          1.86377200e-03,   1.90557626e-03,   1.90978009e-03,
#[Out]#          1.93194005e-03,   2.02762237e-03,   2.05963658e-03,
#[Out]#          2.10702654e-03,   2.13890282e-03,   2.15094644e-03,
#[Out]#          2.34722669e-03,   2.36197562e-03,   2.41984198e-03,
#[Out]#          2.53033775e-03,   2.53573455e-03,   2.59716627e-03,
#[Out]#          2.60385007e-03,   2.61857688e-03,   2.70287987e-03,
#[Out]#          2.83724932e-03,   2.85609318e-03,   2.96917116e-03,
#[Out]#          3.20257365e-03,   3.25547448e-03,   3.37335658e-03,
#[Out]#          3.75534691e-03,   3.75563361e-03,   3.77730527e-03,
#[Out]#          3.78880848e-03,   3.87061309e-03,   4.17753093e-03,
#[Out]#          4.37459671e-03,   4.53387608e-03,   4.66464464e-03,
#[Out]#          4.67653138e-03,   4.81140072e-03,   5.14905377e-03,
#[Out]#          5.53240885e-03,   6.61631779e-03,   7.37083973e-03,
#[Out]#          8.35465412e-03,   9.08756978e-03,   9.21323156e-03,
#[Out]#          9.25711296e-03,   1.11234599e-02,   1.21881764e-02,
#[Out]#          1.26952756e-02,   1.27964146e-02,   1.43077689e-02,
#[Out]#          1.47949796e-02,   1.70487464e-02,   1.83447639e-02,
#[Out]#          2.43777919e-02,   2.72998501e-02,   5.09838974e-02,
#[Out]#          5.28268378e-02,   8.15375127e-02,   4.41964831e-01])
# Sun, 27 Sep 2015 02:13:22
temp=list(temp)
# Sun, 27 Sep 2015 02:13:23
temp
#[Out]# [0.00013067082892140381,
#[Out]#  0.00020202231790879784,
#[Out]#  0.00022961182995188755,
#[Out]#  0.00025602037594930677,
#[Out]#  0.00026560801996828204,
#[Out]#  0.00027742366390731286,
#[Out]#  0.00029791452255448859,
#[Out]#  0.00039872925815505111,
#[Out]#  0.00041116716919429544,
#[Out]#  0.00043706211883904006,
#[Out]#  0.00044731469704016211,
#[Out]#  0.00048266063086820792,
#[Out]#  0.00050555646082315925,
#[Out]#  0.00051462384597232189,
#[Out]#  0.00052129530553082489,
#[Out]#  0.00053097167258171155,
#[Out]#  0.00054603389866892847,
#[Out]#  0.00055373134964798983,
#[Out]#  0.0005756294419753422,
#[Out]#  0.00064908676383868143,
#[Out]#  0.0006819885743686038,
#[Out]#  0.00069036045107968738,
#[Out]#  0.00071286417931450856,
#[Out]#  0.00075375443551529498,
#[Out]#  0.00076085230534933882,
#[Out]#  0.00080761022856102205,
#[Out]#  0.00081311099762849807,
#[Out]#  0.00081447453685789997,
#[Out]#  0.00086258791834983081,
#[Out]#  0.00088058268577424716,
#[Out]#  0.00088186979912618197,
#[Out]#  0.00090770089564042814,
#[Out]#  0.00092322611839460236,
#[Out]#  0.00094896978180574615,
#[Out]#  0.00094990958020013368,
#[Out]#  0.0010395827511020597,
#[Out]#  0.0011173846651903205,
#[Out]#  0.0011237876182720453,
#[Out]#  0.0011322350927577469,
#[Out]#  0.0011364581171867778,
#[Out]#  0.0011592948241941773,
#[Out]#  0.0011982132925744805,
#[Out]#  0.001205664197063016,
#[Out]#  0.0012372564102186504,
#[Out]#  0.0013812907866131174,
#[Out]#  0.0014134808995310299,
#[Out]#  0.0014661538361673279,
#[Out]#  0.0014942246655814959,
#[Out]#  0.0015219842630576847,
#[Out]#  0.0015628972648922118,
#[Out]#  0.0018226012015011826,
#[Out]#  0.0018637719956641654,
#[Out]#  0.0019055762567571199,
#[Out]#  0.0019097800911094935,
#[Out]#  0.0019319400505911014,
#[Out]#  0.0020276223667640746,
#[Out]#  0.0020596365812466755,
#[Out]#  0.0021070265434577043,
#[Out]#  0.0021389028189339968,
#[Out]#  0.0021509464380104217,
#[Out]#  0.0023472266873461592,
#[Out]#  0.0023619756237352641,
#[Out]#  0.0024198419761143884,
#[Out]#  0.0025303377534068401,
#[Out]#  0.0025357345460583157,
#[Out]#  0.0025971662691488466,
#[Out]#  0.0026038500691755763,
#[Out]#  0.0026185768828427543,
#[Out]#  0.0027028798729806952,
#[Out]#  0.0028372493215243462,
#[Out]#  0.0028560931816874291,
#[Out]#  0.0029691711561356865,
#[Out]#  0.0032025736548801282,
#[Out]#  0.0032554744764037289,
#[Out]#  0.0033733565829595557,
#[Out]#  0.0037553469094817869,
#[Out]#  0.0037556336106575289,
#[Out]#  0.003777305272934493,
#[Out]#  0.0037888084799081868,
#[Out]#  0.0038706130932074962,
#[Out]#  0.0041775309254357016,
#[Out]#  0.0043745967145511211,
#[Out]#  0.0045338760758237015,
#[Out]#  0.0046646446431125284,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0048114007201744621,
#[Out]#  0.0051490537739638373,
#[Out]#  0.0055324088460278502,
#[Out]#  0.0066163177909811053,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0083546541240246738,
#[Out]#  0.009087569780794464,
#[Out]#  0.0092132315626342384,
#[Out]#  0.0092571129627260166,
#[Out]#  0.011123459890756639,
#[Out]#  0.012188176351441902,
#[Out]#  0.012695275625702263,
#[Out]#  0.01279641456324768,
#[Out]#  0.014307768932711556,
#[Out]#  0.014794979566331055,
#[Out]#  0.017048746395083705,
#[Out]#  0.018344763916216641,
#[Out]#  0.024377791932052452,
#[Out]#  0.027299850094217849,
#[Out]#  0.050983897362400526,
#[Out]#  0.052826837807277559,
#[Out]#  0.081537512681626734,
#[Out]#  0.44196483074471266]
# Sun, 27 Sep 2015 02:13:29
temp.sort()
# Sun, 27 Sep 2015 02:13:30
temp
#[Out]# [0.00013067082892140381,
#[Out]#  0.00020202231790879784,
#[Out]#  0.00022961182995188755,
#[Out]#  0.00025602037594930677,
#[Out]#  0.00026560801996828204,
#[Out]#  0.00027742366390731286,
#[Out]#  0.00029791452255448859,
#[Out]#  0.00039872925815505111,
#[Out]#  0.00041116716919429544,
#[Out]#  0.00043706211883904006,
#[Out]#  0.00044731469704016211,
#[Out]#  0.00048266063086820792,
#[Out]#  0.00050555646082315925,
#[Out]#  0.00051462384597232189,
#[Out]#  0.00052129530553082489,
#[Out]#  0.00053097167258171155,
#[Out]#  0.00054603389866892847,
#[Out]#  0.00055373134964798983,
#[Out]#  0.0005756294419753422,
#[Out]#  0.00064908676383868143,
#[Out]#  0.0006819885743686038,
#[Out]#  0.00069036045107968738,
#[Out]#  0.00071286417931450856,
#[Out]#  0.00075375443551529498,
#[Out]#  0.00076085230534933882,
#[Out]#  0.00080761022856102205,
#[Out]#  0.00081311099762849807,
#[Out]#  0.00081447453685789997,
#[Out]#  0.00086258791834983081,
#[Out]#  0.00088058268577424716,
#[Out]#  0.00088186979912618197,
#[Out]#  0.00090770089564042814,
#[Out]#  0.00092322611839460236,
#[Out]#  0.00094896978180574615,
#[Out]#  0.00094990958020013368,
#[Out]#  0.0010395827511020597,
#[Out]#  0.0011173846651903205,
#[Out]#  0.0011237876182720453,
#[Out]#  0.0011322350927577469,
#[Out]#  0.0011364581171867778,
#[Out]#  0.0011592948241941773,
#[Out]#  0.0011982132925744805,
#[Out]#  0.001205664197063016,
#[Out]#  0.0012372564102186504,
#[Out]#  0.0013812907866131174,
#[Out]#  0.0014134808995310299,
#[Out]#  0.0014661538361673279,
#[Out]#  0.0014942246655814959,
#[Out]#  0.0015219842630576847,
#[Out]#  0.0015628972648922118,
#[Out]#  0.0018226012015011826,
#[Out]#  0.0018637719956641654,
#[Out]#  0.0019055762567571199,
#[Out]#  0.0019097800911094935,
#[Out]#  0.0019319400505911014,
#[Out]#  0.0020276223667640746,
#[Out]#  0.0020596365812466755,
#[Out]#  0.0021070265434577043,
#[Out]#  0.0021389028189339968,
#[Out]#  0.0021509464380104217,
#[Out]#  0.0023472266873461592,
#[Out]#  0.0023619756237352641,
#[Out]#  0.0024198419761143884,
#[Out]#  0.0025303377534068401,
#[Out]#  0.0025357345460583157,
#[Out]#  0.0025971662691488466,
#[Out]#  0.0026038500691755763,
#[Out]#  0.0026185768828427543,
#[Out]#  0.0027028798729806952,
#[Out]#  0.0028372493215243462,
#[Out]#  0.0028560931816874291,
#[Out]#  0.0029691711561356865,
#[Out]#  0.0032025736548801282,
#[Out]#  0.0032554744764037289,
#[Out]#  0.0033733565829595557,
#[Out]#  0.0037553469094817869,
#[Out]#  0.0037556336106575289,
#[Out]#  0.003777305272934493,
#[Out]#  0.0037888084799081868,
#[Out]#  0.0038706130932074962,
#[Out]#  0.0041775309254357016,
#[Out]#  0.0043745967145511211,
#[Out]#  0.0045338760758237015,
#[Out]#  0.0046646446431125284,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0048114007201744621,
#[Out]#  0.0051490537739638373,
#[Out]#  0.0055324088460278502,
#[Out]#  0.0066163177909811053,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0083546541240246738,
#[Out]#  0.009087569780794464,
#[Out]#  0.0092132315626342384,
#[Out]#  0.0092571129627260166,
#[Out]#  0.011123459890756639,
#[Out]#  0.012188176351441902,
#[Out]#  0.012695275625702263,
#[Out]#  0.01279641456324768,
#[Out]#  0.014307768932711556,
#[Out]#  0.014794979566331055,
#[Out]#  0.017048746395083705,
#[Out]#  0.018344763916216641,
#[Out]#  0.024377791932052452,
#[Out]#  0.027299850094217849,
#[Out]#  0.050983897362400526,
#[Out]#  0.052826837807277559,
#[Out]#  0.081537512681626734,
#[Out]#  0.44196483074471266]
# Sun, 27 Sep 2015 02:13:34
temp.reverse()
# Sun, 27 Sep 2015 02:13:35
temp
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166,
#[Out]#  0.0092132315626342384,
#[Out]#  0.009087569780794464,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0066163177909811053,
#[Out]#  0.0055324088460278502,
#[Out]#  0.0051490537739638373,
#[Out]#  0.0048114007201744621,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0046646446431125284,
#[Out]#  0.0045338760758237015,
#[Out]#  0.0043745967145511211,
#[Out]#  0.0041775309254357016,
#[Out]#  0.0038706130932074962,
#[Out]#  0.0037888084799081868,
#[Out]#  0.003777305272934493,
#[Out]#  0.0037556336106575289,
#[Out]#  0.0037553469094817869,
#[Out]#  0.0033733565829595557,
#[Out]#  0.0032554744764037289,
#[Out]#  0.0032025736548801282,
#[Out]#  0.0029691711561356865,
#[Out]#  0.0028560931816874291,
#[Out]#  0.0028372493215243462,
#[Out]#  0.0027028798729806952,
#[Out]#  0.0026185768828427543,
#[Out]#  0.0026038500691755763,
#[Out]#  0.0025971662691488466,
#[Out]#  0.0025357345460583157,
#[Out]#  0.0025303377534068401,
#[Out]#  0.0024198419761143884,
#[Out]#  0.0023619756237352641,
#[Out]#  0.0023472266873461592,
#[Out]#  0.0021509464380104217,
#[Out]#  0.0021389028189339968,
#[Out]#  0.0021070265434577043,
#[Out]#  0.0020596365812466755,
#[Out]#  0.0020276223667640746,
#[Out]#  0.0019319400505911014,
#[Out]#  0.0019097800911094935,
#[Out]#  0.0019055762567571199,
#[Out]#  0.0018637719956641654,
#[Out]#  0.0018226012015011826,
#[Out]#  0.0015628972648922118,
#[Out]#  0.0015219842630576847,
#[Out]#  0.0014942246655814959,
#[Out]#  0.0014661538361673279,
#[Out]#  0.0014134808995310299,
#[Out]#  0.0013812907866131174,
#[Out]#  0.0012372564102186504,
#[Out]#  0.001205664197063016,
#[Out]#  0.0011982132925744805,
#[Out]#  0.0011592948241941773,
#[Out]#  0.0011364581171867778,
#[Out]#  0.0011322350927577469,
#[Out]#  0.0011237876182720453,
#[Out]#  0.0011173846651903205,
#[Out]#  0.0010395827511020597,
#[Out]#  0.00094990958020013368,
#[Out]#  0.00094896978180574615,
#[Out]#  0.00092322611839460236,
#[Out]#  0.00090770089564042814,
#[Out]#  0.00088186979912618197,
#[Out]#  0.00088058268577424716,
#[Out]#  0.00086258791834983081,
#[Out]#  0.00081447453685789997,
#[Out]#  0.00081311099762849807,
#[Out]#  0.00080761022856102205,
#[Out]#  0.00076085230534933882,
#[Out]#  0.00075375443551529498,
#[Out]#  0.00071286417931450856,
#[Out]#  0.00069036045107968738,
#[Out]#  0.0006819885743686038,
#[Out]#  0.00064908676383868143,
#[Out]#  0.0005756294419753422,
#[Out]#  0.00055373134964798983,
#[Out]#  0.00054603389866892847,
#[Out]#  0.00053097167258171155,
#[Out]#  0.00052129530553082489,
#[Out]#  0.00051462384597232189,
#[Out]#  0.00050555646082315925,
#[Out]#  0.00048266063086820792,
#[Out]#  0.00044731469704016211,
#[Out]#  0.00043706211883904006,
#[Out]#  0.00041116716919429544,
#[Out]#  0.00039872925815505111,
#[Out]#  0.00029791452255448859,
#[Out]#  0.00027742366390731286,
#[Out]#  0.00026560801996828204,
#[Out]#  0.00025602037594930677,
#[Out]#  0.00022961182995188755,
#[Out]#  0.00020202231790879784,
#[Out]#  0.00013067082892140381]
# Sun, 27 Sep 2015 02:13:37
temp[:10]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556]
# Sun, 27 Sep 2015 02:13:40
temp[:20]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166,
#[Out]#  0.0092132315626342384,
#[Out]#  0.009087569780794464,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0066163177909811053]
# Sun, 27 Sep 2015 02:13:46
temp[:30]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166,
#[Out]#  0.0092132315626342384,
#[Out]#  0.009087569780794464,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0066163177909811053,
#[Out]#  0.0055324088460278502,
#[Out]#  0.0051490537739638373,
#[Out]#  0.0048114007201744621,
#[Out]#  0.0046765313752696429,
#[Out]#  0.0046646446431125284,
#[Out]#  0.0045338760758237015,
#[Out]#  0.0043745967145511211,
#[Out]#  0.0041775309254357016,
#[Out]#  0.0038706130932074962,
#[Out]#  0.0037888084799081868]
# Sun, 27 Sep 2015 02:13:48
temp[:20]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166,
#[Out]#  0.0092132315626342384,
#[Out]#  0.009087569780794464,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0066163177909811053]
# Sun, 27 Sep 2015 02:13:54
temp[:15]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166]
# Sun, 27 Sep 2015 02:14:10
temp[:20]
#[Out]# [0.44196483074471266,
#[Out]#  0.081537512681626734,
#[Out]#  0.052826837807277559,
#[Out]#  0.050983897362400526,
#[Out]#  0.027299850094217849,
#[Out]#  0.024377791932052452,
#[Out]#  0.018344763916216641,
#[Out]#  0.017048746395083705,
#[Out]#  0.014794979566331055,
#[Out]#  0.014307768932711556,
#[Out]#  0.01279641456324768,
#[Out]#  0.012695275625702263,
#[Out]#  0.012188176351441902,
#[Out]#  0.011123459890756639,
#[Out]#  0.0092571129627260166,
#[Out]#  0.0092132315626342384,
#[Out]#  0.009087569780794464,
#[Out]#  0.0083546541240246738,
#[Out]#  0.0073708397314112203,
#[Out]#  0.0066163177909811053]
# Sun, 27 Sep 2015 02:15:04
%time rf_inpatient=health.rf_modeling(train,inpatient,"./exog_building_rf_inpatient.txt",100,w)
# Sun, 27 Sep 2015 02:16:46
rf_inpatient.oob
# Sun, 27 Sep 2015 02:16:50
rf_inpatient.oob_score_
#[Out]# 0.4213850375463758
# Sun, 27 Sep 2015 02:16:57
health.rmse?
# Sun, 27 Sep 2015 02:17:20
health.rmse(rf_inpatient,test,inpatient,"./exog_building_rf_inpatient.txt")
#[Out]# ('rmse = 9370.38969647', 'stddev in data = 5881.12927253')
# Sun, 27 Sep 2015 02:18:25
%time rf_inpatient=health.rf_modeling(train,inpatient,"./exog_building_rf_inpatient.txt",100,w)
# Sun, 27 Sep 2015 02:19:11
health.rmse(rf_inpatient,test,inpatient,"./exog_building_rf_inpatient.txt")
#[Out]# ('rmse = 43964.9830554', 'stddev in data = 5881.12927253')
# Sun, 27 Sep 2015 02:20:18
rf_inpatient.oob_score_
#[Out]# 0.41706705421364187
# Sun, 27 Sep 2015 02:21:12
%time rf_inpatient=health.rf_modeling(train,inpatient,"./exog_building_rf_inpatient.txt",100,w)
# Sun, 27 Sep 2015 02:21:51
rf_inpatient.oob_score_
#[Out]# 0.4134003699567308
# Sun, 27 Sep 2015 02:22:01
health.rmse(rf_inpatient,test,inpatient,"./exog_building_rf_inpatient.txt")
#[Out]# ('rmse = 5147.71266037', 'stddev in data = 5881.12927253')
# Sun, 27 Sep 2015 02:23:05
%time rf_inpatient=health.rf_modeling(train,inpatient,"./exog_building_rf_inpatient.txt",100,w)
# Sun, 27 Sep 2015 02:24:13
rf_inpatient.oob_score_
#[Out]# 0.42198883639149676
# Sun, 27 Sep 2015 02:24:15
health.rmse(rf_inpatient,test,inpatient,"./exog_building_rf_inpatient.txt")
#[Out]# ('rmse = 4143.07641084', 'stddev in data = 5881.12927253')
# Sun, 27 Sep 2015 02:25:48
%time rf_er=health.rf_modeling?
# Sun, 27 Sep 2015 02:25:50
%time rf_er=health.rf_modeling?
# Sun, 27 Sep 2015 02:26:11
%time rf_er=health.rf_modeling(train,er,"./exog_building_rf_er.txt",100,w)
# Sun, 27 Sep 2015 02:27:15
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 2156.3761035', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:27:28
rf_er.oob_score_
#[Out]# 0.17946120889497752
# Sun, 27 Sep 2015 02:27:44
temp=rf_er.feature_importances_.copy()
# Sun, 27 Sep 2015 02:27:52
temp=list(temp)
# Sun, 27 Sep 2015 02:27:58
temp.sort()
# Sun, 27 Sep 2015 02:28:01
temp.reverse()
# Sun, 27 Sep 2015 02:28:02
temp
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399,
#[Out]#  0.011447160236560887,
#[Out]#  0.0091813557484593626,
#[Out]#  0.0080460347178868182,
#[Out]#  0.0078850375611500823,
#[Out]#  0.0072303351511553652,
#[Out]#  0.0062662727844820087,
#[Out]#  0.0056703630966819987,
#[Out]#  0.0056089983643181421,
#[Out]#  0.005586547010623456,
#[Out]#  0.0054542235735682322,
#[Out]#  0.0054438283309733134,
#[Out]#  0.0050579590618772177,
#[Out]#  0.004980171163784821,
#[Out]#  0.0048511522248960732,
#[Out]#  0.0047750810858582394,
#[Out]#  0.0046603370925241182,
#[Out]#  0.0043565893502476444,
#[Out]#  0.0041370020177252335,
#[Out]#  0.0038252895330234981,
#[Out]#  0.003519652581705412,
#[Out]#  0.0033028916121709425,
#[Out]#  0.0030374266130809759,
#[Out]#  0.0029567472765642394,
#[Out]#  0.0028651391753410044,
#[Out]#  0.0027801922616989584,
#[Out]#  0.0027051186824372316,
#[Out]#  0.0026961331344867163,
#[Out]#  0.0026917276472152173,
#[Out]#  0.0026556507357517849,
#[Out]#  0.0026368177654629127,
#[Out]#  0.0026314129437834,
#[Out]#  0.0025819568963757781,
#[Out]#  0.0025567992197289891,
#[Out]#  0.0024628044375133388,
#[Out]#  0.0024140540406009624,
#[Out]#  0.0023748911383061669,
#[Out]#  0.0022732819842302579,
#[Out]#  0.0022667292319185251,
#[Out]#  0.002189169338309128,
#[Out]#  0.0021010666645792835,
#[Out]#  0.0020753550692228801,
#[Out]#  0.0019301408864934324,
#[Out]#  0.0019075796671780938,
#[Out]#  0.001889407088362318,
#[Out]#  0.0018692101633624579,
#[Out]#  0.0018552941484999714,
#[Out]#  0.0018198717458853197,
#[Out]#  0.0017150237119232443,
#[Out]#  0.0016053782790997717,
#[Out]#  0.001547542771107071,
#[Out]#  0.0015236076598267839,
#[Out]#  0.0015025243868922122,
#[Out]#  0.0014695317934910607,
#[Out]#  0.0014533536129349108,
#[Out]#  0.001431888760323827,
#[Out]#  0.0014064339777891096,
#[Out]#  0.0013854318591556558,
#[Out]#  0.0013664232171590421,
#[Out]#  0.0013643766090352524,
#[Out]#  0.0013513280000575752,
#[Out]#  0.001344105020908292,
#[Out]#  0.001175835811020163,
#[Out]#  0.0011592781993527087,
#[Out]#  0.0011435936649605554,
#[Out]#  0.0010572611693792345,
#[Out]#  0.00099324663092589891,
#[Out]#  0.00094357215873001887,
#[Out]#  0.0009242668163211322,
#[Out]#  0.00088408390192003044,
#[Out]#  0.00085044610818473031,
#[Out]#  0.00082191210051142313,
#[Out]#  0.0008147646618639361,
#[Out]#  0.00081060453874356715,
#[Out]#  0.00079053711122065347,
#[Out]#  0.00069413695686593017,
#[Out]#  0.00068768242829259931,
#[Out]#  0.00068542772892568897,
#[Out]#  0.00063926793088657887,
#[Out]#  0.00061851877795109013,
#[Out]#  0.00060194532926220018,
#[Out]#  0.00059904384239315095,
#[Out]#  0.00059684260076984369,
#[Out]#  0.00059380520603377282,
#[Out]#  0.00059261310813202826,
#[Out]#  0.00027198423948864383,
#[Out]#  0.00024740665411830518,
#[Out]#  0.00019014294965349245,
#[Out]#  0.00015734489708561748]
# Sun, 27 Sep 2015 02:28:08
temp[:20]
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399]
# Sun, 27 Sep 2015 02:28:15
temp[:30]
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399,
#[Out]#  0.011447160236560887,
#[Out]#  0.0091813557484593626,
#[Out]#  0.0080460347178868182,
#[Out]#  0.0078850375611500823,
#[Out]#  0.0072303351511553652,
#[Out]#  0.0062662727844820087,
#[Out]#  0.0056703630966819987,
#[Out]#  0.0056089983643181421,
#[Out]#  0.005586547010623456,
#[Out]#  0.0054542235735682322]
# Sun, 27 Sep 2015 02:28:27
temp[:25]
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399,
#[Out]#  0.011447160236560887,
#[Out]#  0.0091813557484593626,
#[Out]#  0.0080460347178868182,
#[Out]#  0.0078850375611500823,
#[Out]#  0.0072303351511553652]
# Sun, 27 Sep 2015 02:28:31
temp[:22]
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399,
#[Out]#  0.011447160236560887,
#[Out]#  0.0091813557484593626]
# Sun, 27 Sep 2015 02:28:33
temp[:21]
#[Out]# [0.30452168774902888,
#[Out]#  0.06688694379186394,
#[Out]#  0.052142598670261435,
#[Out]#  0.040216045610947868,
#[Out]#  0.035445954176331541,
#[Out]#  0.027456163569290447,
#[Out]#  0.027053135355783433,
#[Out]#  0.026643420246792372,
#[Out]#  0.023969835231813619,
#[Out]#  0.022056674503978132,
#[Out]#  0.021040663278065534,
#[Out]#  0.018371190730899281,
#[Out]#  0.017155241175254087,
#[Out]#  0.015938947219786212,
#[Out]#  0.01406804422522959,
#[Out]#  0.013468239845709655,
#[Out]#  0.013235237030731492,
#[Out]#  0.01252552853518814,
#[Out]#  0.012470774689650936,
#[Out]#  0.011810898924634399,
#[Out]#  0.011447160236560887]
# Sun, 27 Sep 2015 02:29:04
%time rf_er=health.rf_modeling(train,er,"./exog_building_rf_er.txt",100,w)
# Sun, 27 Sep 2015 02:30:43
rf_er.oob_score_
#[Out]# 0.19985359760086652
# Sun, 27 Sep 2015 02:30:50
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 1094.54725136', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:31:52
%time rf_er=health.rf_modeling(train,er,"./exog_building_rf_er.txt",100,w)
# Sun, 27 Sep 2015 02:33:05
rf_er.oob_score_
#[Out]# 0.19421701104256117
# Sun, 27 Sep 2015 02:33:07
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 1590.9068459', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:33:34
%time rf_er=health.rf_modeling(train,er,"./exog_building_rf_er.txt",100,w)
# Sun, 27 Sep 2015 02:34:26
rf_er.oob_score_
#[Out]# 0.18183554292222481
# Sun, 27 Sep 2015 02:34:28
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 2366.77403465', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:35:31
%time rf_er=health.rf_modeling(train,er,"./exog_building_rf_er.txt",100,w)
# Sun, 27 Sep 2015 02:37:24
rf_er.oob_score_
#[Out]# 0.19569252294347073
# Sun, 27 Sep 2015 02:37:26
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 2024.43833914', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:40:16
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 02:40:59
health.pickle_model(rf_office,"./rf_office_model_final.pkl")
# Sun, 27 Sep 2015 02:41:25
health.pickle_model(rf_outpatient,"./rf_outpatient_model_final.pkl")
# Sun, 27 Sep 2015 02:41:34
health.pickle_model(rf_inpatient,"./rf_inpatient_model_final.pkl")
# Sun, 27 Sep 2015 02:41:44
health.pickle_model(rf_er,"./rf_er_model_final.pkl")
# Sun, 27 Sep 2015 02:43:25
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 02:43:38
temp=health.load_model(office)
# Sun, 27 Sep 2015 02:43:59
temp=health.load_model("office")
# Sun, 27 Sep 2015 02:44:06
temp
#[Out]# RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
#[Out]#            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
#[Out]#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#[Out]#            n_estimators=100, n_jobs=1, oob_score=True, random_state=None,
#[Out]#            verbose=0, warm_start=False)
# Sun, 27 Sep 2015 02:44:24
health.rmse?
# Sun, 27 Sep 2015 02:44:48
health.rmse(temp,test,office,"./exog_building_rf_office.txt")
# Sun, 27 Sep 2015 02:45:05
rf_office
#[Out]# RandomForestRegressor(bootstrap=True, criterion='mse', max_depth=None,
#[Out]#            max_features='auto', max_leaf_nodes=None, min_samples_leaf=1,
#[Out]#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#[Out]#            n_estimators=100, n_jobs=1, oob_score=True, random_state=None,
#[Out]#            verbose=0, warm_start=False)
# Sun, 27 Sep 2015 02:45:35
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
# Sun, 27 Sep 2015 02:45:55
health.rmse(rf_er,test,er,"./exog_building_rf_er.txt")
#[Out]# ('rmse = 2024.43833914', 'stddev in data = 1097.43188921')
# Sun, 27 Sep 2015 02:46:11
health.rmse(rf_outpatient,test,outpatient,"./exog_building_rf_outpatient.txt")
#[Out]# ('rmse = 2157.45708824', 'stddev in data = 1636.96415983')
# Sun, 27 Sep 2015 02:46:21
health.rmse(rf_inpatient,test,inpatient,"./exog_building_rf_inpatient.txt")
#[Out]# ('rmse = 4143.07641084', 'stddev in data = 5881.12927253')
# Sun, 27 Sep 2015 02:46:24
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
# Sun, 27 Sep 2015 02:47:32
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
# Sun, 27 Sep 2015 02:49:41
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 3454.73150162', 'stddev in data = 4157.00593571')
# Sun, 27 Sep 2015 02:49:51
rf_office.oob_score_
#[Out]# 0.29319105450346794
# Sun, 27 Sep 2015 02:50:03
health.pickle_model(rf_office,"./rf_office_model_final.pkl")
# Sun, 27 Sep 2015 02:50:55
temp=rf_office.feature_importances_.copy()
# Sun, 27 Sep 2015 02:50:59
temp.sort()
# Sun, 27 Sep 2015 02:51:04
temp.reverse()
# Sun, 27 Sep 2015 02:51:09
temp=list(temp)
# Sun, 27 Sep 2015 02:51:12
temp.sort()
# Sun, 27 Sep 2015 02:51:15
temp.reverse()
# Sun, 27 Sep 2015 02:51:16
temp
#[Out]# [0.35058221949864754,
#[Out]#  0.16768366577641708,
#[Out]#  0.049223542228644057,
#[Out]#  0.043465525707351425,
#[Out]#  0.031659982563727887,
#[Out]#  0.027684921501225132,
#[Out]#  0.02679922696353302,
#[Out]#  0.022415890767166772,
#[Out]#  0.018891195411333089,
#[Out]#  0.015851597860234764,
#[Out]#  0.014464226172180799,
#[Out]#  0.013181722817257263,
#[Out]#  0.011974802178577413,
#[Out]#  0.0085683930079442708,
#[Out]#  0.0080852837088093016,
#[Out]#  0.0079532638132624872,
#[Out]#  0.0074915610236946225,
#[Out]#  0.007416336328059497,
#[Out]#  0.0068724396400353461,
#[Out]#  0.0068376970336880491,
#[Out]#  0.0068166861282660087,
#[Out]#  0.0067883217552407137,
#[Out]#  0.0063701247993207455,
#[Out]#  0.0057910558254453438,
#[Out]#  0.0054644344399205503,
#[Out]#  0.0051575363255413238,
#[Out]#  0.0046823563315482632,
#[Out]#  0.0042897201806417831,
#[Out]#  0.004023943831845617,
#[Out]#  0.0039633009225639535,
#[Out]#  0.0039444829378636574,
#[Out]#  0.0038669203310115145,
#[Out]#  0.0038563265952696237,
#[Out]#  0.0038263362139630704,
#[Out]#  0.0034387033273660006,
#[Out]#  0.0031280528238112216,
#[Out]#  0.0030991879483175527,
#[Out]#  0.0029663974787342336,
#[Out]#  0.0028594025072280339,
#[Out]#  0.002832770739061088,
#[Out]#  0.0028035304590164024,
#[Out]#  0.0027534160038173339,
#[Out]#  0.0027490204688361912,
#[Out]#  0.0027170572189418258,
#[Out]#  0.0025807819224045519,
#[Out]#  0.0024720302603451717,
#[Out]#  0.0023657225980541785,
#[Out]#  0.0023622908429575653,
#[Out]#  0.0023563220376489054,
#[Out]#  0.0021839752481352167,
#[Out]#  0.0020758964956571519,
#[Out]#  0.0020596088874976636,
#[Out]#  0.0018869169393586677,
#[Out]#  0.0018179638005753154,
#[Out]#  0.0018079748734630753,
#[Out]#  0.0017875472905679621,
#[Out]#  0.0017846490953696956,
#[Out]#  0.0017468032290543776,
#[Out]#  0.0016819562743998543,
#[Out]#  0.0016478157334824466,
#[Out]#  0.0015805464333768485,
#[Out]#  0.0015177808631380069,
#[Out]#  0.0014648798652868334,
#[Out]#  0.0013806249325843125,
#[Out]#  0.0013766090966606237,
#[Out]#  0.0013046116655676863,
#[Out]#  0.0012145784202641698,
#[Out]#  0.0011985587509424651,
#[Out]#  0.0011655399510627827,
#[Out]#  0.0011560486327075986,
#[Out]#  0.0011489162211519664,
#[Out]#  0.0011470565241450605,
#[Out]#  0.0011368038294795229,
#[Out]#  0.0010248330498111953,
#[Out]#  0.0008945753102991864,
#[Out]#  0.00087561584274455627,
#[Out]#  0.00075614737194750468,
#[Out]#  0.00074941120232003406,
#[Out]#  0.00060047253228876548,
#[Out]#  0.00039555437988727111]
# Sun, 27 Sep 2015 02:51:19
temp[:20]
#[Out]# [0.35058221949864754,
#[Out]#  0.16768366577641708,
#[Out]#  0.049223542228644057,
#[Out]#  0.043465525707351425,
#[Out]#  0.031659982563727887,
#[Out]#  0.027684921501225132,
#[Out]#  0.02679922696353302,
#[Out]#  0.022415890767166772,
#[Out]#  0.018891195411333089,
#[Out]#  0.015851597860234764,
#[Out]#  0.014464226172180799,
#[Out]#  0.013181722817257263,
#[Out]#  0.011974802178577413,
#[Out]#  0.0085683930079442708,
#[Out]#  0.0080852837088093016,
#[Out]#  0.0079532638132624872,
#[Out]#  0.0074915610236946225,
#[Out]#  0.007416336328059497,
#[Out]#  0.0068724396400353461,
#[Out]#  0.0068376970336880491]
# Sun, 27 Sep 2015 02:51:26
temp[:15]
#[Out]# [0.35058221949864754,
#[Out]#  0.16768366577641708,
#[Out]#  0.049223542228644057,
#[Out]#  0.043465525707351425,
#[Out]#  0.031659982563727887,
#[Out]#  0.027684921501225132,
#[Out]#  0.02679922696353302,
#[Out]#  0.022415890767166772,
#[Out]#  0.018891195411333089,
#[Out]#  0.015851597860234764,
#[Out]#  0.014464226172180799,
#[Out]#  0.013181722817257263,
#[Out]#  0.011974802178577413,
#[Out]#  0.0085683930079442708,
#[Out]#  0.0080852837088093016]
# Sun, 27 Sep 2015 02:51:30
temp[:13]
#[Out]# [0.35058221949864754,
#[Out]#  0.16768366577641708,
#[Out]#  0.049223542228644057,
#[Out]#  0.043465525707351425,
#[Out]#  0.031659982563727887,
#[Out]#  0.027684921501225132,
#[Out]#  0.02679922696353302,
#[Out]#  0.022415890767166772,
#[Out]#  0.018891195411333089,
#[Out]#  0.015851597860234764,
#[Out]#  0.014464226172180799,
#[Out]#  0.013181722817257263,
#[Out]#  0.011974802178577413]
# Sun, 27 Sep 2015 02:52:14
%time rf_office=health.rf_modeling(train,office,"./exog_building_rf_office.txt",100,w)
# Sun, 27 Sep 2015 02:53:32
rf_office.oob_score_
#[Out]# 0.28112509755560755
# Sun, 27 Sep 2015 02:53:37
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 3504.45542817', 'stddev in data = 4157.00593571')
# Sun, 27 Sep 2015 02:54:00
health.pickle_model(rf_office,"./rf_office_model_final.pkl")
# Sun, 27 Sep 2015 02:56:17
insurance.columns
#[Out]# Index(['Plan ID (standard component)',
#[Out]#        'Medical Deductible-individual-standard',
#[Out]#        'Drug Deductible-individual-standard',
#[Out]#        'Medical Maximum Out Of Pocket - individual - standard',
#[Out]#        'Drug Maximum Out of Pocket - individual - standard',
#[Out]#        'Primary Care Physician  - standard', 'Specialist  - standard',
#[Out]#        'Emergency Room  - standard', 'Inpatient Facility  - standard',
#[Out]#        'Inpatient Physician - standard', 'Generic Drugs - standard',
#[Out]#        'Preferred Brand Drugs - standard',
#[Out]#        'Non-preferred Brand Drugs - standard', 'Specialty Drugs - standard',
#[Out]#        'State', 'County', 'breakout of Primary Care Physician  - standard',
#[Out]#        'breakout of Specialist  - standard',
#[Out]#        'breakout of Inpatient Physician - standard',
#[Out]#        'breakout of Emergency Room  - standard', 'pcp copay before deductible',
#[Out]#        'pcp coinsurance before deductible', 'pcp copay after deductible',
#[Out]#        'pcp coinsurance after deductible',
#[Out]#        'specialist copay before deductible',
#[Out]#        'specialist coinsurance before deductible',
#[Out]#        'specialist copay after deductible',
#[Out]#        'specialist coinsurance after deductible',
#[Out]#        'inpatientdoc copay before deductible',
#[Out]#        'inpatientdoc coinsurance before deductible',
#[Out]#        'inpatientdoc copay after deductible',
#[Out]#        'inpatientdoc coinsurance after deductible',
#[Out]#        'emergency copay before deductible',
#[Out]#        'emergency coinsurance before deductible',
#[Out]#        'emergency copay after deductible',
#[Out]#        'emergency coinsurance after deductible'],
#[Out]#       dtype='object')
# Sun, 27 Sep 2015 03:07:14
all["HIGH BLOOD PRESSURE DIAG (>17)"]
#[Out]# 0        2
#[Out]# 1        2
#[Out]# 2       -1
#[Out]# 3        1
#[Out]# 4        2
#[Out]# 5        2
#[Out]# 6        2
#[Out]# 7        2
#[Out]# 8       -1
#[Out]# 9        2
#[Out]# 10       2
#[Out]# 11       2
#[Out]# 12       2
#[Out]# 13       2
#[Out]# 14      -1
#[Out]# 15      -1
#[Out]# 16      -1
#[Out]# 17       1
#[Out]# 18       1
#[Out]# 19       2
#[Out]# 20       2
#[Out]# 21       2
#[Out]# 22       2
#[Out]# 23      -1
#[Out]# 24      -1
#[Out]# 25      -1
#[Out]# 26      -1
#[Out]# 27       2
#[Out]# 28       1
#[Out]# 29       2
#[Out]#         ..
#[Out]# 36910    2
#[Out]# 36911    2
#[Out]# 36912    1
#[Out]# 36913    1
#[Out]# 36914   -1
#[Out]# 36915   -1
#[Out]# 36916   -1
#[Out]# 36917    1
#[Out]# 36918    2
#[Out]# 36919    2
#[Out]# 36920    1
#[Out]# 36921    2
#[Out]# 36922    1
#[Out]# 36923    1
#[Out]# 36924    2
#[Out]# 36925    2
#[Out]# 36926    2
#[Out]# 36927    2
#[Out]# 36928    2
#[Out]# 36929    2
#[Out]# 36930    2
#[Out]# 36931    2
#[Out]# 36932    2
#[Out]# 36933    2
#[Out]# 36934    1
#[Out]# 36935    2
#[Out]# 36936    1
#[Out]# 36937   -1
#[Out]# 36938    1
#[Out]# 36939    1
#[Out]# Name: HIGH BLOOD PRESSURE DIAG (>17), dtype: int64
# Sun, 27 Sep 2015 03:07:22
all["HIGH BLOOD PRESSURE DIAG (>17)"==1]
# Sun, 27 Sep 2015 03:08:24
all[(all["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (all["DIABETES DIAGNOSIS (>17)]==1) ]
# Sun, 27 Sep 2015 03:08:41
all[(all["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (all["DIABETES DIAGNOSIS (>17)"]==1)]
#[Out]#        DWELLING UNIT ID  PERSON NUMBER  PERSON ID (DUID + PID)  PANEL NUMBER  \
#[Out]# 38                20027            101                20027101            17   
#[Out]# 46                20033            101                20033101            17   
#[Out]# 47                20033            102                20033102            17   
#[Out]# 66                20042            101                20042101            17   
#[Out]# 90                20046            101                20046101            17   
#[Out]# 104               20049            302                20049302            17   
#[Out]# 105               20049            303                20049303            17   
#[Out]# 134               20064            101                20064101            17   
#[Out]# 143               20066            101                20066101            17   
#[Out]# 149               20074            101                20074101            17   
#[Out]# 179               20089            101                20089101            17   
#[Out]# 180               20089            102                20089102            17   
#[Out]# 189               20094            102                20094102            17   
#[Out]# 195               20096            101                20096101            17   
#[Out]# 201               20099            103                20099103            17   
#[Out]# 227               20115            101                20115101            17   
#[Out]# 247               20125            101                20125101            17   
#[Out]# 258               20137            101                20137101            17   
#[Out]# 259               20137            102                20137102            17   
#[Out]# 260               20138            101                20138101            17   
#[Out]# 261               20139            101                20139101            17   
#[Out]# 354               20185            102                20185102            17   
#[Out]# 371               20195            101                20195101            17   
#[Out]# 394               20203            101                20203101            17   
#[Out]# 420               20219            101                20219101            17   
#[Out]# 435               20227            101                20227101            17   
#[Out]# 446               20229            101                20229101            17   
#[Out]# 460               20233            102                20233102            17   
#[Out]# 475               20243            101                20243101            17   
#[Out]# 508               20262            102                20262102            17   
#[Out]# ...                 ...            ...                     ...           ...   
#[Out]# 36482             49446            102                49446102            18   
#[Out]# 36494             49454            101                49454101            18   
#[Out]# 36504             49460            101                49460101            18   
#[Out]# 36512             49463            101                49463101            18   
#[Out]# 36521             49471            101                49471101            18   
#[Out]# 36527             49476            102                49476102            18   
#[Out]# 36533             49479            102                49479102            18   
#[Out]# 36544             49485            101                49485101            18   
#[Out]# 36548             49487            101                49487101            18   
#[Out]# 36582             49506            101                49506101            18   
#[Out]# 36588             49510            101                49510101            18   
#[Out]# 36596             49514            101                49514101            18   
#[Out]# 36627             49532            101                49532101            18   
#[Out]# 36634             49535            103                49535103            18   
#[Out]# 36642             49540            102                49540102            18   
#[Out]# 36652             49548            101                49548101            18   
#[Out]# 36660             49552            101                49552101            18   
#[Out]# 36702             49576            101                49576101            18   
#[Out]# 36706             49576            106                49576106            18   
#[Out]# 36768             49597            101                49597101            18   
#[Out]# 36809             49612            102                49612102            18   
#[Out]# 36832             49622            101                49622101            18   
#[Out]# 36839             49625            101                49625101            18   
#[Out]# 36841             49628            101                49628101            18   
#[Out]# 36866             49643            101                49643101            18   
#[Out]# 36867             49644            101                49644101            18   
#[Out]# 36896             49655            104                49655104            18   
#[Out]# 36900             49657            101                49657101            18   
#[Out]# 36922             49674            101                49674101            18   
#[Out]# 36923             49674            102                49674102            18   
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R3/1  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     0    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R4/2  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     0    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R5/3  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     A    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - 12/31/13 ANNUAL FAMILY IDENTIFIER  \
#[Out]# 38                                          A                        A    
#[Out]# 46                                          A                        A    
#[Out]# 47                                          A                        A    
#[Out]# 66                                          A                        A    
#[Out]# 90                                          A                        A    
#[Out]# 104                                         C                        C    
#[Out]# 105                                         C                        C    
#[Out]# 134                                         A                        A    
#[Out]# 143                                         A                        A    
#[Out]# 149                                         A                        A    
#[Out]# 179                                         A                        A    
#[Out]# 180                                         A                        A    
#[Out]# 189                                         A                        A    
#[Out]# 195                                         A                        A    
#[Out]# 201                                         A                        A    
#[Out]# 227                                         A                        A    
#[Out]# 247                                         A                        A    
#[Out]# 258                                         A                        A    
#[Out]# 259                                         B                        B    
#[Out]# 260                                         A                        A    
#[Out]# 261                                         A                        A    
#[Out]# 354                                         A                        A    
#[Out]# 371                                         A                        A    
#[Out]# 394                                         A                        A    
#[Out]# 420                                         A                        A    
#[Out]# 435                                         A                        A    
#[Out]# 446                                         A                        A    
#[Out]# 460                                         A                        A    
#[Out]# 475                                         A                        A    
#[Out]# 508                                         B                        B    
#[Out]# ...                                        ...                      ...   
#[Out]# 36482                                       A                        A    
#[Out]# 36494                                       A                        A    
#[Out]# 36504                                       A                        A    
#[Out]# 36512                                       A                        A    
#[Out]# 36521                                       A                        A    
#[Out]# 36527                                       A                        A    
#[Out]# 36533                                       A                        A    
#[Out]# 36544                                       A                        A    
#[Out]# 36548                                       A                        A    
#[Out]# 36582                                       A                        A    
#[Out]# 36588                                       A                        A    
#[Out]# 36596                                       A                        A    
#[Out]# 36627                                       A                        A    
#[Out]# 36634                                       A                        A    
#[Out]# 36642                                       B                        B    
#[Out]# 36652                                       A                        A    
#[Out]# 36660                                       A                        A    
#[Out]# 36702                                       A                        A    
#[Out]# 36706                                       A                        A    
#[Out]# 36768                                       A                        A    
#[Out]# 36809                                       A                        A    
#[Out]# 36832                                       A                        A    
#[Out]# 36839                                       A                        A    
#[Out]# 36841                                       A                        A    
#[Out]# 36866                                       A                        A    
#[Out]# 36867                                       A                        A    
#[Out]# 36896                                       A                        A    
#[Out]# 36900                                       A                        A    
#[Out]# 36922                                       A                        A    
#[Out]# 36923                                       A                        A    
#[Out]# 
#[Out]#       CPSFAMID                     ...                       \
#[Out]# 38          A                      ...                        
#[Out]# 46          A                      ...                        
#[Out]# 47          A                      ...                        
#[Out]# 66          A                      ...                        
#[Out]# 90          A                      ...                        
#[Out]# 104         C                      ...                        
#[Out]# 105         C                      ...                        
#[Out]# 134         A                      ...                        
#[Out]# 143         A                      ...                        
#[Out]# 149         A                      ...                        
#[Out]# 179         A                      ...                        
#[Out]# 180         A                      ...                        
#[Out]# 189         A                      ...                        
#[Out]# 195         A                      ...                        
#[Out]# 201         A                      ...                        
#[Out]# 227         A                      ...                        
#[Out]# 247         A                      ...                        
#[Out]# 258         A                      ...                        
#[Out]# 259         B                      ...                        
#[Out]# 260         A                      ...                        
#[Out]# 261         A                      ...                        
#[Out]# 354         A                      ...                        
#[Out]# 371         A                      ...                        
#[Out]# 394         A                      ...                        
#[Out]# 420         A                      ...                        
#[Out]# 435         A                      ...                        
#[Out]# 446         A                      ...                        
#[Out]# 460         A                      ...                        
#[Out]# 475         A                      ...                        
#[Out]# 508         B                      ...                        
#[Out]# ...        ...                     ...                        
#[Out]# 36482       A                      ...                        
#[Out]# 36494       C                      ...                        
#[Out]# 36504       A                      ...                        
#[Out]# 36512       A                      ...                        
#[Out]# 36521       A                      ...                        
#[Out]# 36527       B                      ...                        
#[Out]# 36533       A                      ...                        
#[Out]# 36544       A                      ...                        
#[Out]# 36548       A                      ...                        
#[Out]# 36582       A                      ...                        
#[Out]# 36588       A                      ...                        
#[Out]# 36596       A                      ...                        
#[Out]# 36627       A                      ...                        
#[Out]# 36634       A                      ...                        
#[Out]# 36642       B                      ...                        
#[Out]# 36652       A                      ...                        
#[Out]# 36660       A                      ...                        
#[Out]# 36702       A                      ...                        
#[Out]# 36706       A                      ...                        
#[Out]# 36768       A                      ...                        
#[Out]# 36809       A                      ...                        
#[Out]# 36832       A                      ...                        
#[Out]# 36839       A                      ...                        
#[Out]# 36841       A                      ...                        
#[Out]# 36866       A                      ...                        
#[Out]# 36867       A                      ...                        
#[Out]# 36896       A                      ...                        
#[Out]# 36900       A                      ...                        
#[Out]# 36922       A                      ...                        
#[Out]# 36923       A                      ...                        
#[Out]# 
#[Out]#        WEARS SEAT BELT (>15) - RD 5/3==5  WEARS SEAT BELT (>15) - RD 5/3==4  \
#[Out]# 38                                     0                                  0   
#[Out]# 46                                     0                                  0   
#[Out]# 47                                     0                                  0   
#[Out]# 66                                     0                                  0   
#[Out]# 90                                     0                                  0   
#[Out]# 104                                    0                                  0   
#[Out]# 105                                    0                                  0   
#[Out]# 134                                    0                                  0   
#[Out]# 143                                    0                                  0   
#[Out]# 149                                    0                                  0   
#[Out]# 179                                    0                                  0   
#[Out]# 180                                    0                                  0   
#[Out]# 189                                    0                                  0   
#[Out]# 195                                    0                                  0   
#[Out]# 201                                    0                                  0   
#[Out]# 227                                    0                                  0   
#[Out]# 247                                    0                                  0   
#[Out]# 258                                    0                                  0   
#[Out]# 259                                    0                                  0   
#[Out]# 260                                    0                                  0   
#[Out]# 261                                    0                                  0   
#[Out]# 354                                    0                                  0   
#[Out]# 371                                    0                                  0   
#[Out]# 394                                    0                                  0   
#[Out]# 420                                    0                                  0   
#[Out]# 435                                    0                                  0   
#[Out]# 446                                    0                                  0   
#[Out]# 460                                    0                                  0   
#[Out]# 475                                    0                                  0   
#[Out]# 508                                    0                                  1   
#[Out]# ...                                  ...                                ...   
#[Out]# 36482                                  0                                  0   
#[Out]# 36494                                  0                                  0   
#[Out]# 36504                                  0                                  0   
#[Out]# 36512                                  0                                  0   
#[Out]# 36521                                  0                                  0   
#[Out]# 36527                                  0                                  0   
#[Out]# 36533                                  0                                  0   
#[Out]# 36544                                  0                                  0   
#[Out]# 36548                                  0                                  0   
#[Out]# 36582                                  0                                  0   
#[Out]# 36588                                  0                                  0   
#[Out]# 36596                                  0                                  0   
#[Out]# 36627                                  0                                  0   
#[Out]# 36634                                  0                                  0   
#[Out]# 36642                                  0                                  0   
#[Out]# 36652                                  1                                  0   
#[Out]# 36660                                  0                                  0   
#[Out]# 36702                                  0                                  0   
#[Out]# 36706                                  0                                  0   
#[Out]# 36768                                  0                                  0   
#[Out]# 36809                                  0                                  0   
#[Out]# 36832                                  0                                  0   
#[Out]# 36839                                  0                                  0   
#[Out]# 36841                                  0                                  0   
#[Out]# 36866                                  0                                  0   
#[Out]# 36867                                  0                                  0   
#[Out]# 36896                                  0                                  0   
#[Out]# 36900                                  0                                  0   
#[Out]# 36922                                  0                                  0   
#[Out]# 36923                                  0                                  0   
#[Out]# 
#[Out]#       SAQ: CURRENTLY SMOKE==2 FULL YEAR INSURANCE COVERAGE STATUS 2013==1  \
#[Out]# 38                          1                                           0   
#[Out]# 46                          0                                           1   
#[Out]# 47                          0                                           1   
#[Out]# 66                          0                                           0   
#[Out]# 90                          0                                           0   
#[Out]# 104                         0                                           1   
#[Out]# 105                         0                                           1   
#[Out]# 134                         0                                           0   
#[Out]# 143                         1                                           0   
#[Out]# 149                         1                                           0   
#[Out]# 179                         1                                           0   
#[Out]# 180                         1                                           0   
#[Out]# 189                         1                                           0   
#[Out]# 195                         1                                           0   
#[Out]# 201                         1                                           0   
#[Out]# 227                         1                                           1   
#[Out]# 247                         1                                           0   
#[Out]# 258                         1                                           0   
#[Out]# 259                         0                                           0   
#[Out]# 260                         0                                           0   
#[Out]# 261                         1                                           0   
#[Out]# 354                         1                                           0   
#[Out]# 371                         1                                           0   
#[Out]# 394                         1                                           0   
#[Out]# 420                         1                                           0   
#[Out]# 435                         0                                           1   
#[Out]# 446                         0                                           0   
#[Out]# 460                         1                                           0   
#[Out]# 475                         1                                           0   
#[Out]# 508                         1                                           0   
#[Out]# ...                       ...                                         ...   
#[Out]# 36482                       1                                           0   
#[Out]# 36494                       0                                           0   
#[Out]# 36504                       1                                           1   
#[Out]# 36512                       1                                           0   
#[Out]# 36521                       0                                           0   
#[Out]# 36527                       0                                           0   
#[Out]# 36533                       0                                           1   
#[Out]# 36544                       0                                           1   
#[Out]# 36548                       1                                           0   
#[Out]# 36582                       1                                           0   
#[Out]# 36588                       1                                           0   
#[Out]# 36596                       1                                           0   
#[Out]# 36627                       1                                           0   
#[Out]# 36634                       0                                           0   
#[Out]# 36642                       1                                           0   
#[Out]# 36652                       1                                           0   
#[Out]# 36660                       0                                           1   
#[Out]# 36702                       1                                           0   
#[Out]# 36706                       1                                           0   
#[Out]# 36768                       1                                           0   
#[Out]# 36809                       1                                           0   
#[Out]# 36832                       1                                           0   
#[Out]# 36839                       0                                           0   
#[Out]# 36841                       1                                           1   
#[Out]# 36866                       0                                           0   
#[Out]# 36867                       1                                           0   
#[Out]# 36896                       1                                           0   
#[Out]# 36900                       0                                           0   
#[Out]# 36922                       1                                           0   
#[Out]# 36923                       1                                           1   
#[Out]# 
#[Out]#       FULL YEAR INSURANCE COVERAGE STATUS 2013==2  \
#[Out]# 38                                              0   
#[Out]# 46                                              0   
#[Out]# 47                                              0   
#[Out]# 66                                              0   
#[Out]# 90                                              1   
#[Out]# 104                                             0   
#[Out]# 105                                             0   
#[Out]# 134                                             0   
#[Out]# 143                                             0   
#[Out]# 149                                             0   
#[Out]# 179                                             0   
#[Out]# 180                                             0   
#[Out]# 189                                             0   
#[Out]# 195                                             1   
#[Out]# 201                                             0   
#[Out]# 227                                             0   
#[Out]# 247                                             0   
#[Out]# 258                                             1   
#[Out]# 259                                             0   
#[Out]# 260                                             0   
#[Out]# 261                                             0   
#[Out]# 354                                             0   
#[Out]# 371                                             1   
#[Out]# 394                                             0   
#[Out]# 420                                             0   
#[Out]# 435                                             0   
#[Out]# 446                                             1   
#[Out]# 460                                             0   
#[Out]# 475                                             1   
#[Out]# 508                                             1   
#[Out]# ...                                           ...   
#[Out]# 36482                                           0   
#[Out]# 36494                                           0   
#[Out]# 36504                                           0   
#[Out]# 36512                                           0   
#[Out]# 36521                                           0   
#[Out]# 36527                                           0   
#[Out]# 36533                                           0   
#[Out]# 36544                                           0   
#[Out]# 36548                                           0   
#[Out]# 36582                                           0   
#[Out]# 36588                                           0   
#[Out]# 36596                                           0   
#[Out]# 36627                                           0   
#[Out]# 36634                                           0   
#[Out]# 36642                                           0   
#[Out]# 36652                                           0   
#[Out]# 36660                                           0   
#[Out]# 36702                                           1   
#[Out]# 36706                                           1   
#[Out]# 36768                                           0   
#[Out]# 36809                                           1   
#[Out]# 36832                                           0   
#[Out]# 36839                                           0   
#[Out]# 36841                                           0   
#[Out]# 36866                                           1   
#[Out]# 36867                                           0   
#[Out]# 36896                                           1   
#[Out]# 36900                                           1   
#[Out]# 36922                                           0   
#[Out]# 36923                                           0   
#[Out]# 
#[Out]#       FULL YEAR INSURANCE COVERAGE STATUS 2013==3  \
#[Out]# 38                                              0   
#[Out]# 46                                              0   
#[Out]# 47                                              0   
#[Out]# 66                                              0   
#[Out]# 90                                              0   
#[Out]# 104                                             0   
#[Out]# 105                                             0   
#[Out]# 134                                             0   
#[Out]# 143                                             0   
#[Out]# 149                                             0   
#[Out]# 179                                             0   
#[Out]# 180                                             0   
#[Out]# 189                                             0   
#[Out]# 195                                             0   
#[Out]# 201                                             0   
#[Out]# 227                                             0   
#[Out]# 247                                             1   
#[Out]# 258                                             0   
#[Out]# 259                                             1   
#[Out]# 260                                             0   
#[Out]# 261                                             0   
#[Out]# 354                                             1   
#[Out]# 371                                             0   
#[Out]# 394                                             1   
#[Out]# 420                                             0   
#[Out]# 435                                             0   
#[Out]# 446                                             0   
#[Out]# 460                                             0   
#[Out]# 475                                             0   
#[Out]# 508                                             0   
#[Out]# ...                                           ...   
#[Out]# 36482                                           1   
#[Out]# 36494                                           1   
#[Out]# 36504                                           0   
#[Out]# 36512                                           1   
#[Out]# 36521                                           0   
#[Out]# 36527                                           1   
#[Out]# 36533                                           0   
#[Out]# 36544                                           0   
#[Out]# 36548                                           0   
#[Out]# 36582                                           0   
#[Out]# 36588                                           0   
#[Out]# 36596                                           0   
#[Out]# 36627                                           1   
#[Out]# 36634                                           0   
#[Out]# 36642                                           1   
#[Out]# 36652                                           0   
#[Out]# 36660                                           0   
#[Out]# 36702                                           0   
#[Out]# 36706                                           0   
#[Out]# 36768                                           0   
#[Out]# 36809                                           0   
#[Out]# 36832                                           0   
#[Out]# 36839                                           0   
#[Out]# 36841                                           0   
#[Out]# 36866                                           0   
#[Out]# 36867                                           0   
#[Out]# 36896                                           0   
#[Out]# 36900                                           0   
#[Out]# 36922                                           1   
#[Out]# 36923                                           0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==6  \
#[Out]# 38                                               0   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               0   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              0   
#[Out]# 143                                              0   
#[Out]# 149                                              0   
#[Out]# 179                                              0   
#[Out]# 180                                              0   
#[Out]# 189                                              0   
#[Out]# 195                                              0   
#[Out]# 201                                              0   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              0   
#[Out]# 261                                              0   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              0   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              1   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            0   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            1   
#[Out]# 36582                                            0   
#[Out]# 36588                                            0   
#[Out]# 36596                                            0   
#[Out]# 36627                                            0   
#[Out]# 36634                                            0   
#[Out]# 36642                                            0   
#[Out]# 36652                                            0   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            0   
#[Out]# 36809                                            0   
#[Out]# 36832                                            0   
#[Out]# 36839                                            1   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            1   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==4  \
#[Out]# 38                                               0   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               0   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              1   
#[Out]# 143                                              1   
#[Out]# 149                                              0   
#[Out]# 179                                              1   
#[Out]# 180                                              1   
#[Out]# 189                                              1   
#[Out]# 195                                              0   
#[Out]# 201                                              1   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              1   
#[Out]# 261                                              0   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              0   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              0   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            1   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            0   
#[Out]# 36582                                            0   
#[Out]# 36588                                            0   
#[Out]# 36596                                            0   
#[Out]# 36627                                            0   
#[Out]# 36634                                            0   
#[Out]# 36642                                            0   
#[Out]# 36652                                            0   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            1   
#[Out]# 36809                                            0   
#[Out]# 36832                                            1   
#[Out]# 36839                                            0   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            0   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==5  \
#[Out]# 38                                               1   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               1   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              0   
#[Out]# 143                                              0   
#[Out]# 149                                              1   
#[Out]# 179                                              0   
#[Out]# 180                                              0   
#[Out]# 189                                              0   
#[Out]# 195                                              0   
#[Out]# 201                                              0   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              0   
#[Out]# 261                                              1   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              1   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              0   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            0   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            0   
#[Out]# 36582                                            1   
#[Out]# 36588                                            1   
#[Out]# 36596                                            1   
#[Out]# 36627                                            0   
#[Out]# 36634                                            1   
#[Out]# 36642                                            0   
#[Out]# 36652                                            1   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            0   
#[Out]# 36809                                            0   
#[Out]# 36832                                            0   
#[Out]# 36839                                            0   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            0   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==7  
#[Out]# 38                                               0  
#[Out]# 46                                               0  
#[Out]# 47                                               0  
#[Out]# 66                                               0  
#[Out]# 90                                               0  
#[Out]# 104                                              0  
#[Out]# 105                                              0  
#[Out]# 134                                              0  
#[Out]# 143                                              0  
#[Out]# 149                                              0  
#[Out]# 179                                              0  
#[Out]# 180                                              0  
#[Out]# 189                                              0  
#[Out]# 195                                              0  
#[Out]# 201                                              0  
#[Out]# 227                                              0  
#[Out]# 247                                              0  
#[Out]# 258                                              0  
#[Out]# 259                                              0  
#[Out]# 260                                              0  
#[Out]# 261                                              0  
#[Out]# 354                                              0  
#[Out]# 371                                              0  
#[Out]# 394                                              0  
#[Out]# 420                                              0  
#[Out]# 435                                              0  
#[Out]# 446                                              0  
#[Out]# 460                                              0  
#[Out]# 475                                              0  
#[Out]# 508                                              0  
#[Out]# ...                                            ...  
#[Out]# 36482                                            0  
#[Out]# 36494                                            0  
#[Out]# 36504                                            0  
#[Out]# 36512                                            0  
#[Out]# 36521                                            0  
#[Out]# 36527                                            0  
#[Out]# 36533                                            0  
#[Out]# 36544                                            0  
#[Out]# 36548                                            0  
#[Out]# 36582                                            0  
#[Out]# 36588                                            0  
#[Out]# 36596                                            0  
#[Out]# 36627                                            0  
#[Out]# 36634                                            0  
#[Out]# 36642                                            0  
#[Out]# 36652                                            0  
#[Out]# 36660                                            0  
#[Out]# 36702                                            0  
#[Out]# 36706                                            0  
#[Out]# 36768                                            0  
#[Out]# 36809                                            0  
#[Out]# 36832                                            0  
#[Out]# 36839                                            0  
#[Out]# 36841                                            0  
#[Out]# 36866                                            0  
#[Out]# 36867                                            0  
#[Out]# 36896                                            0  
#[Out]# 36900                                            0  
#[Out]# 36922                                            0  
#[Out]# 36923                                            0  
#[Out]# 
#[Out]# [2152 rows x 1837 columns]
# Sun, 27 Sep 2015 03:09:32
X
# Sun, 27 Sep 2015 03:09:35
X=_
# Sun, 27 Sep 2015 03:09:36
X
#[Out]#        DWELLING UNIT ID  PERSON NUMBER  PERSON ID (DUID + PID)  PANEL NUMBER  \
#[Out]# 38                20027            101                20027101            17   
#[Out]# 46                20033            101                20033101            17   
#[Out]# 47                20033            102                20033102            17   
#[Out]# 66                20042            101                20042101            17   
#[Out]# 90                20046            101                20046101            17   
#[Out]# 104               20049            302                20049302            17   
#[Out]# 105               20049            303                20049303            17   
#[Out]# 134               20064            101                20064101            17   
#[Out]# 143               20066            101                20066101            17   
#[Out]# 149               20074            101                20074101            17   
#[Out]# 179               20089            101                20089101            17   
#[Out]# 180               20089            102                20089102            17   
#[Out]# 189               20094            102                20094102            17   
#[Out]# 195               20096            101                20096101            17   
#[Out]# 201               20099            103                20099103            17   
#[Out]# 227               20115            101                20115101            17   
#[Out]# 247               20125            101                20125101            17   
#[Out]# 258               20137            101                20137101            17   
#[Out]# 259               20137            102                20137102            17   
#[Out]# 260               20138            101                20138101            17   
#[Out]# 261               20139            101                20139101            17   
#[Out]# 354               20185            102                20185102            17   
#[Out]# 371               20195            101                20195101            17   
#[Out]# 394               20203            101                20203101            17   
#[Out]# 420               20219            101                20219101            17   
#[Out]# 435               20227            101                20227101            17   
#[Out]# 446               20229            101                20229101            17   
#[Out]# 460               20233            102                20233102            17   
#[Out]# 475               20243            101                20243101            17   
#[Out]# 508               20262            102                20262102            17   
#[Out]# ...                 ...            ...                     ...           ...   
#[Out]# 36482             49446            102                49446102            18   
#[Out]# 36494             49454            101                49454101            18   
#[Out]# 36504             49460            101                49460101            18   
#[Out]# 36512             49463            101                49463101            18   
#[Out]# 36521             49471            101                49471101            18   
#[Out]# 36527             49476            102                49476102            18   
#[Out]# 36533             49479            102                49479102            18   
#[Out]# 36544             49485            101                49485101            18   
#[Out]# 36548             49487            101                49487101            18   
#[Out]# 36582             49506            101                49506101            18   
#[Out]# 36588             49510            101                49510101            18   
#[Out]# 36596             49514            101                49514101            18   
#[Out]# 36627             49532            101                49532101            18   
#[Out]# 36634             49535            103                49535103            18   
#[Out]# 36642             49540            102                49540102            18   
#[Out]# 36652             49548            101                49548101            18   
#[Out]# 36660             49552            101                49552101            18   
#[Out]# 36702             49576            101                49576101            18   
#[Out]# 36706             49576            106                49576106            18   
#[Out]# 36768             49597            101                49597101            18   
#[Out]# 36809             49612            102                49612102            18   
#[Out]# 36832             49622            101                49622101            18   
#[Out]# 36839             49625            101                49625101            18   
#[Out]# 36841             49628            101                49628101            18   
#[Out]# 36866             49643            101                49643101            18   
#[Out]# 36867             49644            101                49644101            18   
#[Out]# 36896             49655            104                49655104            18   
#[Out]# 36900             49657            101                49657101            18   
#[Out]# 36922             49674            101                49674101            18   
#[Out]# 36923             49674            102                49674102            18   
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R3/1  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     0    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R4/2  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     0    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - R5/3  \
#[Out]# 38                                      A    
#[Out]# 46                                      A    
#[Out]# 47                                      A    
#[Out]# 66                                      A    
#[Out]# 90                                      A    
#[Out]# 104                                     C    
#[Out]# 105                                     C    
#[Out]# 134                                     A    
#[Out]# 143                                     A    
#[Out]# 149                                     A    
#[Out]# 179                                     A    
#[Out]# 180                                     A    
#[Out]# 189                                     A    
#[Out]# 195                                     A    
#[Out]# 201                                     A    
#[Out]# 227                                     A    
#[Out]# 247                                     A    
#[Out]# 258                                     A    
#[Out]# 259                                     B    
#[Out]# 260                                     A    
#[Out]# 261                                     A    
#[Out]# 354                                     A    
#[Out]# 371                                     A    
#[Out]# 394                                     A    
#[Out]# 420                                     A    
#[Out]# 435                                     A    
#[Out]# 446                                     A    
#[Out]# 460                                     A    
#[Out]# 475                                     A    
#[Out]# 508                                     B    
#[Out]# ...                                    ...   
#[Out]# 36482                                   A    
#[Out]# 36494                                   A    
#[Out]# 36504                                   A    
#[Out]# 36512                                   A    
#[Out]# 36521                                   A    
#[Out]# 36527                                   A    
#[Out]# 36533                                   A    
#[Out]# 36544                                   A    
#[Out]# 36548                                   A    
#[Out]# 36582                                   A    
#[Out]# 36588                                   A    
#[Out]# 36596                                   A    
#[Out]# 36627                                   A    
#[Out]# 36634                                   A    
#[Out]# 36642                                   B    
#[Out]# 36652                                   A    
#[Out]# 36660                                   A    
#[Out]# 36702                                   A    
#[Out]# 36706                                   A    
#[Out]# 36768                                   A    
#[Out]# 36809                                   A    
#[Out]# 36832                                   A    
#[Out]# 36839                                   A    
#[Out]# 36841                                   A    
#[Out]# 36866                                   A    
#[Out]# 36867                                   A    
#[Out]# 36896                                   A    
#[Out]# 36900                                   A    
#[Out]# 36922                                   A    
#[Out]# 36923                                   A    
#[Out]# 
#[Out]#       FAMILY ID (STUDENT MERGED IN) - 12/31/13 ANNUAL FAMILY IDENTIFIER  \
#[Out]# 38                                          A                        A    
#[Out]# 46                                          A                        A    
#[Out]# 47                                          A                        A    
#[Out]# 66                                          A                        A    
#[Out]# 90                                          A                        A    
#[Out]# 104                                         C                        C    
#[Out]# 105                                         C                        C    
#[Out]# 134                                         A                        A    
#[Out]# 143                                         A                        A    
#[Out]# 149                                         A                        A    
#[Out]# 179                                         A                        A    
#[Out]# 180                                         A                        A    
#[Out]# 189                                         A                        A    
#[Out]# 195                                         A                        A    
#[Out]# 201                                         A                        A    
#[Out]# 227                                         A                        A    
#[Out]# 247                                         A                        A    
#[Out]# 258                                         A                        A    
#[Out]# 259                                         B                        B    
#[Out]# 260                                         A                        A    
#[Out]# 261                                         A                        A    
#[Out]# 354                                         A                        A    
#[Out]# 371                                         A                        A    
#[Out]# 394                                         A                        A    
#[Out]# 420                                         A                        A    
#[Out]# 435                                         A                        A    
#[Out]# 446                                         A                        A    
#[Out]# 460                                         A                        A    
#[Out]# 475                                         A                        A    
#[Out]# 508                                         B                        B    
#[Out]# ...                                        ...                      ...   
#[Out]# 36482                                       A                        A    
#[Out]# 36494                                       A                        A    
#[Out]# 36504                                       A                        A    
#[Out]# 36512                                       A                        A    
#[Out]# 36521                                       A                        A    
#[Out]# 36527                                       A                        A    
#[Out]# 36533                                       A                        A    
#[Out]# 36544                                       A                        A    
#[Out]# 36548                                       A                        A    
#[Out]# 36582                                       A                        A    
#[Out]# 36588                                       A                        A    
#[Out]# 36596                                       A                        A    
#[Out]# 36627                                       A                        A    
#[Out]# 36634                                       A                        A    
#[Out]# 36642                                       B                        B    
#[Out]# 36652                                       A                        A    
#[Out]# 36660                                       A                        A    
#[Out]# 36702                                       A                        A    
#[Out]# 36706                                       A                        A    
#[Out]# 36768                                       A                        A    
#[Out]# 36809                                       A                        A    
#[Out]# 36832                                       A                        A    
#[Out]# 36839                                       A                        A    
#[Out]# 36841                                       A                        A    
#[Out]# 36866                                       A                        A    
#[Out]# 36867                                       A                        A    
#[Out]# 36896                                       A                        A    
#[Out]# 36900                                       A                        A    
#[Out]# 36922                                       A                        A    
#[Out]# 36923                                       A                        A    
#[Out]# 
#[Out]#       CPSFAMID                     ...                       \
#[Out]# 38          A                      ...                        
#[Out]# 46          A                      ...                        
#[Out]# 47          A                      ...                        
#[Out]# 66          A                      ...                        
#[Out]# 90          A                      ...                        
#[Out]# 104         C                      ...                        
#[Out]# 105         C                      ...                        
#[Out]# 134         A                      ...                        
#[Out]# 143         A                      ...                        
#[Out]# 149         A                      ...                        
#[Out]# 179         A                      ...                        
#[Out]# 180         A                      ...                        
#[Out]# 189         A                      ...                        
#[Out]# 195         A                      ...                        
#[Out]# 201         A                      ...                        
#[Out]# 227         A                      ...                        
#[Out]# 247         A                      ...                        
#[Out]# 258         A                      ...                        
#[Out]# 259         B                      ...                        
#[Out]# 260         A                      ...                        
#[Out]# 261         A                      ...                        
#[Out]# 354         A                      ...                        
#[Out]# 371         A                      ...                        
#[Out]# 394         A                      ...                        
#[Out]# 420         A                      ...                        
#[Out]# 435         A                      ...                        
#[Out]# 446         A                      ...                        
#[Out]# 460         A                      ...                        
#[Out]# 475         A                      ...                        
#[Out]# 508         B                      ...                        
#[Out]# ...        ...                     ...                        
#[Out]# 36482       A                      ...                        
#[Out]# 36494       C                      ...                        
#[Out]# 36504       A                      ...                        
#[Out]# 36512       A                      ...                        
#[Out]# 36521       A                      ...                        
#[Out]# 36527       B                      ...                        
#[Out]# 36533       A                      ...                        
#[Out]# 36544       A                      ...                        
#[Out]# 36548       A                      ...                        
#[Out]# 36582       A                      ...                        
#[Out]# 36588       A                      ...                        
#[Out]# 36596       A                      ...                        
#[Out]# 36627       A                      ...                        
#[Out]# 36634       A                      ...                        
#[Out]# 36642       B                      ...                        
#[Out]# 36652       A                      ...                        
#[Out]# 36660       A                      ...                        
#[Out]# 36702       A                      ...                        
#[Out]# 36706       A                      ...                        
#[Out]# 36768       A                      ...                        
#[Out]# 36809       A                      ...                        
#[Out]# 36832       A                      ...                        
#[Out]# 36839       A                      ...                        
#[Out]# 36841       A                      ...                        
#[Out]# 36866       A                      ...                        
#[Out]# 36867       A                      ...                        
#[Out]# 36896       A                      ...                        
#[Out]# 36900       A                      ...                        
#[Out]# 36922       A                      ...                        
#[Out]# 36923       A                      ...                        
#[Out]# 
#[Out]#        WEARS SEAT BELT (>15) - RD 5/3==5  WEARS SEAT BELT (>15) - RD 5/3==4  \
#[Out]# 38                                     0                                  0   
#[Out]# 46                                     0                                  0   
#[Out]# 47                                     0                                  0   
#[Out]# 66                                     0                                  0   
#[Out]# 90                                     0                                  0   
#[Out]# 104                                    0                                  0   
#[Out]# 105                                    0                                  0   
#[Out]# 134                                    0                                  0   
#[Out]# 143                                    0                                  0   
#[Out]# 149                                    0                                  0   
#[Out]# 179                                    0                                  0   
#[Out]# 180                                    0                                  0   
#[Out]# 189                                    0                                  0   
#[Out]# 195                                    0                                  0   
#[Out]# 201                                    0                                  0   
#[Out]# 227                                    0                                  0   
#[Out]# 247                                    0                                  0   
#[Out]# 258                                    0                                  0   
#[Out]# 259                                    0                                  0   
#[Out]# 260                                    0                                  0   
#[Out]# 261                                    0                                  0   
#[Out]# 354                                    0                                  0   
#[Out]# 371                                    0                                  0   
#[Out]# 394                                    0                                  0   
#[Out]# 420                                    0                                  0   
#[Out]# 435                                    0                                  0   
#[Out]# 446                                    0                                  0   
#[Out]# 460                                    0                                  0   
#[Out]# 475                                    0                                  0   
#[Out]# 508                                    0                                  1   
#[Out]# ...                                  ...                                ...   
#[Out]# 36482                                  0                                  0   
#[Out]# 36494                                  0                                  0   
#[Out]# 36504                                  0                                  0   
#[Out]# 36512                                  0                                  0   
#[Out]# 36521                                  0                                  0   
#[Out]# 36527                                  0                                  0   
#[Out]# 36533                                  0                                  0   
#[Out]# 36544                                  0                                  0   
#[Out]# 36548                                  0                                  0   
#[Out]# 36582                                  0                                  0   
#[Out]# 36588                                  0                                  0   
#[Out]# 36596                                  0                                  0   
#[Out]# 36627                                  0                                  0   
#[Out]# 36634                                  0                                  0   
#[Out]# 36642                                  0                                  0   
#[Out]# 36652                                  1                                  0   
#[Out]# 36660                                  0                                  0   
#[Out]# 36702                                  0                                  0   
#[Out]# 36706                                  0                                  0   
#[Out]# 36768                                  0                                  0   
#[Out]# 36809                                  0                                  0   
#[Out]# 36832                                  0                                  0   
#[Out]# 36839                                  0                                  0   
#[Out]# 36841                                  0                                  0   
#[Out]# 36866                                  0                                  0   
#[Out]# 36867                                  0                                  0   
#[Out]# 36896                                  0                                  0   
#[Out]# 36900                                  0                                  0   
#[Out]# 36922                                  0                                  0   
#[Out]# 36923                                  0                                  0   
#[Out]# 
#[Out]#       SAQ: CURRENTLY SMOKE==2 FULL YEAR INSURANCE COVERAGE STATUS 2013==1  \
#[Out]# 38                          1                                           0   
#[Out]# 46                          0                                           1   
#[Out]# 47                          0                                           1   
#[Out]# 66                          0                                           0   
#[Out]# 90                          0                                           0   
#[Out]# 104                         0                                           1   
#[Out]# 105                         0                                           1   
#[Out]# 134                         0                                           0   
#[Out]# 143                         1                                           0   
#[Out]# 149                         1                                           0   
#[Out]# 179                         1                                           0   
#[Out]# 180                         1                                           0   
#[Out]# 189                         1                                           0   
#[Out]# 195                         1                                           0   
#[Out]# 201                         1                                           0   
#[Out]# 227                         1                                           1   
#[Out]# 247                         1                                           0   
#[Out]# 258                         1                                           0   
#[Out]# 259                         0                                           0   
#[Out]# 260                         0                                           0   
#[Out]# 261                         1                                           0   
#[Out]# 354                         1                                           0   
#[Out]# 371                         1                                           0   
#[Out]# 394                         1                                           0   
#[Out]# 420                         1                                           0   
#[Out]# 435                         0                                           1   
#[Out]# 446                         0                                           0   
#[Out]# 460                         1                                           0   
#[Out]# 475                         1                                           0   
#[Out]# 508                         1                                           0   
#[Out]# ...                       ...                                         ...   
#[Out]# 36482                       1                                           0   
#[Out]# 36494                       0                                           0   
#[Out]# 36504                       1                                           1   
#[Out]# 36512                       1                                           0   
#[Out]# 36521                       0                                           0   
#[Out]# 36527                       0                                           0   
#[Out]# 36533                       0                                           1   
#[Out]# 36544                       0                                           1   
#[Out]# 36548                       1                                           0   
#[Out]# 36582                       1                                           0   
#[Out]# 36588                       1                                           0   
#[Out]# 36596                       1                                           0   
#[Out]# 36627                       1                                           0   
#[Out]# 36634                       0                                           0   
#[Out]# 36642                       1                                           0   
#[Out]# 36652                       1                                           0   
#[Out]# 36660                       0                                           1   
#[Out]# 36702                       1                                           0   
#[Out]# 36706                       1                                           0   
#[Out]# 36768                       1                                           0   
#[Out]# 36809                       1                                           0   
#[Out]# 36832                       1                                           0   
#[Out]# 36839                       0                                           0   
#[Out]# 36841                       1                                           1   
#[Out]# 36866                       0                                           0   
#[Out]# 36867                       1                                           0   
#[Out]# 36896                       1                                           0   
#[Out]# 36900                       0                                           0   
#[Out]# 36922                       1                                           0   
#[Out]# 36923                       1                                           1   
#[Out]# 
#[Out]#       FULL YEAR INSURANCE COVERAGE STATUS 2013==2  \
#[Out]# 38                                              0   
#[Out]# 46                                              0   
#[Out]# 47                                              0   
#[Out]# 66                                              0   
#[Out]# 90                                              1   
#[Out]# 104                                             0   
#[Out]# 105                                             0   
#[Out]# 134                                             0   
#[Out]# 143                                             0   
#[Out]# 149                                             0   
#[Out]# 179                                             0   
#[Out]# 180                                             0   
#[Out]# 189                                             0   
#[Out]# 195                                             1   
#[Out]# 201                                             0   
#[Out]# 227                                             0   
#[Out]# 247                                             0   
#[Out]# 258                                             1   
#[Out]# 259                                             0   
#[Out]# 260                                             0   
#[Out]# 261                                             0   
#[Out]# 354                                             0   
#[Out]# 371                                             1   
#[Out]# 394                                             0   
#[Out]# 420                                             0   
#[Out]# 435                                             0   
#[Out]# 446                                             1   
#[Out]# 460                                             0   
#[Out]# 475                                             1   
#[Out]# 508                                             1   
#[Out]# ...                                           ...   
#[Out]# 36482                                           0   
#[Out]# 36494                                           0   
#[Out]# 36504                                           0   
#[Out]# 36512                                           0   
#[Out]# 36521                                           0   
#[Out]# 36527                                           0   
#[Out]# 36533                                           0   
#[Out]# 36544                                           0   
#[Out]# 36548                                           0   
#[Out]# 36582                                           0   
#[Out]# 36588                                           0   
#[Out]# 36596                                           0   
#[Out]# 36627                                           0   
#[Out]# 36634                                           0   
#[Out]# 36642                                           0   
#[Out]# 36652                                           0   
#[Out]# 36660                                           0   
#[Out]# 36702                                           1   
#[Out]# 36706                                           1   
#[Out]# 36768                                           0   
#[Out]# 36809                                           1   
#[Out]# 36832                                           0   
#[Out]# 36839                                           0   
#[Out]# 36841                                           0   
#[Out]# 36866                                           1   
#[Out]# 36867                                           0   
#[Out]# 36896                                           1   
#[Out]# 36900                                           1   
#[Out]# 36922                                           0   
#[Out]# 36923                                           0   
#[Out]# 
#[Out]#       FULL YEAR INSURANCE COVERAGE STATUS 2013==3  \
#[Out]# 38                                              0   
#[Out]# 46                                              0   
#[Out]# 47                                              0   
#[Out]# 66                                              0   
#[Out]# 90                                              0   
#[Out]# 104                                             0   
#[Out]# 105                                             0   
#[Out]# 134                                             0   
#[Out]# 143                                             0   
#[Out]# 149                                             0   
#[Out]# 179                                             0   
#[Out]# 180                                             0   
#[Out]# 189                                             0   
#[Out]# 195                                             0   
#[Out]# 201                                             0   
#[Out]# 227                                             0   
#[Out]# 247                                             1   
#[Out]# 258                                             0   
#[Out]# 259                                             1   
#[Out]# 260                                             0   
#[Out]# 261                                             0   
#[Out]# 354                                             1   
#[Out]# 371                                             0   
#[Out]# 394                                             1   
#[Out]# 420                                             0   
#[Out]# 435                                             0   
#[Out]# 446                                             0   
#[Out]# 460                                             0   
#[Out]# 475                                             0   
#[Out]# 508                                             0   
#[Out]# ...                                           ...   
#[Out]# 36482                                           1   
#[Out]# 36494                                           1   
#[Out]# 36504                                           0   
#[Out]# 36512                                           1   
#[Out]# 36521                                           0   
#[Out]# 36527                                           1   
#[Out]# 36533                                           0   
#[Out]# 36544                                           0   
#[Out]# 36548                                           0   
#[Out]# 36582                                           0   
#[Out]# 36588                                           0   
#[Out]# 36596                                           0   
#[Out]# 36627                                           1   
#[Out]# 36634                                           0   
#[Out]# 36642                                           1   
#[Out]# 36652                                           0   
#[Out]# 36660                                           0   
#[Out]# 36702                                           0   
#[Out]# 36706                                           0   
#[Out]# 36768                                           0   
#[Out]# 36809                                           0   
#[Out]# 36832                                           0   
#[Out]# 36839                                           0   
#[Out]# 36841                                           0   
#[Out]# 36866                                           0   
#[Out]# 36867                                           0   
#[Out]# 36896                                           0   
#[Out]# 36900                                           0   
#[Out]# 36922                                           1   
#[Out]# 36923                                           0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==6  \
#[Out]# 38                                               0   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               0   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              0   
#[Out]# 143                                              0   
#[Out]# 149                                              0   
#[Out]# 179                                              0   
#[Out]# 180                                              0   
#[Out]# 189                                              0   
#[Out]# 195                                              0   
#[Out]# 201                                              0   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              0   
#[Out]# 261                                              0   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              0   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              1   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            0   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            1   
#[Out]# 36582                                            0   
#[Out]# 36588                                            0   
#[Out]# 36596                                            0   
#[Out]# 36627                                            0   
#[Out]# 36634                                            0   
#[Out]# 36642                                            0   
#[Out]# 36652                                            0   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            0   
#[Out]# 36809                                            0   
#[Out]# 36832                                            0   
#[Out]# 36839                                            1   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            1   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==4  \
#[Out]# 38                                               0   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               0   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              1   
#[Out]# 143                                              1   
#[Out]# 149                                              0   
#[Out]# 179                                              1   
#[Out]# 180                                              1   
#[Out]# 189                                              1   
#[Out]# 195                                              0   
#[Out]# 201                                              1   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              1   
#[Out]# 261                                              0   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              0   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              0   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            1   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            0   
#[Out]# 36582                                            0   
#[Out]# 36588                                            0   
#[Out]# 36596                                            0   
#[Out]# 36627                                            0   
#[Out]# 36634                                            0   
#[Out]# 36642                                            0   
#[Out]# 36652                                            0   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            1   
#[Out]# 36809                                            0   
#[Out]# 36832                                            1   
#[Out]# 36839                                            0   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            0   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==5  \
#[Out]# 38                                               1   
#[Out]# 46                                               0   
#[Out]# 47                                               0   
#[Out]# 66                                               1   
#[Out]# 90                                               0   
#[Out]# 104                                              0   
#[Out]# 105                                              0   
#[Out]# 134                                              0   
#[Out]# 143                                              0   
#[Out]# 149                                              1   
#[Out]# 179                                              0   
#[Out]# 180                                              0   
#[Out]# 189                                              0   
#[Out]# 195                                              0   
#[Out]# 201                                              0   
#[Out]# 227                                              0   
#[Out]# 247                                              0   
#[Out]# 258                                              0   
#[Out]# 259                                              0   
#[Out]# 260                                              0   
#[Out]# 261                                              1   
#[Out]# 354                                              0   
#[Out]# 371                                              0   
#[Out]# 394                                              0   
#[Out]# 420                                              1   
#[Out]# 435                                              0   
#[Out]# 446                                              0   
#[Out]# 460                                              0   
#[Out]# 475                                              0   
#[Out]# 508                                              0   
#[Out]# ...                                            ...   
#[Out]# 36482                                            0   
#[Out]# 36494                                            0   
#[Out]# 36504                                            0   
#[Out]# 36512                                            0   
#[Out]# 36521                                            0   
#[Out]# 36527                                            0   
#[Out]# 36533                                            0   
#[Out]# 36544                                            0   
#[Out]# 36548                                            0   
#[Out]# 36582                                            1   
#[Out]# 36588                                            1   
#[Out]# 36596                                            1   
#[Out]# 36627                                            0   
#[Out]# 36634                                            1   
#[Out]# 36642                                            0   
#[Out]# 36652                                            1   
#[Out]# 36660                                            0   
#[Out]# 36702                                            0   
#[Out]# 36706                                            0   
#[Out]# 36768                                            0   
#[Out]# 36809                                            0   
#[Out]# 36832                                            0   
#[Out]# 36839                                            0   
#[Out]# 36841                                            0   
#[Out]# 36866                                            0   
#[Out]# 36867                                            0   
#[Out]# 36896                                            0   
#[Out]# 36900                                            0   
#[Out]# 36922                                            0   
#[Out]# 36923                                            0   
#[Out]# 
#[Out]#        FULL YEAR INSURANCE COVERAGE STATUS 2013==7  
#[Out]# 38                                               0  
#[Out]# 46                                               0  
#[Out]# 47                                               0  
#[Out]# 66                                               0  
#[Out]# 90                                               0  
#[Out]# 104                                              0  
#[Out]# 105                                              0  
#[Out]# 134                                              0  
#[Out]# 143                                              0  
#[Out]# 149                                              0  
#[Out]# 179                                              0  
#[Out]# 180                                              0  
#[Out]# 189                                              0  
#[Out]# 195                                              0  
#[Out]# 201                                              0  
#[Out]# 227                                              0  
#[Out]# 247                                              0  
#[Out]# 258                                              0  
#[Out]# 259                                              0  
#[Out]# 260                                              0  
#[Out]# 261                                              0  
#[Out]# 354                                              0  
#[Out]# 371                                              0  
#[Out]# 394                                              0  
#[Out]# 420                                              0  
#[Out]# 435                                              0  
#[Out]# 446                                              0  
#[Out]# 460                                              0  
#[Out]# 475                                              0  
#[Out]# 508                                              0  
#[Out]# ...                                            ...  
#[Out]# 36482                                            0  
#[Out]# 36494                                            0  
#[Out]# 36504                                            0  
#[Out]# 36512                                            0  
#[Out]# 36521                                            0  
#[Out]# 36527                                            0  
#[Out]# 36533                                            0  
#[Out]# 36544                                            0  
#[Out]# 36548                                            0  
#[Out]# 36582                                            0  
#[Out]# 36588                                            0  
#[Out]# 36596                                            0  
#[Out]# 36627                                            0  
#[Out]# 36634                                            0  
#[Out]# 36642                                            0  
#[Out]# 36652                                            0  
#[Out]# 36660                                            0  
#[Out]# 36702                                            0  
#[Out]# 36706                                            0  
#[Out]# 36768                                            0  
#[Out]# 36809                                            0  
#[Out]# 36832                                            0  
#[Out]# 36839                                            0  
#[Out]# 36841                                            0  
#[Out]# 36866                                            0  
#[Out]# 36867                                            0  
#[Out]# 36896                                            0  
#[Out]# 36900                                            0  
#[Out]# 36922                                            0  
#[Out]# 36923                                            0  
#[Out]# 
#[Out]# [2152 rows x 1837 columns]
# Sun, 27 Sep 2015 03:12:14
health.rmse(rf_office,test,office,"./exog_building_rf_office.txt")
#[Out]# ('rmse = 3504.45542817', 'stddev in data = 4157.00593571')
# Sun, 27 Sep 2015 03:12:18
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 03:13:55
thestring="(all["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (all["DIABETES DIAGNOSIS (>17)"]==1) & (all["CENSUS REGION AS OF 12/31/13"]==2)"
# Sun, 27 Sep 2015 03:14:02
thestring='''(all["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (all["DIABETES DIAGNOSIS (>17)"]==1) & (all["CENSUS REGION AS OF 12/31/13"]==2)'''
# Sun, 27 Sep 2015 03:14:04
thestring
#[Out]# '(all["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (all["DIABETES DIAGNOSIS (>17)"]==1) & (all["CENSUS REGION AS OF 12/31/13"]==2)'
# Sun, 27 Sep 2015 03:14:07
all[thestring]
# Sun, 27 Sep 2015 03:14:28
all[[thestring]]
# Sun, 27 Sep 2015 03:16:21
conditions=[("HIGH BLOOD PRESSURE DIAG (>17)",1), ("DIABETES DIAGNOSIS (>17)",1)]
# Sun, 27 Sep 2015 03:17:25
all[["all[%s]==%d"%(term[0],term[1]) for term in conditions]]
# Sun, 27 Sep 2015 03:17:39
all["all[%s]==%d"%(term[0],term[1]) for term in conditions]
# Sun, 27 Sep 2015 03:17:47
all[["all[%s]==%d"%(term[0],term[1]) for term in conditions]]
# Sun, 27 Sep 2015 03:18:36
all["%s"%conditions[0][0]==conditions[0][1]]
# Sun, 27 Sep 2015 03:18:49
all['''"%s"%conditions[0][0]==conditions[0][1]''']
# Sun, 27 Sep 2015 03:18:58
all["%s"%conditions[0][0]==conditions[0][1]]
# Sun, 27 Sep 2015 03:20:42
all[all."HIGH BLOOD PRESSURE DIAG (>17)"==1]
# Sun, 27 Sep 2015 03:20:49
all."HIGH BLOOD PRESSURE DIAG (>17)"
# Sun, 27 Sep 2015 03:25:06
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 03:28:35
temp=all.iloc[0]
# Sun, 27 Sep 2015 03:28:36
temp
#[Out]# DWELLING UNIT ID                                  20004
#[Out]# PERSON NUMBER                                       101
#[Out]# PERSON ID (DUID + PID)                         20004101
#[Out]# PANEL NUMBER                                         17
#[Out]# FAMILY ID (STUDENT MERGED IN) - R3/1                 A 
#[Out]# FAMILY ID (STUDENT MERGED IN) - R4/2                 A 
#[Out]# FAMILY ID (STUDENT MERGED IN) - R5/3                 A 
#[Out]# FAMILY ID (STUDENT MERGED IN) - 12/31/13             A 
#[Out]# ANNUAL FAMILY IDENTIFIER                             A 
#[Out]# CPSFAMID                                             A 
#[Out]# FAM SIZE RESPONDING 12/31 CPS FAMILY                  3
#[Out]# REF PERSON OF 12/31 CPS FAMILY                        1
#[Out]# RU LETTER - R3/1                                     A 
#[Out]# RU LETTER - R4/2                                     A 
#[Out]# RU LETTER - R5/3                                     A 
#[Out]# RU LETTER AS OF 12/31/13                             A 
#[Out]# RU SIZE - R3/1                                        3
#[Out]# RU SIZE - R4/2                                        3
#[Out]# RU SIZE - R5/3                                        3
#[Out]# RU SIZE AS OF 12/31/13                                3
#[Out]# RU FIELDED AS:STANDARD/NEW/STUDENT-R3/1               1
#[Out]# RU FIELDED AS:STANDARD/NEW/STUDENT-R4/2               1
#[Out]# RU FIELDED AS:STANDARD/NEW/STUDENT-R5/3               1
#[Out]# RU FIELDED AS:STANDARD/NEW/STUD-12/31/13              1
#[Out]# RU SIZE INCLUDING STUDENTS - R3/1                     3
#[Out]# RU SIZE INCLUDING STUDENTS - R4/2                     3
#[Out]# RU SIZE INCLUDING STUDENTS - R5/3                     3
#[Out]# RU SIZE INCLUDING STUDENT AS OF 12/31/13              3
#[Out]# MEMBER OF RESPONDING 12/31 FAMILY                     1
#[Out]# FAMILY SIZE OF RESPONDING 12/31 FAMILY                3
#[Out]#                                                  ...   
#[Out]# EDUCATION RECODE (EDITED)==14                         1
#[Out]# EDUCATION RECODE (EDITED)==13                         0
#[Out]# EDUCATION RECODE (EDITED)==1                          0
#[Out]# EDUCATION RECODE (EDITED)==2                          0
#[Out]# EDUCATION RECODE (EDITED)==15                         0
#[Out]# HIGH BLOOD PRESSURE DIAG (>17)==2                     1
#[Out]# CORONARY HRT DISEASE DIAG (>17)==2                    1
#[Out]# ANGINA DIAGNOSIS (>17)==2                             1
#[Out]# HEART ATTACK (MI) DIAG (>17)==2                       1
#[Out]# OTHER HEART DISEASE DIAG (>17)==2                     1
#[Out]# STROKE DIAGNOSIS (>17)==2                             1
#[Out]# EMPHYSEMA DIAGNOSIS (>17)==2                          1
#[Out]# HIGH CHOLESTEROL DIAGNOSIS (>17)==2                   1
#[Out]# CANCER DIAGNOSIS (>17)==2                             1
#[Out]# DIABETES DIAGNOSIS (>17)==2                           1
#[Out]# ARTHRITIS DIAGNOSIS (>17)==2                          1
#[Out]# ASTHMA DIAGNOSIS==2                                   1
#[Out]# WEARS SEAT BELT (>15) - RD 5/3==1                     1
#[Out]# WEARS SEAT BELT (>15) - RD 5/3==2                     0
#[Out]# WEARS SEAT BELT (>15) - RD 5/3==3                     0
#[Out]# WEARS SEAT BELT (>15) - RD 5/3==5                     0
#[Out]# WEARS SEAT BELT (>15) - RD 5/3==4                     0
#[Out]# SAQ: CURRENTLY SMOKE==2                               1
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==1           1
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==2           0
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==3           0
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==6           0
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==4           0
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==5           0
#[Out]# FULL YEAR INSURANCE COVERAGE STATUS 2013==7           0
#[Out]# Name: 0, dtype: object
# Sun, 27 Sep 2015 03:28:46
health.estimate_billed_charges(temp)
# Sun, 27 Sep 2015 03:28:54
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 03:28:55
health.estimate_billed_charges(temp)
#[Out]# [array([ 359.67]), array([ 0.]), array([ 0.]), array([ 0.])]
# Sun, 27 Sep 2015 03:29:46
health.estimate_billed_charges(all.iloc[1])
#[Out]# [array([ 0.]), array([ 0.]), array([ 0.]), array([ 0.])]
# Sun, 27 Sep 2015 03:30:28
health.estimate_billed_charges(all.iloc[2])
#[Out]# [array([ 0.]), array([ 3556.36]), array([ 0.]), array([ 0.])]
# Sun, 27 Sep 2015 03:30:38
health.estimate_billed_charges(all.iloc[3])
#[Out]# [array([ 692.55]), array([ 0.]), array([ 0.]), array([ 0.])]
# Sun, 27 Sep 2015 03:33:38
all["estimated billed charges"]=all.apply(lambda x: health.estimate_billed_charges(x))
# Sun, 27 Sep 2015 03:34:03
health.reload(health)
#[Out]# <module 'health' from '/home/soumya/insight/modeling/code/health.py'>
# Sun, 27 Sep 2015 03:34:41
%logstart -rto justincase.py
# Sun, 27 Sep 2015 03:35:04
exit
