# IPython log file

import pandas as pd
plans=pd.read_csv("./plans_stripped.csv")
plans.head()
plans.columns
%logstart -rt cleaning_plans.py
# Wed, 16 Sep 2015 18:04:12
"Sou" in "Soumya"
# Wed, 16 Sep 2015 18:04:49
candidates=[variable for variable in plans.columns if "standard" in variable]
# Wed, 16 Sep 2015 18:04:51
candidates
# Wed, 16 Sep 2015 18:08:51
reduced=plans[candidates]
# Wed, 16 Sep 2015 18:08:56
reduced
# Wed, 16 Sep 2015 18:09:00
reduced.columns
# Wed, 16 Sep 2015 19:42:30
reduced.head()
# Wed, 16 Sep 2015 20:02:38
plans.columns
# Wed, 16 Sep 2015 20:04:28
candidates
# Wed, 16 Sep 2015 20:05:24
geographical=[term for term in plans.columns if ("state" in term.lower() or "county" in term.lower())]
# Wed, 16 Sep 2015 20:05:29
geographical
# Wed, 16 Sep 2015 20:05:39
all_candidates=candidates+geographical
# Wed, 16 Sep 2015 20:05:41
all_candidates
# Wed, 16 Sep 2015 20:06:09
plan_information=plans[all_candidates]
# Wed, 16 Sep 2015 20:06:16
plan_information
# Wed, 16 Sep 2015 20:06:30
plan_information.columns
# Wed, 16 Sep 2015 20:11:29
plan_information.to_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:11:36
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:15:10
plan_information.columns
# Wed, 16 Sep 2015 20:15:14
plan_information.head()
# Wed, 16 Sep 2015 20:15:47
all_candidates
# Wed, 16 Sep 2015 20:17:17
candidates=[variable for variable in plans.columns if "standard" in variable.lower() and "family" not in variables.lower()]
# Wed, 16 Sep 2015 20:17:34
all_candidates
# Wed, 16 Sep 2015 20:17:41
candidates=[variable for variable in plans.columns if "standard" in variable.lower() and "family" not in variable.lower()]
# Wed, 16 Sep 2015 20:17:46
candidates
# Wed, 16 Sep 2015 20:18:09
all_candidates=candidates+geographical
# Wed, 16 Sep 2015 20:18:28
reduced=plans[all_candidates]
# Wed, 16 Sep 2015 20:18:42
plan_information=plans[all_candidates]
# Wed, 16 Sep 2015 20:18:48
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:27:20
plan_information.head()
# Wed, 16 Sep 2015 20:27:44
reduced
# Wed, 16 Sep 2015 20:27:47
reduced.columns
# Wed, 16 Sep 2015 20:27:54
plan_information.columns
# Wed, 16 Sep 2015 20:28:18
plan_information.columns==reduced.columns
# Wed, 16 Sep 2015 20:29:08
plan_information["Standard Plan Cost Sharing"]
# Wed, 16 Sep 2015 20:29:16
plan_information["Standard Plan Cost Sharing"].notnull()
# Wed, 16 Sep 2015 20:29:19
sum(_)
# Wed, 16 Sep 2015 20:29:45
plan_information=plan_information.drop("Standard Plan Cost Sharing",axis=1).copy()
# Wed, 16 Sep 2015 20:29:51
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:29:58
plan_information.columns
# Wed, 16 Sep 2015 20:30:09
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:30:53
temp=plan_information["Medical Deductible-individual-standard"].map(lambda x: (((str(x).replace('$','')))).copy()
)
# Wed, 16 Sep 2015 20:31:11
temp=plan_information["Medical Deductible-individual-standard"].map(lambda x: str(x).replace('$','')).copy()
# Wed, 16 Sep 2015 20:31:13
temp
# Wed, 16 Sep 2015 20:31:37
plan_information["Medical Deductible-individual-standard"]=temp.copy()
# Wed, 16 Sep 2015 20:31:41
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:31:52
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:31:57
plan_information["Medical Deductible-individual-standard"].hist()
# Wed, 16 Sep 2015 20:32:26
(plan_information["Medical Deductible-individual-standard"].dropna()).hist()
# Wed, 16 Sep 2015 20:32:47
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:32:54
plan_information["Medical Deductible-individual-standard"].isnull()
# Wed, 16 Sep 2015 20:32:56
sum(_)
# Wed, 16 Sep 2015 20:32:59
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:33:32
temp=plan_information["Medical Deductible-individual-standard"].map(lambda x: (str(x).replace('$','')).replace(',','')).copy()
# Wed, 16 Sep 2015 20:33:37
plan_information["Medical Deductible-individual-standard"]=temp.copy()
# Wed, 16 Sep 2015 20:33:47
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:33:56
plan_information["Medical Deductible-individual-standard"].hist()
# Wed, 16 Sep 2015 20:34:04
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:34:17
plan_information["Medical Deductible-individual-standard"]=temp.copy().astype(int64)
# Wed, 16 Sep 2015 20:34:19
plan_information["Medical Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:34:26
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:34:31
plan_information["Medical Deductible-individual-standard"].hist()
# Wed, 16 Sep 2015 20:34:38
plan_information["Medical Deductible-individual-standard"].hist()
# Wed, 16 Sep 2015 20:34:46
plan_information["Medical Deductible-individual-standard"].hist(bins=100)
# Wed, 16 Sep 2015 20:35:04
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:35:10
plan_information.columns
# Wed, 16 Sep 2015 20:35:30
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:35:57
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: (str(x).replace('$','')).replace(',','')).copy()
# Wed, 16 Sep 2015 20:36:06
plan_information["Drug Deductible-individual-standard"]=temp.copy()
# Wed, 16 Sep 2015 20:36:11
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:37:05
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: ((str(x).replace('$','')).replace(',','')).replace("Included in Medical",'')).copy()
# Wed, 16 Sep 2015 20:37:07
plan_information["Drug Deductible-individual-standard"]=temp.copy()
# Wed, 16 Sep 2015 20:37:10
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:37:42
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: ((str(x).replace('$','')).replace(',','')).replace('','0')).copy()
# Wed, 16 Sep 2015 20:37:45
temp
# Wed, 16 Sep 2015 20:38:05
plan_information=pd.read_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:38:14
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:38:39
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: ((str(x).replace('$','')).replace(',','')).replace("Included in Medical",'0')).copy()
# Wed, 16 Sep 2015 20:38:43
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:38:49
plan_information["Drug Deductible-individual-standard"]=temp.copy()
# Wed, 16 Sep 2015 20:38:51
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:39:03
plan_information["Drug Deductible-individual-standard"]=temp.copy().astype(int64)
# Wed, 16 Sep 2015 20:39:11
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: ((str(x).replace('$','')).replace(',','')).replace("Included in Medical",'0')).copy()
# Wed, 16 Sep 2015 20:39:12
temp
# Wed, 16 Sep 2015 20:39:22
temp=plan_information["Drug Deductible-individual-standard"].map(lambda x: ((str(x).replace('$','')).replace(',','')).replace("Included in Medical",'0')).copy().astype(int64)
# Wed, 16 Sep 2015 20:39:34
temp
# Wed, 16 Sep 2015 20:39:37
temp.unique()
# Wed, 16 Sep 2015 20:40:03
temp.map(lambda x: str(x).replace("Not Applicable",'0'))
# Wed, 16 Sep 2015 20:40:06
temp.unique()
# Wed, 16 Sep 2015 20:40:17
temp=temp.map(lambda x: str(x).replace("Not Applicable",'0')).copy()
# Wed, 16 Sep 2015 20:40:20
temp.unique()
# Wed, 16 Sep 2015 20:40:42
plan_information["Drug Deductible-individual-standard"]=temp.copy().astype(int64)
# Wed, 16 Sep 2015 20:40:48
plan_information["Drug Deductible-individual-standard"]
# Wed, 16 Sep 2015 20:40:54
plan_information["Drug Deductible-individual-standard"].hist()
# Wed, 16 Sep 2015 20:41:07
plan_information=pd.read_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:41:14
plan_information.columns
# Wed, 16 Sep 2015 20:41:27
plan_information["Medical Maximum Out Of Pocket - individual - standard"]
# Wed, 16 Sep 2015 20:41:59
temp=plan_information["Medical Maximum Out Of Pocket - individual - standard"].map(lambda x: str(x).replace('$','')).copy()
# Wed, 16 Sep 2015 20:42:00
temp
# Wed, 16 Sep 2015 20:42:04
temp.unique()
# Wed, 16 Sep 2015 20:42:30
temp=plan_information["Medical Maximum Out Of Pocket - individual - standard"].map(lambda x: (str(x).replace('$','')).replace(',','')).copy().astype(int64)
# Wed, 16 Sep 2015 20:42:32
temp
# Wed, 16 Sep 2015 20:42:35
temp.unique()
# Wed, 16 Sep 2015 20:43:06
plan_information["Medical Maximum Out Of Pocket - individual - standard"]=temp.copy()
# Wed, 16 Sep 2015 20:43:15
plan_information["Medical Maximum Out Of Pocket - individual - standard"]
# Wed, 16 Sep 2015 20:43:18
plan_information["Medical Maximum Out Of Pocket - individual - standard"].hist()
# Wed, 16 Sep 2015 20:43:48
plan_information.to_csv("./plan_information.csv",index=False)
# Wed, 16 Sep 2015 20:43:57
plan_information.columns
# Wed, 16 Sep 2015 20:44:10
plan_information[plan_information.columns[0]]
# Wed, 16 Sep 2015 20:44:12
plan_information[plan_information.columns[1]]
# Wed, 16 Sep 2015 20:44:15
plan_information[plan_information.columns[2]]
# Wed, 16 Sep 2015 20:44:18
plan_information[plan_information.columns[3]]
# Wed, 16 Sep 2015 20:44:21
plan_information[plan_information.columns[4]]
# Wed, 16 Sep 2015 20:44:25
plan_information[plan_information.columns[2]]
# Wed, 16 Sep 2015 20:46:31
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'')))
# Wed, 16 Sep 2015 20:46:33
temp
# Wed, 16 Sep 2015 20:46:37
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0')))
# Wed, 16 Sep 2015 20:46:39
temp
# Wed, 16 Sep 2015 20:46:45
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0'))).astype(int)
# Wed, 16 Sep 2015 20:46:49
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0')))
# Wed, 16 Sep 2015 20:46:53
temp.unique()
# Wed, 16 Sep 2015 20:47:08
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0').replace("Not Applicable",'0')))
# Wed, 16 Sep 2015 20:47:11
temp.unique()
# Wed, 16 Sep 2015 20:47:20
temp=plan_information[plan_information.columns[2]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0').replace("Not Applicable",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:47:21
temp
# Wed, 16 Sep 2015 20:47:23
temp.hist()
# Wed, 16 Sep 2015 20:47:40
plan_information[plan_information.columns[2]]=temp.copy()
# Wed, 16 Sep 2015 20:47:50
plan_information[plan_information.columns[2]]
# Wed, 16 Sep 2015 20:48:01
plan_information.to_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:48:11
plan_information[plan_information.columns[3]]
# Wed, 16 Sep 2015 20:48:13
plan_information[plan_information.columns[4]]
# Wed, 16 Sep 2015 20:48:26
temp=plan_information[plan_information.columns[4]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0').replace("Not Applicable",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:48:28
temp
# Wed, 16 Sep 2015 20:48:34
temp.unique()
# Wed, 16 Sep 2015 20:48:49
plan_information[plan_information.columns[4]]=temp.copy()
# Wed, 16 Sep 2015 20:49:01
plan_information[plan_information.columns[4]].hist()
# Wed, 16 Sep 2015 20:49:07
plan_information[plan_information.columns[4]].hist(bins=100)
# Wed, 16 Sep 2015 20:49:25
plan_information=pd.read_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:49:36
plan_information[plan_information.columns[2]]
# Wed, 16 Sep 2015 20:49:38
plan_information[plan_information.columns[3]]
# Wed, 16 Sep 2015 20:49:40
plan_information[plan_information.columns[4]]
# Wed, 16 Sep 2015 20:49:45
plan_information[plan_information.columns[5]]
# Wed, 16 Sep 2015 20:49:52
plan_information[plan_information.columns[5]].unique()
# Wed, 16 Sep 2015 20:50:11
temp=plan_information[plan_information.columns[5]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0').replace("Not Applicable",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:50:15
temp.hist()
# Wed, 16 Sep 2015 20:50:20
temp.hist(bins=100)
# Wed, 16 Sep 2015 20:50:29
temp.unique()
# Wed, 16 Sep 2015 20:50:41
temp.describe()
# Wed, 16 Sep 2015 20:50:54
temp=plan_information[plan_information.columns[5]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0').replace("Not Applicable",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:50:58
plan_information[plan_information.columns[5]].unique()
# Wed, 16 Sep 2015 20:51:13
temp=plan_information[plan_information.columns[5]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:51:15
temp
# Wed, 16 Sep 2015 20:51:18
temp.hist()
# Wed, 16 Sep 2015 20:51:29
temp[temp>0].hist()
# Wed, 16 Sep 2015 20:51:46
temp=plan_information[plan_information.columns[5]].map(lambda x: (str(x).replace('$','').replace(',','').replace("Included in Medical",'0'))).copy().astype(int64)
# Wed, 16 Sep 2015 20:51:55
plan_information[plan_information.columns[5]]=temp.copy()
# Wed, 16 Sep 2015 20:52:04
plan_information.to_csv("./plan_information.csv")
# Wed, 16 Sep 2015 20:52:16
plan_information[plan_information.columns[6]]
# Wed, 16 Sep 2015 20:52:20
plan_information[plan_information.columns[7]]
# Wed, 16 Sep 2015 20:52:22
plan_information[plan_information.columns[8]]
# Wed, 16 Sep 2015 20:52:25
plan_information[plan_information.columns[9]]
# Wed, 16 Sep 2015 20:52:27
plan_information[plan_information.columns[10]]
# Wed, 16 Sep 2015 20:52:29
plan_information[plan_information.columns[11]]
# Wed, 16 Sep 2015 20:52:31
plan_information[plan_information.columns[12]]
# Wed, 16 Sep 2015 20:52:33
plan_information[plan_information.columns[13]]
# Wed, 16 Sep 2015 20:52:36
plan_information[plan_information.columns[14]]
# Wed, 16 Sep 2015 20:52:38
plan_information[plan_information.columns[15]]
# Wed, 16 Sep 2015 20:52:41
plan_information[plan_information.columns[16]]
# Wed, 16 Sep 2015 20:52:43
plan_information[plan_information.columns[17]]
# Wed, 16 Sep 2015 20:52:53
plan_information.head()
# Wed, 16 Sep 2015 21:02:10
plan_information.to_csv("./plan_information.csv")
# Wed, 16 Sep 2015 21:02:56
plan_information[plan_information.columns[6]]
# Wed, 16 Sep 2015 21:05:13
plan_information[plan_information.columns[6]].unique()
# Wed, 16 Sep 2015 21:05:57
with open("coins.py","w") as f:
    f.write(_)
    
# Wed, 16 Sep 2015 21:06:02
plan_information[plan_information.columns[6]].unique()
# Wed, 16 Sep 2015 21:06:13
np.savetxt(plan_information[plan_information.columns[6]].unique())
# Wed, 16 Sep 2015 21:06:24
np.savetxt("coins.py",plan_information[plan_information.columns[6]].unique())
# Wed, 16 Sep 2015 21:06:32
np.savetxt(plan_information[plan_information.columns[6]].unique())
# Wed, 16 Sep 2015 21:06:44
plan_information[plan_information.columns[6]].unique()
# Wed, 16 Sep 2015 21:06:47
temp=_
# Wed, 16 Sep 2015 21:06:48
temp
# Wed, 16 Sep 2015 21:06:54
list(temp)
# Wed, 16 Sep 2015 21:07:12
with open("coins.py","w") as f:
    f.write(_)
    
# Wed, 16 Sep 2015 21:07:39
with open("coins.py","w") as f:
    for term in _:
        f.write("%s\n"%term)
        
# Wed, 16 Sep 2015 21:13:19
decomposition=[plan_information[plan_information.columns[place]].unique() for place in range(len(plan_information.columns))]
# Wed, 16 Sep 2015 21:13:22
decomposition
# Wed, 16 Sep 2015 21:13:44
decomposition=[(place,plan_information[plan_information.columns[place]].unique()) for place in range(len(plan_information.columns))]
# Wed, 16 Sep 2015 21:13:45
decomposition
# Wed, 16 Sep 2015 21:14:04
decomposition=[("columns %s"%place,plan_information[plan_information.columns[place]].unique()) for place in range(len(plan_information.columns))]
# Wed, 16 Sep 2015 21:14:07
decomposition
# Wed, 16 Sep 2015 21:14:14
decomposition=[("column %s"%place,plan_information[plan_information.columns[place]].unique()) for place in range(len(plan_information.columns))]
# Wed, 16 Sep 2015 21:14:15
decomposition
# Wed, 16 Sep 2015 21:16:02
for place in range(plan_information.columns):
    with open("uniques %s"%place,"w") as f:
        for term in decomposition[place][1]:
            f.write("%s\n"%term)
            
# Wed, 16 Sep 2015 21:17:43
decomposition[0]
# Wed, 16 Sep 2015 21:17:46
decomposition[0][1]
# Wed, 16 Sep 2015 21:18:08
for place in list(range(plan_information.columns)):
    with open("uniques %s"%place,"w") as f:
        for term in decomposition[place][1]:
            f.write("%s\n"%term)
            
# Wed, 16 Sep 2015 21:18:26
for place in list(range(len(plan_information.columns))):
    with open("uniques %s"%place,"w") as f:
        for term in decomposition[place][1]:
            f.write("%s\n"%term)
            
# Wed, 16 Sep 2015 21:18:54
for place in list(range(len(plan_information.columns))):
    with open("uniques_%s.txt"%place,"w") as f:
        for term in decomposition[place][1]:
            f.write("%s\n"%term)
            
# Wed, 16 Sep 2015 21:33:04
plan_information
# Wed, 16 Sep 2015 21:33:07
plan_information.columns
# Thu, 17 Sep 2015 11:10:50
print(1)
# Thu, 17 Sep 2015 14:31:12
plan_information
# Thu, 17 Sep 2015 14:31:14
plan_information.columns
# Thu, 17 Sep 2015 14:31:24
plan_variables
# Thu, 17 Sep 2015 14:31:27
variables
# Thu, 17 Sep 2015 14:31:30
all_variables
# Thu, 17 Sep 2015 15:21:10
plan_information.columns
# Thu, 17 Sep 2015 15:21:27
plan_information."Plan ID (standard component)"
# Thu, 17 Sep 2015 15:21:35
plan_information.Plan ID (standard component)
# Thu, 17 Sep 2015 16:42:26
plan_information
# Thu, 17 Sep 2015 16:46:20
plan_information[plan_information.columns[7]]
# Thu, 17 Sep 2015 16:46:43
temp=plan_information[plan_information.columns[7]].map(lambda x: str(x).split('and')
)
# Thu, 17 Sep 2015 16:46:55
temp=plan_information[plan_information.columns[7]].map(lambda x: str(x).split('and'))
# Thu, 17 Sep 2015 16:46:57
temp
# Thu, 17 Sep 2015 16:47:08
temp.head()
# Thu, 17 Sep 2015 16:47:26
"this and that".split("and")
# Thu, 17 Sep 2015 16:47:39
temp.head()
# Thu, 17 Sep 2015 16:47:47
temp.iloc[:100]
# Thu, 17 Sep 2015 16:48:13
temp=plan_information[plan_information.columns[7]].map(lambda x: x.split("and"))
# Thu, 17 Sep 2015 16:48:14
temp
# Thu, 17 Sep 2015 16:48:24
plan_information[plan_information.columns[7]]
# Thu, 17 Sep 2015 16:48:55
a=_.map(lambda x: "and" in x)
# Thu, 17 Sep 2015 16:48:56
a
# Thu, 17 Sep 2015 16:49:00
sum(_)
# Thu, 17 Sep 2015 16:49:17
plan_information[plan_information.columns[7]][a]
# Thu, 17 Sep 2015 16:49:25
temp[1]
# Thu, 17 Sep 2015 16:49:28
temp[a]
# Thu, 17 Sep 2015 16:58:56
exit
