# IPython log file

cd outpatient/
ls
%logstart
import pandas as pd
data=pd.read_csv("./Medicare_Provider_Charge_Outpatient_APC30_CY2013_v2.csv")
data
#[Out]#                                                      APC  Provider Id  \
#[Out]# 0              0013 - Level II Debridement & Destruction        10001   
#[Out]# 1             0015 - Level III Debridement & Destruction        10001   
#[Out]# 2                        0019 - Level I Excision/ Biopsy        10001   
#[Out]# 3                       0020 - Level II Excision/ Biopsy        10001   
#[Out]# 4                   0078 - Level III Pulmonary Treatment        10001   
#[Out]# 5        0096 - Level II Noninvasive Physiologic Studies        10001   
#[Out]# 6                        0204 - Level I Nerve Injections        10001   
#[Out]# 7                       0206 - Level II Nerve Injections        10001   
#[Out]# 8                      0207 - Level III Nerve Injections        10001   
#[Out]# 9      0209 - Level II Extended EEG, Sleep, and Cardi...        10001   
#[Out]# 10     0265 - Level I Diagnostic and Screening Ultras...        10001   
#[Out]# 11     0267 - Level III Diagnostic and Screening Ultr...        10001   
#[Out]# 12       0269 - Level II Echocardiogram Without Contrast        10001   
#[Out]# 13      0270 - Level III Echocardiogram Without Contrast        10001   
#[Out]# 14     0336 - Magnetic Resonance Imaging and Magnetic...        10001   
#[Out]# 15                       0368 - Level II Pulmonary Tests        10001   
#[Out]# 16                       0377 - Level II Cardiac Imaging        10001   
#[Out]# 17                 0604 - Level 1 Hospital Clinic Visits        10001   
#[Out]# 18                 0605 - Level 2 Hospital Clinic Visits        10001   
#[Out]# 19                 0606 - Level 3 Hospital Clinic Visits        10001   
#[Out]# 20                 0607 - Level 4 Hospital Clinic Visits        10001   
#[Out]# 21        0692 - Level II Electronic Analysis of Devices        10001   
#[Out]# 22             0013 - Level II Debridement & Destruction        10005   
#[Out]# 23            0015 - Level III Debridement & Destruction        10005   
#[Out]# 24                       0019 - Level I Excision/ Biopsy        10005   
#[Out]# 25                      0020 - Level II Excision/ Biopsy        10005   
#[Out]# 26                  0078 - Level III Pulmonary Treatment        10005   
#[Out]# 27       0096 - Level II Noninvasive Physiologic Studies        10005   
#[Out]# 28                       0204 - Level I Nerve Injections        10005   
#[Out]# 29                     0207 - Level III Nerve Injections        10005   
#[Out]# ...                                                  ...          ...   
#[Out]# 44325                    0204 - Level I Nerve Injections       670049   
#[Out]# 44326                   0206 - Level II Nerve Injections       670049   
#[Out]# 44327                  0207 - Level III Nerve Injections       670049   
#[Out]# 44328  0267 - Level III Diagnostic and Screening Ultr...       670049   
#[Out]# 44329  0336 - Magnetic Resonance Imaging and Magnetic...       670049   
#[Out]# 44330                  0207 - Level III Nerve Injections       670052   
#[Out]# 44331  0336 - Magnetic Resonance Imaging and Magnetic...       670052   
#[Out]# 44332  0265 - Level I Diagnostic and Screening Ultras...       670053   
#[Out]# 44333  0267 - Level III Diagnostic and Screening Ultr...       670053   
#[Out]# 44334    0269 - Level II Echocardiogram Without Contrast       670053   
#[Out]# 44335  0336 - Magnetic Resonance Imaging and Magnetic...       670053   
#[Out]# 44336                  0207 - Level III Nerve Injections       670054   
#[Out]# 44337  0209 - Level II Extended EEG, Sleep, and Cardi...       670054   
#[Out]# 44338     0692 - Level II Electronic Analysis of Devices       670054   
#[Out]# 44339               0078 - Level III Pulmonary Treatment       670055   
#[Out]# 44340                   0206 - Level II Nerve Injections       670055   
#[Out]# 44341  0267 - Level III Diagnostic and Screening Ultr...       670055   
#[Out]# 44342    0269 - Level II Echocardiogram Without Contrast       670055   
#[Out]# 44343   0270 - Level III Echocardiogram Without Contrast       670055   
#[Out]# 44344  0336 - Magnetic Resonance Imaging and Magnetic...       670055   
#[Out]# 44345                    0377 - Level II Cardiac Imaging       670055   
#[Out]# 44346    0096 - Level II Noninvasive Physiologic Studies       670059   
#[Out]# 44347                    0204 - Level I Nerve Injections       670059   
#[Out]# 44348  0267 - Level III Diagnostic and Screening Ultr...       670059   
#[Out]# 44349    0269 - Level II Echocardiogram Without Contrast       670059   
#[Out]# 44350   0270 - Level III Echocardiogram Without Contrast       670059   
#[Out]# 44351  0336 - Magnetic Resonance Imaging and Magnetic...       670059   
#[Out]# 44352                    0377 - Level II Cardiac Imaging       670059   
#[Out]# 44353             0074 - Level IV Endoscopy Upper Airway       670061   
#[Out]# 44354                  0207 - Level III Nerve Injections       670061   
#[Out]# 
#[Out]#                                      Provider Name  \
#[Out]# 0                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 1                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 2                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 3                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 4                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 5                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 6                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 7                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 8                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 9                 SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 10                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 11                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 12                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 13                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 14                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 15                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 16                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 17                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 18                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 19                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 20                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 21                SOUTHEAST ALABAMA MEDICAL CENTER   
#[Out]# 22                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 23                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 24                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 25                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 26                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 27                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 28                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# 29                   MARSHALL MEDICAL CENTER SOUTH   
#[Out]# ...                                            ...   
#[Out]# 44325            NORTH CENTRAL SURGICAL CENTER LLP   
#[Out]# 44326            NORTH CENTRAL SURGICAL CENTER LLP   
#[Out]# 44327            NORTH CENTRAL SURGICAL CENTER LLP   
#[Out]# 44328            NORTH CENTRAL SURGICAL CENTER LLP   
#[Out]# 44329            NORTH CENTRAL SURGICAL CENTER LLP   
#[Out]# 44330                             DOCTORS HOSPITAL   
#[Out]# 44331                             DOCTORS HOSPITAL   
#[Out]# 44332                ST LUKE'S SUGAR LAND HOSPITAL   
#[Out]# 44333                ST LUKE'S SUGAR LAND HOSPITAL   
#[Out]# 44334                ST LUKE'S SUGAR LAND HOSPITAL   
#[Out]# 44335                ST LUKE'S SUGAR LAND HOSPITAL   
#[Out]# 44336  FOUNDATION SURGICAL HOSPITAL OF SAN ANTONIO   
#[Out]# 44337  FOUNDATION SURGICAL HOSPITAL OF SAN ANTONIO   
#[Out]# 44338  FOUNDATION SURGICAL HOSPITAL OF SAN ANTONIO   
#[Out]# 44339                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44340                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44341                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44342                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44343                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44344                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44345                 METHODIST STONE OAK HOSPITAL   
#[Out]# 44346                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44347                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44348                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44349                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44350                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44351                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44352                   ST LUKES LAKESIDE HOSPITAL   
#[Out]# 44353                SOUTH TEXAS SURGICAL HOSPITAL   
#[Out]# 44354                SOUTH TEXAS SURGICAL HOSPITAL   
#[Out]# 
#[Out]#                        Provider Street Address   Provider City Provider State  \
#[Out]# 0                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 1                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 2                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 3                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 4                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 5                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 6                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 7                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 8                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 9                       1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 10                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 11                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 12                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 13                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 14                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 15                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 16                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 17                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 18                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 19                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 20                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 21                      1108 ROSS CLARK CIRCLE          DOTHAN             AL   
#[Out]# 22                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 23                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 24                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 25                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 26                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 27                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 28                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# 29                  2505 U S HIGHWAY 431 NORTH            BOAZ             AL   
#[Out]# ...                                        ...             ...            ...   
#[Out]# 44325  9301 NORTH CENTRAL EXPRESSWAY SUITE 100          DALLAS             TX   
#[Out]# 44326  9301 NORTH CENTRAL EXPRESSWAY SUITE 100          DALLAS             TX   
#[Out]# 44327  9301 NORTH CENTRAL EXPRESSWAY SUITE 100          DALLAS             TX   
#[Out]# 44328  9301 NORTH CENTRAL EXPRESSWAY SUITE 100          DALLAS             TX   
#[Out]# 44329  9301 NORTH CENTRAL EXPRESSWAY SUITE 100          DALLAS             TX   
#[Out]# 44330              1901 DOCTORS HOSPITAL DRIVE      BRIDGEPORT             TX   
#[Out]# 44331              1901 DOCTORS HOSPITAL DRIVE      BRIDGEPORT             TX   
#[Out]# 44332                 1317 LAKE POINTE PARKWAY      SUGAR LAND             TX   
#[Out]# 44333                 1317 LAKE POINTE PARKWAY      SUGAR LAND             TX   
#[Out]# 44334                 1317 LAKE POINTE PARKWAY      SUGAR LAND             TX   
#[Out]# 44335                 1317 LAKE POINTE PARKWAY      SUGAR LAND             TX   
#[Out]# 44336                        9522 HUEBNER ROAD     SAN ANTONIO             TX   
#[Out]# 44337                        9522 HUEBNER ROAD     SAN ANTONIO             TX   
#[Out]# 44338                        9522 HUEBNER ROAD     SAN ANTONIO             TX   
#[Out]# 44339                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44340                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44341                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44342                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44343                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44344                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44345                     1139 E SONTERRA BLVD     SAN ANTONIO             TX   
#[Out]# 44346                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44347                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44348                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44349                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44350                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44351                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44352                       17400 ST LUKES WAY   THE WOODLANDS             TX   
#[Out]# 44353                       6130 PARKWAY DRIVE  CORPUS CHRISTI             TX   
#[Out]# 44354                       6130 PARKWAY DRIVE  CORPUS CHRISTI             TX   
#[Out]# 
#[Out]#        Provider Zip Code Hospital Referral Region (HRR) Description  \
#[Out]# 0                  36301                                AL - Dothan   
#[Out]# 1                  36301                                AL - Dothan   
#[Out]# 2                  36301                                AL - Dothan   
#[Out]# 3                  36301                                AL - Dothan   
#[Out]# 4                  36301                                AL - Dothan   
#[Out]# 5                  36301                                AL - Dothan   
#[Out]# 6                  36301                                AL - Dothan   
#[Out]# 7                  36301                                AL - Dothan   
#[Out]# 8                  36301                                AL - Dothan   
#[Out]# 9                  36301                                AL - Dothan   
#[Out]# 10                 36301                                AL - Dothan   
#[Out]# 11                 36301                                AL - Dothan   
#[Out]# 12                 36301                                AL - Dothan   
#[Out]# 13                 36301                                AL - Dothan   
#[Out]# 14                 36301                                AL - Dothan   
#[Out]# 15                 36301                                AL - Dothan   
#[Out]# 16                 36301                                AL - Dothan   
#[Out]# 17                 36301                                AL - Dothan   
#[Out]# 18                 36301                                AL - Dothan   
#[Out]# 19                 36301                                AL - Dothan   
#[Out]# 20                 36301                                AL - Dothan   
#[Out]# 21                 36301                                AL - Dothan   
#[Out]# 22                 35957                            AL - Birmingham   
#[Out]# 23                 35957                            AL - Birmingham   
#[Out]# 24                 35957                            AL - Birmingham   
#[Out]# 25                 35957                            AL - Birmingham   
#[Out]# 26                 35957                            AL - Birmingham   
#[Out]# 27                 35957                            AL - Birmingham   
#[Out]# 28                 35957                            AL - Birmingham   
#[Out]# 29                 35957                            AL - Birmingham   
#[Out]# ...                  ...                                        ...   
#[Out]# 44325              75231                                TX - Dallas   
#[Out]# 44326              75231                                TX - Dallas   
#[Out]# 44327              75231                                TX - Dallas   
#[Out]# 44328              75231                                TX - Dallas   
#[Out]# 44329              75231                                TX - Dallas   
#[Out]# 44330              76426                                TX - Dallas   
#[Out]# 44331              76426                                TX - Dallas   
#[Out]# 44332              77478                               TX - Houston   
#[Out]# 44333              77478                               TX - Houston   
#[Out]# 44334              77478                               TX - Houston   
#[Out]# 44335              77478                               TX - Houston   
#[Out]# 44336              78240                           TX - San Antonio   
#[Out]# 44337              78240                           TX - San Antonio   
#[Out]# 44338              78240                           TX - San Antonio   
#[Out]# 44339              78258                           TX - San Antonio   
#[Out]# 44340              78258                           TX - San Antonio   
#[Out]# 44341              78258                           TX - San Antonio   
#[Out]# 44342              78258                           TX - San Antonio   
#[Out]# 44343              78258                           TX - San Antonio   
#[Out]# 44344              78258                           TX - San Antonio   
#[Out]# 44345              78258                           TX - San Antonio   
#[Out]# 44346              77384                               TX - Houston   
#[Out]# 44347              77384                               TX - Houston   
#[Out]# 44348              77384                               TX - Houston   
#[Out]# 44349              77384                               TX - Houston   
#[Out]# 44350              77384                               TX - Houston   
#[Out]# 44351              77384                               TX - Houston   
#[Out]# 44352              77384                               TX - Houston   
#[Out]# 44353              78414                        TX - Corpus Christi   
#[Out]# 44354              78414                        TX - Corpus Christi   
#[Out]# 
#[Out]#        Outpatient Services  Average  Estimated Submitted Charges  \
#[Out]# 0                      639                            391.647167   
#[Out]# 1                      503                            595.826918   
#[Out]# 2                       23                           3531.883478   
#[Out]# 3                       32                           4596.947813   
#[Out]# 4                      142                            218.820000   
#[Out]# 5                      363                            988.044821   
#[Out]# 6                      385                           2084.466597   
#[Out]# 7                       67                           1438.853881   
#[Out]# 8                     3169                           2228.793373   
#[Out]# 9                      729                           4468.120329   
#[Out]# 10                     279                            805.981434   
#[Out]# 11                    1737                            858.172303   
#[Out]# 12                     844                           2024.289953   
#[Out]# 13                      40                           2968.674250   
#[Out]# 14                    1656                           3729.894746   
#[Out]# 15                     250                            251.405120   
#[Out]# 16                     509                           7056.103929   
#[Out]# 17                     210                            292.374714   
#[Out]# 18                    4295                            232.627716   
#[Out]# 19                    2410                            210.990788   
#[Out]# 20                     343                            246.103324   
#[Out]# 21                      18                            261.000000   
#[Out]# 22                     123                            229.488293   
#[Out]# 23                     119                            445.773529   
#[Out]# 24                      15                           4494.104667   
#[Out]# 25                      45                           3082.624000   
#[Out]# 26                     389                            427.439589   
#[Out]# 27                     188                            463.559840   
#[Out]# 28                      12                            490.762500   
#[Out]# 29                     405                           1367.972519   
#[Out]# ...                    ...                                   ...   
#[Out]# 44325                  116                           2421.678879   
#[Out]# 44326                   16                           2187.407500   
#[Out]# 44327                  790                           2778.552481   
#[Out]# 44328                  153                            760.844248   
#[Out]# 44329                  537                           2671.794264   
#[Out]# 44330                   11                           3922.515455   
#[Out]# 44331                   31                           2743.193548   
#[Out]# 44332                   87                            726.606322   
#[Out]# 44333                   95                           1911.389474   
#[Out]# 44334                  118                           3941.000000   
#[Out]# 44335                   89                           3394.606742   
#[Out]# 44336                   26                           6096.760000   
#[Out]# 44337                  141                           4738.698936   
#[Out]# 44338                   13                           2371.342308   
#[Out]# 44339                   16                            955.875000   
#[Out]# 44340                   28                           1908.368929   
#[Out]# 44341                   73                           2379.430137   
#[Out]# 44342                  270                           3620.371259   
#[Out]# 44343                   15                           3466.342000   
#[Out]# 44344                   87                           3631.190575   
#[Out]# 44345                  126                           6255.706746   
#[Out]# 44346                   22                           1877.227273   
#[Out]# 44347                   22                           3460.695455   
#[Out]# 44348                  124                           1793.411290   
#[Out]# 44349                   47                           3941.159574   
#[Out]# 44350                   69                           7496.957536   
#[Out]# 44351                  437                           3579.627002   
#[Out]# 44352                   23                           7588.943043   
#[Out]# 44353                   11                           6669.807273   
#[Out]# 44354                   61                           3381.782787   
#[Out]# 
#[Out]#        Average Total Payments  
#[Out]# 0                   54.944992  
#[Out]# 1                   83.515785  
#[Out]# 2                  254.154783  
#[Out]# 3                  507.305000  
#[Out]# 4                   87.600000  
#[Out]# 5                   93.598705  
#[Out]# 6                  147.574442  
#[Out]# 7                  240.756567  
#[Out]# 8                  480.496447  
#[Out]# 9                  697.154664  
#[Out]# 10                  56.257993  
#[Out]# 11                 134.917219  
#[Out]# 12                 339.310510  
#[Out]# 13                 486.599500  
#[Out]# 14                 300.470163  
#[Out]# 15                  54.723600  
#[Out]# 16                 590.798998  
#[Out]# 17                  49.344286  
#[Out]# 18                  63.957399  
#[Out]# 19                  84.187274  
#[Out]# 20                 111.791050  
#[Out]# 21                  97.110556  
#[Out]# 22                  45.754878  
#[Out]# 23                  84.647395  
#[Out]# 24                 297.904000  
#[Out]# 25                 517.594444  
#[Out]# 26                  88.126504  
#[Out]# 27                  95.866383  
#[Out]# 28                 148.742500  
#[Out]# 29                 490.642593  
#[Out]# ...                       ...  
#[Out]# 44325              167.217155  
#[Out]# 44326              247.432500  
#[Out]# 44327              513.445215  
#[Out]# 44328              146.739608  
#[Out]# 44329              325.538603  
#[Out]# 44330              521.726364  
#[Out]# 44331              322.344194  
#[Out]# 44332               63.656897  
#[Out]# 44333              150.613053  
#[Out]# 44334              371.056525  
#[Out]# 44335              441.278539  
#[Out]# 44336              523.621923  
#[Out]# 44337              743.041631  
#[Out]# 44338              102.718462  
#[Out]# 44339               88.745625  
#[Out]# 44340              269.436429  
#[Out]# 44341              140.925342  
#[Out]# 44342              358.458593  
#[Out]# 44343              516.634000  
#[Out]# 44344              396.389540  
#[Out]# 44345              628.617143  
#[Out]# 44346              106.855000  
#[Out]# 44347              175.351364  
#[Out]# 44348              151.844758  
#[Out]# 44349              383.988085  
#[Out]# 44350              544.843478  
#[Out]# 44351              332.909702  
#[Out]# 44352              666.540870  
#[Out]# 44353             1222.833636  
#[Out]# 44354              511.792787  
#[Out]# 
#[Out]# [44355 rows x 11 columns]
data.columns
#[Out]# Index(['APC', 'Provider Id', 'Provider Name', 'Provider Street Address',
#[Out]#        'Provider City', 'Provider State', 'Provider Zip Code',
#[Out]#        'Hospital Referral Region (HRR) Description', 'Outpatient Services',
#[Out]#        'Average  Estimated Submitted Charges', 'Average Total Payments'],
#[Out]#       dtype='object')
data["APC"]
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
data["APC"][data["APC"].map(str.contains("Diabetes"))
]
data["APC"][data["APC"].map(str.contains("Diabetes"))]
data["APC"]
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
data["APC"][data["APC"].astype(str).map(str.contains("Diabetes"))]
data["APC"]
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
a=data["APC"].astype(str)
a
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
a
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
a=data["APC"].astype(str).copy()
a
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
a.head()
#[Out]# 0     0013 - Level II Debridement & Destruction
#[Out]# 1    0015 - Level III Debridement & Destruction
#[Out]# 2               0019 - Level I Excision/ Biopsy
#[Out]# 3              0020 - Level II Excision/ Biopsy
#[Out]# 4          0078 - Level III Pulmonary Treatment
#[Out]# Name: APC, dtype: object
def isin(candidate, search):
    return search in candidate
data["APC"][data["APC"].map(isin("Diabetes"))]
data["APC"][data["APC"].map(lambda x: isin(x,"Diabetes"))]
#[Out]# Series([], Name: APC, dtype: object)
data["APC]
data["APC"]
#[Out]# 0                0013 - Level II Debridement & Destruction
#[Out]# 1               0015 - Level III Debridement & Destruction
#[Out]# 2                          0019 - Level I Excision/ Biopsy
#[Out]# 3                         0020 - Level II Excision/ Biopsy
#[Out]# 4                     0078 - Level III Pulmonary Treatment
#[Out]# 5          0096 - Level II Noninvasive Physiologic Studies
#[Out]# 6                          0204 - Level I Nerve Injections
#[Out]# 7                         0206 - Level II Nerve Injections
#[Out]# 8                        0207 - Level III Nerve Injections
#[Out]# 9        0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 10       0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 11       0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 12         0269 - Level II Echocardiogram Without Contrast
#[Out]# 13        0270 - Level III Echocardiogram Without Contrast
#[Out]# 14       0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 15                         0368 - Level II Pulmonary Tests
#[Out]# 16                         0377 - Level II Cardiac Imaging
#[Out]# 17                   0604 - Level 1 Hospital Clinic Visits
#[Out]# 18                   0605 - Level 2 Hospital Clinic Visits
#[Out]# 19                   0606 - Level 3 Hospital Clinic Visits
#[Out]# 20                   0607 - Level 4 Hospital Clinic Visits
#[Out]# 21          0692 - Level II Electronic Analysis of Devices
#[Out]# 22               0013 - Level II Debridement & Destruction
#[Out]# 23              0015 - Level III Debridement & Destruction
#[Out]# 24                         0019 - Level I Excision/ Biopsy
#[Out]# 25                        0020 - Level II Excision/ Biopsy
#[Out]# 26                    0078 - Level III Pulmonary Treatment
#[Out]# 27         0096 - Level II Noninvasive Physiologic Studies
#[Out]# 28                         0204 - Level I Nerve Injections
#[Out]# 29                       0207 - Level III Nerve Injections
#[Out]#                                ...                        
#[Out]# 44325                      0204 - Level I Nerve Injections
#[Out]# 44326                     0206 - Level II Nerve Injections
#[Out]# 44327                    0207 - Level III Nerve Injections
#[Out]# 44328    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44329    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44330                    0207 - Level III Nerve Injections
#[Out]# 44331    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44332    0265 - Level I Diagnostic and Screening Ultras...
#[Out]# 44333    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44334      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44335    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44336                    0207 - Level III Nerve Injections
#[Out]# 44337    0209 - Level II Extended EEG, Sleep, and Cardi...
#[Out]# 44338       0692 - Level II Electronic Analysis of Devices
#[Out]# 44339                 0078 - Level III Pulmonary Treatment
#[Out]# 44340                     0206 - Level II Nerve Injections
#[Out]# 44341    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44342      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44343     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44344    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44345                      0377 - Level II Cardiac Imaging
#[Out]# 44346      0096 - Level II Noninvasive Physiologic Studies
#[Out]# 44347                      0204 - Level I Nerve Injections
#[Out]# 44348    0267 - Level III Diagnostic and Screening Ultr...
#[Out]# 44349      0269 - Level II Echocardiogram Without Contrast
#[Out]# 44350     0270 - Level III Echocardiogram Without Contrast
#[Out]# 44351    0336 - Magnetic Resonance Imaging and Magnetic...
#[Out]# 44352                      0377 - Level II Cardiac Imaging
#[Out]# 44353               0074 - Level IV Endoscopy Upper Airway
#[Out]# 44354                    0207 - Level III Nerve Injections
#[Out]# Name: APC, dtype: object
del data
del a
import gc
gc.collect()
#[Out]# 244
free -h
cd ../inpatient/
ls
data=pd.read_csv("./Medicare_Provider_Charge_Inpatient_DRG100_FY2013.csv")
len(data)
#[Out]# 157747
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments'],
#[Out]#       dtype='object')
data["markup"]=data["Average Covered Charges"]/data["Average Medicare Payments"]
data["markup"]
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 157717          NaN
#[Out]# 157718          NaN
#[Out]# 157719          NaN
#[Out]# 157720          NaN
#[Out]# 157721          NaN
#[Out]# 157722          NaN
#[Out]# 157723          NaN
#[Out]# 157724          NaN
#[Out]# 157725          NaN
#[Out]# 157726          NaN
#[Out]# 157727          NaN
#[Out]# 157728          NaN
#[Out]# 157729          NaN
#[Out]# 157730          NaN
#[Out]# 157731          NaN
#[Out]# 157732          NaN
#[Out]# 157733          NaN
#[Out]# 157734          NaN
#[Out]# 157735          NaN
#[Out]# 157736          NaN
#[Out]# 157737          NaN
#[Out]# 157738          NaN
#[Out]# 157739          NaN
#[Out]# 157740          NaN
#[Out]# 157741          NaN
#[Out]# 157742          NaN
#[Out]# 157743          NaN
#[Out]# 157744          NaN
#[Out]# 157745          NaN
#[Out]# 157746          NaN
#[Out]# Name: markup, dtype: float64
data["markup"].isnan()
data["markup"].isna()
data["markup"]
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 157717          NaN
#[Out]# 157718          NaN
#[Out]# 157719          NaN
#[Out]# 157720          NaN
#[Out]# 157721          NaN
#[Out]# 157722          NaN
#[Out]# 157723          NaN
#[Out]# 157724          NaN
#[Out]# 157725          NaN
#[Out]# 157726          NaN
#[Out]# 157727          NaN
#[Out]# 157728          NaN
#[Out]# 157729          NaN
#[Out]# 157730          NaN
#[Out]# 157731          NaN
#[Out]# 157732          NaN
#[Out]# 157733          NaN
#[Out]# 157734          NaN
#[Out]# 157735          NaN
#[Out]# 157736          NaN
#[Out]# 157737          NaN
#[Out]# 157738          NaN
#[Out]# 157739          NaN
#[Out]# 157740          NaN
#[Out]# 157741          NaN
#[Out]# 157742          NaN
#[Out]# 157743          NaN
#[Out]# 157744          NaN
#[Out]# 157745          NaN
#[Out]# 157746          NaN
#[Out]# Name: markup, dtype: float64
data["markup"].head()
#[Out]# 0    7.851694
#[Out]# 1    2.959651
#[Out]# 2    9.728113
#[Out]# 3    5.160359
#[Out]# 4    9.345131
#[Out]# Name: markup, dtype: float64
data["markup"].isnull()
#[Out]# 0         False
#[Out]# 1         False
#[Out]# 2         False
#[Out]# 3         False
#[Out]# 4         False
#[Out]# 5         False
#[Out]# 6         False
#[Out]# 7         False
#[Out]# 8         False
#[Out]# 9         False
#[Out]# 10        False
#[Out]# 11        False
#[Out]# 12        False
#[Out]# 13        False
#[Out]# 14        False
#[Out]# 15        False
#[Out]# 16        False
#[Out]# 17        False
#[Out]# 18        False
#[Out]# 19        False
#[Out]# 20        False
#[Out]# 21        False
#[Out]# 22        False
#[Out]# 23        False
#[Out]# 24        False
#[Out]# 25        False
#[Out]# 26        False
#[Out]# 27        False
#[Out]# 28        False
#[Out]# 29        False
#[Out]#           ...  
#[Out]# 157717     True
#[Out]# 157718     True
#[Out]# 157719     True
#[Out]# 157720     True
#[Out]# 157721     True
#[Out]# 157722     True
#[Out]# 157723     True
#[Out]# 157724     True
#[Out]# 157725     True
#[Out]# 157726     True
#[Out]# 157727     True
#[Out]# 157728     True
#[Out]# 157729     True
#[Out]# 157730     True
#[Out]# 157731     True
#[Out]# 157732     True
#[Out]# 157733     True
#[Out]# 157734     True
#[Out]# 157735     True
#[Out]# 157736     True
#[Out]# 157737     True
#[Out]# 157738     True
#[Out]# 157739     True
#[Out]# 157740     True
#[Out]# 157741     True
#[Out]# 157742     True
#[Out]# 157743     True
#[Out]# 157744     True
#[Out]# 157745     True
#[Out]# 157746     True
#[Out]# Name: markup, dtype: bool
sum(_)
#[Out]# 4147
a=data["markup"].notnull().copy()
a
#[Out]# 0          True
#[Out]# 1          True
#[Out]# 2          True
#[Out]# 3          True
#[Out]# 4          True
#[Out]# 5          True
#[Out]# 6          True
#[Out]# 7          True
#[Out]# 8          True
#[Out]# 9          True
#[Out]# 10         True
#[Out]# 11         True
#[Out]# 12         True
#[Out]# 13         True
#[Out]# 14         True
#[Out]# 15         True
#[Out]# 16         True
#[Out]# 17         True
#[Out]# 18         True
#[Out]# 19         True
#[Out]# 20         True
#[Out]# 21         True
#[Out]# 22         True
#[Out]# 23         True
#[Out]# 24         True
#[Out]# 25         True
#[Out]# 26         True
#[Out]# 27         True
#[Out]# 28         True
#[Out]# 29         True
#[Out]#           ...  
#[Out]# 157717    False
#[Out]# 157718    False
#[Out]# 157719    False
#[Out]# 157720    False
#[Out]# 157721    False
#[Out]# 157722    False
#[Out]# 157723    False
#[Out]# 157724    False
#[Out]# 157725    False
#[Out]# 157726    False
#[Out]# 157727    False
#[Out]# 157728    False
#[Out]# 157729    False
#[Out]# 157730    False
#[Out]# 157731    False
#[Out]# 157732    False
#[Out]# 157733    False
#[Out]# 157734    False
#[Out]# 157735    False
#[Out]# 157736    False
#[Out]# 157737    False
#[Out]# 157738    False
#[Out]# 157739    False
#[Out]# 157740    False
#[Out]# 157741    False
#[Out]# 157742    False
#[Out]# 157743    False
#[Out]# 157744    False
#[Out]# 157745    False
#[Out]# 157746    False
#[Out]# Name: markup, dtype: bool
a=data["markup"][data["markup"].notnull()].copy()
a
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 153570     4.221004
#[Out]# 153571     4.231542
#[Out]# 153572     4.357912
#[Out]# 153573     5.611160
#[Out]# 153574     5.485714
#[Out]# 153575     6.606009
#[Out]# 153576     5.929498
#[Out]# 153577     4.599760
#[Out]# 153578     3.920645
#[Out]# 153579     5.150522
#[Out]# 153580     4.015352
#[Out]# 153581     3.127321
#[Out]# 153582     6.614019
#[Out]# 153583     4.194986
#[Out]# 153584     4.113659
#[Out]# 153585     5.186539
#[Out]# 153586     5.402401
#[Out]# 153587     4.273100
#[Out]# 153588     5.986412
#[Out]# 153589     5.487793
#[Out]# 153590     6.497838
#[Out]# 153591     7.705330
#[Out]# 153592     4.317665
#[Out]# 153593     4.853535
#[Out]# 153594     3.513581
#[Out]# 153595     8.288979
#[Out]# 153596    12.270735
#[Out]# 153597     8.906624
#[Out]# 153598     7.553721
#[Out]# 153599    13.125079
#[Out]# Name: markup, dtype: float64
plt.hist(a,bins=1000)
#[Out]# (array([  4.00000000e+00,   5.00000000e+00,   6.00000000e+00,
#[Out]#          8.00000000e+00,   5.00000000e+00,   1.40000000e+01,
#[Out]#          1.60000000e+01,   3.00000000e+01,   1.50000000e+01,
#[Out]#          1.40000000e+01,   3.60000000e+01,   3.70000000e+01,
#[Out]#          3.40000000e+01,   5.10000000e+01,   5.30000000e+01,
#[Out]#          6.50000000e+01,   1.73000000e+02,   4.64000000e+02,
#[Out]#          6.72000000e+02,   6.55000000e+02,   5.87000000e+02,
#[Out]#          5.24000000e+02,   4.29000000e+02,   2.97000000e+02,
#[Out]#          2.46000000e+02,   2.18000000e+02,   2.23000000e+02,
#[Out]#          2.06000000e+02,   2.11000000e+02,   2.15000000e+02,
#[Out]#          2.24000000e+02,   2.72000000e+02,   2.72000000e+02,
#[Out]#          2.70000000e+02,   3.02000000e+02,   2.93000000e+02,
#[Out]#          3.67000000e+02,   3.32000000e+02,   3.93000000e+02,
#[Out]#          3.98000000e+02,   3.85000000e+02,   4.46000000e+02,
#[Out]#          4.50000000e+02,   4.76000000e+02,   4.88000000e+02,
#[Out]#          5.17000000e+02,   4.94000000e+02,   5.48000000e+02,
#[Out]#          5.39000000e+02,   6.00000000e+02,   6.41000000e+02,
#[Out]#          6.51000000e+02,   6.56000000e+02,   6.53000000e+02,
#[Out]#          6.77000000e+02,   6.97000000e+02,   7.65000000e+02,
#[Out]#          7.52000000e+02,   7.51000000e+02,   8.31000000e+02,
#[Out]#          7.80000000e+02,   8.17000000e+02,   7.89000000e+02,
#[Out]#          8.88000000e+02,   8.89000000e+02,   9.40000000e+02,
#[Out]#          8.69000000e+02,   9.47000000e+02,   8.89000000e+02,
#[Out]#          9.22000000e+02,   9.90000000e+02,   9.81000000e+02,
#[Out]#          9.69000000e+02,   9.57000000e+02,   9.95000000e+02,
#[Out]#          9.26000000e+02,   1.05000000e+03,   1.03500000e+03,
#[Out]#          1.03700000e+03,   1.05400000e+03,   1.03700000e+03,
#[Out]#          1.04700000e+03,   1.11100000e+03,   1.02600000e+03,
#[Out]#          1.05100000e+03,   1.11000000e+03,   1.08300000e+03,
#[Out]#          1.05800000e+03,   1.11300000e+03,   1.09900000e+03,
#[Out]#          1.13500000e+03,   1.06900000e+03,   1.00100000e+03,
#[Out]#          1.09300000e+03,   1.11900000e+03,   1.14400000e+03,
#[Out]#          1.04300000e+03,   1.08000000e+03,   1.07100000e+03,
#[Out]#          1.09500000e+03,   1.08500000e+03,   1.01100000e+03,
#[Out]#          1.08700000e+03,   1.03500000e+03,   1.06100000e+03,
#[Out]#          1.02400000e+03,   1.07400000e+03,   1.03800000e+03,
#[Out]#          1.07100000e+03,   1.01200000e+03,   9.54000000e+02,
#[Out]#          9.80000000e+02,   9.64000000e+02,   9.33000000e+02,
#[Out]#          9.82000000e+02,   9.64000000e+02,   8.91000000e+02,
#[Out]#          9.35000000e+02,   9.15000000e+02,   9.54000000e+02,
#[Out]#          9.06000000e+02,   8.98000000e+02,   8.62000000e+02,
#[Out]#          8.86000000e+02,   8.69000000e+02,   8.85000000e+02,
#[Out]#          8.63000000e+02,   8.09000000e+02,   8.10000000e+02,
#[Out]#          8.18000000e+02,   8.21000000e+02,   8.23000000e+02,
#[Out]#          7.90000000e+02,   8.12000000e+02,   8.06000000e+02,
#[Out]#          7.78000000e+02,   7.95000000e+02,   7.76000000e+02,
#[Out]#          7.44000000e+02,   7.24000000e+02,   7.48000000e+02,
#[Out]#          7.47000000e+02,   7.03000000e+02,   7.14000000e+02,
#[Out]#          7.02000000e+02,   6.85000000e+02,   6.75000000e+02,
#[Out]#          6.29000000e+02,   6.64000000e+02,   6.70000000e+02,
#[Out]#          6.15000000e+02,   6.49000000e+02,   6.13000000e+02,
#[Out]#          6.41000000e+02,   6.41000000e+02,   5.93000000e+02,
#[Out]#          5.86000000e+02,   5.91000000e+02,   6.10000000e+02,
#[Out]#          5.61000000e+02,   5.93000000e+02,   4.86000000e+02,
#[Out]#          5.76000000e+02,   5.07000000e+02,   5.34000000e+02,
#[Out]#          5.05000000e+02,   5.29000000e+02,   5.11000000e+02,
#[Out]#          4.85000000e+02,   4.91000000e+02,   5.21000000e+02,
#[Out]#          5.04000000e+02,   5.02000000e+02,   5.14000000e+02,
#[Out]#          5.07000000e+02,   4.58000000e+02,   4.92000000e+02,
#[Out]#          4.55000000e+02,   4.19000000e+02,   4.26000000e+02,
#[Out]#          4.53000000e+02,   4.60000000e+02,   4.46000000e+02,
#[Out]#          4.10000000e+02,   4.14000000e+02,   4.26000000e+02,
#[Out]#          3.77000000e+02,   3.99000000e+02,   3.87000000e+02,
#[Out]#          3.98000000e+02,   3.84000000e+02,   3.68000000e+02,
#[Out]#          3.88000000e+02,   3.80000000e+02,   3.43000000e+02,
#[Out]#          3.69000000e+02,   3.44000000e+02,   3.53000000e+02,
#[Out]#          3.07000000e+02,   3.28000000e+02,   3.47000000e+02,
#[Out]#          3.30000000e+02,   3.35000000e+02,   3.24000000e+02,
#[Out]#          3.05000000e+02,   3.30000000e+02,   3.15000000e+02,
#[Out]#          2.94000000e+02,   3.18000000e+02,   2.80000000e+02,
#[Out]#          2.92000000e+02,   2.80000000e+02,   3.01000000e+02,
#[Out]#          2.93000000e+02,   2.98000000e+02,   2.53000000e+02,
#[Out]#          2.92000000e+02,   2.64000000e+02,   2.35000000e+02,
#[Out]#          2.66000000e+02,   2.92000000e+02,   2.60000000e+02,
#[Out]#          2.46000000e+02,   2.23000000e+02,   2.52000000e+02,
#[Out]#          2.31000000e+02,   2.20000000e+02,   2.59000000e+02,
#[Out]#          2.57000000e+02,   2.32000000e+02,   2.12000000e+02,
#[Out]#          2.53000000e+02,   2.09000000e+02,   2.15000000e+02,
#[Out]#          2.19000000e+02,   2.25000000e+02,   2.52000000e+02,
#[Out]#          2.19000000e+02,   2.17000000e+02,   1.94000000e+02,
#[Out]#          2.04000000e+02,   2.06000000e+02,   2.08000000e+02,
#[Out]#          2.10000000e+02,   1.80000000e+02,   1.63000000e+02,
#[Out]#          2.17000000e+02,   1.81000000e+02,   1.87000000e+02,
#[Out]#          1.86000000e+02,   1.76000000e+02,   1.89000000e+02,
#[Out]#          1.71000000e+02,   1.51000000e+02,   1.65000000e+02,
#[Out]#          1.65000000e+02,   1.54000000e+02,   1.71000000e+02,
#[Out]#          1.39000000e+02,   1.72000000e+02,   1.37000000e+02,
#[Out]#          1.49000000e+02,   1.67000000e+02,   1.55000000e+02,
#[Out]#          1.51000000e+02,   1.33000000e+02,   1.30000000e+02,
#[Out]#          1.42000000e+02,   1.51000000e+02,   1.33000000e+02,
#[Out]#          1.35000000e+02,   1.19000000e+02,   1.38000000e+02,
#[Out]#          1.22000000e+02,   1.38000000e+02,   1.21000000e+02,
#[Out]#          1.36000000e+02,   1.18000000e+02,   1.18000000e+02,
#[Out]#          1.33000000e+02,   1.27000000e+02,   1.17000000e+02,
#[Out]#          1.20000000e+02,   1.28000000e+02,   1.36000000e+02,
#[Out]#          1.10000000e+02,   1.05000000e+02,   1.20000000e+02,
#[Out]#          1.03000000e+02,   8.30000000e+01,   1.06000000e+02,
#[Out]#          1.04000000e+02,   1.10000000e+02,   1.12000000e+02,
#[Out]#          1.14000000e+02,   1.08000000e+02,   1.04000000e+02,
#[Out]#          9.80000000e+01,   1.12000000e+02,   9.90000000e+01,
#[Out]#          7.70000000e+01,   9.50000000e+01,   1.00000000e+02,
#[Out]#          7.30000000e+01,   9.80000000e+01,   9.40000000e+01,
#[Out]#          7.20000000e+01,   8.30000000e+01,   7.10000000e+01,
#[Out]#          8.20000000e+01,   7.90000000e+01,   7.90000000e+01,
#[Out]#          7.60000000e+01,   8.20000000e+01,   6.70000000e+01,
#[Out]#          7.10000000e+01,   7.70000000e+01,   7.90000000e+01,
#[Out]#          7.60000000e+01,   6.40000000e+01,   7.40000000e+01,
#[Out]#          8.20000000e+01,   8.20000000e+01,   6.70000000e+01,
#[Out]#          6.00000000e+01,   6.90000000e+01,   7.00000000e+01,
#[Out]#          5.90000000e+01,   5.80000000e+01,   5.60000000e+01,
#[Out]#          5.10000000e+01,   5.90000000e+01,   5.50000000e+01,
#[Out]#          5.80000000e+01,   5.00000000e+01,   4.20000000e+01,
#[Out]#          5.60000000e+01,   5.20000000e+01,   6.00000000e+01,
#[Out]#          5.70000000e+01,   5.40000000e+01,   6.70000000e+01,
#[Out]#          4.70000000e+01,   7.10000000e+01,   5.30000000e+01,
#[Out]#          6.10000000e+01,   5.00000000e+01,   5.50000000e+01,
#[Out]#          4.50000000e+01,   5.50000000e+01,   4.30000000e+01,
#[Out]#          5.60000000e+01,   5.40000000e+01,   4.00000000e+01,
#[Out]#          4.40000000e+01,   4.50000000e+01,   5.40000000e+01,
#[Out]#          3.60000000e+01,   3.60000000e+01,   4.40000000e+01,
#[Out]#          3.10000000e+01,   2.60000000e+01,   3.70000000e+01,
#[Out]#          3.90000000e+01,   3.60000000e+01,   5.20000000e+01,
#[Out]#          3.70000000e+01,   3.30000000e+01,   3.30000000e+01,
#[Out]#          2.60000000e+01,   4.80000000e+01,   3.00000000e+01,
#[Out]#          3.10000000e+01,   2.60000000e+01,   3.20000000e+01,
#[Out]#          3.90000000e+01,   3.40000000e+01,   2.80000000e+01,
#[Out]#          3.10000000e+01,   2.70000000e+01,   3.10000000e+01,
#[Out]#          2.30000000e+01,   3.70000000e+01,   3.50000000e+01,
#[Out]#          1.60000000e+01,   2.50000000e+01,   3.00000000e+01,
#[Out]#          3.10000000e+01,   2.70000000e+01,   2.10000000e+01,
#[Out]#          2.40000000e+01,   2.10000000e+01,   2.10000000e+01,
#[Out]#          2.40000000e+01,   1.90000000e+01,   1.80000000e+01,
#[Out]#          2.40000000e+01,   2.00000000e+01,   2.30000000e+01,
#[Out]#          3.10000000e+01,   1.80000000e+01,   1.60000000e+01,
#[Out]#          2.20000000e+01,   1.60000000e+01,   1.80000000e+01,
#[Out]#          1.30000000e+01,   1.80000000e+01,   1.90000000e+01,
#[Out]#          1.70000000e+01,   1.90000000e+01,   1.60000000e+01,
#[Out]#          1.30000000e+01,   2.00000000e+01,   7.00000000e+00,
#[Out]#          1.60000000e+01,   1.20000000e+01,   1.50000000e+01,
#[Out]#          9.00000000e+00,   1.40000000e+01,   1.30000000e+01,
#[Out]#          1.30000000e+01,   1.70000000e+01,   1.70000000e+01,
#[Out]#          1.40000000e+01,   1.60000000e+01,   6.00000000e+00,
#[Out]#          1.30000000e+01,   1.10000000e+01,   1.70000000e+01,
#[Out]#          1.10000000e+01,   1.00000000e+01,   1.00000000e+01,
#[Out]#          1.20000000e+01,   1.40000000e+01,   1.20000000e+01,
#[Out]#          1.50000000e+01,   1.60000000e+01,   1.60000000e+01,
#[Out]#          1.00000000e+01,   1.00000000e+01,   9.00000000e+00,
#[Out]#          1.20000000e+01,   1.10000000e+01,   1.40000000e+01,
#[Out]#          1.00000000e+01,   7.00000000e+00,   7.00000000e+00,
#[Out]#          6.00000000e+00,   8.00000000e+00,   9.00000000e+00,
#[Out]#          1.10000000e+01,   1.10000000e+01,   1.50000000e+01,
#[Out]#          5.00000000e+00,   8.00000000e+00,   6.00000000e+00,
#[Out]#          8.00000000e+00,   7.00000000e+00,   6.00000000e+00,
#[Out]#          4.00000000e+00,   9.00000000e+00,   5.00000000e+00,
#[Out]#          6.00000000e+00,   4.00000000e+00,   9.00000000e+00,
#[Out]#          7.00000000e+00,   5.00000000e+00,   1.20000000e+01,
#[Out]#          4.00000000e+00,   8.00000000e+00,   1.10000000e+01,
#[Out]#          5.00000000e+00,   7.00000000e+00,   5.00000000e+00,
#[Out]#          5.00000000e+00,   7.00000000e+00,   3.00000000e+00,
#[Out]#          3.00000000e+00,   5.00000000e+00,   4.00000000e+00,
#[Out]#          6.00000000e+00,   8.00000000e+00,   7.00000000e+00,
#[Out]#          3.00000000e+00,   5.00000000e+00,   7.00000000e+00,
#[Out]#          3.00000000e+00,   2.00000000e+00,   3.00000000e+00,
#[Out]#          5.00000000e+00,   5.00000000e+00,   6.00000000e+00,
#[Out]#          5.00000000e+00,   1.00000000e+00,   3.00000000e+00,
#[Out]#          3.00000000e+00,   5.00000000e+00,   3.00000000e+00,
#[Out]#          4.00000000e+00,   3.00000000e+00,   3.00000000e+00,
#[Out]#          5.00000000e+00,   3.00000000e+00,   1.00000000e+00,
#[Out]#          6.00000000e+00,   3.00000000e+00,   6.00000000e+00,
#[Out]#          7.00000000e+00,   3.00000000e+00,   2.00000000e+00,
#[Out]#          3.00000000e+00,   4.00000000e+00,   4.00000000e+00,
#[Out]#          3.00000000e+00,   2.00000000e+00,   2.00000000e+00,
#[Out]#          4.00000000e+00,   4.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   3.00000000e+00,   6.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   6.00000000e+00,
#[Out]#          3.00000000e+00,   5.00000000e+00,   1.00000000e+00,
#[Out]#          2.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          2.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          4.00000000e+00,   1.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   3.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   3.00000000e+00,   3.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   2.00000000e+00,
#[Out]#          1.00000000e+00,   2.00000000e+00,   2.00000000e+00,
#[Out]#          2.00000000e+00,   2.00000000e+00,   4.00000000e+00,
#[Out]#          4.00000000e+00,   1.00000000e+00,   2.00000000e+00,
#[Out]#          2.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          4.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   4.00000000e+00,   2.00000000e+00,
#[Out]#          4.00000000e+00,   4.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   2.00000000e+00,
#[Out]#          2.00000000e+00,   1.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   2.00000000e+00,   2.00000000e+00,
#[Out]#          1.00000000e+00,   2.00000000e+00,   3.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   1.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   2.00000000e+00,   0.00000000e+00,
#[Out]#          2.00000000e+00,   2.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   3.00000000e+00,   2.00000000e+00,
#[Out]#          2.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   4.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   2.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          2.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          3.00000000e+00,   1.00000000e+00,   2.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          2.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   1.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   1.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          0.00000000e+00,   0.00000000e+00,   0.00000000e+00,
#[Out]#          1.00000000e+00]), array([  0.53886044,   0.57207502,   0.6052896 , ...,  33.68700745,
#[Out]#         33.72022203,  33.7534366 ]), <a list of 1000 Patch objects>)
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'markup'],
#[Out]#       dtype='object')
b=data["markup"][(data["DRG Definition"][:3]=="305)].dropna().copy()
b=data["markup"][(data["DRG Definition"][:3]=="305].dropna().copy()
b=data["markup"][(data["DRG Definition"][:3]=="305")].dropna().copy()
data["DRG"]
data["DRG Definition"]
#[Out]# 0         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 5         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 6         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 7         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 8         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 9         039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 10        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 11        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 12        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 13        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 14        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 15        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 16        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 17        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 18        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 19        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 20        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 21        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 22        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 23        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 24        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 25        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 26        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 27        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 28        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 29        039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]#                             ...                   
#[Out]# 157717                                         NaN
#[Out]# 157718                                         NaN
#[Out]# 157719                                         NaN
#[Out]# 157720                                         NaN
#[Out]# 157721                                         NaN
#[Out]# 157722                                         NaN
#[Out]# 157723                                         NaN
#[Out]# 157724                                         NaN
#[Out]# 157725                                         NaN
#[Out]# 157726                                         NaN
#[Out]# 157727                                         NaN
#[Out]# 157728                                         NaN
#[Out]# 157729                                         NaN
#[Out]# 157730                                         NaN
#[Out]# 157731                                         NaN
#[Out]# 157732                                         NaN
#[Out]# 157733                                         NaN
#[Out]# 157734                                         NaN
#[Out]# 157735                                         NaN
#[Out]# 157736                                         NaN
#[Out]# 157737                                         NaN
#[Out]# 157738                                         NaN
#[Out]# 157739                                         NaN
#[Out]# 157740                                         NaN
#[Out]# 157741                                         NaN
#[Out]# 157742                                         NaN
#[Out]# 157743                                         NaN
#[Out]# 157744                                         NaN
#[Out]# 157745                                         NaN
#[Out]# 157746                                         NaN
#[Out]# Name: DRG Definition, dtype: object
data["DRG Definition"].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 4    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
data["DRG Definition"][:4].head()
#[Out]# 0    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 1    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 2    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# 3    039 - EXTRACRANIAL PROCEDURES W/O CC/MCC
#[Out]# Name: DRG Definition, dtype: object
%logstart
%stoplog
%logstop
%logstart
%logstart?
%logstop
# Fri, 11 Sep 2015 05:32:37
data.columns
#[Out]# Index(['DRG Definition', 'Provider Id', 'Provider Name',
#[Out]#        'Provider Street Address', 'Provider City', 'Provider State',
#[Out]#        'Provider Zip Code', 'Hospital Referral Region (HRR) Description',
#[Out]#        'Total Discharges', 'Average Covered Charges', 'Average Total Payments',
#[Out]#        'Average Medicare Payments', 'markup'],
#[Out]#       dtype='object')
# Fri, 11 Sep 2015 05:32:44
a
#[Out]# 0          7.851694
#[Out]# 1          2.959651
#[Out]# 2          9.728113
#[Out]# 3          5.160359
#[Out]# 4          9.345131
#[Out]# 5          3.084526
#[Out]# 6          5.310911
#[Out]# 7          2.477040
#[Out]# 8          7.259064
#[Out]# 9          6.926306
#[Out]# 10        17.117287
#[Out]# 11        19.784737
#[Out]# 12         8.971919
#[Out]# 13         5.076921
#[Out]# 14         7.496747
#[Out]# 15         3.625974
#[Out]# 16         3.421455
#[Out]# 17         4.101119
#[Out]# 18         3.890762
#[Out]# 19         9.222944
#[Out]# 20        14.558852
#[Out]# 21         4.141600
#[Out]# 22        25.201655
#[Out]# 23         6.011638
#[Out]# 24         5.120508
#[Out]# 25         4.421321
#[Out]# 26         3.788699
#[Out]# 27         5.814073
#[Out]# 28         5.553179
#[Out]# 29         5.293210
#[Out]#             ...    
#[Out]# 153570     4.221004
#[Out]# 153571     4.231542
#[Out]# 153572     4.357912
#[Out]# 153573     5.611160
#[Out]# 153574     5.485714
#[Out]# 153575     6.606009
#[Out]# 153576     5.929498
#[Out]# 153577     4.599760
#[Out]# 153578     3.920645
#[Out]# 153579     5.150522
#[Out]# 153580     4.015352
#[Out]# 153581     3.127321
#[Out]# 153582     6.614019
#[Out]# 153583     4.194986
#[Out]# 153584     4.113659
#[Out]# 153585     5.186539
#[Out]# 153586     5.402401
#[Out]# 153587     4.273100
#[Out]# 153588     5.986412
#[Out]# 153589     5.487793
#[Out]# 153590     6.497838
#[Out]# 153591     7.705330
#[Out]# 153592     4.317665
#[Out]# 153593     4.853535
#[Out]# 153594     3.513581
#[Out]# 153595     8.288979
#[Out]# 153596    12.270735
#[Out]# 153597     8.906624
#[Out]# 153598     7.553721
#[Out]# 153599    13.125079
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 05:32:50
a.head()
#[Out]# 0    7.851694
#[Out]# 1    2.959651
#[Out]# 2    9.728113
#[Out]# 3    5.160359
#[Out]# 4    9.345131
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 05:33:02
def f(x):
    return x+1

# Fri, 11 Sep 2015 05:33:23
b=a.map(f)
# Fri, 11 Sep 2015 05:33:33
b.head()
#[Out]# 0     8.851694
#[Out]# 1     3.959651
#[Out]# 2    10.728113
#[Out]# 3     6.160359
#[Out]# 4    10.345131
#[Out]# Name: markup, dtype: float64
# Fri, 11 Sep 2015 05:35:51
exit
