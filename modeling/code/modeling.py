# IPython log file

%logstart -rt modeling.py
# Wed, 16 Sep 2015 17:41:51
import pandas as pd
# Wed, 16 Sep 2015 17:42:03
cons=pd.read_csv("./consolidated_frame_stripped.csv")
# Wed, 16 Sep 2015 17:42:28
cons.columns
# Wed, 16 Sep 2015 17:42:41
del cons
# Wed, 16 Sep 2015 17:42:45
cons=pd.read_csv("./consolidated_frame.csv")
# Wed, 16 Sep 2015 17:44:24
import get_table_columns as g
# Wed, 16 Sep 2015 17:44:35
cons=g.clean_columns(cons)
# Wed, 16 Sep 2015 17:44:38
cons.columns
# Wed, 16 Sep 2015 17:45:31
cons["HIGH BLOOD PRESSURE DIAG (>17)"]
# Wed, 16 Sep 2015 17:45:43
cons["HIGH BLOOD PRESSURE DIAG (>17)"].hist()
# Wed, 16 Sep 2015 17:45:52
cons["HIGH BLOOD PRESSURE DIAG (>17)"].hist(bins=5)
# Wed, 16 Sep 2015 17:46:08
cons["HIGH BLOOD PRESSURE DIAG (>17)"].hist(bins=10)
# Wed, 16 Sep 2015 17:46:21
cons["HIGH BLOOD PRESSURE DIAG (>17)"].unique()
# Wed, 16 Sep 2015 17:46:29
cons["HIGH BLOOD PRESSURE DIAG (>17)"].unique()
# Wed, 16 Sep 2015 17:46:32
cons["HIGH BLOOD PRESSURE DIAG (>17)"].hist(bins=6)
# Wed, 16 Sep 2015 17:47:12
cons["SEX"]
# Wed, 16 Sep 2015 17:47:15
cons["SEX"].hist()
# Wed, 16 Sep 2015 17:48:25
cons["AGE AS OF 12/31/12 (EDITED/IMPUTED)"]
# Wed, 16 Sep 2015 17:48:31
cons["AGE AS OF 12/31/12 (EDITED/IMPUTED)"].hist()
# Wed, 16 Sep 2015 17:48:39
cons["AGE AS OF 12/31/12 (EDITED/IMPUTED)"].hist(bins=1000)
# Wed, 16 Sep 2015 17:49:29
cons["AGE AS OF 12/31/12 (EDITED/IMPUTED)"].hist(bins=1000)
# Wed, 16 Sep 2015 17:50:19
cons["FINAL PERSON WEIGHT 2012"].hist(bins=1000)
# Wed, 16 Sep 2015 17:50:36
cons["FINAL PERSON WEIGHT 2012"]
# Wed, 16 Sep 2015 17:51:06
cons["FINAL SAQ PERSON WEIGHT 2012"]
# Wed, 16 Sep 2015 17:57:51
cons["ADULT BODY MASS INDEX (>17) - RD 5/3"]
# Wed, 16 Sep 2015 17:57:59
cons["ADULT BODY MASS INDEX (>17) - RD 5/3"].hist(bins=1000)
# Wed, 16 Sep 2015 17:58:17
cons["ADULT BODY MASS INDEX (>17) - RD 5/3"].count(-1.0)
# Wed, 16 Sep 2015 17:58:25
cons["ADULT BODY MASS INDEX (>17) - RD 5/3"].describe()
# Wed, 16 Sep 2015 17:59:25
cons["FAMILY'S TOTAL INCOME"]
# Wed, 16 Sep 2015 17:59:29
cons["FAMILY'S TOTAL INCOME"].describe()
# Wed, 16 Sep 2015 17:59:45
cons["FAMILY'S TOTAL INCOME"].hist(bins=1000)
# Wed, 16 Sep 2015 18:00:10
cons[cons["FAMILY'S TOTAL INCOME"]>0].hist(bins=1000)
# Wed, 16 Sep 2015 18:06:43
cons["FAMILY'S TOTAL INCOME"]>0
# Wed, 16 Sep 2015 18:06:55
cons["FAMILY'S TOTAL INCOME"]<=0
# Wed, 16 Sep 2015 18:06:57
sum(_)
# Wed, 16 Sep 2015 18:07:00
len(cons)
# Wed, 16 Sep 2015 18:07:39
temp=cons[cons["FAMILY'S TOTAL INCOME"]>0]["FAMILY'S TOTAL INCOME"]
# Wed, 16 Sep 2015 18:09:42
temp=cons["FAMILY'S TOTAL INCOME"][cons["FAMILY'S TOTAL INCOME"]>0]
# Wed, 16 Sep 2015 18:09:47
temp.hist()
# Wed, 16 Sep 2015 18:09:59
temp.hist(bins=100)
# Wed, 16 Sep 2015 18:10:25
temp.hist(bins=100)
# Wed, 16 Sep 2015 18:11:04
cons["TOTAL OFF-BASED DR EXP 12"]
# Wed, 16 Sep 2015 18:11:37
cons["TOTAL OFF-BASED DR EXP 12"].describe()
# Wed, 16 Sep 2015 18:11:52
cons["TOTAL OFF-BASED DR EXP 12"].hist()
# Wed, 16 Sep 2015 18:11:58
cons["TOTAL OFF-BASED DR EXP 12"].hist(bins=100)
# Wed, 16 Sep 2015 18:12:06
cons["TOTAL OFF-BASED DR EXP 12"].hist(bins=1000)
# Wed, 16 Sep 2015 18:12:18
cons["TOTAL OFF-BASED DR EXP 12"].describe()
# Wed, 16 Sep 2015 18:12:30
cons["TOTAL OFF-BASED DR EXP 12"].mode()
# Wed, 16 Sep 2015 18:13:19
temp=cons["TOTAL OFF-BASED DR EXP 12"][cons["TOTAL OFF-BASED DR EXP 12"]>0]
# Wed, 16 Sep 2015 18:13:24
temp.describe()
# Wed, 16 Sep 2015 18:13:33
temp.hist(bins=100)
# Wed, 16 Sep 2015 18:13:43
temp.hist(bins=1000)
# Wed, 16 Sep 2015 18:14:27
cons["TOTAL OFF-BASED EXP 12"].mode()
# Wed, 16 Sep 2015 18:14:34
cons["TOTAL OFFICE-BASED EXP 12"].mode()
# Wed, 16 Sep 2015 18:14:37
cons["TOTAL OFFICE-BASED EXP 12"]
# Wed, 16 Sep 2015 18:14:41
cons["TOTAL OFFICE-BASED EXP 12"].describe()
# Wed, 16 Sep 2015 18:15:05
temp=cons["TOTAL OFFICE-BASED EXP 12"][cons["TOTAL OFFICE-BASED EXP 12"]>0]
# Wed, 16 Sep 2015 18:15:12
temp.hist(bins=100)
# Wed, 16 Sep 2015 18:15:48
office=cons["TOTAL OFFICE-BASED EXP 12"]
# Wed, 16 Sep 2015 18:16:06
outpatient=cons["TOTAL OUTPATIENT PROVIDER EXP 12"]
# Wed, 16 Sep 2015 18:16:17
outpatient[outpatient>0]
# Wed, 16 Sep 2015 18:16:27
outpatient[outpatient>0].hist(bins=100)
# Wed, 16 Sep 2015 18:53:23
office
# Wed, 16 Sep 2015 19:03:44
weight=cons["FINAL PERSON WEIGHT 2012"]
# Wed, 16 Sep 2015 19:03:45
weight
# Wed, 16 Sep 2015 19:20:37
weight
# Wed, 16 Sep 2015 19:20:44
er
# Wed, 16 Sep 2015 19:21:09
er=cons["TOTAL ER FACILITY + DR EXP 12"]
# Wed, 16 Sep 2015 19:21:10
er
# Wed, 16 Sep 2015 19:21:45
inpatient=cons["TOT HOSP IP FACILITY + DR EXP 12"]
# Wed, 16 Sep 2015 19:21:49
outpatient
# Wed, 16 Sep 2015 19:23:44
age=cons["AGE AS OF 12/31/12 (EDITED/IMPUTED)"]
# Wed, 16 Sep 2015 19:23:55
sex=cons["SEX"]
# Wed, 16 Sep 2015 19:24:07
blood_pressure=cons["HIGH BLOOD PRESSURE DIAG (>17)"]
# Wed, 16 Sep 2015 19:24:18
income=cons["FAMILY'S TOTAL INCOME"]
# Wed, 16 Sep 2015 19:24:37
married=cons["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"]
# Wed, 16 Sep 2015 19:30:21
married
# Wed, 16 Sep 2015 19:30:27
married.name
# Wed, 16 Sep 2015 19:31:59
design_variables=[x.name for x in [married, income, blood_pressure, sex, age, er, inpatient, outpatient, office]]
# Wed, 16 Sep 2015 19:32:02
design_variables
# Wed, 16 Sep 2015 19:32:14
design_matrix=cons[design_variables]
# Wed, 16 Sep 2015 19:32:18
design_matrix
# Wed, 16 Sep 2015 19:32:21
design_matrix.columns
# Wed, 16 Sep 2015 19:32:36
design_matrix.to_csv?
# Wed, 16 Sep 2015 19:32:58
design_matrix.to_csv("./design1.csv",header=False)
# Wed, 16 Sep 2015 19:33:14
design_matrix.to_csv("./design_w_header.csv",header=True)
# Wed, 16 Sep 2015 19:33:21
design_matrix.to_csv("./design_wout_header.csv",header=False)
# Wed, 16 Sep 2015 19:38:11
design_matrix.to_csv("./design_matrix.csv",index=False)
# Wed, 16 Sep 2015 20:06:50
design_matrix.columns
# Wed, 16 Sep 2015 20:08:41
design_variables=[x.name for x in [married, income, blood_pressure, sex, age, er, inpatient, outpatient, office]]
# Wed, 16 Sep 2015 20:08:58
region=cons["CENSUS REGION AS OF 12/31/12"]
# Wed, 16 Sep 2015 20:09:02
design_variables=[x.name for x in [married, income, blood_pressure, sex, age, er, inpatient, outpatient, office, region]]
# Wed, 16 Sep 2015 20:09:05
design_matrix.columns
# Wed, 16 Sep 2015 20:09:19
design_matrix=cons[design_variables]
# Wed, 16 Sep 2015 20:09:23
design_variables=[x.name for x in [married, income, blood_pressure, sex, age, er, inpatient, outpatient, office, region]]
# Wed, 16 Sep 2015 20:09:27
design_matrix.columns
# Wed, 16 Sep 2015 20:09:54
design_matrix.to_csv("./design_matrix.csv",index=False)
# Wed, 16 Sep 2015 20:21:25
drug=cons["TOTAL RX-EXP 12"]
# Wed, 16 Sep 2015 20:21:31
design_variables=[x.name for x in [married, income, blood_pressure, sex, age, er, inpatient, outpatient, office, region, drug]]
# Wed, 16 Sep 2015 20:21:35
design_matrix=cons[design_variables]
# Wed, 16 Sep 2015 20:21:40
design_matrix.to_csv("./design_matrix.csv",index=False)
# Wed, 16 Sep 2015 20:24:09
design_matrix.head()
# Wed, 16 Sep 2015 21:33:16
design_matrix.columns
# Wed, 16 Sep 2015 21:58:33
X=design_matrix[design_matrix.columns[0:4],design_matrix.columns[9]]
# Wed, 16 Sep 2015 21:58:49
X=design_matrix[design_matrix.columns[0:4]+design_matrix.columns[9]]
# Wed, 16 Sep 2015 21:59:05
design_matrix.columns[0:4]
# Wed, 16 Sep 2015 21:59:11
list(design_matrix.columns[0:4])
# Wed, 16 Sep 2015 21:59:25
X=design_matrix[list(design_matrix.columns[0:4])+list(design_matrix.columns[9])]
# Wed, 16 Sep 2015 21:59:34
X=design_matrix[list(design_matrix.columns[0:4])+design_matrix.columns[9]]
# Wed, 16 Sep 2015 21:59:43
X=design_matrix[list(design_matrix.columns[0:4])]
# Wed, 16 Sep 2015 21:59:45
X
# Wed, 16 Sep 2015 22:03:52
design_matrix
# Wed, 16 Sep 2015 22:03:55
design_matrix.columns
# Wed, 16 Sep 2015 22:04:33
X=design_matrix[design_matrix["FAMILY\'S TOTAL INCOME"]>0]
# Wed, 16 Sep 2015 22:05:02
X=X[X["AGE AS OF 12/31/12 (EDITED/IMPUTED)"]>=0]
# Wed, 16 Sep 2015 22:05:37
X=X[X["HIGH BLOOD PRESSURE DIAG (>17)"]>=-1]
# Wed, 16 Sep 2015 22:06:08
X=X[X["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"]<6]
# Wed, 16 Sep 2015 22:06:33
X=X[X["CENSUS REGION AS OF 12/31/12"]>0]
# Wed, 16 Sep 2015 22:06:47
X=X[X["CENSUS REGION AS OF 12/31/12"]>1]
# Wed, 16 Sep 2015 22:06:49
X
# Wed, 16 Sep 2015 22:07:36
X=design_matrix[design_matrix["FAMILY\'S TOTAL INCOME"]>0]
# Wed, 16 Sep 2015 22:07:44
X=X[X["AGE AS OF 12/31/12 (EDITED/IMPUTED)"]>=0]
# Wed, 16 Sep 2015 22:07:53
X=X[X["HIGH BLOOD PRESSURE DIAG (>17)"]>=-1]
# Wed, 16 Sep 2015 22:08:07
X=X[X["CENSUS REGION AS OF 12/31/12"]>0]
# Wed, 16 Sep 2015 22:08:12
X
# Wed, 16 Sep 2015 22:08:30
X.to_csv("./cleaned_design_matrix.csv")
# Wed, 16 Sep 2015 22:08:38
X.to_csv("./cleaned_design_matrix_for_regression.csv")
# Wed, 16 Sep 2015 22:10:07
X.columns
# Wed, 16 Sep 2015 22:11:40
y=X[["TOTAL OFFICE-BASED EXP 12","TOTAL OUTPATIENT PROVIDER EXP 12","TOT HOSP IP FACILITY + DR EXP 12","TOTAL ER FACILITY + DR EXP 12"]]
# Wed, 16 Sep 2015 22:12:22
x=X[X.columns[0,1,2,3,4,9]]
# Wed, 16 Sep 2015 22:12:35
X.columns[0]
# Wed, 16 Sep 2015 22:12:38
X.columns[0,1]
# Wed, 16 Sep 2015 22:12:42
X.columns[[0,1]]
# Wed, 16 Sep 2015 22:12:55
x=X[X.list(columns[[0,1,2,3,4,9]])]
# Wed, 16 Sep 2015 22:13:02
x=X[X.(list(columns[[0,1,2,3,4,9]]))]
# Wed, 16 Sep 2015 22:13:22
vars=list(columns[[0,1,2,3,4,9]]
)
# Wed, 16 Sep 2015 22:13:26
vars=list(X.columns[[0,1,2,3,4,9]]
)
# Wed, 16 Sep 2015 22:13:27
vars
# Wed, 16 Sep 2015 22:13:33
x=X[vars]
# Wed, 16 Sep 2015 22:13:34
x
# Wed, 16 Sep 2015 22:13:36
x.columns
# Wed, 16 Sep 2015 22:13:39
y
# Wed, 16 Sep 2015 22:13:49
x.to_csv("./regressors.csv")
# Wed, 16 Sep 2015 22:14:05
y.to_csv("./endogs.csv")
# Wed, 16 Sep 2015 22:14:10
x.to_csv("./exogs.csv")
# Thu, 17 Sep 2015 11:10:32
import statsmodels.api as sm
# Thu, 17 Sep 2015 11:10:58
x
# Thu, 17 Sep 2015 11:11:00
x.columns
# Thu, 17 Sep 2015 11:11:19
exog=sm.add_constant(x)
# Thu, 17 Sep 2015 11:11:28
endog=y
# Thu, 17 Sep 2015 11:12:10
wls_model=sm.WLS(endog,exog,weights=list([1]*len(exog)))
# Thu, 17 Sep 2015 11:12:21
results=wls_model.fit()
# Thu, 17 Sep 2015 11:12:25
results.params
# Thu, 17 Sep 2015 11:17:57
y
# Thu, 17 Sep 2015 11:18:05
endog=y[0]
# Thu, 17 Sep 2015 11:18:09
y[0]
# Thu, 17 Sep 2015 11:18:14
y
# Thu, 17 Sep 2015 11:18:19
y.columns
# Thu, 17 Sep 2015 11:19:13
endog={}
# Thu, 17 Sep 2015 11:19:32
for place in range(len(y.columns)):
    endog[place]=y[y.columns[place]]
    
# Thu, 17 Sep 2015 11:19:33
endog
# Thu, 17 Sep 2015 11:19:38
endog.col
# Thu, 17 Sep 2015 11:19:41
endog[0]
# Thu, 17 Sep 2015 11:19:44
endog[1]
# Thu, 17 Sep 2015 11:19:46
endog[2]
# Thu, 17 Sep 2015 11:19:48
endog[3]
# Thu, 17 Sep 2015 11:19:59
del wls_model
# Thu, 17 Sep 2015 11:20:04
wls_model
# Thu, 17 Sep 2015 11:20:13
wls_model=sm.WLS(endog,exog[0],weights=list([1]*len(exog[0])))
# Thu, 17 Sep 2015 11:20:22
wls_model=sm.WLS(endog[0],exog,weights=list([1]*len(exog)))
# Thu, 17 Sep 2015 11:20:28
del result
# Thu, 17 Sep 2015 11:20:32
del results
# Thu, 17 Sep 2015 11:20:39
results=wls_model.fit()
# Thu, 17 Sep 2015 11:20:47
results.params
# Thu, 17 Sep 2015 11:22:17
del wls_model
# Thu, 17 Sep 2015 11:22:19
del results
# Thu, 17 Sep 2015 11:22:33
wls_model[0]=sm.WLS(endog[0],exog,weights=list([1]*len(exog)))
# Thu, 17 Sep 2015 11:22:39
wls_model={}
# Thu, 17 Sep 2015 11:22:40
wls_model[0]=sm.WLS(endog[0],exog,weights=list([1]*len(exog)))
# Thu, 17 Sep 2015 11:22:47
wls_model[1]=sm.WLS(endog[1],exog,weights=list([1]*len(exog)))
# Thu, 17 Sep 2015 11:22:53
del wls_model
# Thu, 17 Sep 2015 11:23:24
wls_model={sm.WLS(endog[place],exog,weights=list([place]*len(exog))) for place in range(4)}
# Thu, 17 Sep 2015 11:23:36
del wls_model
# Thu, 17 Sep 2015 11:23:43
wls_model={place: sm.WLS(endog[place],exog,weights=list([place]*len(exog))) for place in range(4)}
# Thu, 17 Sep 2015 11:24:02
results={place: wls_model[place].fit() for place in range(4)}
# Thu, 17 Sep 2015 11:27:40
results[0]
# Thu, 17 Sep 2015 11:27:46
results[0].params
# Thu, 17 Sep 2015 11:27:51
results[1].params
# Thu, 17 Sep 2015 11:27:54
results[2].params
# Thu, 17 Sep 2015 11:27:57
results[3].params
# Thu, 17 Sep 2015 11:28:00
engo[0]
# Thu, 17 Sep 2015 11:28:03
endog[0]
# Thu, 17 Sep 2015 11:28:09
results[0].params
# Thu, 17 Sep 2015 11:28:37
range(4)
# Thu, 17 Sep 2015 11:28:41
range(4)
# Thu, 17 Sep 2015 11:28:45
del wls_model
# Thu, 17 Sep 2015 11:28:49
del results
# Thu, 17 Sep 2015 11:28:58
wls_model={place: sm.WLS(endog[place],exog,weights=list([place]*len(exog))) for place in list(range(4))}
# Thu, 17 Sep 2015 11:29:09
results={place: wls_model[place].fit() for place in list(range(4))}
# Thu, 17 Sep 2015 11:29:23
results[0]
# Thu, 17 Sep 2015 11:29:26
results[0].params
# Thu, 17 Sep 2015 11:30:24
wls_model={place: sm.WLS(endog[place],exog,weights=list([1]*len(exog))) for place in list(range(4))}
# Thu, 17 Sep 2015 11:30:27
del wls_model
# Thu, 17 Sep 2015 11:30:29
del results
# Thu, 17 Sep 2015 11:31:14
wls_model={place: sm.WLS(endog[place],exog,weights=list([1]*len(exog))) for place in list(range(4))}
# Thu, 17 Sep 2015 11:31:19
results={place: wls_model[place].fit() for place in list(range(4))}
# Thu, 17 Sep 2015 11:31:26
results[0].params
# Thu, 17 Sep 2015 11:31:40
exog["CENSUS REGION AS OF 12/31/12"]
# Thu, 17 Sep 2015 11:31:51
endog
# Thu, 17 Sep 2015 11:31:53
endog.col
# Thu, 17 Sep 2015 11:31:57
endog.columns
# Thu, 17 Sep 2015 11:31:59
endog
# Thu, 17 Sep 2015 11:32:07
endog[0].col
# Thu, 17 Sep 2015 11:32:10
endog[0].columns
# Thu, 17 Sep 2015 11:32:21
endog[0]
# Thu, 17 Sep 2015 11:32:23
endog[1]
# Thu, 17 Sep 2015 11:32:26
endog[2]
# Thu, 17 Sep 2015 11:32:35
exog["CENSUS REGION AS OF 12/31/12"]
# Thu, 17 Sep 2015 11:32:43
exog["CENSUS REGION AS OF 12/31/12"].unique()
# Thu, 17 Sep 2015 11:34:57
exog["Northeast"]=int(exog["CENSUS REGION AS OF 12/31/12"])==1
# Thu, 17 Sep 2015 11:35:16
exog["Northeast"]=int(exog["CENSUS REGION AS OF 12/31/12"]==1)
# Thu, 17 Sep 2015 11:35:21
exog["Northeast"]=(exog["CENSUS REGION AS OF 12/31/12"]==1)
# Thu, 17 Sep 2015 11:35:28
exog["Northeast"]
# Thu, 17 Sep 2015 11:35:36
exog["Northeast"]=(exog["CENSUS REGION AS OF 12/31/12"]==1).astype(int64)
# Thu, 17 Sep 2015 11:35:37
exog["Northeast"]
# Thu, 17 Sep 2015 11:35:53
exog["midwest"]=(exog["CENSUS REGION AS OF 12/31/12"]==2).astype(int64)
# Thu, 17 Sep 2015 11:35:56
exog["Northeast"]
# Thu, 17 Sep 2015 11:36:00
exog["northeast"]=(exog["CENSUS REGION AS OF 12/31/12"]==1).astype(int64)
# Thu, 17 Sep 2015 11:36:11
exog["south"]=(exog["CENSUS REGION AS OF 12/31/12"]==3).astype(int64)
# Thu, 17 Sep 2015 11:36:31
exog.columns
# Thu, 17 Sep 2015 11:37:20
exog["hypertension"]=(exog["HIGH BLOOD PRESSURE DIAG (>17)"]==1).astype(int64)
# Thu, 17 Sep 2015 11:38:56
exog["male"]=(exog["SEX"]==1).astype(int64)
# Thu, 17 Sep 2015 11:39:00
exog.columns
# Thu, 17 Sep 2015 11:40:09
exog["no longer married"]=(exog["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"] in [2,3,4]).astype(int64)
# Thu, 17 Sep 2015 11:40:37
exog["no longer married"]=(exog["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"].map(lambda x: x in [2,3,4])).astype(int64)
# Thu, 17 Sep 2015 11:41:13
exog["married"]=(exog["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"].map(lambda x: x==1)).astype(int64)
# Thu, 17 Sep 2015 11:41:37
exog.columns
# Thu, 17 Sep 2015 11:42:09
exog.drop("Northeast",axis=1)
# Thu, 17 Sep 2015 11:42:11
exog.columns
# Thu, 17 Sep 2015 11:42:16
exog=exog.drop("Northeast",axis=1)
# Thu, 17 Sep 2015 11:42:19
exog.columns
# Thu, 17 Sep 2015 11:43:08
exog.to_csv("./exog_with_dummies.csv")
# Thu, 17 Sep 2015 11:44:29
variables_with_dummies=["const","married","no longer married","
# Thu, 17 Sep 2015 11:44:32
exog.columns
# Thu, 17 Sep 2015 11:45:15
variables_with_dummies=["const","married","no longer married","FAMILY'S TOTAL INCOME","hypertension","male","AGE AS OF 12/31
# Thu, 17 Sep 2015 11:45:16
exog.columns
# Thu, 17 Sep 2015 11:45:45
variables_with_dummies=["const","married","no longer married","FAMILY'S TOTAL INCOME","hypertension","male","AGE AS OF 12/31/12 (EDITED/IMPUTED)","northeast","midwest","south"]
# Thu, 17 Sep 2015 11:45:58
regressors=exog[variables_with_dummies]
# Thu, 17 Sep 2015 11:46:10
del wls_model
# Thu, 17 Sep 2015 11:46:13
del results
# Thu, 17 Sep 2015 11:46:38
wls_model={place: sm.WLS(endog[place],regressors,weights=list([1]*len(regressors))) for place in list(range(4))}
# Thu, 17 Sep 2015 11:46:45
results={place: wls_model[place].fit() for place in list(range(4))}
# Thu, 17 Sep 2015 11:46:51
results[0].params
# Thu, 17 Sep 2015 11:47:30
results[1].params
# Thu, 17 Sep 2015 11:52:11
results[2].params
# Thu, 17 Sep 2015 11:52:19
results[3].params
# Thu, 17 Sep 2015 11:59:13
print(results[0])
# Thu, 17 Sep 2015 11:59:52
print(results[0].params)
# Thu, 17 Sep 2015 11:59:58
results[3].params[0]
# Thu, 17 Sep 2015 12:00:16
a=results[3].params.copy()
# Thu, 17 Sep 2015 12:00:16
a
# Thu, 17 Sep 2015 12:00:24
a[1]
# Thu, 17 Sep 2015 12:00:33
results[0].params[0]
# Thu, 17 Sep 2015 12:00:35
results[0].params
# Thu, 17 Sep 2015 12:01:21
person=[1,1,0,100000,1,1,30,0,1,0]
# Thu, 17 Sep 2015 12:01:34
np.dot(results[0].params,person)
# Thu, 17 Sep 2015 12:01:39
np.dot(results[1].params,person)
# Thu, 17 Sep 2015 12:01:43
np.dot(results[2].params,person)
# Thu, 17 Sep 2015 12:01:47
np.dot(results[3].params,person)
# Thu, 17 Sep 2015 12:02:23
wls_model[0]
# Thu, 17 Sep 2015 12:04:03
wls_model[0].predict
# Thu, 17 Sep 2015 12:04:09
result[0].summary()
# Thu, 17 Sep 2015 12:04:13
results[0].summary()
# Thu, 17 Sep 2015 12:04:46
results[1].summary()
# Thu, 17 Sep 2015 12:05:26
results[2].summary()
# Thu, 17 Sep 2015 12:05:40
results[3].summary()
# Thu, 17 Sep 2015 12:08:42
endog.col
# Thu, 17 Sep 2015 12:08:47
endog[0].columns
# Thu, 17 Sep 2015 12:08:51
endog[0]
# Thu, 17 Sep 2015 12:08:54
endog[1]
# Thu, 17 Sep 2015 12:08:56
endog[2]
# Thu, 17 Sep 2015 12:08:58
endog[3]
# Thu, 17 Sep 2015 12:08:59
endog[4]
# Thu, 17 Sep 2015 12:09:05
y.columns
# Thu, 17 Sep 2015 12:09:57
x.columns
# Thu, 17 Sep 2015 12:10:03
regressors.columns
# Thu, 17 Sep 2015 12:10:40
regressors.to_csv("./design_w_dummies_wout_extra.csv")
# Thu, 17 Sep 2015 12:10:56
endog[0]
# Thu, 17 Sep 2015 12:11:06
temp=endog[0]
# Thu, 17 Sep 2015 12:11:24
temp[0::5]
# Thu, 17 Sep 2015 12:11:31
temp[1::5]
# Thu, 17 Sep 2015 12:11:33
temp[0::5]
# Thu, 17 Sep 2015 12:11:57
temp[0::5]+temp[1::5]
# Thu, 17 Sep 2015 12:12:13
pd.merge(temp[0::5],temp[1::5],axis=0)
# Thu, 17 Sep 2015 12:12:19
a=pd.merge(temp[0::5],temp[1::5])
# Thu, 17 Sep 2015 12:13:40
total_matrix=[endog[0],endog[1],endog[2],endog[3],regressors]
# Thu, 17 Sep 2015 12:13:43
total_matrix
# Thu, 17 Sep 2015 12:13:45
total_matrix.col
# Thu, 17 Sep 2015 12:13:48
total_matrix.columns
# Thu, 17 Sep 2015 12:14:03
total_matrix=pd.DataFrame(endog[0],endog[1],endog[2],endog[3],regressors)
# Thu, 17 Sep 2015 12:15:43
total_matrix=endog[0]
# Thu, 17 Sep 2015 12:15:51
total_matrix.append(endog[1])
# Thu, 17 Sep 2015 12:16:05
total_matrix.columns
# Thu, 17 Sep 2015 12:16:11
del total_matrix
# Thu, 17 Sep 2015 12:17:17
frames=[endog[0],endog[1],endog[2],endog[3],regressors]
# Thu, 17 Sep 2015 12:17:31
total_matrix=pd.concat(frames,axis=0)
# Thu, 17 Sep 2015 12:17:36
total_matrix.columns
# Thu, 17 Sep 2015 12:17:45
total_matrix[0]
# Thu, 17 Sep 2015 12:17:50
total_matrix.columns
# Thu, 17 Sep 2015 12:18:10
frames[0]
# Thu, 17 Sep 2015 12:18:32
total_matrix=pd.concat(frames)
# Thu, 17 Sep 2015 12:18:41
X.columns
# Thu, 17 Sep 2015 12:19:21
X["married"]=(X["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"]==1).astype(int)
# Thu, 17 Sep 2015 12:19:47
X["no longer married"]=(X["MARITAL STATUS-12/31/12 (EDITED/IMPUTED)"].map(lambda x: x in [2,3,4])).astype(int)
# Thu, 17 Sep 2015 12:19:56
regressors.columns
# Thu, 17 Sep 2015 12:20:34
X["hypertension"]=(X["HIGH BLOOD PRESSURE DIAG (>17)"].map(lambda x: x==1)).astype(int)
# Thu, 17 Sep 2015 12:21:05
X["male"]=(X["SEX"].map(lambda x: x==1)).astype(int)
# Thu, 17 Sep 2015 12:21:31
X["northeast"]=(X["CENSUS REGION AS OF 12/31/12"].map(lambda x: x==1)).astype(int)
# Thu, 17 Sep 2015 12:21:42
X["midwest"]=(X["CENSUS REGION AS OF 12/31/12"].map(lambda x: x==2)).astype(int)
# Thu, 17 Sep 2015 12:21:51
X["south"]=(X["CENSUS REGION AS OF 12/31/12"].map(lambda x: x==3)).astype(int)
# Thu, 17 Sep 2015 12:21:53
X.columns
# Thu, 17 Sep 2015 12:21:57
regressors.columns
# Thu, 17 Sep 2015 12:22:21
X.to_csv("./design_matrix_with_dummies.csv")
# Thu, 17 Sep 2015 12:22:30
X.to_csv("./all_data_with_dummies.csv")
# Thu, 17 Sep 2015 12:23:49
select=pd.DataFrame(np.random.randn(100,2))
# Thu, 17 Sep 2015 12:23:51
select
# Thu, 17 Sep 2015 12:24:59
select=pd.DataFrame(np.random.randn(len(X),1))
# Thu, 17 Sep 2015 12:25:00
select
# Thu, 17 Sep 2015 12:25:23
select=pd.DataFrame(np.random.randu(len(X),1))
# Thu, 17 Sep 2015 12:25:27
select=pd.DataFrame(np.random.runif(len(X),1))
# Thu, 17 Sep 2015 12:25:43
select=pd.DataFrame(np.random.uniform(len(X),1))
# Thu, 17 Sep 2015 12:25:58
select=pd.DataFrame(np.random.uniform(len(X),1))
# Thu, 17 Sep 2015 12:26:08
np.random.uniform(len(X),1)
# Thu, 17 Sep 2015 12:26:48
select=pd.DataFrame(np.random.uniform(0,1,[len(X),1]))
# Thu, 17 Sep 2015 12:26:50
select
# Thu, 17 Sep 2015 12:27:08
mask=select[select<=0.8]
# Thu, 17 Sep 2015 12:27:16
train=X[mask]
# Thu, 17 Sep 2015 12:27:21
mask
# Thu, 17 Sep 2015 12:27:28
train=X[X[mask]]
# Thu, 17 Sep 2015 12:27:45
mask
# Thu, 17 Sep 2015 12:28:17
mask=select[select<0.8]
# Thu, 17 Sep 2015 12:28:19
mask
# Thu, 17 Sep 2015 12:28:31
mask=select<0.8]
# Thu, 17 Sep 2015 12:28:32
mask=select<0.8
# Thu, 17 Sep 2015 12:28:33
mask
# Thu, 17 Sep 2015 12:28:38
train=X[mask]
# Thu, 17 Sep 2015 12:28:45
test=X[~mask]
# Thu, 17 Sep 2015 12:28:47
train
# Thu, 17 Sep 2015 12:28:54
test=X[~mask]
# Thu, 17 Sep 2015 12:30:50
df=pd.DataFrame(np.random.uniform(0,1,[len(X),1]))
# Thu, 17 Sep 2015 12:31:13
msk=df<.8
# Thu, 17 Sep 2015 12:31:14
msk
# Thu, 17 Sep 2015 12:31:20
t=df[msk]
# Thu, 17 Sep 2015 12:31:21
t
# Thu, 17 Sep 2015 12:31:30
t=df[msk].dropna()
# Thu, 17 Sep 2015 12:31:31
t
# Thu, 17 Sep 2015 12:31:36
t=df[msk].dropna().copy()
# Thu, 17 Sep 2015 12:31:37
t
# Thu, 17 Sep 2015 12:31:55
s=df[~msk]
# Thu, 17 Sep 2015 12:32:00
len(s)
# Thu, 17 Sep 2015 12:32:07
s=s.dropna()
# Thu, 17 Sep 2015 12:32:09
len(s)
# Thu, 17 Sep 2015 12:32:16
len(s)+len(t)==len(X)
# Thu, 17 Sep 2015 12:32:26
select=pd.DataFrame(np.random.uniform(0,1,[len(X),1]))
# Thu, 17 Sep 2015 12:32:41
train=X[select].dropna().copy()
# Thu, 17 Sep 2015 12:32:51
select=pd.DataFrame(np.random.uniform(0,1,[len(X),1]))
# Thu, 17 Sep 2015 12:32:53
select
# Thu, 17 Sep 2015 12:33:02
mask=select<.8
# Thu, 17 Sep 2015 12:33:03
mask
# Thu, 17 Sep 2015 12:33:10
train=X[mask]
# Thu, 17 Sep 2015 12:33:11
train
# Thu, 17 Sep 2015 12:33:21
train=X[mask].dropna().copy()
# Thu, 17 Sep 2015 12:33:23
train
# Thu, 17 Sep 2015 12:33:46
X["train"]=mask
# Thu, 17 Sep 2015 12:33:50
X["test"]=~mask
# Thu, 17 Sep 2015 12:34:12
X["train"]+X["test"]==[1]*len(X)
# Thu, 17 Sep 2015 12:34:19
X["train"]
# Thu, 17 Sep 2015 12:34:27
mask
# Thu, 17 Sep 2015 12:34:34
X.columns
# Thu, 17 Sep 2015 12:34:50
mask
# Thu, 17 Sep 2015 12:34:54
sum(_)
# Thu, 17 Sep 2015 12:35:02
sum(mask)
# Thu, 17 Sep 2015 12:35:05
sum(~mask)
# Thu, 17 Sep 2015 12:35:16
sum(mask)+sum(~mask)==len(X)
# Thu, 17 Sep 2015 12:35:40
X["train"]=mask
# Thu, 17 Sep 2015 12:35:43
X["train"]
# Thu, 17 Sep 2015 12:36:03
X["train"][-100:]
# Thu, 17 Sep 2015 12:36:10
X["train"].isnull()
# Thu, 17 Sep 2015 12:36:11
sum(_)
# Thu, 17 Sep 2015 12:36:19
len(X)
# Thu, 17 Sep 2015 12:36:21
X
# Thu, 17 Sep 2015 12:36:42
random=np.random.unif(0,1,len(X))
# Thu, 17 Sep 2015 12:36:45
random=np.random.uniform(0,1,len(X))
# Thu, 17 Sep 2015 12:36:47
random
# Thu, 17 Sep 2015 12:36:51
len(random)
# Thu, 17 Sep 2015 12:37:04
random=pd.DataFrame(np.random.uniform(0,1,len(X)))
# Thu, 17 Sep 2015 12:37:05
random
# Thu, 17 Sep 2015 12:37:14
mask=random<.8
# Thu, 17 Sep 2015 12:37:15
mask
# Thu, 17 Sep 2015 12:37:36
X["train"]=int(mask)
# Thu, 17 Sep 2015 12:37:49
X["train"]=mask
# Thu, 17 Sep 2015 12:37:56
X["train"]
# Thu, 17 Sep 2015 12:46:15
len(X)==len(mask)
# Thu, 17 Sep 2015 12:46:23
mask
# Thu, 17 Sep 2015 12:46:29
X["train"]
# Thu, 17 Sep 2015 12:46:32
mask
# Thu, 17 Sep 2015 12:46:41
len(X.index)
# Thu, 17 Sep 2015 12:46:59
len(mask.index)
# Thu, 17 Sep 2015 12:47:43
X.drop("train",axis=1)
# Thu, 17 Sep 2015 12:47:46
X.columns
# Thu, 17 Sep 2015 12:47:52
X=X.drop("train",axis=1)
# Thu, 17 Sep 2015 12:47:56
X=X.drop("test",axis=1)
# Thu, 17 Sep 2015 12:47:58
X.columns
# Thu, 17 Sep 2015 12:48:13
%logstart -rt
# Thu, 17 Sep 2015 12:48:34
X["train"]=mask.copy()
# Thu, 17 Sep 2015 12:48:37
X["train"]
# Thu, 17 Sep 2015 12:48:56
mask
# Thu, 17 Sep 2015 12:49:02
mask.isnull()
# Thu, 17 Sep 2015 12:49:04
sum(_)
# Thu, 17 Sep 2015 12:49:21
X=X.drop("train",axis=1)
# Thu, 17 Sep 2015 12:49:39
temp=pd.concat([X,mask,~mask],axis=1)
# Thu, 17 Sep 2015 12:49:41
temp.columns
# Thu, 17 Sep 2015 12:50:11
temp=pd.concat([X,mask,~mask],axis=1)
# Thu, 17 Sep 2015 12:50:20
mask.index
# Thu, 17 Sep 2015 12:50:22
mask.columns
# Thu, 17 Sep 2015 12:50:30
mask.columns=["train"]
# Thu, 17 Sep 2015 12:50:36
mask.columns=["mask"]
# Thu, 17 Sep 2015 12:50:38
mask.columns
# Thu, 17 Sep 2015 12:50:46
temp=pd.concat([X,mask],axis=1)
# Thu, 17 Sep 2015 12:50:47
temp.columns
# Thu, 17 Sep 2015 12:50:52
temp["mask"]
# Thu, 17 Sep 2015 12:51:18
X.index==mask.index
# Thu, 17 Sep 2015 12:51:58
X.index=list(range(len(X)))
# Thu, 17 Sep 2015 12:52:04
X.index==mask.index
# Thu, 17 Sep 2015 12:52:17
X["train"]=mask.copy()
# Thu, 17 Sep 2015 12:52:23
X["test"]=~mask.copy()
# Thu, 17 Sep 2015 12:52:27
X["train"]
# Thu, 17 Sep 2015 12:52:39
X["test"]=~mask.astype(int).copy()
# Thu, 17 Sep 2015 12:52:46
X["train"]=mask.astype(int).copy()
# Thu, 17 Sep 2015 12:52:51
X["train"]
# Thu, 17 Sep 2015 12:53:03
X["train"]+X["test"]==1
# Thu, 17 Sep 2015 12:53:06
X["train"]+X["test"]
# Thu, 17 Sep 2015 12:53:12
X["train"]
# Thu, 17 Sep 2015 12:53:16
X["test"]
# Thu, 17 Sep 2015 12:53:24
mask
# Thu, 17 Sep 2015 12:53:28
~mask
# Thu, 17 Sep 2015 12:53:34
X.columns
# Thu, 17 Sep 2015 12:53:43
X.drop("train",axis=1)
# Thu, 17 Sep 2015 12:53:46
X.drop("test",axis=1)
# Thu, 17 Sep 2015 12:54:07
mask.astype(int)
# Thu, 17 Sep 2015 12:54:10
~mask.astype(int)
# Thu, 17 Sep 2015 12:54:26
X["train"]=mask.astype(int).copy()
# Thu, 17 Sep 2015 12:54:37
X["test"]=(1-mask).astype(int).copy()
# Thu, 17 Sep 2015 12:54:40
X["test"]
# Thu, 17 Sep 2015 12:54:48
X["train"]+X["test"]
# Thu, 17 Sep 2015 12:54:50
X["train"]+X["test"]==1
# Thu, 17 Sep 2015 12:54:58
X["train"]+X["test"]!=1
# Thu, 17 Sep 2015 12:55:01
sum(_)
# Thu, 17 Sep 2015 12:55:06
X.columns
# Thu, 17 Sep 2015 12:55:43
X.to_csv("./all_data_with_dummies_with_train_test_dummies_201509171255.csv",index=False)
# Thu, 17 Sep 2015 12:55:46
X.columns
# Thu, 17 Sep 2015 12:55:51
X["train"]
# Thu, 17 Sep 2015 12:55:54
X["test"]
# Thu, 17 Sep 2015 12:56:08
train=X[X["train"]==1]
# Thu, 17 Sep 2015 12:56:24
test=X[X["test"]==1]
# Thu, 17 Sep 2015 12:56:39
random
# Thu, 17 Sep 2015 12:56:56
trainmask=random<.6
# Thu, 17 Sep 2015 12:57:17
testmask=random<.8 && random>=.6
# Thu, 17 Sep 2015 12:57:19
testmask=random<.8 & random>=.6
# Thu, 17 Sep 2015 12:57:26
testmask=(random<.8) & (random>=.6)
# Thu, 17 Sep 2015 12:57:29
testmask
# Thu, 17 Sep 2015 12:57:32
trainmask
# Thu, 17 Sep 2015 12:57:33
testmask
# Thu, 17 Sep 2015 12:57:35
trainmask
# Thu, 17 Sep 2015 12:58:01
validatemask=random>=.8
# Thu, 17 Sep 2015 12:58:15
trainmask+testmask+validatemask==1
# Thu, 17 Sep 2015 12:58:19
trainmask+testmask+validatemask!=1
# Thu, 17 Sep 2015 12:58:21
sum(_)
# Thu, 17 Sep 2015 12:58:45
X["training"]=trainmask.astype(int).copy()
# Thu, 17 Sep 2015 12:58:59
X["test"]=testmask.astype(int).copy()
# Thu, 17 Sep 2015 12:59:11
X["validate"]=validatemask.astype(int).copy()
# Thu, 17 Sep 2015 12:59:13
X.columns
# Thu, 17 Sep 2015 12:59:28
X.to_csv("./all_data_with_dummies_with_train_test_validate_dummies_201509171259.csv",index=False)
# Thu, 17 Sep 2015 16:43:18
variabels
# Thu, 17 Sep 2015 16:43:21
variables
# Thu, 17 Sep 2015 16:58:31
exit
