# IPython log file

import health
this=health.prep_for_modeling()
(all, train, test, validate, endog, office, outpatient, inpatient, er, w)=this
train
office
X=train["TOTAL OFFICE-BASED EXP 13"][(train["TOTAL OFFICE-BASED EXP 13"]>0) & (train["HIGH BLOOD PRESSURE DIAG (>17)"]==1) & (train["HIGH CHOLESTEROL DIAGNOSIS (>17)"]) & (train["MULT DIAG HIGH BLOOD PRESS (>17)"]==1)]
X.shape
X.head()
f=plt.hist(X,bins=len(X),normed=True)
f=plt.hist(np.asarray(X),bins=len(X),normed=True)
f[0]
health.find_kernel(X)
health.reload(health)
health.find_kernel(X)
health.reload(health)
health.find_kernel(X,bw=.1)
f=plt.hist(np.asarray(X),bins=len(X),normed=True)
f=plt.hist(np.asarray(X),bins=len(X),normed=True, log=True)
f=plt.hist(np.asarray(X),bins=1000,normed=True, log=True)
f=plt.hist(np.asarray(np.log(X)),bins=1000,normed=True, log=True)
f=plt.hist(np.asarray(X),bins=1000,normed=True,log=True)
ax.set_yscale("log")
%logstart -rt modeling_201509241700.py
# Thu, 24 Sep 2015 18:38:58
f[0]
# Thu, 24 Sep 2015 18:40:48
t=[int(term>0) for term in f[0]]
# Thu, 24 Sep 2015 18:40:49
t
# Thu, 24 Sep 2015 18:40:56
len(t)
# Thu, 24 Sep 2015 18:42:09
s=t[::-1]
# Thu, 24 Sep 2015 18:42:10
s
# Thu, 24 Sep 2015 18:42:19
s.index(1)
# Thu, 24 Sep 2015 18:47:41
health.reload(health)
# Thu, 24 Sep 2015 18:48:45
health.find_kernel(X,bw=.1)
# Thu, 24 Sep 2015 18:52:27
health.reload(health)
# Thu, 24 Sep 2015 18:52:28
health.find_kernel(X,bw=.1)
# Thu, 24 Sep 2015 18:52:56
health.find_kernel(X,bw=.1)
# Thu, 24 Sep 2015 18:56:20
health.reload(health)
# Thu, 24 Sep 2015 18:56:25
health.find_kernel(X,bw=.1)
# Thu, 24 Sep 2015 18:56:34
health.find_kernel(X)
# Thu, 24 Sep 2015 18:57:15
f[0]
# Thu, 24 Sep 2015 18:57:21
max(f[0])
# Thu, 24 Sep 2015 18:57:31
min(f[0])
# Thu, 24 Sep 2015 18:57:52
health.reload(health)
# Thu, 24 Sep 2015 18:58:03
health.find_kernel(X)
# Thu, 24 Sep 2015 19:53:21
health.reload(health)
# Thu, 24 Sep 2015 19:53:22
health.find_kernel(X)
# Thu, 24 Sep 2015 19:53:37
plt.show()
# Thu, 24 Sep 2015 19:57:52
health.reload(health)
# Thu, 24 Sep 2015 19:57:54
health.find_kernel(X)
# Thu, 24 Sep 2015 19:58:22
health.reload(health)
# Thu, 24 Sep 2015 19:58:25
health.find_kernel(X)
# Thu, 24 Sep 2015 19:58:44
health.reload(health)
# Thu, 24 Sep 2015 19:58:45
health.find_kernel(X)
# Thu, 24 Sep 2015 20:00:01
health.find_kernel(X)
# Thu, 24 Sep 2015 20:00:27
health.reload(health)
# Thu, 24 Sep 2015 20:00:28
health.find_kernel(X)
# Thu, 24 Sep 2015 20:00:43
health.find_kernel(X)
# Thu, 24 Sep 2015 20:01:03
health.find_kernel(X)
# Thu, 24 Sep 2015 20:02:54
health.find_kernel(X)
# Thu, 24 Sep 2015 20:06:04
insurance=health.pd.read_csv("../data/plan_information.csv")
# Thu, 24 Sep 2015 20:18:42
insurance.columns
# Thu, 24 Sep 2015 20:18:59
insurance=health.pd.read_csv("../data/plan_information.csv",index=False)
# Thu, 24 Sep 2015 20:19:03
insurance=health.pd.read_csv("../data/plan_information.csv")
# Thu, 24 Sep 2015 20:20:39
insurance=health.pd.read_csv("../data/plan_information.csv")
# Thu, 24 Sep 2015 20:20:45
insurance.columns
