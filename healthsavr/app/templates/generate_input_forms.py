
def generate_person_forms(thedictionary,target):
 with open(target,"w") as f:
  for key in thedictionary.keys():
   f.write('''        <div class = 'col-md-3'>
			<label for = "%s">"%s":</label>
			<select class = 'form-control input-md' name = "%s">\n'''%(clean(key),clean(key),clean(key)))
   templist=thedictionary[key]
   templist.sort()
   for level in templist:
    f.write('''				<option value = '%d'>%s</option>'''%(level,level))
   f.write('''
			</select>
		</div>\n\n''')
  
def generate_plan_forms(thelist,target):
 with open(target,"w") as f:
  for term in thelist:
   f.write('''	<option value = '%s'>%s</option>\n'''%(term,term))



def clean(thestring):
 return ((((((thestring.replace("\'","APOS")).replace("#","NUM")).replace("+","PLUS")).replace("/","FORSLASH")).replace(">","GTHAN")).replace("<","LTHAN"))


def dirty(thestring):
 return ((((((thestring.replace("APOS","\'")).replace("NUM","#")).replace("PLUS","+")).replace("FORSLASH","/")).replace("GTHAN",">")).replace("LTHAN","<"))



